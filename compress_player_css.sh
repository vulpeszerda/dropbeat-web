#!/bin/bash

BASEDIR=$(dirname $0)
CSSDIR=$BASEDIR/css
LESSDIR=$BASEDIR/less
LIBDIR=$BASEDIR/lib
DEST_GTE_IE9_PATH=$BASEDIR/css/dropbeat-v2.player.min*
DEST_LT_IE9_PATH=$BASEDIR/css/dropbeat-v2.player.ltie9.min.css
TEMP_LESSPATH=$BASEDIR/combinded.player.less
TEMP_SHARED_CSSPATH=$BASEDIR/shared.player.css
TEMP_CSSPATH=$BASEDIR/combinded.player.css
DATETIME=`date +%Y%m%d%H%M%S`

rm $TEMP_SHARED_CSSPATH
touch $TEMP_SHARED_CSSPATH

cat $LIBDIR/jquery_tooltipster/tooltipster.css >> $TEMP_SHARED_CSSPATH
cat $LIBDIR/mprogress/mprogress.min.css >> $TEMP_SHARED_CSSPATH
cat $CSSDIR/glyphicons.regular.css >> $TEMP_SHARED_CSSPATH
cat $CSSDIR/base.css >> $TEMP_SHARED_CSSPATH

rm $TEMP_CSSPATH
touch $TEMP_CSSPATH

cat $TEMP_SHARED_CSSPATH >> $TEMP_CSSPATH
cat $CSSDIR/dropbeat_ltie9.player.css >> $TEMP_CSSPATH

rm $DEST_LT_IE9_PATH
touch $DEST_LT_IE9_PATH
yui-compressor $TEMP_CSSPATH >> $DEST_LT_IE9_PATH


rm $TEMP_CSSPATH
touch $TEMP_CSSPATH

cat $TEMP_SHARED_CSSPATH >> $TEMP_CSSPATH

rm $TEMP_LESSPATH
touch $TEMP_LESSPATH
cat $LESSDIR/dropbeat.player.less >> $TEMP_LESSPATH
lessc -x $TEMP_LESSPATH >> $TEMP_CSSPATH 

rm $DEST_GTE_IE9_PATH
DEST_GTE_IE9_PATH=$BASEDIR/css/dropbeat-v2.player.min.$DATETIME.css

touch $DEST_GTE_IE9_PATH
yui-compressor $TEMP_CSSPATH >> $DEST_GTE_IE9_PATH


rm $TEMP_LESSPATH
rm $TEMP_CSSPATH
rm $TEMP_SHARED_CSSPATH

export datekey=$DATETIME
sed -i -- "s/dropbeat-v2\.player\.min\.[^\.]*\.css/dropbeat-v2\.player\.min\.${datekey}\.css/g" ./player.html

rm ./player.html--
