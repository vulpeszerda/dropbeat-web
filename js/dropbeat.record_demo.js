var $dbt = (function(module) {
    'use strict';

    var module = module || {},
        beforeunloadCallbacks = {};

    module.$ = $.noConflict();
    module._ = _.noConflict();

    module.shortcuts = function () {
        module.s = {
// This object provides alias for deep-depth module reference.
// All properties defined in this object should access that long references
// in one depth.
// XXX: NOTE that only `playerManager` and `playlistManager` can be accessed
// directly from `$dbt`.
            Music: module.music.Music,
            musicQ: module.music.musicQueue,
            Playlist: module.playlist.Playlist,
            viewControl: module.view,
            playerControl: module.player.control,
            playerButton: module.player.control.button,
            playerBase: module.player.control.base,
            repeatState: module.player.control.repeat.state,
            repeatSequence: module.player.control.repeat.sequence,
            repeatControl: module.player.control.repeat.control,
            shuffleState: module.player.control.shuffle.state,
            shuffleControl: module.player.control.shuffle.control,
            volumeControl: module.player.control.volume.control,
            progressController: module.progress.controller,
            notifyMessage: module.notification.message,
            notifyManager: module.notification.manager,
            notify: module.notification.notify
        };
    };

    module.initialize = function () {
        module.$('.hidden-before-init').removeClass('hidden-before-init');
        $dbt.s.viewControl.init();

// check browser support
        if (!module.context.browserSupport.support) {
            Backbone.history.start({pushState: true});
            module.router.navigate('/this-browser-is-not-supported',
                { trigger: true, replace: true});
            return;
        }

        if (module.context.isMobile) {
            module.playerManager.init();
            module.s.playerControl.init();
            module.s.progressController.init('base');
            Backbone.history.start({pushState: true});
            return;
        }

        module.playerManager.init();
        module.s.playerControl.init();
        module.s.progressController.init('base');

        if (module.utils.gup('dbt_grant_korean')) {
            module.s.notify('Korean granted :)', 'success');
            module.$.cookie('grant_korean', true, {expires: 9999});
        }

        Backbone.history.start({pushState: true});
    };

    module.mobileControl = (function() {
        var ctrl = {},
            wasPlaying = false;

        ctrl.init = function() {
            if (!module.context.isMobile) {
                return;
            }
            window.document.addEventListener("visibilitychange", function(e) {
                if (window.document.hidden) {
                    if (module.playerManager.playing) {
                        wasPlaying = true;
                        module.playerManager.pause();
                    }
                    module.$(module).trigger('mobileBackground', e);
                } else {
                    if (wasPlaying) {
                        wasPlaying = false;
                        if (module.playerManager.getCurrentMusic()) {
                            module.playerManager.play();
                        }
                    }
                    module.$(module).trigger('mobileForground', e);
                }
            });
        };

        return ctrl;
    }());

    module.state = {
        youtubeApiReady: false,
        soundManagerReady: false,
        dropbeatReady: false
    };

    module.context = (function() {
        function hasFlash() {
            var hasFlash = false;
            try {
                hasFlash = Boolean(new ActiveXObject('ShockwaveFlash.ShockwaveFlash'));
            } catch(exception) {
                hasFlash = ('undefined' != 
                    typeof navigator.mimeTypes['application/x-shockwave-flash']);
            }
            return hasFlash;
        }

        function browserVersion() {
            var ua= navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem =  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'MSIE '+ (tem[1] || '');
            }
            if(M[1] === 'Chrome'){
                tem = ua.match(/\bOPR\/(\d+)/)
                if(tem) {
                    return 'Opera ' + tem[1];
                }
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem = ua.match(/version\/(\d+)/i))) {
                M.splice(1, 1, tem[1]);
            }
            return M.join(' ');
        }

        function getBrowser() {
            var v,
                browser,
                version;
            v = browserVersion().split(' ');
            browser = v[0];
            version = v[1];
            if (browser === 'MSIE') {
                version = Number(version);
                if (version < 9) {
                    return {
                        support: false,
                        currentBrowser: v
                    };
                }
            }
            return {
                support: true,
                currentBrowser:v,
                fbInitialized: false
            };
        }

        function isMobile() {
            return (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
        }

        function isIOS() {
            return (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()));
        }

        function isAndroid() {
            return (/android/i.test(navigator.userAgent.toLowerCase()));
        }

        // XXX:DEBUG MODE ONLY
        var FB_CLIENT_IDS = {
                'spark.dropbeat.net': '1450325018550300',
                'hack.coroutine.io': '1567473853502082',
                'spark.coroutine.io': '1567477583501709',
                'dropbeat.net': '754181154701901'
            },
            context = {};

        context.host = window.location.host;
        context.account = null;
        context.isMobile = isMobile();
        context.isIOS = isIOS();
        context.isAndroid = isAndroid();
        context.browserSupport = getBrowser();
        context.fbClientId = FB_CLIENT_IDS[context.host];
        context.hasFlash = hasFlash();
        if (!context.fbClientId) {
            context.fbClientId = 'XXXXXXXXXXXXX';
        }

        context.userTrackLikes = {};
        context.externalTrackLikes = {};

        return context;
    }());

    module.api = (function () {
        var scheme = 'http',
            baseApiHost = window.location.host,
            uri = '/api/',
            version = 'v1',
            url = scheme + '://' + baseApiHost + uri + version + '/';

        function endpoint(name, version) {
            if (version) {
                return scheme + '://' + baseApiHost + uri +
                    version + '/' + name + '/';
            }
            return url + name + '/';
        }
        return {
            metaUrl: endpoint('meta/key'),
            metaUploadHostUrl: endpoint('meta/upload_host'),

            signinFbUrl: endpoint('user/signin_fb'),
            userUrl: endpoint('user/self'),
            signupWithEmailUrl: endpoint('user/email_signup'),
            signinWithEmailUrl: endpoint('user/email_signin'),
            signoutUrl: endpoint('user/signout'),
            unlockUrl: endpoint('user/unlock'),
            emailChangeUrl: endpoint('user/change_email'),
            changeNickname: endpoint('user/change_nickname'),
            changeResourceNameUrl: endpoint('user/change_resource_name'),
            changeProfileImageUrl: endpoint('user/change_profile_img'),
            changeUserDescUrl: endpoint('user/change_desc'),
            changeCoverImageUrl: endpoint('user/change_profile_cover_img'),

            followUserUrl: endpoint('user/follow'),
            unfollowUserUrl: endpoint('user/unfollow'),
            followingUserUrl: endpoint('user/following'),
            followersUserUrl: endpoint('user/followers'),

            trackUrl: endpoint('usertrack'),
            userTrackCommentUrl: endpoint('usertrack/comment'),
            userTrackNewestUrl: endpoint('usertrack/newest'),
            userTrackLikeUrl: endpoint('usertrack/like'),

            genreFavoritesUrl: endpoint('genre/favorite'),
            genreAddFavoriteUrl: endpoint('genre/add_favorite'),
            genreDelFavoriteUrl: endpoint('genre/del_favorite'),

            userLikeUrl: endpoint('user/like'),
            externalTrackLikeUrl: endpoint('track/like'),
            externalTrackDisLikeUrl: endpoint('track/dislike'),

            playlistUrl: endpoint('playlist'),
            playlistListUrl: endpoint('playlist/list'),
            playlistSharedUrl: endpoint('playlist/shared'),
            playlistImportUrl: endpoint('playlist/import'),
            playlistSetUrl: endpoint('playlist/set'),
            initialPlaylistUrl: endpoint('playlist/initial'),
            // deprecated
            //playlistAllUrl: endpoint('playlist/all'),

            feedbackUrl: endpoint('async/feedback'),
            importSoundcloud: endpoint('import/soundcloud'),
            importSoundcloudTrackUrl: endpoint('import/soundcloud_track'),

            feedUrl: endpoint('feed'),
            feedChannelUrl: endpoint('feed/channel'),


            trackShareUrl: endpoint('track/shared'),

            streamFollowingUrl: endpoint('stream/following', 'v2'),


            resolveObjectUrl: endpoint('resolve'),

            statisticsPlaybackUrl: endpoint('statistics/playback'),

            // Log api endpoint
            searchLogUrl: endpoint('log/search'),
            resolveLogUrl: endpoint('log/resolve'),
            playLogUrl: endpoint('log/play'),
            playdropLogUrl: endpoint('log/playdrop'),
            trackAddLogUrl: endpoint('log/trackadd'),
            playbackDetailLogUrl: endpoint('log/playback_detail'),
            downloadLogUrl: endpoint('log/download_count'),

            // upload server credential
            uploadCredentialUrl: endpoint('user/credential'),

            ////////////////
            // Deprecated //
            ////////////////
            channelBookmarkUrl: endpoint('channelbookmark'),

            followArtistUrl: endpoint('artist/follow'),
            unfollowArtistUrl: endpoint('artist/unfollow'),
            followingArtistUrl: endpoint('artist/following')

        };
    }());

    module.coreApi = (function () {
        var scheme = 'http',
            debug = false,
            prodBaseApiHost = 'core.dropbeat.net',
            devBaseApiHost = 'core.coroutine.io',
            //devBaseApiHost = 'dropbeat.net:19020',
            uri = '/api/',
            url = scheme + '://' + (debug ? devBaseApiHost : prodBaseApiHost) + uri;

        function endpoint(name) {
            return url + name + '/';
        }
        return {
            debug: debug,

            resolveUrl: endpoint('resolve'),
            dropUrl: endpoint('drop'),
            liveTrackListUrl: endpoint('live/tracklist'),

            searchUrl: endpoint('v1/search'),
            searchRelatedUrl: endpoint('search/related'),
            searchLivesetUrl: endpoint('search/liveset'),
            searchOtherUrl: endpoint('search/other'),
            searchRemixUrl: endpoint('search/remix'),
            searchAnotherTracksUrl: endpoint('search/another_tracks'),
            searchArtistUrl: endpoint('search/artist'),

            podcastUrl: endpoint('podcast'),
            podcastTrackListUrl: endpoint('podcast/tracklist'),

            channelFeedUrl: endpoint('channel/feed'),
            channelListUrl: endpoint('channel/list'),
            channelDetailUrl: endpoint('channel/detail'),
            channelDescExtractUrl: endpoint('channel/extract'),
            channelGproxy: endpoint('v1/channel/gproxy'),

            eventInfoUrl: endpoint('event'),
            trendingDjUrl: endpoint('trending/dj'),
            topDjUrl: endpoint('trending/top_djs'),
            trendingChartUrl: endpoint('trending/bpchart'),
            featuredPlaylistUrl: endpoint('trending/bpfeatured'),
            amateurDj: endpoint('trending/amateur_djs'),
            hotReleaseUrl: endpoint('trending/hot_release'),
            hotPodcast: endpoint('trending/hot_podcast'),
            featuredContentsUrl: endpoint('trending/featured_contents'),

            promotionUrl: endpoint('promotion/data'),

            genreUrl: endpoint('genre'),

            tracklistUrl: endpoint('tracklist'),

            queryTrackUrl: endpoint('query/track'),
            queryIndexUrl: endpoint('query/index'),

            artistFilterUrl: endpoint('artist/filter'),

            streamTrendingUrl: endpoint('stream/trending'),
            streamNewUrl: endpoint('stream/new'),


            imageUrl: endpoint('image/soundcloud')
        };
    }());

    module.resolveApi = (function () {
        var scheme = 'http',
            debug = false,
            prodBaseApiHost = 'resolve.dropbeat.net',
            devBaseApiHost = '14.63.224.95:19001',
            //devBaseApiHost = 'dropbeat.net:19020',
            uri = '/',
            url = scheme + '://' + (debug ? devBaseApiHost : prodBaseApiHost) + uri;

        function endpoint(name) {
            return url + name + '/';
        }
        return {
            debug: debug,

            resolveUrl: endpoint('resolve')
        };
    }());


    module.uploadApi = (function() {
        var scheme = 'http',
            debug = false,

            // prod
            prodBaseApiPort = '19090',

            // dev
            devBaseApiPort = '19091';

        function endpoint(name) {
            return function(host) {
                //host = '210.122.7.152';
                return scheme + '://' + host + ':' + 
                    (debug ? devBaseApiPort : prodBaseApiPort) + '/' + name + '/';
            }
        }

        return {
            uploadSoundUrl: endpoint('upload_sound'),
            uploadImageUrl: endpoint('upload_image'),
            postProcessUrl: endpoint('post_process'),
            preProcessUrl: endpoint('pre_process'),
            debug: debug
        };
    }());

    module.clientKeys = {
        google: 'AIzaSyCoieDdwxgy01P7MBIdR48tFxAtyEYHPmA',
        soundcloud: 'd5249ae899d7b26e6c6af608d876d12c'
    };

    module.compatibility = (function () {
        var navigator = window.navigator,
            chrome = navigator.userAgent.match(/(Chrome)/g),
            firefox = navigator.userAgent.match(/(Firefox)/g),
            safari = (navigator.userAgent.match(/(Safari)/g)) && !chrome,
            ie = !chrome && !firefox && !safari,

            browser = module.context.browserSupport.currentBrowser;

        return {
            isExplorer: browser[0] === 'MSIE',
            isSafari: browser[0] === 'Safari'
        };
    }());

    module.escapes = (function () {
        return {
            title: function (title) {
                return title.replace(/"/g, "'");
            }
        };
    }());

    module.requestFacebookSignin = function(section) {
        var url = 'https://www.facebook.com/dialog/oauth/',
            redirectUri = module.api.signinFbUrl,
            currPath = window.location.href,
            host = window.location.origin,
            localRedirectUri = currPath.replace(host, '');

        redirectUri += '?redirect=' +
            encodeURIComponent(localRedirectUri) + '/';

        module.log('signin', section);
        redirectUri = redirectUri.substring(
            0, redirectUri.length - 1);

        url += '?' + module.$.param({
            client_id: module.context.fbClientId,
            redirect_uri: redirectUri,
            scope: '[\'email\', \'user_likes\']'
        });
        window.location = url;
    };

    module.constants = {
        shareUriKey: 'playlist',
        tempEmailPostfix: '@dropbeat.net',
        queueEOL: 'stop',
        bgImages: [
            {
                name: 'wall3.jpg',
                width:1920,
                height: 800
            }
        /*
            {name : 'afrojack_01.jpg', width: 2048, height: 1365},
            {name : 'alesso_01.jpg', width: 640, height: 448},
            {name : 'alesso_02.jpeg', width: 800, height: 532},
            {name : 'alesso_03.jpg', width: 646, height: 430},
            {name : 'axwellingrosso_01.jpg', width: 1500, height: 998},
            {name : 'axwellingrosso_02.jpg', width: 640, height: 640},
            {name : 'axwellingrosso_03.jpg', width: 520, height: 330},
            {name : 'dadalife_01.jpg', width: 600, height: 386},
            {name : 'deadmau5_01.jpg', width: 1211, height: 720},
            {name : 'deadmaus_01.jpg', width: 1600, height: 1067},
            {name : 'dj_tiesto_club_life_img384.jpg', width: 1278, height: 846},
            {name : 'dyroanddannic_01.jpg', width: 500, height: 333},
            {name : 'hardwell_01.jpg', width: 2048, height: 1365},
            {name : 'hardwell_02.jpg', width: 1023, height: 569},
            {name : 'kaskade_01.jpg', width: 720, height: 479},
            {name : 'martingarrix_01.jpg', width: 606, height: 450},
            {name : 'martingarrix_02.jpg', width: 1024, height: 683},
            {name : 'martingarrix_03.jpg', width: 1600, height: 1067},
            {name : 'maxresdefault-1.jpg', width: 1280, height: 720},
            {name : 'oliverheldens_01.jpg', width: 640, height: 351},
            {name : 'oliverheldens_02.jpg', width: 1060, height: 706},
            {name : 'oliverheldens_03.jpeg', width: 1024, height: 681},
            {name : 'tiesto_01.jpg', width: 1778, height: 1200},
            {name : 'zedd_01.jpg', width: 960, height: 640}
           */
        ]
    };

    module.host = window.location.protocol + '//' +  window.location.host;

    module.isKorean = function(str) {
        var korCheck = /[ㄱ-ㅎㅏ-ㅣ가-힣]/,
            korEumsoStart = 0x1100,
            korEumsoEnd = 0x11fa,
            i, code;

        if (module.$.cookie('grant_korean')) {
            return false;
        }
        if (!str || str.length === 0) {
            return false;
        }
        if (korCheck.test(str)) {
            return true;
        }

        for (i = 0; i < str.length; i += 1) {
            code = str.charCodeAt(i);
            if (code >= korEumsoStart && code <= korEumsoEnd) {
                return true;
            }
        }
        return false;
    };

    module.log = function (category, action, label, value) {
        ga('send', 'event', category, action, label, value);
        return true;
    };


    module.initUserLog = function () {
        if (module.context.account) {
            var userId = module.context.account.id;
            // Set user id
            ga('set', '&uid', userId);
        }
    };

    module.coreLog = (function() {
        var ACTION_HANDLER,
            playEvents = [],
            playEventMusic,
            lastProgress = 0;

        module.$(module).on('musicProgressUpdate',
            function(e, progress, music, section) {
            if (playEventMusic && playEventMusic.id === music.id) {
                lastProgress = progress.playback;
            }
        });

        ACTION_HANDLER = {
            search: function(url) {
                module.$.ajax({
                    url:module.api.searchLogUrl,
                    data: {
                        q: url,
                        device_type: "web"
                    },
                    dataType: 'json',
                    cache:false
                }).done(function() {
                    //do nothing
                });
            },
            resolve: function(url) {
                if (!module.context.account) {
                    return;
                }
                module.$.ajax({
                    url:module.api.resolveLogUrl,
                    data: {
                        q: url,
                        device_type: "web"
                    },
                    cache:false,
                    dataType: 'json'
                }).done(function() {
                    //do nothing
                });
            },
            playdrop: function(track) {
                if (!track || !track.title) {
                    return;
                }
                module.$.ajax({
                    url:module.api.playdropLogUrl,
                    data: {
                        uid:track.id,
                        t:track.title,
                        device_type: "web"
                    },
                    cache:false,
                    dataType: 'json'
                }).done(function() {
                    //do nothing
                });
            },
            play: function(track) {
                if (!track || !track.title) {
                    return;
                }
                module.$.ajax({
                    url:module.api.playLogUrl,
                    data: {
                        uid:track.id,
                        t:track.title,
                        device_type: "web"
                    },
                    cache:false,
                    dataType: 'json'
                }).done(function() {
                    //do nothing
                });
            },
            trackAdd: function(title) {
                if (!module.context.account) {
                    return;
                }
                module.$.ajax({
                    url:module.api.trackAddLogUrl,
                    data: {
                        t:title,
                        device_type: "web"
                    },
                    cache:false,
                    dataType: 'json'
                }).done(function() {
                    //do nothing
                });
            },
            download: function(track) {
                if (!module.context.account) {
                    return;
                }
                if (!track || !track.uid) {
                    return;
                }
                module.$.ajax({
                    url:module.api.downloadLogUrl,
                    data: JSON.stringify({
                        track_id: track.uid
                    }),
                    cache:false,
                    dataType: 'json',
                    contentType:'application/json',
                    type:'POST'
                }).done(function() {
                    //do nothing
                });
            },
            playDetailStart: function(music) {
                if (playEventMusic && playEventMusic.id !== music.id) {
                    if (playEvents.length > 0 && lastProgress > 0) {
                        ACTION_HANDLER.playDetailExit(playEventMusic);
                    }
                    playEventMusic = null;
                }
                if (music.type !== 'dropbeat') {
                    return;
                }
                playEventMusic = music;
                playEvents.length = 0;

                playEvents.push({type: 'start'});
            },
            playDetailSeekFrom: function(music, seekFrom) {
                if (music.type !== 'dropbeat') {
                    return;
                }

                if (!playEventMusic || playEventMusic.id !== music.id) {
                    return;
                }

                playEvents.push({type: 'seek_from', ts: parseInt(seekFrom)});
            },
            playDetailSeekTo: function(music, seekTo) {
                if (music.type !== 'dropbeat') {
                    return;
                }

                if (!playEventMusic || playEventMusic.id !== music.id) {
                    return;
                }

                playEvents.push({type: 'seek_to', ts: parseInt(seekTo)});
            },
            playDetailExit: function() {
                var exitAt = lastProgress;
                if (!playEventMusic) {
                    return;
                }
                playEvents.push({type: 'exit', ts: parseInt(exitAt)});
                sendPlayDetailLog();
            },
            playDetailEnd: function(music) {
                if (music.type !== 'dropbeat') {
                    return;
                }
                if (!playEventMusic || playEventMusic.id !== music.id) {
                    return;
                }
                playEvents.push({type: 'end'});
                sendPlayDetailLog();
            },
            playDetailError: function(music) {
                playEventMusic = null;
                playEvents.length = null;
            }
        };

        function sendPlayDetailLog() {
            var geoLocation = module.context.geolocation;

            if (!geoLocation) {
                playEvents.length = 0;
                return;
            }

            if (!playEventMusic || playEventMusic.type !== 'dropbeat') {
                playEvents.length = 0;
                return;
            }

            module.$.ajax({
                url:module.api.playbackDetailLogUrl,
                data: JSON.stringify({
                    track_id: playEventMusic.uid,
                    data: playEvents,
                    location: {
                        lat: geoLocation.lat,
                        lng: geoLocation.lng,
                        country_name: geoLocation.countryName,
                        country_code: geoLocation.countryCode,
                        city_name: geoLocation.cityName
                    }
                }),
                dataType: 'json',
                contentType: 'application/json',
                cache:false,
                type:'POST'
            }).done(function() {
            });

            playEvents.length = 0;
            playEventMusic = null;
        };

        return function() {
            var actionName,
                params = Array.prototype.slice.call(arguments, 0),
                handler;

            actionName = params[0];
            handler = ACTION_HANDLER[actionName];

            if (handler) {
                handler.apply(this, params.slice(1));
            }
        };
    }());

    module.beforeunload = function() {
        var v;
        module.$.each(beforeunloadCallbacks, function(key, val) {
            v = v || val();
        });
        return v;
    };

    module.addBeforeunloadCallback = function(key, func) {
        beforeunloadCallbacks[key] = func;
        module.router.registerBeforeNavigate(key, func);
    };

    module.removeBeforeunloadCallback = function(key) {
        delete beforeunloadCallbacks[key];
        module.router.unregisterBeforeNavigate(key);
    };

    return module;
}($dbt));

$dbt.$(document).ready(function () {
    'use strict';

    var genreDeferred = $dbt.$.Deferred(),
        metaDeferred = $dbt.$.Deferred(),
        initDeferreds = [];

// init shortcut
    $dbt.shortcuts();

    if ($dbt.context.isMobile) {
        onMobileLoaded();
    } else {
        onDesktopLoaded();
    }

    window.onbeforeunload = function (event) {
        var message = $dbt.beforeunload();
        if (!message) {
            return;
        }
        if (typeof event == 'undefined') {
            event = window.event;
        }
        if (event) {
            event.returnValue = message;
        }
        return message;
    };


// register for playdetail log sending
    $dbt.addBeforeunloadCallback('playDetailLog', function() {
        $dbt.coreLog('playDetailExit');
    });

    $dbt.$.ajax({
        url: $dbt.coreApi.genreUrl,
        crossDomain: true,
        dataType: 'json'
    }).done(function(res) {
        var genres = {};
        if (!res || !res.success) {
            $dbt.s.notify('Failed to fetch genre information', 'error');
        } else {
            $dbt.$.each(res, function(groupKey, genreGroup) {
                if (groupKey !== 'success') {
                    genres[groupKey] = genreGroup;
                }
            });
        }
        genreDeferred.resolve(genres);
    });
    initDeferreds.push(genreDeferred);


    $dbt.$.ajax({
        url: $dbt.api.metaUrl,
        dataType: 'json'
    }).done(function(res) {
        if (!res.success || !res.soundcloud_key) {
            $dbt.s.notify('Failed to fetch meta information', 'error');
            metaDeferred.reject();
            return;
        }
        $dbt.clientKeys['soundcloud'] = res.soundcloud_key;
        metaDeferred.resolve(res);
    }).fail(function() {
        metaDeferred.reject();
    });


    initDeferreds.push(metaDeferred);

// wait init preprocess
    $dbt.$.when.apply(this, initDeferreds).done(function(genre, meta) {
        var locale, deferred;

        $dbt.context.genre = genre;

        if ($dbt.coreApi.debug) {
            $dbt.notification.notify('DEV CORE', 'warn');
        }
        if ($dbt.uploadApi.debug) {
            $dbt.notification.notify('DEV UPLOAD', 'warn');
        }

        locale = 'en_US';
        $dbt.i18n = new $dbt.I18n();
        deferred = $dbt.i18n.setLocale(locale);
        $dbt.context.locale = locale;

        deferred.done(function() {
            moment.locale(locale);
            $dbt.$('.hidden-before-locale').removeClass('hidden-before-locale');
            $dbt.initialize();
        });
    });

    function onMobileLoaded() {
        $dbt.$('body').addClass('mobile-view');
        $dbt.$('input,textarea').remove();
    }

    function onDesktopLoaded() {
        $dbt.$('body').addClass('desktop-view');
    }
});

// IOS HACK WAY
// Prevent rubber-banding of the body, but allow for scrolling elements
// SEE http://stackoverflow.com/questions/10357844/how-to-disable-rubber-band-in-ios-web-apps
(function registerScrolling($) {
    var prevTouchPosition = {},
        scrollYClass = 'scroll-y',
        scrollXClass = 'scroll-x',
        searchTerms = '.' + scrollYClass + ', .' + scrollXClass;

    $dbt.$(document).bind('touchstart', function (e) {
        var $scroll = $dbt.$(e.target).closest(searchTerms),
            targetTouch = e.originalEvent.targetTouches[0];

// Store previous touch position if within a scroll element
        prevTouchPosition = $scroll.length ?
            { x: targetTouch.pageX, y: targetTouch.pageY } : {};
    });

    $dbt.$(document).bind('touchmove', function (e) {
        var $scroll = $dbt.$(e.target).closest(searchTerms),
            targetTouch = e.originalEvent.targetTouches[0];

        if (prevTouchPosition && $scroll.length) {
            // Set move helper and update previous touch position
            var move = {
                x: targetTouch.pageX - prevTouchPosition.x,
                y: targetTouch.pageY - prevTouchPosition.y
            };
            prevTouchPosition = { x: targetTouch.pageX, y: targetTouch.pageY };

            // Check for scroll-y or scroll-x classes
            if ($scroll.hasClass(scrollYClass)) {
                var scrollHeight = $scroll[0].scrollHeight,
                    outerHeight = $scroll.outerHeight(),

                    atUpperLimit = ($scroll.scrollTop() === 0),
                    atLowerLimit =
                        (scrollHeight - $scroll.scrollTop() === outerHeight);

                if (scrollHeight > outerHeight) {
// If at either limit move 1px away to allow normal scroll behavior on future moves,
// but stop propagation on this move to remove limit behavior bubbling up to body
                    if (move.y > 0 && atUpperLimit) {
                        $scroll.scrollTop(1);
                        e.stopPropagation();
                    } else if (move.y < 0 && atLowerLimit) {
                        $scroll.scrollTop($scroll.scrollTop() - 1);
                        e.stopPropagation();
                    }

                    // If only moving right or left, prevent bad scroll.
                    if(Math.abs(move.x) > 0 && Math.abs(move.y) < 3){
                      e.preventDefault()
                    }

                    // Normal scrolling behavior passes through
                } else {
                    // No scrolling / adjustment when there is nothing to scroll
                    e.preventDefault();
                }
            } else if ($scroll.hasClass(scrollXClass)) {
                var scrollWidth = $scroll[0].scrollWidth,
                    outerWidth = $scroll.outerWidth(),

                    atLeftLimit = $scroll.scrollLeft() === 0,
                    atRightLimit =
                        scrollWidth - $scroll.scrollLeft() === outerWidth;

                if (scrollWidth > outerWidth) {
                    if (move.x > 0 && atLeftLimit) {
                        $scroll.scrollLeft(1);
                        e.stopPropagation();
                    } else if (move.x < 0 && atRightLimit) {
                        $scroll.scrollLeft($scroll.scrollLeft() - 1);
                        e.stopPropagation();
                    }
                    // If only moving up or down, prevent bad scroll.
                    if(Math.abs(move.y) > 0 && Math.abs(move.x) < 3){
                      e.preventDefault();
                    }

                    // Normal scrolling behavior passes through
                } else {
                    // No scrolling / adjustment when there is nothing to scroll
                    e.preventDefault();
                }
            }
        } else {
            // Prevent scrolling on non-scrolling elements
            e.preventDefault();
        }
    });
})(jQuery);



function onYouTubeIframeAPIReady() {
    'use strict';
    $dbt.state.youtubeApiReady = true;
    $dbt.$($dbt).trigger('youtubeApiReady');
}

// below jquery is needed for crors support in IE
$dbt.$.support.cors = true;
