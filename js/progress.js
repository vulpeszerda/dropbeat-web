/*jslint browser: true*/
/*global $*/
var $dbt = (function (module) {
    'use strict';

    module = module || {};
    module.progress = {};

    function Progress(wrapper, section) {
        return {
            isDragging: null,
            $bar: null,
            $buffer: null,
            $bullet: null,
            $position: null,
            $length: null,
            $handle: null,
            bulletUpdateCaller: null,
            bufferUpdateCaller: null,
            isStarted: false,
            isSeeking: false,

            init: function () {
                var that = this;

                module._.bindAll(this, 'onBarClicked');

                this.isDragging = false;
                this.$buffer = module.$('.buffer', wrapper);
                this.$bullet = module.$('.bullet', wrapper);
                this.$position = module.$('.curr-play-time', wrapper);
                this.$length = module.$('.total-play-time', wrapper);
                this.$bar = module.$('.progress-bar', wrapper);
                this.$inverseTimeTextWrapper = module.$('.inverse-play-times-wrapper', wrapper);
                this.$inverseTimeText = module.$('.inverse-play-times', wrapper);
                this.$handle = module.$('.progress-handle', wrapper);
                this.$handle.removeClass('drag');


                module.$(window).on('mouseup', function (e) {
                    that.stopDrag(e);
                });

                module.$(window).on('mousemove', function (e) {
                    that.onDrag(e);
                });
            },

            start: function () {
                var that = this,
                    interval = 100;

                that.$handle.show();
                if (!that.bulletUpdateCaller) {
                    that.bulletUpdateCaller =
                        setInterval(function () {
                            that.updateBullet();
                        }, interval);
                }
                if (!that.bufferUpdateCaller) {
                    that.bufferUpdateCaller =
                        setInterval(function () {
                            that.updateBuffer();
                        }, interval * 2);
                }
                that.isStarted = true;

                that.$bar.on('mousedown', that.onBarClicked);

                that.$handle.on('mousedown', function (e) {
                    var currentPlayer = module.playerManager.currentPlayer;

                    if (currentPlayer) {
                        that.startDrag();
                    }
                });

                if (that.$inverseTimeText.size() > 0) {
// In case we use inverse time text
// - which means time text placed over progressbar,
// so we should listen mousedown event on time text
                    that.$inverseTimeTextWrapper.on('mousedown', that.onBarClicked);
                    that.$position.on('mousedown', that.onBarClicked);
                    that.$length.on('mousedown', that.onBarClicked);
                }
            },

            stop: function () {
                var that = this;

                if (that.bulletUpdateCaller) {
                    clearInterval(that.bulletUpdateCaller);
                    that.bulletUpdateCaller = null;
                }
                if (that.bufferUpdateCaller) {
                    clearInterval(that.bufferUpdateCaller);
                    that.bufferUpdateCaller = null;
                }
                that.$bar.off('mousedown');
                that.$handle.off('mousedown');
                that.$inverseTimeTextWrapper.off('mousedown');
                that.$position.off('mousedown');
                that.$length.off('mousedown');
                that.isStarted = false;
            },

            startDrag: function () {
                var that = this;

                that.isDragging = true;
                that.$handle.addClass('drag');
            },

            onBarClicked: function (e) {
                var currentPlayer = module.playerManager.currentPlayer;

                if (currentPlayer) {
                    this.updateByMouse(e);
                }
            },

            onDrag: function (event) {
                var that = this;

                if (!that.isDragging) {
                    return;
                }
                that.updateByMouse(event);
            },

            stopDrag: function (event) {
                var that = this;

                if (that.isDragging) {
                    that.stop();
                    that.isDragging = false;
                    that.$handle.removeClass('drag');
                    that.updateByMouse(event);
                    that.start();
                }
            },

            updateByMouse: function (event) {
                var that = this,
                    cursorX = event.pageX - that.$bullet.offset().left,
                    barWidth = that.$bar.width(),
                    len = module.playerManager.getTotalPlaybackTime(),
                    pos,
                    posText,
                    lenText,
                    bulletWidth,
                    currentMusic;

                if (cursorX < 0) {
                    cursorX = 0;
                }
                if (cursorX > barWidth) {
                    cursorX = barWidth;
                }
                pos = cursorX / barWidth * len;

                if (pos && len) {
                    posText = that.getTimeText(pos);
                    that.$position.text(posText);

                    lenText = that.getTimeText(len);
                    that.$length.text(lenText);
                    bulletWidth = (pos * 100) / len;
                    that.$bullet.width(bulletWidth + '%');
                    that.$inverseTimeText.width(bulletWidth + '%');
                    that.$handle.css('left', bulletWidth + '%');
                }

                if (!that.isDragging) {
                    currentMusic = module.playerManager.getCurrentMusic();
                    if (!currentMusic) {
                        return;
                    }
                    module.playerManager.seekTo(pos);
                }
                that.isSeeking = true;
                setTimeout(function() {
                    that.isSeeking = false;
                }, 200);
            },

            updateBullet: function (event) {
                var that = this,
                    pos = module.playerManager.getCurrentPlaybackTime(),
                    posText = '',
                    len = module.playerManager.getTotalPlaybackTime(),
                    lenText = '',
                    bulletWidth,
                    currentMusic,
                    section;

                currentMusic = module.playerManager.getCurrentMusic();
                section = module.playerManager.getCurrentSection();

                if (!currentMusic) {
                    return;
                }

                if (that.isDragging || that.isSeeking) {
                    return;
                }

                if (pos && len) {
                    posText = that.getTimeText(pos);
                    that.$position.text(posText);

                    lenText = that.getTimeText(len);
                    that.$length.text(lenText);
                    bulletWidth = pos * 100 / len;
                    that.$bullet.width(bulletWidth + '%');
                    that.$inverseTimeText.width(bulletWidth + '%');
                    that.$handle.css('left', bulletWidth + '%');

                    module.$(module).trigger('musicProgressUpdate', [{
                        playback: pos,
                        duration: len
                    }, currentMusic, section]);

                } else {
                    that.$position.text('00:00');
                    that.$length.text('00:00');

                    that.$bullet.width(0);
                    that.$inverseTimeText.width(0);

                    module.$(module).trigger('musicProgressUpdate', [{
                        playback: 0,
                        duration: 0
                    }, currentMusic, section]);
                }
            },

            updateBuffer: function (buffer) {
                var that = this;

                if (that.isStarted) {
                    buffer = !buffer ? module.playerManager.getBuffer() : buffer;
                    that.$buffer.width(buffer + '%');
                }
            },

            reset: function () {
                var that = this;
                that.updateBuffer(0);
            },

            flush: function () {
                var that = this;
                that.reset();
                that.stop();
                that.$position.text('00:00');
                that.$length.text('00:00');
                that.$bullet.width(0);
                that.$inverseTimeText.width(0);
                that.$handle.css('left', 0);
            },

            getTimeText: function (sec) {
                var m = Math.floor(sec / 60.0),
                    s = Math.floor(sec % 60);

                return ((m < 10) ? '0' : '') + m + ':' +
                    ((s < 10) ? '0' : '') + s;
            }
        };
    }

    module.progress.items = {};
    module.progress.items.base = new Progress('#player', 'base');

    module.progress.controller = {
        init: function (section, container, option) {
            if (section === 'drop') {
                return;
            }
            if (section === 'player_tracklist') {
                module.progress.items[section] = new Progress(container, section);
                module.progress.items[section].init();
                return;
            }
            module.progress.items.base.init();
        },
        start: function (section) {
            if (section === 'drop') {
                return;
            }
            if (section === 'player_tracklist' && module.progress.items[section]) {
                module.progress.items[section].start();
                return;
            }
            module.progress.items.base.start();
        },
        stop: function (section) {
            if (section === 'drop') {
                return;
            }
            if (section === 'player_tracklist' && module.progress.items[section]) {
                module.progress.items[section].stop();
                return;
            }
            module.progress.items.base.stop();
        },
        stopAll: function() {
            module.$.each(module.progress.items, function(key, module) {
                if (module.stop) {
                    module.stop();
                }
            });
        },
        reset: function (section) {
            if (section === 'drop') {
                return;
            }
            if (section === 'player_tracklist' && module.progress.items[section]) {
                module.progress.items[section].reset();
                return;
            }
            module.progress.items.base.reset();
        },
        flush: function (section) {
            if (section === 'drop') {
                return;
            }
            if (section === 'player_tracklist' && module.progress.items[section]) {
                // do nothing
                return;
            }
            module.progress.items.base.flush();
        },
        flushAll: function() {
            module.$.each(module.progress.items, function(key, module) {
                if (module.flush) {
                    module.flush();
                }
            });
        }
    };

    return module;
}($dbt));
