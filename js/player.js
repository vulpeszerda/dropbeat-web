/*jslint browser: true*/
/*global $, soundManager, YT*/
var $dbt = (function (module) {
    'use strict';
    var player;

    player = {
        create: function (type, section) {
            var that = this,
                player;

// section: base(original player), search, related
            if (type === 'youtube') {
                player = new that.YoutubePlayer(section);
            } else if (type === 'soundcloud') {
                player = new that.SoundcloudPlayer(section);
            } else {
                throw 'UndefinedPlayerType';
            }

            player.init();
            return player;
        },

        BaseExternalPlayer: function () {
            var that = this;

            that.type = undefined;

            that.initialized = false;
            that.playing = false;
            that.isReady = false;

            that.init = function (callback) {
                throw 'NotImplementedError';
            };

            that.hide = function () {
                throw 'NotImplementedError';
            };

            that.show = function () {
                throw 'NotImplementedError';
            };

            that.play = function (music) {
                throw 'NotImplementedError';
            };

            that.stop = function () {
                throw 'NotImplementedError';
            };

            that.pause = function () {
                throw 'NotImplementedError';
            };

            that.seekTo = function (time) {
                throw 'NotImplementedError';
            };

            that.setVolume = function(vol) {
                throw 'NotImplementedError';
            };

            that.mute = function() {
                throw 'NotImplementedError';
            };

            that.unmute = function() {
                throw 'NotImplementedError';
            };

            that.getCurrentPlaybackTime = function () {
                throw 'NotImplementedError';
            };

            that.getTotalPlaybackTime = function () {
                throw 'NotImplementedError';
            };

            that.getBuffer = function () {
                throw 'NotImplementedError';
            };

            that.calculateBuffer = function (fraction) {
                var buffer = 0;

                if (fraction) {
                    buffer = fraction * 100;
                    buffer = buffer > 100 ? 100 : buffer;
                    buffer = buffer < 0 ? 0 : buffer;
                }
                return buffer;
            };

            that.onLoading = function () {
                var player = module.playerManager.getCurrentPlayer(),
                    music = module.playerManager.currentMusic,
                    originalMusic;

                module.$(module).trigger('loadingMusic', [
                    music,
                    player.section
                ]);

                if (player.section === 'drop') {
                    if (music.original) {
                        originalMusic = music.original;
                        module.log(
                            'player-play-from-' + player.section,
                            'play-' + music.type || "unknown",
                            music.title);
                        module.coreLog('playdrop', originalMusic);
                        module.coreLog('playDetailExit');
                    }
                } else {
                    module.log(
                        'player-play-from-' + player.section,
                        'play-' + music.type,
                        music.title);
                    module.coreLog('play', music);
                    module.coreLog('playDetailStart', music);
                }
            };

            that.onFinish = function () {
                var music = null,
                    player = module.playerManager.getCurrentPlayer();

                if (!player) {
                    return;
                }

                module.$(module).trigger('finishedMusic', [
                    module.playerManager.currentMusic,
                    player.section
                ]);

                music = module.playerManager.getCurrentMusic();
                if (module.s.repeatControl.state ===
                        module.s.repeatState.repeatOne) {
                    module.playerManager.playing = false;
                    if (music) {
                        module.log(
                            'player-play-from-' + player.section,
                            'play-' + music.type,
                            music.title);
                        if (player.section !== 'drop') {
                            module.coreLog('play', music);
                            module.coreLog('playDetailEnd', music);
                            module.coreLog('playDetailStart', music);
                        }
                        module.playerManager.onPlayMusic(music, player.section);
                        return;
                    }
                }
                if (music) {
                    module.coreLog('playDetailEnd', music);
                }
                if (module.playerManager.forth() !== -1) {
                    return;
                }
                module.playerManager.stop();
                module.$(module).trigger('playerStopped', player.section);
            };

            that.onPlaying = function () {
                if (!module.playerManager.currentMusic) {
                    return;
                }

                var player = module.playerManager.getCurrentPlayer();

                module.$(module).trigger('playMusic', [
                    module.playerManager.currentMusic,
                    player.section
                ]);

                module.$(module).trigger('mayPlayInfoUpdate',
                    [module.playerManager.currentMusic, player.section]);
            };
        },

        YoutubePlayer: function (section) {
            var that = this;

            if (section === 'base') {
                that.container = {
                    'wrapper': '#external-player',
                    'inner': '#youtube-player',
                    'id': 'youtube-player'
                };
            } else if (section === 'search') {
                that.container = {
                    'wrapper': '#search-result-player',
                    'inner': '#search-youtube-player',
                    'id': 'search-youtube-player'
                };
            }

            that.section = section;
            that.player = null;
            that.view = null;
            that.type = section + 'youtube';
            that.playerWidth = module.$(that.container.wrapper).width();
            that.playerHeight = module.$(that.container.wrapper).height();
            that.playing = false;
            that.isSafariHack = false;
            that.safariHackDone = false;
            that.isReady = false;
            that.currentVideo = null;
            that.hasPermission = false;
            that.firstPlayed = false;
            that.loaded = false;

            that.init = function () {
                var initWidth = that.playerWidth,
                    initHeight = that.playerHeight,
                    options,
                    videoId;

                that.isSafariHack = module.compatibility.isSafari &&
                    !module.context.isMobile;

                if (that.isSafariHack) {
                    that.loaded = true;
                }

                that.hasPermission = !module.context.isMobile || 
                    !module.compatibility.isSafari;

                module.$('#external-player .close-init-box-btn').on('click', function() {
                    delete that._onAfterAcquirePermission
                    that.hide();
                });

                that.firstPlayed = !module.context.isMobile ||
                    module.context.isIOS;

// Fucking safari blocks flash plugin automatically after `mavericks`.
// (Because of fucking low energy policy)
// There may be two hacks for this problem.
// 1. Emulate automatic mouse click to force use of flash plugin in safari
// No initialize time is needed in next run of dropbeat in this solution.
// 2. When youtube frame size is large enough, youtube automatically
// acquires flash plugin permission from safari.
// Therefore, we make `500, 500` size frame to acquire flash plugin
// permission, and hide it to left by `-(widthSize - 1)`.
// If we fully hide youtube frame in this step, the whole youtube
// javascript api crashes and is not initialized. So we leave just 1px.

// We choose second magical hack to fucking safari workaround.
                if (that.isSafariHack) {
                    initWidth = 500;
                    initHeight = 500;
                    module.$(that.container.wrapper).addClass('safari-hack');
                    options = {'autoplay': 1, 'controls': 0, 'showinfo': 0};
                } else {
                    options = {'autoplay': 0, 'controls': 0, 'showinfo': 0};
                    if (module.context.isMobile ||
                        (that.section === 'search' ||
                            that.section === 'related')) {
                        that.hide();
                    }
                }
                options['wmode'] = 'opaque';

                videoId = 'A-8-Y5BhZFg';
                that.player = new YT.Player(that.container.id, {
                    width: initWidth,
                    height: initHeight,
                    videoId: videoId,
                    enablejsapi: 1,
                    modestbranding: 1,
                    playerVars: options,
                    events: {
                        'onReady' : function (data) {
                            that.initialized = true;
                            that.view = module.$(that.container.inner);
                            that.player.mute();
                            that.setVolume(module.s.volumeControl.volume);
                            data.target.addEventListener(
                                'onStateChange',
                                that.onStateChange
                            );
                            that.isReady = true;
                            module.$(module).trigger('playerReady.' + section + '.youtube');
                        },
                        'onError': function(data) {
// 2   - Invalid param
// 100 – Failed to find video; in case of video deleted or hidden.
// 101 – Uploader not allowed to play this video outside of youtube.
// 150 – Same as 101.
                            var code = data.data,
                                current = that.currentVideo;

                            if (code === 2 || !current) {
                                return;
                            }
                            module.coreLog('playDetailError', current);
                            module.s.notifyManager.notPlayable(current.title);
                            if (module.s.repeatControl.state
                                    === module.s.repeatState.repeatOne ||
                                    module.playerManager.forth() === -1) {
                                module.playerManager.stop();
                                module.$(module).trigger('playerStopped', section);
                            }
                        }
                    }
                });
            };

            that.hide = function () {
                var $el = module.$(that.container.wrapper);
                $el.removeClass('popup').addClass('hide-video');
            };

            that.show = function () {
                module.$(that.container.wrapper).removeClass('hide-video');
            };

            that.expose = function() {
                module.$(that.container.wrapper).addClass('popup')
                    .removeClass('hide-video')
                    .removeClass('minimized')
                    .removeClass('hidden-in-mobile');
            };

            that.onStateChange = function (state) {
                if (!state) {
                    return;
                }
                switch (state.data) {
                case YT.PlayerState.ENDED:
                    that.onFinish();
                    break;
                case YT.PlayerState.PLAYING:
                    if (that.isSafariHack) {
                        that.stop();
                        that.isSafariHack = false;
                        that.view.height(that.playerHeight);
                        that.view.width(that.playerWidth);
                    } else {
                        that.onPlaying();
                    }

                    that.firstPlayed = true;
                    if (that._firstPlayTimeout) {
                        clearTimeout(that._firstPlayTimeout);
                        delete that._firstPlayTimeout;
                    }
                    if (module.context.isMobile ||
                            (that.isSafariHack &&
                            (that.section === 'search' ||
                            that.section === 'related'))) {
                        that.hide();
                    }
                    break;
                case YT.PlayerState.PAUSED:
                    break;
                case YT.PlayerState.BUFFERING:
                    break;
                case YT.PlayerState.CUED:
                    break;
                case YT.PlayerState.UNSTARTED:
                    if (that.currentVideo) {
                        if (!that.loaded) {
                            that.onLoading();
                            that.loaded = true;
                            return;
                        }
                    }
                    if (that.hasPermission) {
                        return;
                    }
                    if (that._onAfterAcquirePermission) {
                        that.hasPermission = true;
                        that._onAfterAcquirePermission();
                        delete that._onAfterAcquirePermission;
                    }
                    break;
                default:
                    break;
                }
            };

            that.play = function (video) {
                var play = function() {
                    var quality = module.context.isMobile ? 'small': 'hd720';

                    if (video) {
                        that.currentVideo = video;
                        that.loaded = false;
                        that.player.loadVideoById(video.id, 0, quality);
                    } else {
                        that.player.playVideo();
                    }
                    if (!module.s.volumeControl.isMuted) {
                        that.player.unMute();
                    }
                };

                if (!that.hasPermission) {
                    that._onAfterAcquirePermission = play;
                    that.expose();
                    return;
                } else {
                    play();
                    if (!that.firstPlayed) {
                        if (that._firstPlayTimeout) {
                            clearTimeout(that._firstPlayTimeout);
                        }
                        that._firstPlayTimeout = setTimeout(function() {
                            that.hasPermission = false;
                            that._onAfterAcquirePermission = play;
                            that.expose();
                        }, 3000);
                    }
                }
            };

            that.stop = function () {
                if (that.player && that.player.stopVideo) {
                    that.player.stopVideo();
                    that.currentVideo = null;
                }
            };

            that.pause = function () {
                if (that.player && that.player.pauseVideo) {
                    that.player.pauseVideo();
                }
            };

            that.seekTo = function (time) {
                if (that.currentVideo && that.player.seekTo) {
                    module.coreLog('playDetailSeekFrom',
                        that.currentVideo, this.getCurrentPlaybackTime());
                    that.player.seekTo(time, true);
                    module.coreLog('playDetailSeekTo', that.currentVideo, time);
                }
            };

            that.setVolume = function(vol) {
                if (that.player && that.player.setVolume) {
                    that.player.setVolume(vol);
                }
            };

            that.mute = function() {
                if (that.player && that.player.mute) {
                    that.player.mute();
                }
            };

            that.unmute = function() {
                if (that.player && that.player.unMute) {
                    that.player.unMute();
                }
            };

            that.getCurrentPlaybackTime = function () {
                if (that.currentVideo) {
                    return that.player.getCurrentTime();
                }
                return 0;
            };

            that.getTotalPlaybackTime = function () {
                if (that.currentVideo) {
                    return that.player.getDuration();
                }
                return 0;
            };

            that.getBuffer = function () {
                if (that.player && that.player.getVideoLoadedFraction) {
                    return that.calculateBuffer(
                        that.player.getVideoLoadedFraction()
                    );
                }
                return 0;
            };
        },

        SoundcloudPlayer: function () {
            var that = this;

            that.section = 'base';
            that.streamApiBase = 'https://api.soundcloud.com/tracks/';
            that.developerKey = module.clientKeys['soundcloud'];
            that.currentMusic = null;
            that.validationTimer = null;
            that.titleHack = false;
            that.type = 'basesoundcloud';

            that.playing = false;
            that.isReady = false;
            that.hasPermission = false;
            that.resolveReq = null;

            that.init = function (callback) {
                that.initialized = true;
                that.hasPermission = !module.context.isMobile;

                soundManager.onready(function () {
                    module.state.soundManagerReady = true;
                    module.$(module).trigger('playerReady.' + that.section + '.soundcloud');
                    that.isReady = true;
                    if (callback) {
                        setTimeout(function () {
                            callback();
                        }, 1000);
                    }
                });
            };

// Acquire sound permission for IOS.
// Because sound should be played with click event in IOS (first time only)
// We will play empty mp3 for handle this case
            that.acquirePermission = function() {
                var sound;
                sound = soundManager.createSound({
                    id: '_',
                    url:'https://s3-ap-northeast-1.amazonaws.com/dropbeat/empty.mp3' ,
                    volume: 0,
                    onplay: function() {
                        soundManager.pause('_');
                        that.hasPermission = true;
                    },
                })
                sound.play();
            };

            that.hide = function () {
                module.$('soundcloud-player').css('left', '-10000px');
            };

            that.show = function () {
                module.$('soundcloud-player').css('left', '0px');
            };

            that.play = function (music) {
                var streamUrl,
                    musicId,
                    callback;

                if (!music && !that.currentMusic) {
                    return;
                }

                if (!music && that.currentMusic) {
                    soundManager.play(that.currentMusic.id);
                    that.onPlaying();
                    return;
                }

                if (music.type === 'soundcloud') {
                    streamUrl =
                        that.streamApiBase + music.id +
                        '/stream?consumer_key=' + that.developerKey;
                } else if (music.stream_url) {
                    streamUrl = music.stream_url;
                } else if (music.type !== 'youtube') {
                    musicId = unescape(music.id);
                    if (musicId.indexOf('/') === -1) {
                        return;
                    }
                    streamUrl = musicId;
                }

                callback = function(streamUrl) {
                    that._sound = soundManager.createSound({
                        id: music.id,
                        url: streamUrl,
                        volume: module.s.volumeControl.volume,
                        onload: that.onStateChange,
                        onplay: that.onStateChange,
                        onfinish: that.onFinish
                    });
                    if (module.s.volumeControl.isMuted) {
                        that.mute();
                    }
                    soundManager.play(music.id);
                };

                that.titleHack = true;
                that.playing = false;
                clearTimeout(that.validationTimer);
                that.currentMusic = music;
                that.onLoading();

                if (streamUrl) {
                    that.hasPermission = true;
                    callback(streamUrl);
                    return;
                }

                if (music.type !== 'youtube') {
                    return;
                }

                if (!that.hasPermission) {
                    that.acquirePermission();
                }

                if (that.resolveReq) {
                    that.resolveReq.abort();
                }
                that.resolveReq = module.$.ajax({
                    url: module.resolveApi.resolveUrl,
                    data: {
                        t: 'web',
                        uid: music.id
                    },
                    crossDomain: true,
                    contentType: 'application/json',
                    dataType: 'json'
                }).done(function(res) {
                    var url;
                    if (res.urls.length === 0) {
                        that.onStateChange(false);
                        return;
                    }
                    url = res.urls[res.urls.length - 1].url;
                    music.stream_url = url;
                    callback(url);
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus !== 'abort') {
                        that.onStateChange(false);
                    }
                }).always(function() {
                    that.resolveReq = null;
                });
            };

            that.stop = function () {
                soundManager.stopAll();
                if (that.currentMusic) {
                    soundManager.destroySound(that.currentMusic.id);
                }
                that.currentMusic = null;
            };

            that.pause = function () {
                if (that.currentMusic) {
                    soundManager.pause(that.currentMusic.id);
                }
            };

            that.seekTo = function (time) {
                var musicId,
                    previousMuteState = module.s.volumeControl.isMuted;

                if (that.currentMusic) {
                    module.coreLog('playDetailSeekFrom',
                        that.currentMusic, this.getCurrentPlaybackTime());
                    module.coreLog('playDetailSeekTo', that.currentMusic, time || 0);
                    //that.pause();
                    that.unmute && that.unmute();

                    musicId = that.currentMusic.id;

                    soundManager.clearOnPosition(musicId, 0);
                    soundManager.onPosition(
                        musicId,
                        0,
                        function() {
                            //that.play();
                            if (!previousMuteState) {
                                that.unmute && that.unmute();
                            }
                            soundManager.clearOnPosition(musicId, 0);
                        });

                    soundManager.setPosition(
                        musicId,
                        time * 1000
                    );
                }
            };

            that.setVolume = function(vol) {
                if (that.currentMusic) {
                    soundManager.setVolume(that.currentMusic.id, vol);
                }
            };

            that.mute = function() {
                soundManager.mute();
            };

            that.unmute = function() {
                soundManager.unmute();
            };

            that.getCurrentPlaybackTime = function () {
                var time = 0,
                    sound;

                if (that.currentMusic) {
                    sound = soundManager.getSoundById(that.currentMusic.id);
                    if (sound) {
                        time = sound.position / 1000.0;
                    }
                }

                if (that.titleHack && time > 0) {
                    module.$(module).trigger('mayPlayInfoUpdate', [that.currentMusic, that.section]);
                    that.titleHack = false;
                }
                return time;
            };

            that.getTotalPlaybackTime = function () {
                var time = 0,
                    sound;

                if (that.currentMusic) {
                    sound = soundManager.getSoundById(that.currentMusic.id);
                    if (sound) {
                        time = sound.durationEstimate / 1000.0;
                    }
                }
                return time;
            };

            that.getBuffer = function () {
                var sound;

                if (!that.currentMusic) {
                    return 0;
                }
                sound = soundManager.getSoundById(that.currentMusic.id);
                if (sound) {
                    return that.calculateBuffer(
                        sound.bytesLoaded / sound.bytesTotal
                    );
                }
                return 0;
            };

            that.onStateChange = function (success) {
                var current = module.playerManager.getCurrentMusic();

                if (success !== undefined && success !== null && !success) {
                    module.coreLog('playDetailError', current);
                    module.s.notifyManager.notPlayable(current.title);
                    if (module.s.repeatControl.state
                            === module.s.repeatState.repeatOne ||
                            module.playerManager.forth() === -1) {
                        module.playerManager.stop(that.section);
                        module.$(module).trigger('playerStopped', that.section);
                    }
                }
                if (!that.playing) {
                    //that.onLoading();
                    that.playing = true;
                } else {
                    that.onPlaying();
                }
            };
        }
    };

    module = module || {};
    module.player = module.$.extend(module.player || {}, player);

    module.player.YoutubePlayer.prototype =
        new module.player.BaseExternalPlayer();
    module.player.SoundcloudPlayer.prototype =
        new module.player.BaseExternalPlayer();

    return module;
}($dbt));
