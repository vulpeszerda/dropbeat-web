/*jslint browser: true*/
var $dbt = (function (module) {
    'use strict';

    var credentialReq,
        uploadHostReq,
        uploadCredentialDeferreds = [];

    module.utils = module.utils || {};

    module.utils.gup = function (name, url) {
        var regexS, regex, results;
        if (!url) {
            url = location.href
        }
        name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
        regexS = '[\\?&]' + name + '=([^&#]*)';
        regex = new RegExp( regexS );
        results = regex.exec( url );
        return results == null ? null : results[1];
    };

    module.utils.isURL = function(text) {
        var urlRegex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i;
        return urlRegex.test(text);
    };

    module.utils.toHtml = function(text) {
        var urlRegex;

        urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&amp;@#\/%?=~_|!:,.;]*[-A-Z0-9+&amp;@#\/%=~_|])/ig;
        text = text.replace(urlRegex, '<a target=\'_blank\' href=\'$1\'>$1</a>');

        text = text.replace('\n', '<br/>').replace('\r', '<br/>');
        text = (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g,
            '$1'+ '<br/>' +'$2');
        return text;
    };

    module.utils.createCORSRequest = function(method, url) {
        var xhr = new XMLHttpRequest();
        if ("withCredentials" in xhr) {
        // XHR for Chrome/Firefox/Opera/Safari.
            if (method && url) {
                xhr.open(method, url, true);
            }
        } else if (typeof XDomainRequest !== "undefined") {
            // XDomainRequest for IE.
            xhr = new XDomainRequest();
            if (method && url) {
                xhr.open(method, url);
            }
        } else {
            // CORS not supported.
            xhr = null;
        }
        return xhr;
    };

    module.utils.makeId = function(length) {
        var text, possible, i;
        text = '',
        length = length || 5;
        possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for(i=0 ; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return text;
    };

    module.utils.uploadImage = function(host, key, file, type, publisher, callback) {
        var url = module.uploadApi.uploadImageUrl(host),
            formData = new FormData(),
            idx, ext, filename,
            imageUploadReq;

        if (file.name) {
            filename = file.name;
            idx = file.name.lastIndexOf('.');
            if (idx > -1 && idx < filename.length - 1) {
                ext = filename.substring(idx + 1, filename.length).toLowerCase();
            }
        } else if (type === 'w') {
            ext = 'json';
        } else {
            ext = 'jpg';
        }

        if (ext === 'jpeg') {
            ext = 'jpg';
        }

        formData.append('ext', ext);
        formData.append('type', type);
        formData.append('content', file);

        publisher && publisher(-1);

        imageUploadReq = module.$.ajax({
            xhr: function() {
                var x = module.utils.createCORSRequest('post', url);
                if (!x) {
                    return null;
                }
                x.upload.addEventListener("progress", function(e) {
                    if (e.lengthComputable) {
                        var percentage;
                        percentage = Math.min(Math.round(
                            (e.loaded * 100) / file.size), 100);
                        publisher && publisher(percentage);
                    }
                }, false);
                x.withCredentials = true;
                return x;
            },
            cache:false,
            url:url,
            type:'post',
            data: formData,
            dataType:'text/html',
            contentType:false,
            processData: false,
            crossDomain: true,
            xhrFields: {
                withCredentials: true
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                xhr.setRequestHeader('dbt_key', key);
            }
        }).done(function(res) {
            if (!res) {
                callback && callback(false);
                return;
            }
            callback && callback(true, res);
        }).fail(function(jqXHR, textStatus, error) {
            if (jqXHR.status === 201) {
                callback && callback(true, jqXHR.responseText);
                return;
            }
            callback && callback(false, textStatus);
        });
        return imageUploadReq;
    };

    module.utils.commaNumber = function (val){
        while (/(\d+)(\d{3})/.test(val.toString())){
            val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
        }
        return val;
    };

    module.utils.extractFilename = function(s) {
        return (typeof s==='string' && (s=s.match(/[^\\\/]+$/)) && s[0]) || '';
    };

    module.utils.extractExt = function(s) {
        var idx = -1;
        idx = s.lastIndexOf('.');
        if (idx === -1) {
            return null;
        }
        return s.substring(idx + 1, s.length);
    };

    module.utils.downsizeArray = function(array, size) {
        var i, len, barIdx, bars = [], agg, count,
            prevBarIdx, maxValue;

        len = array.length;
        agg = 0;
        count = 0;
        prevBarIdx = 0;

        for (i=0; i <  len; i++) {
            barIdx = Math.floor(i * size / len);
            if (prevBarIdx !== barIdx) {
                bars.push(agg / count);
                agg = 0;
                count = 0;
                if (i === len) {
                    break;
                }
            }
            prevBarIdx = barIdx;
            agg += Math.abs(array[i]);
            count += 1;
        }

        // Normalize
        maxValue = Math.max.apply(null, bars);
        module.$.each(bars, function(idx, val) {
            bars[idx] = Math.round(val * 100 / maxValue);
        });

        return bars;
    };

    module.utils.toShortNumber = function(num) {
        if (num >= 1000000000) {
            return (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'G';
        }
        if (num >= 1000000) {
            return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
        }
        if (num >= 1000) {
            return (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
        }
        return num;
    }

    module.utils.toCommaNumber = function(numb) {
        return numb.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    module.utils.canvasToBlob = function(c) {
        var deferred = module.$.Deferred(),
            quality = 0.8;

        if (c.toBlob) {
            c.toBlob(function(blob) {
                if (!blob) {
                    deferred.reject();
                    return;
                }
                deferred.resolve(blob);
            }, 'image/jpeg', quality);
        } else {
            deferred.reject('not_supported');
        }
        return deferred;
    };

    module.utils.getUploadCredential = function() {
        var deferred = module.$.Deferred(),
            deferreds = [];

        uploadCredentialDeferreds.push(deferred);

        if (credentialReq || uploadHostReq) {
            return deferred;
        }

        credentialReq = module.$.get(module.api.uploadCredentialUrl)
            .then(function() {
                credentialReq = null;
                return arguments;
            });
        deferreds.push(credentialReq);


        uploadHostReq = module.$.get(module.api.metaUploadHostUrl)
            .then(function() {
                uploadHostReq = null;
                return arguments;
            });
        deferreds.push(uploadHostReq);


        module.$.when.apply(this, deferreds)
            .done(function(credentialResult, uploadHostResult) {
                var credentialRes = credentialResult[0],
                    uploadHostRes = uploadHostResult[0],
                    credentialKey, uploadHost;

                if (!credentialRes || 
                        !credentialRes.success || !credentialRes.data) {
                    credentialDeferred.reject(
                        module.string('failed_to_authenticate'));
                    return;
                }
                if (!uploadHostRes || 
                        !uploadHostRes.success || !uploadHostRes.host) {
                    credentialDeferred.reject(
                        module.string('failed_to_authenticate'));
                    return;
                }
                credentialKey = credentialRes.data;
                uploadHost = uploadHostRes.host;

                module.$.each(uploadCredentialDeferreds, function(idx, d) {
                    d.resolve(credentialKey, uploadHost);
                });

            })
            .fail(function(jqXHR, textStatus, error) {
                module.$.each(uploadCredentialDeferreds, function(idx, d) {
                    d.reject(textStatus !== 'abort' ? 
                        module.string('failed_to_authenticate') : 'canceled');
                });
            })
            .always(function() {
                uploadCredentialDeferreds.length = 0;
            });
        return deferred;
    };

    return module;
}($dbt));
