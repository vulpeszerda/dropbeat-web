/*jslint browser: true*/
/*global $*/
var $dbt = (function (module) {
    'use strict';

    module = module || {};

    function PlayerManager() {
        var that = this;

        that.players = {
            objs: {},
            get: function (k) {
                var that = this,
                    scIdx = k.indexOf('soundcloud'),
                    ytIdx = k.indexOf('youtube'),
                    dbIdx = k.indexOf('dropbeat'),
                    spIdx = k.indexOf('spotify'),
                    pdIdx = k.indexOf('podcast'),
                    player;

                if (scIdx !== -1) {
                    that.objs.basesoundcloud.section = k.slice(0, scIdx);
                    return that.objs.basesoundcloud;
                }
                player = that.objs[k];
                if (!player) {
                    player = that.objs.baseyoutube;
                }
                if (ytIdx > -1 && player.section !== k.slice(0, ytIdx)) {
                    player.section = k.slice(0, ytIdx);
                }
                if (dbIdx > -1 && player.section !== k.slice(0, dbIdx)) {
                    player.section = k.slice(0, dbIdx);
                }
                if (spIdx > -1 && player.section !== k.slice(0, spIdx)) {
                    player.section = k.slice(0, spIdx);
                }
                if (pdIdx > -1 && player.section !== k.slice(0, pdIdx)) {
                    player.section = k.slice(0, pdIdx);
                }
                return player;
            },
            set: function (k, v) {
                var that = this;

                that.objs[k] = v;
            },
            pop: function (k) {
                var that = this,
                    v = that.objs[k];

                delete that.objs[k];
                return v;
            }
        };

        that.playing = false;
        that.moving = false;
        that.currentPlayer = null;
        that.currentMusic = null;
        that.currentMusicLength = 0;
        that.lastPlayedMusicInfo = null;

        that.elems = {
            loadingFilter: '.player-section .player-initialize-filter'
        };

        that.init = function (callback) {
            var baseYtPlayer,
                baseScPlayer;

            function onPlayerReady(e) {
                var isAllReady = true;
                module.$.each(that.players.objs, function(key, player) {
                    if (!player.isReady) {
                        isAllReady = false;
                        return false;
                    }
                });
                if (isAllReady) {
                    module.state.dropbeatReady = true;
                    module.$(that.elems.loadingFilter).fadeOut({
                        duration: 300,
                        queue: false
                    });
                    module.$(module).trigger('player.init.end');
                    callback && callback();
                }
            }


            if (module.$(that.elems.loadingFilter).is(':hidden')) {
                module.$(that.elems.loadingFilter).show();
            }
            module.$(module).trigger('player.init.start');
            if (!module.state.youtubeApiReady && !module.context.isMobile) {
                module.$(module).one('youtubeApiReady', function() {
                    that.init(callback);
                });
                return;
            }

            if (!module.context.isMobile && module.$('#youtube-player').size() > 0) {
                baseYtPlayer = module.player.create('youtube', 'base');
                that.players.set('baseyoutube', baseYtPlayer);
            }

            baseScPlayer = module.player.create('soundcloud', 'base');
            that.players.set('basesoundcloud', baseScPlayer);
            onPlayerReady();

            module.$(module).on(
                'playerReady.base.youtube playerReady.base.soundcloud',
                module.$.proxy(onPlayerReady, this));

            module.$(module).on('playMusic', function(e, music, section) {
                if (!music || !music.id ||
                        (that.lastPlayedMusicInfo &&
                        that.lastPlayedMusicInfo.section === section &&
                        that.lastPlayedMusicInfo.music.id === music.id)) {
                    return;
                }
                that.lastPlayedMusicInfo = {
                    music: music,
                    section: section
                }
                // triggered only one per music
                module.$(module).trigger('playMusicFirst', [music, section]);
            });

            module.$(module).on('mayPlayInfoUpdate', function(e, music, section) {
                if (!music || !music.id ||
                        (that.lastUpdatedMusicInfo &&
                        that.lastUpdatedMusicInfo.section === section &&
                        that.lastUpdatedMusicInfo.music.id === music.id)) {
                    return;
                }
                that.lastUpdatedMusicInfo = {
                    music: music,
                    section: section
                }
                // triggered only one per music
                module.$(module).trigger('playInfoUpdate', [music, section]);
            });


            module.$(module).on('loadingMusic finishedMusic', function() {
                that.lastPlayedMusicInfo = null;
                that.lastUpdatedMusicInfo = null;
            });
        };

        that.play = function (music, section) {
            var musicType;

            if (!music) {
                if (that.currentPlayer) {
                    that.currentPlayer.play(null);
                } else {
                    throw 'UndefinedError';
                }
            } else if (music.type !== 'dropbeat' && module.isKorean(music.title)) {
                module.s.notify(module.string('korean_cannot_be_played'), 'error');
                that.currentMusic = music;
                if (section === 'base') {
                    that.forth();
                }
                return;
            } else {
                if (that.currentPlayer && !that.playing) {
                    if (that.isSameMusic(music) &&
                            that.currentPlayer.section === section) {
                        that.currentPlayer.play();
                        that.playing = true;
                        return;
                    }


                    if (that.currentPlayer.type !== (section + music.type) ||
                            that.currentPlayer.section !== section) {
                        musicType = music.type;
// Streaming from external souce with url, like podcast, spotify
// use soundmnager2 player. so we pretend it's music type is soundcloud
                        if (['podcast', 'spotify', 'dropbeat']
                                .indexOf(musicType) > -1) {
                            musicType = 'soundcloud';
                        }
                        if (('youtube' === musicType && module.context.isMobile) || 
                                module.$('#youtube-player').size() === 0) {
                            musicType = 'soundcloud';
                        }
                        that.currentPlayer =
                            that.players.get(section + musicType);
                    }

                    that.stop(section);
                    that.currentMusic = music;

                    module.s.playerControl.toggle(music, section, true);
                    module.s.progressController.reset(section);

                    that.currentPlayer.play(music);
                    that.playing = true;

                    return;
                }

                if (that.currentPlayer) {
                    that.stop(section);
                }
                musicType = music.type;
                if (['podcast', 'spotify', 'dropbeat']
                        .indexOf(musicType) > -1) {
                    musicType = 'soundcloud';
                }
                if (('youtube' === musicType && module.context.isMobile) ||
                                module.$('#youtube-player').size() === 0) {
                    musicType = 'soundcloud';
                }
                that.currentPlayer = that.players.get(section + musicType);

                that.currentMusic = music;
                module.s.progressController.reset(section);

                if (!that.currentPlayer.initialized) {
                    that.currentPlayer.init(function () {
                        that.play(music);
                    });
                } else {
                    that.currentPlayer.play(music);
                }
            }
            that.playing = true;
        };

        that.pause = function () {
            if (that.currentPlayer && that.playing) {
                that.currentPlayer.pause();
            }
            that.playing = false;
        };

        that.stop = function (section) {
            that.playing = false;
            that.currentMusic = null;
            if (that.currentPlayer) {
                that.currentPlayer.stop();
                module.s.progressController.stopAll();
                if (!section) {
                    module.s.progressController.flushAll();
                } else {
                    module.s.progressController.flush(section);
                }
                module.s.playerControl.toggle(that.currentMusic,
                    that.currentPlayer.section, false);
            }
        };

        that.onPlayMusic = function (music, section, startPoint) {
            var isImmediatePlay = false;
            if (that.currentPlayer && that.playing) {
// Pause the music here.
                that.pause();
                module.s.playerControl.toggle(that.currentMusic,
                    that.currentPlayer.section, false);
                module.s.progressController.stop(that.currentPlayer.section);
                if (!music || (that.currentMusic &&
                        music.id === that.currentMusic.id)) {
                    return;
                }
            }

            if (section === 'base') {
                module.playlistManager.playingPlaylistId =
                        module.playlistManager.localKey +
                            module.s.playlistTabs.currentIdx();
            }

            if (!that.playing) {
                if (!music) {
                    if (!that.currentMusic) {
                        return;
                    }
                    music = that.currentMusic;
                }
                if (!section && that.currentPlayer) {
                    section = that.currentPlayer.section;
                }
                if (startPoint !== undefined) {
                    // HACK
                    // becuase seeking right after play not properly work on IE,
                    // so we mute player first when play and
                    // unmute when player state become play
                    module.$(module).one('playMusic', function(e, m, s) {
                        var playback;
                        if (m.id === music.id && section === s) {
                            playback = that.currentPlayer.getCurrentPlaybackTime();
                            if (startPoint !== 0) {
                                module.playerManager.seekTo(startPoint);
                            }
                        }
                        that.currentPlayer.unmute();
                    });
                    isImmediatePlay = that.currentPlayer && that.currentMusic &&
                                that.currentMusic.id === music.id &&
                                that.currentMusic.type === music.type;

                    if (isImmediatePlay) {
                        that.currentPlayer.mute();
                    }
                    that.play(music, section);
                    if (!isImmediatePlay && that.currentPlayer) {
                        that.currentPlayer.mute();
                    }
                    //module.playerManager.seekTo(startPoint);
                } else {
                    that.play(music, section);
                }

// Make prevNext clickable here again.
                module.s.playerBase.updateButton();
                module.s.playerBase.updatePlayButton(section !== 'drop');

                module.s.playerControl.toggle(music, section, true);
                module.s.progressController.start(section);
                return;
            }
        };

        that.onMusicClicked = function (music, section, options) {
            var playlist,
                currMusicIdx,
                q, idx, i, isImmediatePlay;

// For safari hack. (Do not play music before init!)
            if (!module.state.dropbeatReady) {
                return;
            }

            if (!music) {
                return;
            }

            if (options.playlistId) {
                playlist = module.playlistManager.getPlaylist(options.playlistId);
            }

// Music queue should be ordered.
            if (playlist) {
                module.playlistManager.playingPlaylistId = playlist.id;
                if (module.s.shuffleControl.isShuffle()) {
                    q = playlist.raw().slice(0);
                    module.s.shuffleControl.shuffle(q);
                    idx = -1;

                    for (i = 0; i < q.length; i += 1) {
                        if (q[i].id === music.id) {
                            idx = i;
                        }
                    }

                    module.s.musicQ.init(q, idx);
                } else {
                    currMusicIdx = playlist.findIdx(music.id);
                    module.s.musicQ.init(playlist.playlist, currMusicIdx);
                }
            } else {
                if (module.playlistManager) {
                    module.playlistManager.playingPlaylistId = null;
                }
                module.s.musicQ.init();
            }

            that.onPlayMusic(music, section, options.startPoint);

// In the case that another music is already playing.
            if (!that.playing) {
                if (options.startPoint !== undefined) {
                    // HACK
                    // becuase seeking right after play not properly work on IE,
                    // so we mute player first when play and
                    // unmute when player state become play
                    module.$(module).one('playMusic', function(e, m, s) {
                        var playback;
                        if (m.id === music.id && section === s) {
                            playback = that.currentPlayer.getCurrentPlaybackTime();
                            if (options.startPoint !== 0) {
                                module.playerManager.seekTo(options.startPoint);
                            }
                        }
                        that.currentPlayer.unmute();
                    });
                    isImmediatePlay = that.currentPlayer && that.currentMusic &&
                                that.currentMusic.id === music.id &&
                                that.currentMusic.type === music.type;

                    if (isImmediatePlay) {
                        that.currentPlayer.mute();
                    }
                    that.play(music, section);
                    if (!isImmediatePlay && that.currentPlayer) {
                        that.currentPlayer.mute();
                    }
                    //module.playerManager.seekTo(options.startPoint);
                } else {
                    that.play(music, section);
                }
                module.s.playerControl.toggle(music, section, true);
                module.s.progressController.start(section);
            }
        };

        that.back = function () {
            return that.move(false);
        };

        that.forth = function () {
            return that.move(true);
        };

        that.move = function (forward) {
            var music = module.s.musicQ.walk(forward, true);
            if (!module.state.dropbeatReady || !music) {
                return -1;
            }
            if (!that.currentPlayer) {
                return -1;
            }
            that.onPlayMusic(music, that.currentPlayer.section);
        };

        that.getCurrentPlaybackTime = function () {
            if (that.currentPlayer && that.playing) {
                return that.currentPlayer.getCurrentPlaybackTime();
            }
            return 0;
        };

        that.getTotalPlaybackTime = function () {
            if (that.currentPlayer && that.playing) {
                return that.currentPlayer.getTotalPlaybackTime();
            }
            return 0;
        };

        that.getBuffer = function () {
            if (that.currentPlayer && that.playing) {
                return that.currentPlayer.getBuffer();
            }
            return 0;
        };

        that.seekTo = function (pos) {
            if (that.currentPlayer) {
                that.currentPlayer.seekTo(pos);
            }
        };

        that.getCurrentMusic = function () {
            return that.currentMusic;
        };

        that.getCurrentPlayer = function () {
            return that.currentPlayer;
        };

        that.getCurrentSection = function () {
            if (that.currentPlayer) {
                return that.currentPlayer.section;
            }
        };

        that.isSameMusic = function (music) {
            return that.currentMusic && music.id === that.currentMusic.id;
        };
    }

    module.playerManager = new PlayerManager();

    return module;
}($dbt));
