/*jslint browser: true*/
/*global $, alert*/
var $dbt = (function (module) {
    'use strict';

    var control;

    module = module || {};
    module.playlist = module.playlist || {};
    module.playlist.control = module.playlist.control || {};
    control = module.playlist.control;

    control.tabs = (function (){
        var view = {},
            selectedIdx = null;

        view.elems = {
            playlistManagerCloseBtn: '.my-playlists .close-button',
            playlists: '.my-playlists .playlists',
            addNewPlaylistBtn: '.create-new-playlist-button',
            playlistRowTmpl: '#tmpl-playlist-manage-row',
            managerView: '.my-playlists',
            myPlaylistBtn: '.my-playlist-button',
            sharePlaylistBtn: '.share-playlist-button',
            playlistFooter: '.playlist-section .playlist-footer'
        };

        view.init = function () {
            var that = this;

            this._playlistRowTmpl = module._.template(
                module.$(this.elems.playlistRowTmpl).html());
            this.$playlists = module.$(this.elems.playlists);
            this.$managerView = module.$(this.elems.managerView);

            module.$(this.elems.addNewPlaylistBtn).on('click',
                module.$.proxy(this.addNewPlaylist, this));
            this.$playlists.on('click', '.menu-btn',
                module.$.proxy(this.onMenuClicked, this));
            this.$playlists.on('click', '.apply-edit-button',
                module.$.proxy(this.onApplyEditClicked, this));
            this.$playlists.on('click', '.cancel-edit-button',
                module.$.proxy(this.onCancelEditClicked, this));
            this.$playlists.on('click', '.remove-menu',
                module.$.proxy(this.onRemoveClicked, this));
            this.$playlists.on('click', '.rename-menu',
                module.$.proxy(this.onRenameClicked, this));
            this.$playlists.on('click', '.share-menu',
                module.$.proxy(this.onShareClicked, this));
            this.$playlists.on('click', '.playlist-item-select',
                module.$.proxy(this.onPlaylistClicked, this));
            this.$playlists.on('submit', '.playlist-form', function(e) {
                var $playlist = module.$(e.currentTarget).closest('.playlist-item');
                $playlist.find('.apply-edit-button').click();
                return false;
            });

            module.$(this.elems.playlistManagerCloseBtn, this.$el)
                    .on('click', function() {
                that.hideManagerView();
            });

            module.$(this.elems.playlistFooter).on('click',
                    this.elems.myPlaylistBtn, function() {
                if (!module.context.account) {
                    module.view.NeedSigninView.show();
                    return;
                }
                that.showManagerView();
                module.log('playlist-manage', 'open-manager');
            });

            module.$(this.elems.playlistFooter).on('click',
                    this.elems.sharePlaylistBtn, function() {
                var playlist,
                    url;

                if (!module.context.account) {
                    module.view.NeedSigninView.show();
                    return;
                }

                playlist = module.playlistManager.getSelectedPlaylist();
                if (playlist.empty()) {
                    module.s.notifyManager.sharePlaylist(false);
                    return;
                }

                module.$.ajax({
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: module.api.playlistSharedUrl,
                    data: JSON.stringify({
                        'name' : playlist.name,
                        'data' : playlist.toUploadablePlaylist()
                    })
                }).done(function(resp) {
                    var obj = resp.obj;
                    if (obj && obj.uid && resp.success) {
                        url = module.host + '/?playlist=' + obj.uid;
                        module.view.SharePlaylistView.show(playlist, url);
                        module.log('playlist-manage', 'share', obj.uid);
                    } else {
                        module.s.notify('Failed to create playlist', 'error');
                    }

                }).fail(function(jqXHR, textStatus) {
                    module.s.notify('Failed to create playlist', 'error');
                });
            });
        };

        view.showManagerView = function() {
            this.$managerView.show();
        };

        view.hideManagerView = function() {
            this.$managerView.hide();
        };

        view.renderAll = function() {
            var that = this,
                playlistsHtml = '',
                playlists = module.playlistManager.getAll() || [];

            module.$.each(playlists, function(idx, playlist) {
                playlistsHtml += that.renderRow(playlist, false);
            });
            this.$playlists.html(playlistsHtml);
        };

        view.setSelected = function(seqId) {
            selectedIdx = seqId;
        };

        view.renderRow = function(playlist, isEditMode) {
            var isSelected = playlist && playlist.seqId === selectedIdx;

            return this._playlistRowTmpl({
                playlist:playlist,
                isEditMode: isEditMode,
                isSelected: isSelected
            });
        };

        view.currentIdx = function () {
            var playlists = module.playlistManager.getAll() || [],
                foundIdx;
            if (selectedIdx) {
                module.$.each(playlists, function(idx, playlist) {
                    if (selectedIdx === playlist.seqId) {
                        foundIdx = idx;
                        return false;
                    }
                });
                if (foundIdx) {
                    return foundIdx;
                }
            }
            return 0;
        };

        view.currentPlaylistId = function() {
            return module.playlistManager.localKey +
                module.s.playlistTabs.currentIdx();
        };

        view.addNewPlaylist = function(e) {
            var $newPlaylist;

            if (!module.context.account) {
                return;
            }
            $newPlaylist = module.$('.playlist[data-playlist-id=\'-1\']',
                this.$playlists);
            if ($newPlaylist.size() === 0) {
                $newPlaylist = module.$(this.renderRow(null, true));
                this.$playlists.prepend($newPlaylist);
            }
            setTimeout(function() {
                module.$('.title-input', $newPlaylist).focus();
            }, 200);
        };

        view.onMenuClicked = function(e) {
            var $target = module.$(e.currentTarget),
                $playlist = $target.closest('.playlist-item'),
                showableMaxY,
                maxOffset,
                $wrapper,
                $menus;

            $wrapper = module.$('.playlists-wrapper');
            $menus = $playlist.find('.nonedit-mode-view .menus');
            maxOffset = $menus.height() + $playlist.height() +
                $playlist.offset().top;
            showableMaxY = $wrapper.offset().top  + $wrapper.height();

            if (showableMaxY < maxOffset) {
                $menus.addClass('upside');
            } else {
                $menus.removeClass('upside');
            }

            module.$('.playlist-item.target').removeClass('target');
            $playlist.addClass('target');
            module.$('.overlay-filter').one('click', function() {
                $playlist.removeClass('target');
            }).show();
        };

        view.onShareClicked = function(e) {

        };

        view.onApplyEditClicked = function(e) {
            var that = this,
                $playlist = module.$(e.currentTarget).closest('.playlist-item'),
                title = $playlist.find('.title-input').val(),
                seqId = parseInt($playlist.data('playlistId')),
                cb;

            if (title === '') {
                return;
            }

            if (title.length > 50) {
                module.s.notifyManager.longPlaylistName();
                return;
            }

            cb = function() {
                module.playlistManager.loadPlaylists(function() {
                    that.renderAll();
// Because changed playlist name should be applied.
                    module.playlistManager.updateCurrentPlaylistView();
                });
            };

            if (seqId > -1) {
                module.s.storage.renamePlaylist(seqId, title, cb);
                module.log('playlist-manage', 'playlist-rename');
            } else {
                module.s.storage.createPlaylist(title, cb);
                module.log('playlist-manage', 'playlist-create');
            }

            $playlist.find('.title').text(title);
            $playlist.removeClass('edit-mode');
            $playlist.addClass('temp');
        };

        view.onCancelEditClicked = function(e) {
            var $playlist = module.$(e.currentTarget).closest('.playlist-item'),
                title = $playlist.find('.title').text();
            $playlist.find('.title-input').val(title);
            if (parseInt($playlist.data('playlistId')) > -1) {
                $playlist.removeClass('edit-mode');
            } else {
                $playlist.remove();
            }
        };

        view.onRenameClicked = function(e) {
            var $playlist = module.$(e.currentTarget).closest('.playlist-item');
            $playlist.addClass('edit-mode');
            $playlist.find('.title-input').trigger('focus');
            module.$('.overlay-filter').click();
        };

        view.onRemoveClicked = function(e) {
            var that = this,
                playlists = module.playlistManager.getAll() || [],
                $playlist = module.$(e.currentTarget).closest('.playlist-item'),
                seqId = parseInt($playlist.data('playlistId')),
                currSelectedSeqId = selectedIdx,
                playlistName = $playlist.find('.title').text(),
                cb;


            if (playlists.length > 1) {
                cb = function(confirmed) {
                    module.$('.overlay-filter').click();
                    if (!confirmed) {
                        return;
                    }
                    module.s.storage.deletePlaylist(seqId, function() {
                        module.playlistManager.loadPlaylists(function(playlists) {
                            if (playlists.length > 0 &&
                                    seqId === currSelectedSeqId) {
                                selectedIdx = playlists[0].seqId;
                                module.playlistManager.loadLocal(0, true);
                            }
                            that.renderAll();
                        });
                    });
                    $playlist.addClass('temp');
                    module.log('playlist-manage', 'playlist-remove');
                };
                module.view.ConfirmView.show({
                    title: module.string('are_you_sure'),
                    text: module.string('are_you_sure_delete_playlist_%s', playlistName),
                    confirmText: module.string('delete')
                }, cb);
            } else {
                module.s.notify(module.string('at_least_one_playlist'), 'error');
            }
        };

        view.onPlaylistClicked = function(e) {
            var $playlist = module.$(e.currentTarget).closest('.playlist-item'),
                seqId = parseInt($playlist.data('playlistId')),
                idx = $playlist.index();
            if ($playlist.hasClass('temp') && seqId > -1) {
                return;
            }
            selectedIdx = seqId;
            module.playlistManager.loadLocal(idx, true);
            module.$.cookie('last_playlist_id', selectedIdx, {expires: 9999});
            this.$managerView.hide();
            this.renderAll();
            module.log('playlist-manage', 'playlist-switch');
        };

        return view;
    }());

    control.base = {
        elems: {
            playlistFilterInput: '.playlist-section #search-playlist-input'
        },

        init: function () {
            var that = this;
            module.$(that.elems.playlistFilterInput).bind(
                'propertychange keyup input paste',
                function (event) {
                    clearTimeout(that.filterTimer);
                    that.filterTimer = setTimeout(function () {
                        that.filter(
                            module.$(that.elems.playlistFilterInput).
                                val().toLowerCase()
                        );
                    }, 800);
                }
            );
        },

        filterTimer: null,

        filter: function (keyword) {
            var temp = new module.s.Playlist({
                    name: 'filter result :' + keyword
                }),
                current = module.playlistManager.getSelectedPlaylist(),
                i,
                m;

            if (keyword) {
                for (i = 0; i < current.length(); i += 1) {
                    m = current.getWithIdx(i);
                    if (m.title.toLowerCase().indexOf(keyword) !== -1) {
                        temp.raw().push(m);
                    }
                }
                temp.toTable(true, true);
            } else {
                module.playlistManager.getSelectedPlaylist().toTable(true);
            }
        }
    };

    return module;
}($dbt));
