/*jslint browser: true*/
/*global $*/
var $dbt = (function (module) {
    'use strict';

    module = module || {};

    function PlaylistManager() {
        var that = this;

// This `playlists` object has `k,v` of `playlist name, playlist.
        that.publicPlaylists = {};
        that.privatePlaylists = {};
        that.localKey = 'local';
        that.shareKey = 'share';
        that.publicKey = 'public';
        that.playingPlaylistId = null;
        that.musicStorage = {};

        that.elems = {
            playlistName: '.selected-playlist .playlist-name',
            playlist: ".playlist",
            playlistInner: ".playlist-section .playlist .playlist-inner",
            playlistRowTemplate: "#tmpl-playlist-row",
            playlistMusicContainer: ".a-playlist-music",
            musicPlayBtn: ".music-title-wrapper",
            musicDotMenuBtn: ".music-dot-menu",
            musicShareBtn: ".music-share",
            playlistMusicCount:
                ".playlist-section .playlist-footer .music-count",
            musicTitleScroller: ".music-title-scroll-wrapper",
            musicTitle: ".music-title",
            musicContainer: ".a-playlist-music"
        };

        that.init = function () {
            module.$(that.elems.playlistInner).sortable({
                axis: 'y'
            }).disableSelection();

            that.delegateTriggers();

            if (module.context.account) {
                that.loadPlaylists(function(playlists) {
                    var tabsView = module.playlist.control.tabs,
                        lastPlaylistId = parseInt(module.$.cookie('last_playlist_id')),
                        lastPlaylistIdx = 0;

                    if (playlists.length > 0) {
                        if (lastPlaylistId) {
                            module.$.each(playlists, function(idx, playlist) {
                                if (lastPlaylistId === playlist.seqId) {
                                    lastPlaylistIdx = idx;
                                    return false;
                                }
                            });
                        }
                        that.loadLocal(lastPlaylistIdx);
                        tabsView.setSelected(playlists[lastPlaylistIdx].seqId);
                        tabsView.renderAll();
                    }
                });
            } else {
                that.loadInitialPlaylist(function() {
                    that.loadLocal(0);
                    module.playlist.control.tabs.renderAll();
                });
            }

        };

        that.getAll = function() {
            var that = this,
                keys = Object.keys(this.privatePlaylists),
                playlists = [];
            keys.sort(function(lhs, rhs) {
                var lIdx = parseInt(lhs.replace(that.localKey, '')) || 0,
                    rIdx = parseInt(rhs.replace(that.localKey, '')) || 0;
                return lIdx < rIdx ? -1 : lIdx === rIdx ? 0 : 1;
            });
            module.$.each(keys, function(idx, key) {
                playlists.push(that.privatePlaylists[key]);
            });
            return playlists;
        };

        that.remove = function (key) {
            delete that.privatePlaylists[key];
        };

        that.loadPlaylists = function(cb) {
            module.s.storage.getPlaylistList(function (playlists) {
                that.privatePlaylists = {};
                module.$.each(playlists, function(idx, playlist) {
                    playlist.id = that.localKey + idx;
                    that.privatePlaylists[playlist.id] = playlist;
                });
                cb && cb(playlists);
            });
        };

        that.loadInitialPlaylist = function(cb) {
            module.$.ajax({
                url: module.api.initialPlaylistUrl,
                data: {
                    t: module.context.isIOS ? 's' : 'y'
                },
                dataType: 'json',
                contentType: 'application/json',
                type: 'get'
            }).done(function(resp) {
                var idx = 0,
                    playlist,
                    playlistObj,
                    tracks = [];

                if (!resp.success || !resp.playlist) {
                    return;
                }
                playlistObj = resp.playlist;

                if (playlistObj.data) {
                    module.$.each(playlistObj.data, function(idx, music) {
                        tracks.push(new module.s.Music(music));
                    });
                }

                playlist = new module.s.Playlist({
                    id: that.localKey + idx,
                    name: playlistObj.name,
                    tracks: tracks,
                    seqId: playlistObj.id,
                    isInitialPlaylist: true
                });
                that.privatePlaylists[playlist.id] = playlist;
                cb && cb([playlist]);
            }).fail(function() {
                module.s.notify('Failed to load playlists', 'error');
            });
        };

        that.loadSharedPlaylist = function(key, cb) {
            module.$.ajax({
                type: 'get',
                dataType: 'json',
                contentType: 'application/json',
                url: module.api.playlistSharedUrl,
                data: {
                    uid: key
                }
            }).done(function(resp) {
                var playlistObj, playlist, tracks = [];

                if (!resp || !resp.playlist) {
                    cb && cb(false, resp);
                    return;
                }
                playlistObj = resp.playlist;

                if (playlistObj.data) {
                    module.$.each(playlistObj.data, function(idx, music) {
                        var m;
                        if (music.type === 'dropbeat') {
                            m = new module.music.DropbeatMusic(music);
                        } else {
                            m = new module.music.Music(music);
                        }
                        tracks.push(m);
                    });
                }

                playlist = new module.s.Playlist({
                    id: that.shareKey + key,
                    name: playlistObj.name,
                    tracks: tracks,
                    seqId: playlistObj.id
                });
                playlist.sharedKey = playlistObj.uid;
                playlist.addable = false;

                cb && cb(true, [playlist]);
            }).fail(function(jqXHR, textStatus) {
                cb && cb(false);
            });
        };

        that.loadLocal = function (idx, trigger) {
            that.updatePlaylistView(that.localKey + idx, trigger);
        };

        that.updatePlaylistView = function (key, trigger) {
            var playlist = that.privatePlaylists[key];

            if (!playlist) {
                playlist = that.getSelectedPlaylist();
            }
            if (playlist) {
                module.$(that.elems.playlistName).text(playlist.name);
// Initial playlist dont have to reload from server
                if (playlist.isInitialPlaylist) {
                    playlist.toTable(true);
                    if (trigger) {
                        module.$(module).trigger('playlistChanged', {
                            title: module.string('playlist_changed'),
                            desc: playlist.name
                        });
                    }
                    return;
                }
                playlist.showLoading();
                playlist.loadFromRemote(function(success) {
                    playlist.hideLoading();
                    if (!success) {
                        playlist.addable = false;
                        playlist.showError();
                        return;
                    }
                    playlist.toTable(true);
                    if (trigger) {
                        module.$(module).trigger('playlistChanged', {
                            title: module.string('playlist_changed'),
                            desc: playlist.name
                        });
                    }
                });
            } else {
                module.$(that.elems.playlistName).text('Default playlist');
                module.$(that.elems.playlistInner).html('');
            }
        };

        that.updateCurrentPlaylistView = function () {
            that.updatePlaylistView(module.s.playlistTabs.currentIdx());
        };

        that.getSelectedPlaylist = function () {
            var idx = module.s.playlistTabs.currentIdx() || 0;
            return that.privatePlaylists[that.localKey + idx];
        };

        that.getPlaylist = function(id) {
            if (id.startsWith(that.localKey)) {
                return that.privatePlaylists[id];
            }
            return that.publicPlaylists[id];
        };

        that.clearMusicStorage = function() {
            this.musicStorage = {};
        };

        that.setMusicToStorage = function(key, music) {
            this.musicStorage[key] = music;
        };

        that.getMusicFromStorage = function(key) {
            return this.musicStorage[key];
        }

        that.delegateTriggers = function () {
            module.$(that.elems.playlistInner).on(
                "sortupdate",
                function(e, ui) {
                    var manager,
                        currentPlaylist,
                        $track,
                        playlistTracks,
                        currMusic,
                        currMusicIdx,
                        playerManager = module.playerManager,
                        q, idx, i;

                    manager = module.playlistManager;
                    currentPlaylist = manager.getSelectedPlaylist();

                    if (!currentPlaylist) {
                        return;
                    }
                    if (!module.$(that.elems.playlistInner).hasClass('sortable') ||
                            !module.context.account) {
                        currentPlaylist.toTable(true);
                        return;
                    }

                    playlistTracks = currentPlaylist.raw();
                    playlistTracks.length = 0;

                    $track = module.$('.a-playlist-music', module.$(this));
                    $track.each(function(idx, el) {
                        var $el = module.$(this),
                            musicData,
                            musicStorageKey;

                        musicStorageKey = $el.data('musicStorageKey');
                        if (!musicStorageKey) {
                            return true;
                        }
                        musicData = that.getMusicFromStorage(musicStorageKey);
                        if (!musicData) {
                            return;
                        }

                        playlistTracks.push(musicData);
                    });
                    currentPlaylist.toTable(true);

                    currMusic =  playerManager.currentMusic;

                    if (manager.playingPlaylistId === currentPlaylist.id &&
                            currMusic && currMusic.id) {

                        currMusicIdx = currentPlaylist.findIdx(currMusic.id);
                        if (currMusicIdx === -1) {
                            // dont need to handle this case
                            module.s.musicQ.init();
                        } else if (module.s.shuffleControl.isShuffle()) {
                            q = currentPlaylist.raw().slice(0);
                            module.s.shuffleControl.shuffle(q);
                            idx = -1;

                            for (i = 0; i < q.length; i += 1) {
                                if (q[i].id === currMusic.id) {
                                    idx = i;
                                    break;
                                }
                            }
                            module.s.musicQ.init(q, idx);
                        } else if (currMusicIdx !== -1) {
                            module.s.musicQ.init(
                                currentPlaylist.raw(), currMusicIdx);
                        }

                        module.playlist.boldTitle(currMusic.id);
                        module.s.playerControl.base.updateButton();
                    }
                    currentPlaylist.remoteSync();
                });

            module.$(that.elems.playlistInner).on(
                'click',
                that.elems.musicPlayBtn,
                function () {
                    var self = this,
                        $musicContainer,
                        musicStorageKey,
                        musicData,
                        playlist;

                    $musicContainer = module.$(self).parents(that.elems.musicContainer);
                    musicStorageKey = $musicContainer.data('musicStorageKey');

                    if (!musicStorageKey) {
                        return;
                    }
                    musicData = that.getMusicFromStorage(musicStorageKey);
                    if (!musicData) {
                        return;
                    }

                    playlist = module.playlistManager.getSelectedPlaylist();

                    module.playerManager.onMusicClicked(
                        musicData, 'base',
                        { playlistId: playlist.id });
                }
            );


            module.$(that.elems.playlistInner).on(
                'click',
                that.elems.musicDotMenuBtn,
                function (e) {
                    var $menu = module.$(e.currentTarget),
                        _excludes = $menu.data('exclude'),
                        excludes = [],
                        $musicContainer,
                        musicStorageKey,
                        musicData,
                        playlist = module.playlistManager.getSelectedPlaylist(),
                        success;

                    if (_excludes) {
                        excludes = _excludes.split(',');
                    }

                    $musicContainer = $menu.parents(that.elems.musicContainer);
                    musicStorageKey = $musicContainer.data('musicStorageKey');

                    if (!musicStorageKey) {
                        return;
                    }
                    musicData = that.getMusicFromStorage(musicStorageKey);
                    if (!musicData) {
                        return;
                    }

                    module.view.PopupMusicMenuView.show({
                        anchor: $menu,
                        excludes: excludes,
                        track: musicData,
                        onClose: function(action) {
                            var account = module.context.account;

                            if ('add' === action) {
                                return;
                            }

                            if ('share' === action) {
                                module.view.ShareSingleTrackView.show(musicData, "playlist");
                                return;
                            }
                            if (!account) {
                                module.view.NeedSigninView.show();
                                return;
                            }

                            if ('delete-from-playlist' === action) {
                                success = playlist.remove(new module.s.Music(musicData), true);
                                if (success) {
                                    module.playlistManager.getSelectedPlaylist().sync();
                                    module.log('playlist-manage', 'remove', musicData);
                                    module.$(module).trigger('playlistChanged', {
                                        title: module.string('track_removed_from'),
                                        desc: playlist.name
                                    });
                                }
                                return;
                            }

                            if ('like' === action) {
                                if (account.isLiked(musicData)) {
                                    account.dislike(musicData, function(success, msg) {
                                        var errorMsg;

                                        if (!success) {
                                            errorMsg = module.string('failed_to_update_like');
                                            module.s.notify(errorMsg, 'error');
                                            return;
                                        }
                                        module.notification.notify(module.string('track_disliked'), 'success');
                                    });
                                } else {
                                    account.like(musicData, function(success, msg) {
                                        var errorMsg;

                                        if (!success) {
                                            errorMsg = module.string('failed_to_update_like');
                                            module.s.notify(errorMsg, 'error');
                                            return;
                                        }
                                        module.s.notify(module.string('track_liked'), 'success');
                                    });
                                }
                                return;
                            }

                            if ('repost' === action) {
                                account.repost(musicData, function(success) {
                                    if (!success) {
                                        errorMsg = module.string('already_reposted');
                                        module.s.notify(errorMsg, 'error');
                                        return;
                                    }
                                    module.s.notify(module.string('reposted'), 'success');
                                });
                                return;
                            }
                        }
                    });
                    return false;
                }
            );

            module.$(that.elems.playlistInner).on(
                'click',
                that.elems.musicShareBtn,
                function () {
                    var self = this,
                        $musicContainer,
                        musicStorageKey,
                        musicData;

                    $musicContainer = module.$(self).parents(that.elems.musicContainer);
                    musicStorageKey = $musicContainer.data('musicStorageKey');

                    if (!musicStorageKey) {
                        return;
                    }
                    musicData = that.getMusicFromStorage(musicStorageKey);
                    if (!musicData) {
                        return;
                    }

                    module.view.ShareSingleTrackView.show(musicData, "playlist");
                }
            );

            module.$(that.elems.playlistInner).on(
                'mouseenter',
                that.elems.musicPlayBtn,
                function () {
                    var $marquee = module.$(this).find(that.elems.musicTitleScroller),
                        $title = module.$(this).find(that.elems.musicTitle),
                        speed,
                        animPeriod;

                    if ($title.width() <= $marquee.width()) {
                        return;
                    }

                    speed = 20;
                    animPeriod = speed * $title.width();
                    $title.css({left: 0});
                    $title.animate(
                        {left: -$title.width()},
                        animPeriod,
                        'linear',
                        function () {
                            $title.trigger('marquee');
                        }
                    );
                    $title.bind(
                        'marquee',
                        function () {
                            var contentsWidth = $title.width(),
                                frameWidth = $marquee.width(),
                                animPeriod = speed * (contentsWidth + frameWidth);
                            $title.css({left: frameWidth});
                            $title.animate(
                                {left: -contentsWidth},
                                animPeriod,
                                'linear',
                                function () {
                                    $title.trigger('marquee');
                                }
                            );
                        }
                    );
                }
            );

            module.$(that.elems.playlistInner).on(
                'mouseleave',
                that.elems.musicPlayBtn,
                function () {
                    var self = this,
                        $title = module.$(self).find('.music-title');
                    $title.unbind('marquee');
                    $title.clearQueue();
                    $title.stop();
                    $title.css({left: 0});
                }
            );

            module.$(module).on('playInfoUpdate loadingMusic', function(e, music, section) {
                var playlistId = that.localKey +
                        module.s.playlistTabs.currentIdx();

                if (section === 'base' &&
                        that.playingPlaylistId === playlistId) {
                    module.s.boldPlaylistTitle(music.id);
                } else {
                    module.s.unboldPlaylistTitle();
                }
            });
        };
    }

    module.playlistManager = new PlaylistManager();

    function ShareManager() {
        var that = this;

        function gup(name, url) {
            var regexS, regex, results;
            if (!url) {
                url = location.href
            }
            name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
            regexS = "[\\?&]"+name+"=([^&#]*)";
            regex = new RegExp( regexS );
            results = regex.exec( url );
            return results == null ? null : results[1];
        }

        that.loadedAsShared = false;

        that.keyFromUri = function (key) {
            var uri = location.search;
            if (!key) {
                return false;
            }
            return gup(key, uri) || false;
        };

        that.onSharedList = function () {
            return that.keyFromUri(module.constants.shareUriKey);
        };
    }

    module.shareManager = new ShareManager();

    return module;
}($dbt));
