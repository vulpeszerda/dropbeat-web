/*jslint browser: true*/
/*jslint nomen: true*/
/*global $*/

// Dropbeat

// Copyright (c) 2014 Park Il Su

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


var $dbt = (function (module) {
    'use strict';

    var module = module || {},
        beforeunloadCallbacks = {};

    module.$ = $.noConflict();
    module._ = _.noConflict();

    module.shortcuts = function () {
        module.s = {
// This object provides alias for deep-depth module reference.
// All properties defined in this object should access that long references
// in one depth.
// XXX: NOTE that only `playerManager` and `playlistManager` can be accessed
// directly from `$dbt`.
            Music: module.music.Music,
            musicQ: module.music.musicQueue,
            playerControl: module.player.control,
            playerButton: module.player.control.button,
            playerBase: module.player.control.base,
            repeatState: module.player.control.repeat.state,
            repeatSequence: module.player.control.repeat.sequence,
            repeatControl: module.player.control.repeat.control,
            shuffleState: module.player.control.shuffle.state,
            shuffleControl: module.player.control.shuffle.control,
            volumeControl: module.player.control.volume.control,
            progressController: module.progress.controller,
            notifyMessage: module.notification.message,
            notifyManager: module.notification.manager,
            notify: module.notification.notify
        };
    };

    module.initialize = function () {
        var autoPlay,
            url,
            deferreds = [],
            playerDeferred,
            resolveDeferred;

        autoPlay = module.utils.gup('autoplay') === 'true';
        url = module.utils.gup('url');

        resolveDeferred = module.resolve(url);
        playerDeferred = module.$.Deferred();

        deferreds.push(resolveDeferred);
        deferreds.push(playerDeferred);

        module.playerManager.init(function() {
            playerDeferred.resolve();
        });

        module.s.playerControl.init();
        module.s.progressController.init('base');

        module.$.when.apply(this, deferreds)
            .done(function(track) {
                $dbt.view.setPage(new module.view.PlayerView({
                    track: track,
                    trackLink: url,
                    autoPlay: autoPlay
                }));
            })
            .fail(function() {
                module.s.notify(module.string('player_resolve_failure'), 'error');
            });
    };

    module.resolveSoundcloud = function(track) {
        var deferred = module.$.Deferred(),
            url = 'http://api.soundcloud.com/tracks/' + track.id,
            waveformUrl = null;

        module.$.ajax({
            url: url,
            data: {
                client_id: $dbt.clientKeys['soundcloud']
            },
            dataType:'json',
            crossDomain: true
        }).then(function(res) {
            track.duration = Math.round(res.duration / 1000);
            track.track_name = res.title;
            track.artwork = res.artwork_url || res.user.avatar_url;
            if (module.context.isMobile) {
                track.artwork = track.artwork.replace('large.jpg', 't300x300.jpg');
            } else {
                track.artwork = track.artwork.replace('large.jpg', 't500x500.jpg');
            }
            if (!res.waveform_url) {
                deferred.resolve(track);
                return;
            }

            waveformUrl = res.waveform_url;
            
            return module.$.ajax({
                url: 'http://www.waveformjs.org/w',
                dataType:'jsonp',
                data: {
                    url: res.waveform_url
                }
            });
        }).then(function(res) {
            track.waveform_data = res;
            track.waveform_url = waveformUrl;
            deferred.resolve(track);
        }).fail(function() {
            deferred.resolve(track);
        });

        return deferred;
    };

    module.resolve = function(url) {
        var req,
            deferred = module.$.Deferred(),
            uid = module.utils.gup('track', url);

        if (uid) {
            req = module.$.ajax({
                url:module.api.trackShareUrl,
                data: {
                    uid:uid
                },
                dataType:'json'
            }).done(function(res) {
                var track;

                if (!res || !res.success || !res.data) {
                    deferred.reject();
                    return;
                }

                track = new module.music.Music({
                    title: res.data.track_name,
                    type: res.data.type,
                    id: res.data.ref
                });
                if (track.type === 'soundcloud') {
                    module.resolveSoundcloud(track).always(function() {
                        deferred.resolve(track);
                    });
                    return;
                }
                deferred.resolve(track);
            })
        } else {
            req = module.$.ajax({
                url:module.api.resolveObjectUrl,
                data: {
                    url:url
                },
                dataType:'json',
                cache:false
            }).done(function(res) {
                var track;

                if (!res.success || !res.data) {
                    deferred.reject();
                    return;
                }

                track = new module.music.DropbeatMusic(res.data);
                deferred.resolve(track);
            })
        }

        req.fail(function(jqXHR, textStatus, error) {
            if (textStatus === 'abort') { 
                return;
            }
            deferred.reject();
        });

        return deferred;
    };

    module.state = {
        youtubeApiReady: false,
        soundManagerReady: false,
        dropbeatReady: false
    };

    module.context = (function() {
        function browserVersion() {
            var ua= navigator.userAgent, tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem =  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'MSIE '+ (tem[1] || '');
            }
            if(M[1] === 'Chrome'){
                tem = ua.match(/\bOPR\/(\d+)/)
                if(tem) {
                    return 'Opera ' + tem[1];
                }
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem = ua.match(/version\/(\d+)/i))) {
                M.splice(1, 1, tem[1]);
            }
            return M.join(' ');
        }

        function getBrowser() {
            var v,
                browser,
                version;
            v = browserVersion().split(' ');
            browser = v[0];
            version = v[1];
            if (browser === 'MSIE') {
                version = Number(version);
                if (version < 9) {
                    return {
                        support: false,
                        currentBrowser: v
                    };
                }
            }
            return {
                support: true,
                currentBrowser:v,
                fbInitialized: false
            };
        }

        function isMobile() {
            return (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()));
        }

        function isIOS() {
            return (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()));
        }

        function isAndroid() {
            return (/android/i.test(navigator.userAgent.toLowerCase()));
        }

        // XXX:DEBUG MODE ONLY
        var FB_CLIENT_IDS = {
                'spark.dropbeat.net': '1450325018550300',
                'hack.coroutine.io': '1567473853502082',
                'spark.coroutine.io': '1567477583501709',
                'dropbeat.net': '754181154701901'
            },
            context = {};

        context.host = window.location.host;
        context.account = null;
        context.isMobile = isMobile();
        context.isIOS = isIOS();
        context.isAndroid = isAndroid();
        context.browserSupport = getBrowser();
        context.fbClientId = FB_CLIENT_IDS[context.host];
        if (!context.fbClientId) {
            context.fbClientId = 'XXXXXXXXXXXXX';
        }

        return context;
    }());

    module.api = (function () {
        var scheme = 'http',
            baseApiHost = window.location.host,
            uri = '/api/',
            version = 'v1',
            url = scheme + '://' + baseApiHost + uri + version + '/';

        function endpoint(name, version) {
            if (version) {
                return scheme + '://' + baseApiHost + uri +
                    version + '/' + name + '/';
            }
            return url + name + '/';
        }
        return {
            metaUrl: endpoint('meta/key'),
            trackShareUrl: endpoint('track/shared'),
            resolveObjectUrl: endpoint('resolve'),

            // Log api endpoint
            playLogUrl: endpoint('log/play'),
            playdropLogUrl: endpoint('log/playdrop'),
            playbackDetailLogUrl: endpoint('log/playback_detail')
        };
    }());

    module.coreApi = (function () {
        var scheme = 'http',
            debug = false,
            prodBaseApiHost = 'core.dropbeat.net',
            devBaseApiHost = 'core.coroutine.io',
            //devBaseApiHost = 'dropbeat.net:19020',
            uri = '/api/',
            url = scheme + '://' + (debug ? devBaseApiHost : prodBaseApiHost) + uri;

        function endpoint(name) {
            return url + name + '/';
        }
        return {
            debug: debug,
            genreUrl: endpoint('genre'),
            dropUrl: endpoint('drop')
        };
    }());

    module.resolveApi = (function () {
        var scheme = 'http',
            debug = false,
            prodBaseApiHost = 'resolve.dropbeat.net',
            devBaseApiHost = '14.63.224.95:19001',
            //devBaseApiHost = 'dropbeat.net:19020',
            uri = '/',
            url = scheme + '://' + (debug ? devBaseApiHost : prodBaseApiHost) + uri;

        function endpoint(name) {
            return url + name + '/';
        }
        return {
            debug: debug,

            resolveUrl: endpoint('resolve')
        };
    }());

    module.clientKeys = {
        google: 'AIzaSyCoieDdwxgy01P7MBIdR48tFxAtyEYHPmA',
        soundcloud: 'd5249ae899d7b26e6c6af608d876d12c'
    };

    module.compatibility = (function () {
        var navigator = window.navigator,
            chrome = navigator.userAgent.match(/(Chrome)/g),
            firefox = navigator.userAgent.match(/(Firefox)/g),
            safari = (navigator.userAgent.match(/(Safari)/g)) && !chrome,
            ie = !chrome && !firefox && !safari,

            browser = module.context.browserSupport.currentBrowser;

        return {
            isExplorer: browser[0] === 'MSIE',
            isSafari: browser[0] === 'Safari'
        };
    }());

    module.escapes = (function () {
        return {
            title: function (title) {
                return title.replace(/"/g, "'");
            }
        };
    }());

    module.constants = {
        shareUriKey: 'playlist',
        tempEmailPostfix: '@dropbeat.net',
        queueEOL: 'stop'
    };

    module.host = window.location.protocol + '//' +  window.location.host;

    module.isKorean = function(str) {
        return false;
    };


    module.log = function (category, action, label, value) {
        ga('send', 'event', category, action, label, value);
        return true;
    };

    module.coreLog = (function() {
        var ACTION_HANDLER,
            playEvents = [],
            playEventMusic,
            lastProgress = 0;

        module.$(module).on('musicProgressUpdate',
            function(e, progress, music, section) {
            if (playEventMusic && playEventMusic.id === music.id) {
                lastProgress = progress.playback;
            }
        });

        ACTION_HANDLER = {
            playdrop: function(track) {
                if (!track || !track.title) {
                    return;
                }
                module.$.ajax({
                    url:module.api.playdropLogUrl,
                    data: {
                        uid:track.id,
                        t:track.title,
                        device_type: "html5embed"
                    },
                    cache:false,
                    dataType: 'json'
                }).done(function() {
                    //do nothing
                });
            },
            play: function(track) {
                if (!track || !track.title) {
                    return;
                }
                module.$.ajax({
                    url:module.api.playLogUrl,
                    data: {
                        uid:track.id,
                        t:track.title,
                        device_type: "html5embed"
                    },
                    cache:false,
                    dataType: 'json'
                }).done(function() {
                    //do nothing
                });
            },
            playDetailStart: function(music) {
                if (playEventMusic && playEventMusic.id !== music.id) {
                    if (playEvents.length > 0 && lastProgress > 0) {
                        ACTION_HANDLER.playDetailExit(playEventMusic);
                    }
                    playEventMusic = null;
                }
                if (music.type !== 'dropbeat') {
                    return;
                }
                playEventMusic = music;
                playEvents.length = 0;

                playEvents.push({type: 'start'});
            },
            playDetailSeekFrom: function(music, seekFrom) {
                if (music.type !== 'dropbeat') {
                    return;
                }

                if (!playEventMusic || playEventMusic.id !== music.id) {
                    return;
                }

                playEvents.push({type: 'seek_from', ts: parseInt(seekFrom)});
            },
            playDetailSeekTo: function(music, seekTo) {
                if (music.type !== 'dropbeat') {
                    return;
                }

                if (!playEventMusic || playEventMusic.id !== music.id) {
                    return;
                }

                playEvents.push({type: 'seek_to', ts: parseInt(seekTo)});
            },
            playDetailExit: function() {
                var exitAt = lastProgress;
                if (!playEventMusic) {
                    return;
                }
                playEvents.push({type: 'exit', ts: parseInt(exitAt)});
                sendPlayDetailLog();
            },
            playDetailEnd: function(music) {
                if (music.type !== 'dropbeat') {
                    return;
                }
                if (!playEventMusic || playEventMusic.id !== music.id) {
                    return;
                }
                playEvents.push({type: 'end'});
                sendPlayDetailLog();
            },
            playDetailError: function(music) {
                playEventMusic = null;
                playEvents.length = null;
            }
        };

        function sendPlayDetailLog() {
            // NOT SENDING PLAYBACK DETAIL LOG
            playEvents.length = 0;
            playEventMusic = null;
            return;

            var geoLocation = module.context.geolocation;

            if (!geoLocation) {
                playEvents.length = 0;
                return;
            }

            if (!playEventMusic || playEventMusic.type !== 'dropbeat') {
                playEvents.length = 0;
                return;
            }

            module.$.ajax({
                url:module.api.playbackDetailLogUrl,
                data: JSON.stringify({
                    track_id: playEventMusic.uid,
                    data: playEvents,
                    location: {
                        lat: geoLocation.lat,
                        lng: geoLocation.lng,
                        country_name: geoLocation.countryName,
                        country_code: geoLocation.countryCode,
                        city_name: geoLocation.cityName
                    }
                }),
                dataType: 'json',
                contentType: 'application/json',
                cache:false,
                type:'POST'
            }).done(function() {
            });

            playEvents.length = 0;
            playEventMusic = null;
        };

        return function() {
            var actionName,
                params = Array.prototype.slice.call(arguments, 0),
                handler;

            actionName = params[0];
            handler = ACTION_HANDLER[actionName];

            if (handler) {
                handler.apply(this, params.slice(1));
            }
        };
    }());

    module.beforeunload = function() {
        var v;
        module.$.each(beforeunloadCallbacks, function(key, val) {
            v = v || val();
        });
        return v;
    };

    module.addBeforeunloadCallback = function(key, func) {
        beforeunloadCallbacks[key] = func;
    };

    module.removeBeforeunloadCallback = function(key) {
        delete beforeunloadCallbacks[key];
    };

    return module;
}($dbt));

$dbt.$(document).ready(function () {
    'use strict';

    var genreDeferred = $dbt.$.Deferred(),
        geoDeferred = $dbt.$.Deferred(),
        metaDeferred = $dbt.$.Deferred(),
        initDeferreds = [],
        geolocation,
        localeInitialized;

// init shortcut
    $dbt.shortcuts();

    window.onbeforeunload = function (event) {
        var message = $dbt.beforeunload();
        if (!message) {
            return;
        }
        if (typeof event == 'undefined') {
            event = window.event;
        }
        if (event) {
            event.returnValue = message;
        }
        return message;
    };

// register for playdetail log sending
    $dbt.addBeforeunloadCallback('playDetailLog', function() {
        $dbt.coreLog('playDetailExit');
    });

// override notification to show failure page
    $dbt.s.notify = $dbt.notification.notify = function(msg, type, options) {
        var klass,
            opt = {},
            notifyObj = {};

        if (type === 'error') {
            $dbt.view.setPage(new $dbt.view.FailureView({ message: msg }));
            return;
        }

        if (type === 'success') {
            klass = 'glyphicons-ok-2';
        } else if (type === 'info') {
            klass = 'glyphicons-circle-info';
        } else if (type === 'error') {
            klass = 'glyphicons-alert';
        } else if (type === 'warn') {
            klass = 'glyphicons-warning-sign';
        }
        if (msg instanceof Object) {
            $dbt.$.extend(notifyObj, msg);
        } else {
            notifyObj['msg'] = msg;
        }
        if (!options || !options.ignoreIcon) {
            notifyObj['ic'] = '<i style=\'margin-right:5px\' class=\'glyphicons ' + klass + '\'></i>';
        }
        if (options) {
            $dbt.$.extend(opt, options);
        }
        opt['className'] = type;
        $dbt.$.notify(notifyObj, opt);
    };

    $dbt.view.setPage(new $dbt.view.LoadingView({}));

    $dbt.$.ajax({
        url: $dbt.coreApi.genreUrl,
        crossDomain: true,
        dataType: 'json'
    }).done(function(res) {
        var genres = {};
        if (!res || !res.success) {
            $dbt.s.notify('Failed to fetch genre information', 'error');
        } else {
            $dbt.$.each(res, function(groupKey, genreGroup) {
                if (groupKey !== 'success') {
                    genres[groupKey] = genreGroup;
                }
            });
        }
        genreDeferred.resolve(genres);
    });
    initDeferreds.push(genreDeferred);


    $dbt.$.ajax({
        url: $dbt.api.metaUrl,
        dataType: 'json'
    }).done(function(res) {
        if (!res.success || !res.soundcloud_key) {
            $dbt.s.notify('Failed to fetch meta information', 'error');
            metaDeferred.reject();
            return;
        }
        $dbt.clientKeys['soundcloud'] = res.soundcloud_key;
        metaDeferred.resolve(res);
    }).fail(function() {
        metaDeferred.reject();
    });


    initDeferreds.push(metaDeferred);

// load geolocation
    if ($dbt.$.cookie('geolocation')) {
        geolocation = JSON.parse($dbt.$.cookie('geolocation'));
        geoDeferred.resolve(geolocation);
    } else {
        $dbt.$.ajax({
            url:'http://geo.ironbricks.com/json/',
            type: 'POST',
            dataType: 'jsonp',
            timeout: 5000
        }).done(function(res) {
            var lat = res.latitude,
                lng = res.longitude,
                countryCode = res.country_code,
                countryName = res.country_name;

            $dbt.$.get('http://maps.google.com/maps/api/geocode/json?latlng=' +
                    lat + ', ' + lng + '&sensor=false&language=en')
                .done(function(res) {
                    var results = res.results,
                        status = res.status.toLowerCase(),
                        townObj,
                        countryObj,
                        cityObj,
                        cityName,
                        geolocation;

                    if (status !== 'ok') {
                        geoDeferred.resolve();
                        return;
                    }

                    try {
                        countryObj = results[results.length - 1];
                        countryName = countryObj.address_components[0].long_name;
                        countryCode = countryObj.address_components[0].short_name;
                        cityObj = results[results.length - 2];
                        cityName = cityObj.address_components[0].long_name;
                        townObj = results[0];

                        lat = townObj.geometry.location.lat;
                        lng = townObj.geometry.location.lng;

                        geolocation = {
                            lat: lat,
                            lng: lng,
                            countryCode : countryCode,
                            countryName : countryName,
                            cityName: cityName
                        };

                        $dbt.$.cookie('geolocation', JSON.stringify(geolocation), 
                            {expires: 9999});
                    } catch (e) {
                    }
                    geoDeferred.resolve(geolocation);
                }).fail(function() {
                    geoDeferred.resolve();
                });
        }).fail(function() {
            geoDeferred.resolve();
        });
    }

    initDeferreds.push(geoDeferred.then(onLocaleLoaded));


// wait init preprocess
    $dbt.$.when.apply(this, initDeferreds).done(function(genre, meta, geolocation) {
        $dbt.context.genre = genre;
        $dbt.context.geolocation = geolocation;

        if ($dbt.coreApi.debug) {
            $dbt.notification.notify('DEV CORE', 'warn');
        }


// check browser support
        if (!$dbt.context.browserSupport.support) {
            $dbt.s.notify($dbt.string('player_browser_not_supported'), 'error');
            return;
        }

        if (!$dbt.utils.gup('url')) {
            $dbt.s.notify($dbt.string('player_resolve_failure'), 'error');
            return;
        }

        $dbt.initialize();
    });

    function onLocaleLoaded(geolocation) {
        var locale, deferred;

        if (geolocation && geolocation.countryCode === 'KR') {
            locale = 'ko_KR';
        } else {
            locale = 'en_US';
        }

        if ($dbt.$.cookie('force_en')) {
            locale = 'en_US';
        }
        $dbt.i18n = new $dbt.I18n();
        deferred = $dbt.i18n.setLocale(locale);
        $dbt.context.locale = locale;

        if (locale === "ko_KR") {
            moment.locale("ko", {
                longDateFormat : {
                    LT : 'A h:mm ',
                    LTS : 'A h:m:ss ',
                    L : 'YYYY.MM.DD',
                    LL : 'YYYY년 MM월 D일',
                    LLL : 'YYYY년 MM월 D일 LT',
                    LLLL : 'YYYY년 MM월 D일 dddd LT'
                }
            });
        } else {
            moment.locale(locale);
        }

        return deferred.then(function() {
            $dbt.$('.hidden-before-locale').removeClass('hidden-before-locale');
            return geolocation;
        });
    }

});

function onYouTubeIframeAPIReady() {
    'use strict';
    $dbt.state.youtubeApiReady = true;
    $dbt.$($dbt).trigger('youtubeApiReady');
}

// below jquery is needed for crors support in IE
$dbt.$.support.cors = true;
