/*jslint browser: true*/
/*global $*/
var $dbt = (function (module) {
    'use strict';

    module = module || {};
    module.notification = {};

    module.notification.manager = {
        init: function () {},

        getWrapper: function (elem) {
            var $wrapper = module.$(elem).find('.notifyjs-wrapper');
            if ($wrapper.size() > 0) {
                return $wrapper;
            }
            return module.$('.notifyjs-wrapper');
        },

        hide: function (elem) {
            this.getWrapper(elem).trigger('notify-hide');
        },

        onclick: function (elem, callback) {
// This method will attach click event to whole notification box
// regardless of original click event in `notifyjs`.
            var box = this.getWrapper(elem);

            box.unbind('click');
            box.click(callback);
        },

        getMessage: function (key) {
            return module.s.notifyMessage.get(key);
        },

        sharePlaylist: function (success, url) {
            var msg = success ?
                        this.getMessage('shared') + url :
                            this.getMessage('cannotShare'),
                style = success ? 'info' : 'warn',
                notifyOptions = {
                    autoHideDelay: 10000,
                    clickToHide: false,
                    ignoreIcon: true,
                    className: style
                },
                $el;

            $el = module.$('.playlist-section .playlist-footer .share-playlist-button');
            if ($el.offset().top < 200) {
                notifyOptions['elementPosition'] = 'bottom left';
                notifyOptions['clickToHide'] = true;
                module.notification.notify(msg, style, notifyOptions);
            } else {
                notifyOptions['elementPosition'] = 'top right';
                $el.notify({msg: msg}, notifyOptions);
            }

        },

        playlistLoaded: function () {
            module.notification.notify(this.getMessage('loaded'), 'success');
        },

        playlistCleared: function () {
            module.$('.clear-playlist-button').notify(
                { msg : this.getMessage('cleared') }, { className: 'success' });
        },

        invalidAdderUrl: function () {
            var notifyOptions = {
                    elementPosition: 'bottom left',
                    className: 'error'
                };

            module.$('#add-by-url-input').notify({
                    msg: this.getMessage('invalidUrl')
                }, notifyOptions);
        },

        inSharedPlaylist: function () {
            module.notification.notify(
                {'html': this.getMessage('inSharedPlaylist')},
                'info',
                {
                    position: 'bottom left',
                    autoHide: false,
                    ignoreIcon: true
                }
            );
        },

        notPlayable: function (title) {
            var message = this.getMessage('notPlayable');
            if (title) {
                message += title.slice(0, 40) + '...';
            }
            module.notification.notify(
                message,
                'error',
                {
                    autoHideDelay: 10000
                }
            );
        },

        longPlaylistName: function () {
            module.notification.notify(
                this.getMessage('longPlaylistName'),
                'error',
                {
                    autoHideDelay: 10000
                }
            );
        },

        artistUrl: function (artist, url) {
            module.$('.input-field-suppliment').notify(
                { msg : artist + this.getMessage('artistUrl') + url },
                {
                    clickToHide: false,
                    autoHideDelay: 60000,
                    className: 'info'
                });
        }
    };

    module.notification.message = (function () {
        var EN = {
                added: 'Track added',
                alreadyOnPlaylist: 'Already on playlist',
                shared: 'Share this URL to your friends! \n',
                loaded: 'Playlist loaded!',
                cleared: 'Playlist cleared!',
                invalidUrl: 'Invalid URL.\nPlease input individual content URL\nfrom Youtube or Soundcloud (no playlist URL)',
                inSharedPlaylist: 'You are on a shared playlist. <br/>'
                    + 'All changes in this playlist will not affect yours.<br/><br/>'
                    + 'If you want to make your own playlist, <a href="/">click here</a>',
                notPlayable: 'This track is currently unavailable : ',
                cannotShare: 'You cannot share an empty playlist',
                artistUrl: ' full album : ',
                needSignin: 'Need facebook signin',
                sharedPlaylistNotEditable: 'Shared playlist is not editable',
                longPlaylistName: 'Playlist name is too long'
            },
            KR = {
                added: 'Track added KR',
                alreadyOnPlaylist: 'Already on playlist',
                shared: 'Share this URL to your friends! \n',
                loaded: 'Playlist loaded!',
                cleared: 'Playlist cleared!',
                invalidUrl: 'Invalid URL.\nPlease input individual content URL\nfrom Youtube or Soundcloud (no playlist URL)',
                inSharedPlaylist: 'You are on a shared playlist. <br/>'
                    + 'All changes in this playlist will not affect yours.<br/><br/>'
                    + 'If you want to make your own playlist, <a href="/">click here</a>',
                notPlayable: 'This track is currently unavailable : ',
                cannotShare: 'You cannot share an empty playlist',
                artistUrl: ' full album : ',
                needSignin: 'Need facebook signin',
                sharedPlaylistNotEditable: 'Shared playlist is not editable',
                longPlaylistName: 'Playlist name is too long'
            };

        return {
            get: function (key) {
// Message translation is currently disabled so that this method
// only returns `English` messages.
                if (module.context.locale === 'ko_KR') {
                    return KR[key];
                }
                return EN[key];
            }
        };
    }());

    module.notification.notify = function(msg, type, options) {
        var klass,
            opt = {},
            notifyObj = {};
        if (type === 'success') {
            klass = 'glyphicons-ok-2';
        } else if (type === 'info') {
            klass = 'glyphicons-circle-info';
        } else if (type === 'error') {
            klass = 'glyphicons-alert';
        } else if (type === 'warn') {
            klass = 'glyphicons-warning-sign';
        }
        if (msg instanceof Object) {
            module.$.extend(notifyObj, msg);
        } else {
            notifyObj['msg'] = msg;
        }
        if (!options || !options.ignoreIcon) {
            notifyObj['ic'] = '<i style=\'margin-right:5px\' class=\'glyphicons ' + klass + '\'></i>';
        }
        if (options) {
            module.$.extend(opt, options);
        }
        opt['className'] = type;
        module.$.notify(notifyObj, opt);
    };

    return module;
}($dbt));
