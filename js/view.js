/*jslint browser: true*/
/*global $*/
var $dbt = (function (module) {
    'use strict';

    var DropbeatView,
        DropbeatPageView,

        PlayableViewMixin,
        WaveformViewMixin,

        Failure404PageView,

        AboutPageView,
        HomePageView,
        ExplorePageView,
        SearchResultPageView,
        SharedPlaylistPageView,
        SharedTrackPageView,
        MobileSharedTrackPageView,
        IframePageView,
        AppDownloadPageView,
        UploadPageView,
        ProfilePageView,
        ResourcePageView,
        ResourceFollowingPageView,
        ResourceFollowersPageView,

        FollowersSectionView,
        FollowingSectionView,

        LoadResourceView,
        ArtistResourceSubViews,
        UserResourceSubViews,
        ChannelResourceSubViews,
        TrackPageView,
        TrackEditPageView,
        StatisticsPageView,
        StatisticsSubView,

        SoundcloudUploadView,
        SoundcloudImportPageView,
        SoundcloudMassUploadView,

        ScImportTask;

    module = module || {};

////////////////
// View Utils //
////////////////

    function extractMusicFromDOM(context, $el) {
        var musicStorageKey,
            musicData,
            drop;

        musicStorageKey = $el.data('musicStorageKey');
        if (musicStorageKey && context.getMusicFromStorage) {
            musicData = context.getMusicFromStorage(musicStorageKey);
            if (musicData) {
                return musicData;
            }
        }

        drop = {
            when: parseInt($el.data('dropSec')) || 0,
            type: $el.data('dropType'),
            dref: String($el.data('dropMusicId'))
        };
        if (!drop.type || !drop.dref) {
            drop = null;
        }
        musicData = {
            id: String($el.data('musicId')),
            title: unescape($el.data('musicTitle')),
            type: $el.data('musicType'),
            tag: $el.data('musicTag'),
            duration: $el.data('musicDuration'),
            drop: drop
        };
        musicData = new module.s.Music(musicData);
        if (!musicData.id || !musicData.type || !musicData.title) {
            return null;
        }

        return musicData;
    }

    function validateEmail(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

    function showSubTracks($el, callback) {
// Show current selected music
        var $musicContainer = $el,
            $tracks, $tracksInner,
            tracksSlideAnim;

        if ($musicContainer.next().hasClass('tracks')) {
            $tracks = $musicContainer.next();
            $tracksInner = $tracks.find('.tracks-inner');
            $tracksInner.css({
                'opacity': 0
            });
            tracksSlideAnim = $tracks.slideDown(300)
            module.$.when(tracksSlideAnim).done(function() {
                $tracksInner.animate({
                        opacity: '1'
                }, 200, function() {
                    callback && callback();
                });
            });
        }
    }

    function hideSubTracks($prev, $curr, callback) {
        var currParentMusicId, prevParentMusicId,
            $prevTracks, $prevTracksInner,
            tracksFadeAnim;

        currParentMusicId = $curr.closest('.a-addable-music-wrapper')
            .data('parentMusicId');
        prevParentMusicId = $prev.closest('.a-addable-music-wrapper')
            .data('parentMusicId');
        if (currParentMusicId === prevParentMusicId) {
            if (!$prev.hasClass('track-music')) {
// case when opened is parent. we dont need to hide prev parent
                callback && callback();
                return;
            }
        } else {
// case when we need to close parent.
            if ($prev.hasClass('track-music')) {
                $prev = $prev.closest('.a-addable-music-wrapper')
                    .find('.parent-music');
            }
        }


        if ($prev.next().hasClass('tracks')) {
            $prevTracks = $prev.next();
        }
        if ($prevTracks) {
            $prevTracksInner = $prevTracks.find('.tracks-inner');
            tracksFadeAnim = $prevTracksInner.animate({
                    opacity: '0'
                }, 200);
        }
        module.$.when(tracksFadeAnim).done(function() {
            var tracksSlideAnim;

            if (!$prevTracks) {
                callback && callback();
                return;
            }
            tracksSlideAnim = $prevTracks.slideUp(300);

            module.$.when(tracksSlideAnim).done(function() {
                var $wrapper, $folded, $openedChildren;
                if ($prevTracks) {
                    $wrapper = $prevTracks.find('.btn-wrapper');
                    $folded = $wrapper.next();
                    $wrapper.show();
                    $folded.hide();

                    $openedChildren = module.$('.a-addable-music.opened', $prevTracks);
                    $openedChildren.removeClass('opened');
                }

                $prev.removeClass('opened');
                callback && callback();
            });
        });
    }


    function showDrop($el, music, drop) {
        var params = {},
            $drop = module.$('.drop', $el),
            $progress = module.$('.progress', $el),
            url = module.coreApi.dropUrl,
            totalTime,
            display,
            when;

// TODO : we need to handle this case later.
// we hide all youtube music's drop mark from progress
        if (music.type === 'youtube') {
            return;
        }

        if (music.duration) {
            if (music.type === 'dropbeat') {
                totalTime = parseInt(music.duration);
                if (drop) {
                    when = drop.start;
                }
            } else {
                totalTime = parseInt(music.duration) / 1000;
                if (drop) {
                    when = drop.when;
                }
            }
        } else {
            totalTime = module.playerManager.getTotalPlaybackTime();
            totalTime = totalTime === 0? 300 : totalTime;
            if (drop) {
                when = drop.when;
            }
        }
        if (totalTime > 30 * 60) {
            return;
        }

        display = function(drop) {
            var percent,
                dropDurationPercent,
                dropWidth,
                startPoint;
            percent = Math.min((100 * drop) / totalTime, 100);
            if (percent === 100) {
                return;
            }
            dropDurationPercent = Math.min((100 * 20) / totalTime, 100)

            dropWidth = $progress.width() * dropDurationPercent / 100;
            startPoint = $progress.width() * percent / 100;
            if (dropWidth + startPoint > $progress.width()) {
                dropWidth = $progress.width() - startPoint;
            }
            if (dropWidth < 100) {
                dropWidth = 100;
            }
            $drop.css({
                'width': dropWidth + 'px',
                'left': percent + '%'
            });
            $drop.show();
        };

        if (when) {
            if (when === -1) {
                return;
            }
            display(when);
            return;
        }

        if (music.type === 'youtube') {
            params['q'] = music.title;
        } else {
            params['q'] = music.id;
        }

        url += '?' + module.$.param(params);

        module.$.ajax({
            crossDomain: true,
            url: url,
            dataType: 'json'
        }).done(function(res) {
            if (!res.success || !res.drop || res.drop < 0) {
                return;
            }
            display(res.drop);
        });
    }

    function initTooltipster($el, options) {
        options = options || {};
        options.theme = 'dropbeat-tooltip-theme';
        options.position = 'bottom';

        module.$('.tooltip', $el).tooltipster(options);
        module.$('.tooltip-right', $el).tooltipster(
            module.$.extend({}, options, {
                position:'right'
            }));
        module.$('.tooltip-always').tooltipster('show');
    }


/////////////////////////
// Default view module //
/////////////////////////

    module.view = {
        init: function () {
            var that = this;

            module._.bindAll(this, 'onFollowBtnMouseEnter', 'onFollowBtnMouseLeave');

            this.placeholderCompat();

            this.mobilePlaylistView.init();
            this.playlistMenuView.init();
            this.headerView.init();
            this.accountMenuView.init();

            this.resizeContentsBg();
            module.$(window).on('resize', function (e) {
                that.resizeContentsBg();
            });

            if (module.context.isMobile) {
                setTimeout(function() {
                    that.fixScreenSizeOnKeyboardShow();
                }, 3000);
            }

            module.$('#dropbeat').on('click', '.signin-btn', function() {
                module.view.SigninView.show();
            });

            module.$('#dropbeat').on('click', '.signup-btn', function() {
                module.view.SignupView.show();
            });

            module.$('.bg-body-filter').on('click', function() {
                var $el = module.$(this);
                $el.removeClass('exposed');
            });

            module.$('body').on('click', '.overlay-filter', function() {
                module.$('.overlay-filter').hide();
            });

            module.$('body').on('click', 'a[data-href]', function(e) {
                var href = module.$(this).data('href'), path, a;
                a = document.createElement('a');
                a.href = href;
                path = a.pathname + a.search;
                if (!path.startsWith('/')) {
                    path = '/' + path;
                }
                module.$('.account-menus .menu-window').hide();
                module.$('.overlay-filter').hide();
                module.router.navigate(path, {trigger: true});
            });

            module.$('body').on('mouseenter', '.ellipsised-text', function(e) {
                var $el = module.$(this);
                if(this.offsetWidth < this.scrollWidth && !$el.attr('title')){
                    $el.attr('title', $el.text().trim());
                }
            });

            module.$('body').on('mouseenter', '.channel-track .btn-follow', this.onFollowBtnMouseEnter);
            module.$('body').on('mouseleave', '.channel-track .btn-follow', this.onFollowBtnMouseLeave);
        },

        fixScreenSizeOnKeyboardShow: function() {
            var that = this;

            this.screenSize = {
                width: module.$(window).width(),
                height: module.$(window).height()
            };
            module.$('body').css({
                width: that.screenSize.width,
                height: that.screenSize.height
            });

            module.$(window).on('resize', function() {
// If the current active element is a text input,
// we can assume the soft keyboard is visible.
// And also check width diff is less than 1% and height diff greater than 35%
                var prevWidth, prevHeight,
                    widthDiff, heightDiff,
                    winWidth, winHeight;

                winWidth = module.$(window).width();
                winHeight = module.$(window).height();

                if(module.$(document.activeElement).attr('type') === 'text') {
                    prevWidth = that.screenSize.width;
                    prevHeight = that.screenSize.height;

                    if (prevHeight > winHeight) {
                        widthDiff = Math.abs(prevWidth - winWidth) * 100 / prevWidth;
                        heightDiff = Math.abs(prevHeight - winHeight) * 100 / prevHeight;
                        if (widthDiff <= 1 && heightDiff > 35) {
                            if (prevHeight > winHeight) {
                                module.$('.player-section').hide();
                            }
                            return;
                        }
                    }
                }
                module.$('.player-section').show();
                that.screenSize = {
                    width: winWidth,
                    height: winHeight
                };
                module.$('body').css({
                    width: that.screenSize.width,
                    height: that.screenSize.height
                });
            });
        },

        initContentsBg: function() {
            var $imageFrame, size, selectedIdx, imgObj, $img, url;
            $imageFrame = module.$('.bg-image');
            size = module.constants.bgImages.length;
            selectedIdx = Math.floor(Math.random() * size);
            imgObj = module.constants.bgImages[selectedIdx];
            if (imgObj.name) {
                url = '/bg_images/' + imgObj.name;
            }
            if (imgObj.url) {
                url = imgObj.url;
            }

            $img = module.$('<img class=\'bg-image-inner\' src=\'' + url +
                    '\' data-width=\'' + imgObj.width +
                    '\' data-height=\'' + imgObj.height + '\'/>');
            $imageFrame.html($img);
            if (module.context.browserSupport.support) {
                $imageFrame.css(
                    'backgroundImage',
                    'url(\'' + url + '\')');
            }

            module.view.resizeContentsBg();
        },

        resizeContentsBg: function () {
            var $img = module.$('.bg-image img'),
                imgWidth = $img.data('width'),
                imgHeight = $img.data('height'),
                aspectRatio = imgWidth / imgHeight,
                $frame = module.$('.contents'),
                frameWidth = $frame.width(),
                frameHeight = $frame.height(),
                frameAspectRatio = frameWidth / frameHeight;
            if (frameAspectRatio > aspectRatio) {
// adjust image to window width
                $img.css({
                    width: frameWidth + 'px',
                    height: (frameWidth / aspectRatio) + 'px',
                    marginTop: -1 * (frameWidth / aspectRatio) / 2 + 'px',
                    marginLeft: -1 * frameWidth / 2
                });
            } else {
                $img.css({
                    width: frameHeight * aspectRatio + 'px',
                    height: frameHeight + 'px',
                    marginTop: -1 * frameHeight / 2,
                    marginLeft: -1 * (frameHeight * aspectRatio) / 2 + 'px'
                });
            }
        },

        placeholderCompat: function () {
// IE 7,8 does not support `placeholder`.
            module.$('input[placeholder]').placeholder();
        },

        onFollowBtnMouseEnter: function(e) {
            var $btn = module.$(e.currentTarget),
                $btnText = $btn.find('.text');

            if ($btn.hasClass('progress') || !$btn.hasClass('following')) {
                return;
            }
            $btnText.text($btnText.data('textUnfollow'));
        },

        onFollowBtnMouseLeave: function(e) {
            var $btn = module.$(e.currentTarget),
                $btnText = $btn.find('.text');

            if ($btn.hasClass('progress') || !$btn.hasClass('following')) {
                return;
            }
            $btnText.text($btnText.data('textFollowing'));
        }

    };


///////////////////
// Default view  //
///////////////////

    DropbeatView = Backbone.View.extend({
        initialize: function(options) {
            var context,
                parentView;
            options = options || {};

            context = options.context || {};
            context = module.$.extend({}, module.context, context);
            this._context = { _context: context };

            parentView = options.parentView;
            if (parentView) {
                this.parentView = parentView;
            }

            if (this.tmpl) {
                this._tmpl = module._.template(module.$(this.tmpl).html());
            }
        },
        events: {
            'click .page-inner-link': 'pageInnerLink'
        },
        pageInnerLink: function(e) {
            var $el = module.$(e.currentTarget),
                targetId = $el.data('target'),
                $target = module.$('#' + targetId),
                scrollTop;
            if (!$target.size()) {
                return;
            }
            scrollTop = $target.offset().top;
            module.$(window).scrollTop(scrollTop);
            e.stopPropagation();
        }
    });

    DropbeatPageView = DropbeatView.extend({
        subViews: {},
        initialize: function(options) {
            module._.bindAll(this, 'remove', 'onImportBtnClicked');
            DropbeatView.prototype.initialize.call(this, options);
            module.$('#dropbeat').on('click', '.import-playlist-btn',
                this.onImportBtnClicked);
            this._$pageTarget = module.$('.body-section-inner');
            this._$headerSearchMenu = module.$('.header-section .search-menu');
            this._$headerSearchMenu.show();

            this.subViews.playerView.init();
            this.subViews.tracklistTabView.init();

            if (window['twttr'] && twttr.widgets) {
                twttr.widgets.load(module.$('#dropbeat')[0]);
            }
            if (module.context.fbInitialized &&
                    window['FB'] && FB.XFBML && FB.XFBML.parse) {
                FB.XFBML.parse(module.$('#dropbeat')[0]);
            }
        },

        onImportBtnClicked: function(e) {
            if (module.context.account) {
                module.view.SoundcloudPlaylistImportView.show();
            } else {
                module.view.NeedSigninView.show();
            }
        },

        remove: function() {
            module.$.each(this.subViews, function(viewName, view) {
                if (view.remove) {
                    view.remove();
                }
            });
            this.hideNotification();
            module.$('#dropbeat').off('click', this.onImportBtnClicked);

            DropbeatView.prototype.remove.apply(this, arguments);

// Stop all music when page removed
            if (module.context.isMobile) {
                module.playerManager.stop();
            }
        },

        unbind: function() {
            this.undelegateEvents();
            module.$.each(this.subViews, function(viewName, view) {
                if (view.remove) {
                    view.remove();
                }
            });
        },

        showNotification: function(message, type, timeout) {
            var that = this,
                $el = module.$('#dropbeat .notice-header');

            $el.find('.text').text(message);
            $el.css('top', 0);
            $el.animate({ top: 48 }, 300);

            $el.removeClass('notice-warn')
                .removeClass('notice-success')
                .removeClass('notice-error')
                .removeClass('notice-info');
            if (type === 'success') {
                $el.addClass('notice-success');
            } else if (type === 'warn') {
                $el.addClass('notice-warn');
            } else if (type === 'info') {
                $el.addClass('notice-info');
            } else if (type === 'error') {
                $el.addClass('notice-error');
            }
            if (timeout && timeout > 0) {
                if (this._headerNotificationTimer) {
                    clearTimeout(this._headerNotificationTimer);
                }
                this._headerNotificationTimer = setTimeout(function() {
                    that.hideNotification();
                }, timeout);
            }
        },

        hideNotification: function() {
            var $el = module.$('#dropbeat .notice-header');

            this._headerNotificationVisible = false;

            $el.find('.text').empty();
            $el.animate({ top: 0 }, 300);
        }
    });

    DropbeatPageView.prototype.subViews.tracklistTabView = (function() {
        var view = {};
        view.init = function() {
            var that = this;
            module._.bindAll(this, 'whenPlayInfoUpdate', 'onMinimizeBtnClicked',
                'whenLoadingMusic', 'whenPlayMusic', 'whenFinishedMusic',
                'onTabCloseBtnClicked', 'onShowBtnClicked', 'onTrackInfoHover',
                'onTrackInfoUnhover', 'onPlayedAtClicked',
                'onMarkerHover', 'onMarkerUnhover', 'whenProgressUpdate');

            this._$el = module.$('.player-section .tracklist-tab');
            this._$elBg = module.$('.player-section .tracklist-tab-bg');

            this._$closeBtn = module.$('.close-button', this._$el);
            this._$showBtn = module.$('.show-tracklist-btn', this._$el);
            this._$minimizeBtn = module.$('.minimize-button', this._$el);
            this._$trackInfoWrapper = module.$('.track-info-wrapper', this._$el);
            this._$trackInfo = module.$('.track-info', this._$el);
            this._$tracks = module.$('.tracks', this._$el);
            this._$markers = module.$('#player .trackmarks');
            this._tracklistTmpl = module._.template(module.$('#tmpl-player-tracklist').html());

            this._$closeBtn.on('click', this.onTabCloseBtnClicked);
            this._$minimizeBtn.on('click', this.onMinimizeBtnClicked);
            this._$showBtn.on('click', this.onShowBtnClicked);
            this._trackInfoTmpl = module._.template(module.$('#tmpl-player-tracklist-track-info').html());
            this._hideTab = false;

            module.$(module).on('playInfoUpdate', this.whenPlayInfoUpdate);
            module.$(module).on('playMusicFirst', this.whenPlayMusic);
            module.$(module).on('loadingMusic', this.whenLoadingMusic);
            module.$(module).on('finishedMusic', this.whenFinishedMusic);
            module.$(module).on('musicProgressUpdate', this.whenProgressUpdate);

            this._$el.on('mouseenter',
                '.music-title-scroll-wrapper', this.onTrackInfoHover);

            this._$el.on('mouseleave',
                '.music-title-scroll-wrapper', this.onTrackInfoUnhover);
            this._$el.on('click',
                '.a-addable-music .played-at', this.onPlayedAtClicked);
            this._$markers.on('mouseenter', '.marker', this.onMarkerHover);
            this._$markers.on('mouseleave', '.marker', this.onMarkerUnhover);

            PlayableViewMixin.bind(this, this._$el, ['player_tracklist']);
        };

        view.onTogglePlay = function(music, section, isPlaying) {
            var $onMusic,
                $targetMusic;
            $onMusic = module.$('.a-addable-music.on', this._$el);
            if (!music) {
                if (!isPlaying) {
                    $onMusic.find('.ctrl-play').removeClass('pause');
                }
                return;
            }
            $targetMusic = module.$('.a-addable-music[data-music-id=\'' +
                music.id + '\']', this._$el);
            if ($targetMusic.size() > 0) {
                if (isPlaying) {
                    $targetMusic.find('.ctrl-play').addClass('pause');
                } else {
                    $targetMusic.find('.ctrl-play').removeClass('pause');
                }
            }
        };

        view.whenPlayMusic = function(e, music, section) {
            var duration,
                tracklistSearchMinDruation = 15 * 60,
                that = this;

            if (section === 'drop' || section === 'player_tracklist' ||
                    section === 'promotion_liveset' ||
                    (music.type !== 'podcast' &&
                    music.type !== 'youtube' &&
                    music.type !== 'soundcloud')) {
                return;
            }

            duration = module.playerManager.getTotalPlaybackTime();
            if (duration < tracklistSearchMinDruation) {
                this._tracklist = null;
                return;
            }

            if (this._tracklist && this._tracklist.parentTrack.id === music.id) {
                return;
            }
            this._tracklist = null;

            this.showProgressTab();

            if (this._tracklistReq) {
                this._tracklistReq.abort();
                this._tracklistReq = null;
            }

            if (music.type === 'podcast') {
                this._tracklistReq = module.$.ajax({
                    url: module.coreApi.podcastTrackListUrl,
                    data: {
                        t: music.title
                    },
                    crossDomain: true,
                    dataType: 'json'
                });
            } else {
                this._tracklistReq = module.$.ajax({
                    crossDomain: true,
                    url: module.coreApi.tracklistUrl,
                    data: {
                        uid:music.id
                    },
                    dataType: 'json'
                })
            }
            this._tracklistReq.done(function(res) {
                if (!res || !res.success || !res.data || res.data.length === 0) {
                    that.hide();
                    return;
                }
                that.showTab(music, res.data, section);
            }).fail(function(jqXHR, textStatus, error) {
                that.hide();
            }).always(function() {
                that._tracklistReq = null;
            });
        };

        view.whenLoadingMusic = function(e, music, section) {
            if ((this._tracklist &&
                    this._tracklist.parentTrack.id === music.id) ||
                    section === 'player_tracklist') {
                return;
            }
            this.hide();
        };

        view.whenPlayInfoUpdate = function(e, music, section) {
            if (this._tracklist &&
                    this._tracklist.parentTrack.id !== music.id) {
                if (section === 'player_tracklist') {
// We should pause parent here.
// So we should do unbind only.
                    this.unbindTabUpdate();
                    return;
                }
            }
            if (this._tracklist &&
                    this._tracklist.parentTrack.id === music.id &&
                    this._tracklist.section === section) {
                this.bindTabUpdate();
                return;
            }
            this.unbindTabUpdate();
            if (!this._$el.hasClass('loading')) {
                this.hide();
            }
            this._tracklist = null;
        };

        view.whenProgressUpdate = function(e, progText, music, section) {
            var $onMusic,
                currTime,
                durationTime,
                percent = 0;

            if (section !== 'player_tracklist') {
                return;
            }

            currTime = moment.duration({seconds:progText.playback}).format('mm:ss');
            durationTime = moment.duration({seconds:progText.duration}).format('mm:ss');

            if (progText.duration > 0) {
                percent = 100 * progText.playback / progText.duration;
            }

            $onMusic = module.$('.a-addable-music.on[data-music-id=\'' +
                music.id + '\']', this._$el);
            $onMusic.find('.curr-play-time').text(currTime);
            $onMusic.find('.total-play-time').text(durationTime);
            $onMusic.find('.bullet').width(percent + '%');
            $onMusic.find('.progress-handle').css('left', percent + '%');
        };

        view.showProgressTab = function(e) {
            if (!this._$el.hasClass('closed')) {
                this._$el.addClass('closed')
            }
            this._$el.addClass('loading');
            this._$el.show();
            this._$el.switchClass('closed', '', 300);
        };

        view.onPlayedAtClicked = function(e) {
            var $target = module.$(e.currentTarget),
                timeStr = $target.data('playedAt'),
                time;

            if (!timeStr) {
                return;
            }
            if (!this._tracklist) {
                return;
            }
            time = parseInt(timeStr);

            module.playerManager.onMusicClicked(
                new module.s.Music({
                    'id': this._tracklist.parentTrack.id,
                    'title': this._tracklist.parentTrack.title,
                    'type': this._tracklist.parentTrack.type
                }),
                this._tracklist.section, {
                    playlistId: module.playlistManager.playingPlaylistId,
                    startPoint: time
                });
        };

        view.onTrackInfoHover = function(e) {
            var $marquee = module.$(e.currentTarget),
                $title = $marquee.find('.music-title'),
                speed,
                animPeriod;

            if ($title.width() <= $marquee.width()) {
                return;
            }

            speed = 20;
            animPeriod = speed * $title.width();
            $title.css({left: 0});
            $title.animate(
                {left: -$title.width()},
                animPeriod,
                'linear',
                function () {
                    $title.trigger('marquee');
                }
            );
            $title.bind(
                'marquee',
                function () {
                    var contentsWidth = $title.width(),
                        frameWidth = $marquee.width(),
                        animPeriod = speed * (contentsWidth + frameWidth);
                    $title.css({left: frameWidth});
                    $title.animate(
                        {left: -contentsWidth},
                        animPeriod,
                        'linear',
                        function () {
                            $title.trigger('marquee');
                        }
                    );
                }
            );
        };

        view.onTrackInfoUnhover = function (e) {
            var $title = module.$(e.currentTarget).find('.music-title');
            $title.unbind('marquee');
            $title.clearQueue();
            $title.stop();
            $title.css({left: 0});
        };

        view.showTab = function(parentTrack, rawTracks, section) {
            var timedTracks = [],
                duration,
                tracks = [];

            module.$.each(rawTracks, function(idx, track) {
                tracks.push({
                    seq: track.seq,
                    tag: track.tag,
                    title: track.title,
                    type: track.type,
                    id: track.uid || track.id,
                    when: track.when
                });
            });

            module.$.each(tracks, function(idx, track) {
                if (track.when && track.when !== '') {
                    track.when = parseInt(track.when);
                    timedTracks.push(track);
                }
            });

            timedTracks.sort(function(l, r) {
                return l.when > r.when ? 1 : -1;
            });

            this._tracklist = {
                parentTrack: parentTrack,
                section: section,
                tracks: tracks,
                timedTracks: timedTracks
            };

//  show markers
            if (module.playerManager.currentPlayer) {
                duration =  module.playerManager.currentPlayer.getTotalPlaybackTime();
                this.showMarker(duration, timedTracks);
            }

            this._$tracks.html(this._tracklistTmpl(this._tracklist));
            this.updateTab();
            this._$el.removeClass('loading');
            this.bindTabUpdate();
        };

        view.updateTab = function(music) {
            var $track = this._trackInfoTmpl({
                    music : music,
                    parentMusic: this._tracklist.parentTrack
                });
            this._$trackInfo.html($track);
            if (music) {
                this._$trackInfoWrapper.show();
            } else {
                this._$trackInfoWrapper.hide();
            }
        };

        view.bindTabUpdate = function() {
            var that = this;

            if (!this._tracklist || this._tracklist.timedTracks.length === 0) {
                return;
            }
            if (this._tracklistTrackUpdateTimer) {
                clearInterval(this._tracklistTrackUpdateTimer);
                this._tracklistTrackUpdateTimer = null;
            }
            this._tracklistTrackUpdateTimer = setInterval(function() {
                var currTime = module.playerManager.getCurrentPlaybackTime(),
                    foundTrack,
                    $track,
                    $onTrack;

                if (currTime === 0) {
                    return;
                }
                if (that._tracklist) {
                    module.$.each(that._tracklist.timedTracks || [], function(idx, track) {
                        if (currTime < track.when) {
                            return false;
                        }
                        foundTrack = track;
                    });
                }
                if (foundTrack !== that._tracklistDisplayTrack) {
                    that._tracklistDisplayTrack = foundTrack;
                    that.updateTab(foundTrack);
                }
                if (foundTrack) {
                    $track = module.$('.a-addable-music[data-music-id=\'' +
                        foundTrack.id + '\']', that._$tracks);
                    $onTrack = module.$('.a-addable-music.current-play', that._$tracks);
                    if (!$onTrack.is($track)) {
                        $onTrack.removeClass('current-play');
                        $track.addClass('current-play');
                    }
                }
            }, 1000);

        };

        view.whenFinishedMusic = function(e, music, section) {
            if (section !== 'player_tracklist') {
                this.unbindTabUpdate();
                this.hide();
                this._tracklist = null;
            }
        };

        view.unbindTabUpdate = function() {
            if (this._tracklistTrackUpdateTimer) {
                clearInterval(this._tracklistTrackUpdateTimer);
            }
        };

        view.hide = function() {
            var that = this;
// Clear marker children
            this.hideMarker();

// Hide and remove tracklist tab
            if (!this._$el.hasClass('closed')) {
                this._$el.switchClass('', 'closed', 300, function() {
                    that._$el.removeClass('maximized');
                    that._$el.removeClass('loading');
                    that._$showBtn.text(module.string('checkout_full_tracklist'));
                });
                this._$elBg.switchClass('', 'closed', 300, function() {
                    module.$(this).hide();
                });
            } else {
                this._$el.removeClass('loading');
                this._$el.removeClass('maximized');
                this._$elBg.hide();
                this._$elBg.removeClass('closed');
                this._$showBtn.text(module.string('checkout_full_tracklist'));
            }
        };

        view.onTabCloseBtnClicked = function(e) {
            this._hideTab = true;
            this.hide();
        };

        view.onShowBtnClicked = function(e) {
            if (this._$el.hasClass('maxmized')) {
                return;
            }
            this._$el.addClass('maximized');
            this._$elBg.removeClass('closed');
            this._$elBg.show();
            this._$showBtn.text(module.string('tracklist'));
        };

        view.onMinimizeBtnClicked = function(e) {
            if (!this._$el.hasClass('maximized')) {
                return;
            }
            this._$el.removeClass('maximized');
            this._$elBg.hide();
            this._$showBtn.text(module.string('checkout_full_tracklist'));
        };

        view.showMarker = function(duration, tracks) {
            var htmlContents = '';

            if (tracks.length === 0 || duration === 0) {
                return;
            }

            module.$.each(tracks, function(idx, track) {
                var percent = 100 * track.when / duration,
                    $marker = module.$('<div class=\'marker\'></div>');
                if (percent >= 100) {
                    return true;
                }
                $marker.html('<div class=\'marker-inner\'></div>');
                $marker.attr('data-title', track.title);
                $marker.css('left', percent + '%');
                htmlContents += $marker.prop('outerHTML');
            });

            this._$markers.html(htmlContents);
        };

        view.hideMarker = function() {
            this._$markers.empty();
        };

        view.onMarkerHover = function(e) {
            var $target = module.$(e.currentTarget),
                $wrapper = $target.closest('.trackmarks'),
                $bubble;
            $wrapper.find('.title-bubble').remove();

            $bubble = module.$('<div class=\'title-bubble\'></div>');
            $bubble.append('<div class=\'title\'>' + $target.data('title') + '</div>');
            $bubble.append('<div class=\'carrot\'></div>');
            $bubble.css('left', $target.css('left'));
            $wrapper.append($bubble);
        };

        view.onMarkerUnhover = function(e) {
            var $target = module.$(e.currentTarget),
                $wrapper = $target.closest('.trackmarks');
            $wrapper.find('.title-bubble').remove();
        };

        view.remove = function() {
            this._$closeBtn.off('click', this.onTabCloseBtnClicked);
            this._$minimizeBtn.off('click', this.onMinimizeBtnClicked);
            this._$showBtn.off('click', this.onShowBtnClicked);

            this._$el.off('mouseenter',
                '.music-title-scroll-wrapper', this.onTrackInfoHover);
            this._$el.off('mouseleave',
                '.music-title-scroll-wrapper', this.onTrackInfoUnhover);
            this._$markers.off('mouseenter', '.marker', this.onMarkerHover);
            this._$markers.off('mouseleave', '.marker', this.onMarkerUnhover);

            module.$(module).off('playInfoUpdate', this.whenPlayInfoUpdate);
            module.$(module).off('playMusicFirst', this.whenPlayMusic);
            module.$(module).off('loadingMusic', this.whenLoadingMusic);
            module.$(module).off('finishedMusic', this.whenFinishedMusic);
            module.$(module).off('musicProgressUpdate', this.whenProgressUpdate);
            if (this._tracklistReq) {
                this._tracklistReq.abort();
                this._tracklistReq = null;
            }

            PlayableViewMixin.unbind(this);
        };

        return view;
    }());

    DropbeatPageView.prototype.subViews.playerView = (function() {
        var view = {},
            statusText = {
                onInitial: 'Choose track',
                onEnd: 'Stopped',
                onLoading: 'Loading',
                onPlaying: 'Now playing',
                onPaused: 'Paused'
            };

        view.init = function() {
            var player;

            module._.bindAll(this, 'onPlaylistShowBtnClicked', 'onPlayInfoUpdate',
                'onLoadingMusic', 'onTogglePlay', 'onStopMusic', 'onPlayMusic',
                'onPlaylistChanged', 'onExtraBtnClicked', 'onAddBtnClicked',
                'onPlayCtrlClicked', 'onPrevCtrlClicked', 'onNextCtrlClicked',
                'onKeydown', 'onShareBtnClicked', 'onPlayerFilterClicked',
                'onLikeBtnClicked', 'updateLikeBtn', 'onMenuBtnClicked');

            this._$playerFilter = module.$('.player-filter');
            this._$playlist = module.$('.playlist-section');
            this._$cover = module.$('#player .cover-wrapper .cover');
            this._$coverWrapper = module.$('#player .cover-wrapper');
            this._$playerTitle = module.$('#player .title');
            this._$trackInfo = module.$('#player .track-info');
            this._$playerContextWrapper = module.$('#player .track-context');
            this._$playerContext = module.$('#player .context-name');
            this._$playerStatus = module.$('#player .status');
            this._$ctrlPlayBtn = module.$('#player .ctrl-play');
            this._$ctrlNextBtn = module.$('#player .ctrl-next');
            this._$ctrlPrevBtn = module.$('#player .ctrl-prev');
            this._$ctrlLikeBtn = module.$('#player .ctrl-like');
            this._$ctrlAddBtn = module.$('#player .ctrl-add-to-playlist');
            this._$ctrlShareBtn = module.$('#player .ctrl-share');
            this._$menuClickableBtns = module.$('#player .extra-menus a.menu');
            this._$menuLikeBtn = module.$('#player .extra-menus .like-menu');
            this._$menuAddBtn = module.$('#player .extra-menus .add-menu');
            this._$menuShareBtn = module.$('#player .extra-menus .share-menu');
            this._$playLoadingFilter = module.$('.player-loading-filter');
            this._$playlistBtn = module.$('.btn-show-playlist');
            this._$extraBtn = module.$('#player .controls .extra-btn');
            this._$extraMenu = module.$('#player .controls .extra-menus');
            this._$playbackTime = module.$('#player .curr-play-time');
            this._$playlistChangeInfo = module.$('.playlist-change-info',
                this._$playlistBtn);

            this._$playerFilter.on('click', this.onPlayerFilterClicked);
            this._$playlistBtn.on('click', this.onPlaylistShowBtnClicked);
            this._$extraBtn.on('click', this.onExtraBtnClicked);
            this._$ctrlNextBtn.on('click', this.onNextCtrlClicked);
            this._$ctrlPrevBtn.on('click', this.onPrevCtrlClicked);
            this._$ctrlPlayBtn.on('click', this.onPlayCtrlClicked);
            this._$ctrlAddBtn.on('click', this.onAddBtnClicked);
            this._$menuAddBtn.on('click', this.onAddBtnClicked);
            this._$ctrlShareBtn.on('click', this.onShareBtnClicked);
            this._$menuShareBtn.on('click', this.onShareBtnClicked);
            this._$ctrlLikeBtn.on('click', this.onLikeBtnClicked);
            this._$menuLikeBtn.on('click', this.onLikeBtnClicked);
            this._$menuClickableBtns.on('click', this.onMenuBtnClicked);

            this._flippedTimer = null;
            player = module.playerManager.currentPlayer;
            if (player && player.section !== 'player_tracklist' &&
                    player.section !== 'drop') {
                this._lastPlayMusic = module.playerManager.currentMusic;
                this._lastPlaySection = player.section;
            } else {
                this._lastPlayMusic = null;
                this._lastPlaySection = null;
            }

            module.$(module).on('playerStopped', this.onStopMusic);
            module.$(module).on('playInfoUpdate playMusicFirst', this.onPlayInfoUpdate);
            module.$(module).on('togglePlay', this.onTogglePlay);
            module.$(module).on('loadingMusic', this.onLoadingMusic);
            module.$(module).on('playMusicFirst', this.onPlayMusic);
            module.$(module).on('playlistChanged', this.onPlaylistChanged);
            module.$(module).on('trackLiked trackDisliked', this.updateLikeBtn);

            module.$(document).on('keydown', this.onKeydown);

            if (module.context.account) {
                this.showPlayer();
            }
        };

        view.onPlayerFilterClicked = function(e) {
            var $filter = module.$(e.currentTarget),
                videoModule = module.player.control.video;

            if (!$filter.hasClass('clickable')) {
                return false;
            }
            module.$(videoModule.control.elems.videoToggle).click();
        };

        view.onKeydown = function(e) {
            var $target = module.$(e.target),
                $inputParent = $target.closest('input,textarea'),
                keyCode = e.keyCode,
                volume,
                playerManager = module.playerManager,
                seekTo,
                totalPlayback,
                currPlayback,
                videoControl;

            if ($inputParent.size() > 0) {
                return;
            }
// Case space
            if (keyCode === 32) {
                if (playerManager.currentPlayer &&
                        playerManager.currentPlayer.section !== 'drop') {
                    playerManager.onPlayMusic();
                    return false;
                }
                return;
            }
// Case up
            if (keyCode === 38) {
                volume = module.s.volumeControl.volume;
                volume = Math.min(volume + 20, 100);
                module.s.volumeControl.updateVolume(volume);
                return false;
            }
// Case down
            if (keyCode === 40) {
                volume = module.s.volumeControl.volume;
                volume = Math.max(volume - 20, 0);
                module.s.volumeControl.updateVolume(volume);
                return false;
            }
// Case >
            if (keyCode === 39) {
                totalPlayback = playerManager.getTotalPlaybackTime();
                currPlayback = playerManager.getCurrentPlaybackTime();
                seekTo = currPlayback + 10;
                if (totalPlayback > 0 && playerManager.playing &&
                        playerManager.currentPlayer &&
                        playerManager.currentPlayer.section !== 'drop') {
                    if (totalPlayback < seekTo) {
                        seekTo = totalPlayback;
                    }
                    playerManager.seekTo(seekTo);
                    return false;
                }
                return;
            }
// Case <
            if (keyCode === 37) {
                totalPlayback = playerManager.getTotalPlaybackTime();
                currPlayback = playerManager.getCurrentPlaybackTime();
                seekTo = currPlayback - 10;
                if (totalPlayback > 0 && playerManager.playing &&
                        playerManager.currentPlayer &&
                        playerManager.currentPlayer.section !== 'drop') {
                    if (seekTo < 0) {
                        seekTo = 0;
                    }
                    playerManager.seekTo(seekTo);
                    return false;
                }
                return;
            }
// Case esc
            if (keyCode === 27) {
// close playlist first
                if (!module.$('.playlist-section').hasClass('folded')) {
                    module.$('.bg-body-filter').click();
                    return false;
                }
                videoControl = module.s.playerControl.video;
                if (videoControl.control.state === videoControl.state.exposed) {
                    videoControl.control.hideVideo();
                }
            }
        };

        view.onMenuBtnClicked = function(e) {
            var $el = module.$(e.currentTarget);

            if ($el.hasClass('disabled')) {
                return false;
            }
            module.$('.overlay-filter').click();
        };

        view.onExtraBtnClicked = function(e) {
            var that = this;
            module.$('.overlay-filter').one('click', function() {
                that._$extraMenu.hide();
            }).show();
            module.s.playerControl.volume.control.onShowVolumeCtrl();
            this._$extraMenu.show();
        };

        view.onAddBtnClicked = function(e) {
            var music,
                playlist,
                success;

            if (module.$(e.currentTarget).hasClass('disabled')) {
                return;
            }

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            music = module.playerManager.currentMusic;
            if (!music) {
                return;
            }

            playlist = module.playlistManager.getSelectedPlaylist();
            success = playlist.add(music, true);

            if (success) {
                module.playlistManager.getSelectedPlaylist().sync();
                module.log(
                    'playlist-add-from-player',
                    'add-' + music.type,
                    music.title);
                module.coreLog('trackAdd', music.title);

                module.$(module).trigger('playlistChanged', {
                        title: 'Track added to',
                        desc: playlist.name
                });
            }
        };

        view.onShareBtnClicked = function(e) {
            var music;

            if (module.$(e.currentTarget).hasClass('disabled')) {
                return;
            }

            music = module.playerManager.currentMusic;
            if (!music) {
                return;
            }

            module.view.ShareSingleTrackView.show(music, 'player');
        };

        view.onLikeBtnClicked = function(e) {
            var music,
                that = this,
                isLiked;

            if (module.$(e.currentTarget).hasClass('disabled')) {
                return;
            }

            music = module.playerManager.currentMusic;
            if (!music) {
                return;
            }

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            isLiked = module.context.account.isLiked(music);

            if (isLiked) {
                module.context.account.dislike(music, function(success, msg) {
                    var errorMsg;

                    that.updateLikeBtn();

                    if (!success) {
                        errorMsg = module.string('failed_to_update_like');
                        module.s.notify(errorMsg, 'error');
                        return;
                    }
                    module.notification.notify(module.string('track_disliked'), 'success');
                });
            } else {
                module.context.account.like(music, function(success, msg) {
                    var errorMsg;

                    that.updateLikeBtn();

                    if (!success) {
                        errorMsg = module.string('failed_to_update_like');
                        module.s.notify(errorMsg, 'error');
                        return;
                    }
                    module.notification.notify(module.string('track_liked'), 'success');
                });
            }
        };

        view.onPrevCtrlClicked = function(e) {
            var $ctrl = module.$(e.currentTarget);
            if (!$ctrl.hasClass('disabled')) {
                module.playerManager.back();
                module.log('player', 'click-prev');
            }
        };

        view.onNextCtrlClicked = function(e) {
            var $ctrl = module.$(e.currentTarget);
            if (!$ctrl.hasClass('disabled')) {
                module.playerManager.forth();
                module.log('player', 'click-next');
            }
        };

        view.onPlayCtrlClicked = function(e) {
            var $ctrl = module.$(e.currentTarget),
                currentPlayer = module.playerManager.currentPlayer,
                timeStr,
                timeArray,
                time;
            if (!currentPlayer || $ctrl.hasClass('disabled') ||
                    !this._lastPlaySection) {
                return;
            }
            if (currentPlayer.section !== this._lastPlaySection) {
                timeStr = this._$playbackTime.text();
                timeArray = timeStr.split(':');
                time = parseInt(timeArray[0]) * 60 + parseInt(timeArray[1]);
            }

            module.playerManager.onPlayMusic(this._lastPlayMusic,
                this._lastPlaySection, time);
        };

        view.onPlaylistChanged = function(e, info) {
            var that = this,
                $info;

            this.updateAddBtn();

            $info = module.$('<div class=\'info\'></div>');

            if (this._flippedTimer) {
                clearTimeout(this._flippedTimer);
                this._flippedTimer = null;
            }

            $info.append('<div class=\'title\'>' + info.title + '</div>');
            $info.append('<div class=\'desc\'>' + info.desc + '</div>');
            this._$playlistChangeInfo.html($info);

            if (!this._$playlistBtn.hasClass('flipped')) {
                this._$playlistBtn.addClass('flipped')
            }
            this._flippedTimer = setTimeout(function() {
                that._$playlistBtn.removeClass('flipped')
                that._flippedTimer = null;
            }, 2000);
        };

        view.onStopMusic = function(e) {
            this.updateAddBtn();
            this.updateShareBtn();
            this.updateLikeBtn();
            this._$playerTitle.text(statusText.onInitial);
            this._$playerStatus.text(statusText.onEnd);
            this._$trackInfo.addClass('without-context');
            this._$ctrlPlayBtn.removeClass('pause').addClass('disabled');
            this._$ctrlPrevBtn.addClass('disabled');
            this._$ctrlNextBtn.addClass('disabled');
            this._$ctrlAddBtn.addClass('disabled');
            this._$menuAddBtn.addClass('disabled');
        };

        view.onPlayMusic = function(e, music, section) {
            if (section === 'drop' || section === 'player_tracklist') {
                return;
            }
            showDrop(module.$('#player'), music, music.drop);
        };

        view.showPlayer = function() {
            var $dropbeat,
                that = this;

            $dropbeat = module.$('#dropbeat');
            if ($dropbeat.hasClass('hide-player') && !this._isPlayerAnimating) {
                $dropbeat.find('.player-section').show()
                    .animate({
                        bottom:0
                    }, 200, function() {
                        $dropbeat.removeClass('hide-player');
                        delete that._isPlayerAnimating;
                    });
            }
        };

        view.onPlayInfoUpdate = function(e, music, section) {
            var playlist;

            if (section === 'drop' || section === 'player_tracklist') {
                return;
            }

            this.showPlayer();

            this._lastPlayMusic = music;
            this._lastPlaySection = section;
            this._$playerTitle.text(music.title);
            this._$playerStatus.text(statusText.onPlaying);
            this._$trackInfo.removeClass('without-context');
            this._$playLoadingFilter.hide();

            if (music.type === 'youtube') {
                this._$coverWrapper.hide();
                this._$cover.hide();
                module.s.playerBase.showYTVideo();
            } else {
                this._$coverWrapper.show();
                module.s.playerBase.hideYTVideo();
                if (music.artwork) {
                    this._$cover.attr('src', music.artwork);
                    this._$cover.show();
                } else {
                    this._$cover.hide();
                }
            }


            if (module.playlistManager.playingPlaylistId) {
                playlist = module.playlistManager.getPlaylist(
                        module.playlistManager.playingPlaylistId);
                if (playlist) {
                    this._$playerContext.text(playlist.name);
                }
                return;
            }
            if (section === 'search') {
                this._$playerContext.text('from search results');
            } else if (section === 'channel') {
                this._$playerContext.text('channel');
            } else if (section === 'shared_track') {
                this._$playerContext.text('shared track');
            } else {
                this._$playerContext.text('');
            }
        };

        view.updateAddBtn = function() {
            var player = module.playerManager.currentPlayer,
                playlist = module.playlistManager.getSelectedPlaylist(),
                music = module.playerManager.currentMusic,
                isAddable;

            isAddable = player &&
                    (player.section !== 'base' ||
                        playlist && music &&
                        playlist.findIdx(music.id) === -1);

            if (isAddable) {
                this._$ctrlAddBtn.removeClass('disabled');
                this._$menuAddBtn.removeClass('disabled');
            } else {
                this._$ctrlAddBtn.addClass('disabled');
                this._$menuAddBtn.addClass('disabled');
            }
        };

        view.updateShareBtn = function() {
            var music = module.playerManager.currentMusic;

            if (music) {
                this._$ctrlShareBtn.removeClass('disabled');
                this._$menuShareBtn.removeClass('disabled');
            } else {
                this._$ctrlShareBtn.addClass('disabled');
                this._$menuShareBtn.addClass('disabled');
            }
        };

        view.updateLikeBtn = function() {
            var music = module.playerManager.currentMusic;

            if (music) {

                this._$ctrlLikeBtn.removeClass('disabled');
                this._$menuLikeBtn.removeClass('disabled');

                if (module.context.account &&
                        module.context.account.isLiked(music)) {
                    this._$ctrlLikeBtn.addClass('liked');
                    this._$menuLikeBtn.addClass('liked');
                    this._$menuLikeBtn.find('.text').text('Liked');
                } else {
                    this._$ctrlLikeBtn.removeClass('liked');
                    this._$menuLikeBtn.removeClass('liked');
                    this._$menuLikeBtn.find('.text').text('Like');
                }
            } else {
                this._$ctrlLikeBtn.addClass('disabled').removeClass('liked');
                this._$menuLikeBtn.addClass('disabled').removeClass('liked');
                this._$menuLikeBtn.find('.text').text('Like');
            }
        };

        view.onLoadingMusic = function(e, music, section) {
            var $drop = module.$('#player .drop');
            $drop.hide();
            if (section === 'drop' || section === 'player_tracklist') {
                return;
            }

            this.updateAddBtn();
            this.updateShareBtn();
            this.updateLikeBtn();

            this._$playerTitle.text(statusText.onLoading);
            this._$playerStatus.text(statusText.onLoading);
            this._$trackInfo.addClass('without-context');
            if (music.type === 'youtube') {
                this._$playLoadingFilter.hide();
                this._$coverWrapper.hide();
                this._$cover.hide();
                module.s.playerBase.showYTVideo();
            } else {
                this._$playLoadingFilter.show();
                this._$coverWrapper.show();
                module.s.playerBase.hideYTVideo();
                if (music.artwork) {
                    this._$cover.attr('src', music.artwork);
                    this._$cover.show();
                } else {
                    this._$cover.hide();
                }
            }
        };

        view.onTogglePlay = function(e, music, section, isPlaying) {
            this.updateAddBtn();
            this.updateShareBtn();
            this.updateLikeBtn();
            module.s.playerControl.base.updatePlayButton(true);
            if (isPlaying) {
                if (section === 'drop') {
                    this.onStopMusic();
                    return;
                } else if (section === 'player_tracklist') {
                    this.setPause();
                    return;
                }
                this.setPlay();
            } else {
                this.setPause();
            }
        };

        view.setPlay = function () {
            this._$ctrlPlayBtn.addClass('pause');
            if (this._$playerStatus.text() === statusText.onPaused) {
                this._$playerStatus.text(statusText.onPlaying);
            }
        };

        view.setPause = function () {
            this._$ctrlPlayBtn.removeClass('pause');
            this._$playerStatus.text(statusText.onPaused);
        };

        view.onPlaylistShowBtnClicked = function(e) {
            var that = this;
            if (this._$playlist.hasClass('folded')) {
                this._$playlist.css('display', 'block');
                setTimeout(function() {
                    that._$playlist.removeClass('folded');
                }, 0);
                module.$('.bg-body-filter').one('click', function() {
                    that._$playlist.addClass('folded');
                    setTimeout(function() {
                        that._$playlist.css('display', 'none');
                    }, 150);
                }).addClass('exposed');
            } else {
                module.$('.bg-body-filter').click();
            }
        };

        view.remove = function() {
            this._$playerFilter.off('click', this.onPlayerFilterClicked);
            this._$playlistBtn.off('click', this.onPlaylistShowBtnClicked);
            this._$extraBtn.off('click', this.onExtraBtnClicked);
            this._$ctrlNextBtn.off('click', this.onNextCtrlClicked);
            this._$ctrlPrevBtn.off('click', this.onPrevCtrlClicked);
            this._$ctrlPlayBtn.off('click', this.onPlayCtrlClicked);
            this._$ctrlAddBtn.off('click', this.onAddBtnClicked);
            this._$menuAddBtn.off('click', this.onAddBtnClicked);
            this._$ctrlShareBtn.off('click', this.onShareBtnClicked);
            this._$menuShareBtn.off('click', this.onShareBtnClicked);
            this._$ctrlLikeBtn.off('click', this.onLikeBtnClicked);
            this._$menuLikeBtn.off('click', this.onLikeBtnClicked);
            this._$menuClickableBtns.off('click', this.onMenuBtnClicked);

            module.$(module).off('playerStopped', this.onStopMusic);
            module.$(module).off('playInfoUpdate playMusicFirst', this.onPlayInfoUpdate);
            module.$(module).off('togglePlay', this.onTogglePlay);
            module.$(module).off('playMusicFirst', this.onPlayMusic);
            module.$(module).off('loadingMusic', this.onLoadingMusic);
            module.$(module).off('playlistChanged', this.onPlaylistChanged);
            module.$(module).off('trackLiked trackDisliked', this.updateLikeBtn);

            module.$(document).off('keydown', this.onKeydown);
        };

        return view;
    }());

    module.view.headerView = {
        init: function () {
            module._.bindAll(this, 'onSearchMenuClicked');
            this.$el = module.$('header');
            this.$el.on('click', '.search-menu-expose-btn', this.onSearchMenuClicked);
            this.$searchInputWrapper = module.$('.search-input-field', this.$el);
            this.$searchInput = module.$('.search-input', this.$searchInputWrapper);
        },
        remove: function () {
            this.$el.off('click', '.search-menu-expose-btn', this.onSearchMenuClicked);
        },
        onSearchMenuClicked: function () {
            return;
            if (module.$('header .search-input-field').is(':visible')) {
                this.$searchInput.focus();
            } else {
                module.router.navigate('/?q=_blank', {trigger: true});
            }
        }
    };

    module.view.accountMenuView = {
        init: function() {
            var tmpl = module.$('#tmpl-account-menu').html(),
                tmplCompiler = module._.template(tmpl);

            module._.bindAll(this, 'openMenu', 'doSignout',
                    'showLegalPage', 'showFeedback', 'doSignin',
                    'onClickWindowMenu');

            this.$el = module.$('.account-menus');
            this.$el.html(tmplCompiler());
            this.$el
                .on('click', '.menu-btn', this.openMenu)
                .on('click', '.menu-window .menu', this.onClickWindowMenu)
                .on('click', '.signout-btn', this.doSignout)
                .on('click', '.signin-btn', this.doSignin)
                .on('click', '.legal-btn', this.showLegalPage)
                .on('click', '.feedback-btn', this.showFeedback);
            this.$menuWindow = module.$('.menu-window', this.$el);
            this.$menuBtn = module.$('.menu-btn', this.$el);
        },
        openMenu: function() {
            if (!this.$menuWindow.is(':visible')) {
                module.$('.overlay-filter').show()
                    .one('click', module.$.proxy(this.closeMenu, this));
                this.$menuBtn.addClass('opened');
                this.$menuWindow.show();
            }
        },
        closeMenu: function() {
            if (this.$menuWindow.is(':visible')) {
                this.$menuBtn.removeClass('opened');
                this.$menuWindow.hide();
            }
        },
        onClickWindowMenu: function() {
            this.closeMenu();
        },
        doSignout: function() {
            module.view.ConfirmView.show({
                title: module.string('are_you_sure'),
                text: module.string('are_you_sure_logout'),
                confirmText: module.string('yes_logout'),
                closeText: module.string('not_now')
            }, function(isConfirmed) {
                if (!isConfirmed) {
                    return;
                }
                module.$.get($dbt.api.signoutUrl).done(function(resp) {
                    window.location = '/';
                });
            });
            this.closeMenu();
        },
        doSignin: function() {
            this.closeMenu();
        },
        showLegalPage: function() {
            module.view.LegalView.show();
            this.closeMenu();
            module.log('global-menu', 'click-legal');
        },
        showFeedback: function() {
            this.closeMenu();
            module.view.FeedbackView.show();
            module.log('global-menu', 'click-feedback');
        },
        remove: function() {
            if (this.$el) {
                this.$menuBtn.removeClass('opened');
                this.$menuWindow.hide();
                this.$el.off('click', '.menu-btn', this.openMenu)
                    .off('click', '.menu-window .menu', this.onClickWindowMenu)
                    .off('click', '.signin-btn', this.doSignin)
                    .off('click', '.signout-btn', this.doSignout)
                    .off('click', '.legal-btn', this.showLegalPage)
                    .off('click', '.feedback-btn', this.showFeedback);
            }
        }
    };

    module.view.playlistMenuView = {
        init: function() {
            var $playlist = module.$('.playlist-section .playlist'),
                $searchMenuSection = module.$('.playlist-section .playlist-search'),
                $addByUrlMenuSection = module.$('.playlist-section .add-by-url-section');

            module.$('.playlist-section .playlist-menu-search').on(
                'click', function() {
                    if (module.$(this).hasClass('opened')) {
                        module.$(this).removeClass('opened');
                        $searchMenuSection.removeClass('opened');
                        $playlist.removeClass('search-menu-opened');
                    } else {
                        module.$(this).addClass('opened');
                        $searchMenuSection.addClass('opened');
                        $playlist.addClass('search-menu-opened');
                    }
                });

            module.$('.playlist-section .playlist-menu-add-by-url').on(
                'click', function() {
                    if (module.$(this).hasClass('opened')) {
                        module.$(this).removeClass('opened');
                        $addByUrlMenuSection.removeClass('opened');
                        $playlist.removeClass('add-by-url-menu-opened');
                    } else {
                        module.$(this).addClass('opened');
                        $addByUrlMenuSection.addClass('opened');
                        $playlist.addClass('add-by-url-menu-opened');
                    }
                });
        }
    };

    module.view.ThisBrowserIsNotSupportedView = (function() {
        var PageView = DropbeatView.extend({
            tmpl: '#tmpl-this-browser-is-not-supported',
            initialize: function() {
                var tmpl = module._.template(module.$(this.tmpl).html());
                module.$('#dropbeat').html(tmpl());
                module.view.initContentsBg();
            }
        });
        return {
            show: function() {
                this.view = new PageView();
            }
        };
    }());

    module.view.mobilePlaylistView = (function() {
        var view = {}
        view.init = function() {
            var that = this;

            this._$playerSection = module.$('.player-section');
            this._$player = module.$('#player');
            this._$playControls = module.$('.play-controls');
            this._$btn = module.$('.player-section .mobile-playlist-show-btn');
            this._$inverseTotalPlayTime =
                module.$('.play-controls .inverse-play-times .total-play-time');
            this._$closePlaylistBtn = module.$('.play-controls > .close-button');
            this._$logo = module.$('.header-section .logo');

            module._.bindAll(this, 'toggle');

            this._$btn.on('click', this.toggle);
            this._$closePlaylistBtn.on('click', function() {
                var $ctrl = module.$(this).closest('.play-controls');
                if (!$ctrl.hasClass('expanded')) {
                    return;
                }
                that.hide();
            });
            this._$logo.on('click', function() {
// click both playlist close button & playlist manage button
                module.$('.play-controls .close-button').click();
            });

            this._$inverseTotalPlayTime
                .css('width', module.$(window).width());
            module.$(window).on('resize', module.$.proxy(function() {
                this._$inverseTotalPlayTime
                    .css('width', module.$(window).width());
            }, this));
        };

        view.toggle = function() {
            if (this._$playerSection.hasClass('expanded')) {
                this.hide();
                this._$btn.removeClass('opened');
            } else {
                this.show();
                this._$btn.addClass('opened');
            }
        };

        view.show = function() {
            this._$playerSection.switchClass('', 'expanded', 0);
            this._$playControls.switchClass(
                '', 'expanded', module.context.isMobile ? 0 : 300);
        };

        view.hide = function() {
            this._$playerSection.switchClass('expanded', '', 0);
            this._$playControls.switchClass(
                'expanded', '', module.context.isMobile ? 0 : 300);
        };

        return view;
    }());



////////////
// Mixins //
////////////

    WaveformViewMixin = {
        bind: function(context, $el) {
            module.$.extend(context, this);
            context.bindWaveformRenderer($el);
        },

        unbind: function(context) {
            if (context.unbindWaveformRenderer) {
                context.unbindWaveformRenderer();
            }
        },

        bindWaveformRenderer: function($el) {
            module._.bindAll(this, '_onWindowResize', '_onWindowScroll',
                '_onMusicProgressUpdate', '_onWaveformProgressDown',
                '_onWaveformProgressMove', '_onWaveformProgressUp',
                '_onWaveformLoadingMusic');

            this.waveformRawData = {};
            this.waveformData = {};
            this.waveformReq = {};
            this._musicContainer = '.a-addable-music';
            this._playSections = this._playSections || [];
            this._$playableFrame = $el;

            module.$(module).on('loadingMusic', this._onWaveformLoadingMusic);
            module.$(module).on('musicProgressUpdate', this._onMusicProgressUpdate);
            this._$playableFrame.on('mousedown', '.waveform-progress', this._onWaveformProgressDown);
            module.$('.contents .scroll-y').on('scroll', this._onWindowScroll);

            module.$(window).on('resize', this._onWindowResize);
            module.$(window).on('mousemove', this._onWaveformProgressMove);
            module.$(window).on('mouseup', this._onWaveformProgressUp);
        },

        unbindWaveformRenderer: function() {
            var that = this;

            module.$(module).off('loadingMusic', this._onWaveformLoadingMusic);
            module.$(module).off('musicProgressUpdate', this._onMusicProgressUpdate);
            this._$playableFrame.off('mousedown', '.waveform-progress', this._onWaveformProgressDown);
            module.$('.contents .scroll-y').off('scroll', this._onWindowScroll);

            module.$(window).off('resize', this._onWindowResize);
            module.$(window).off('mousemove', '.waveform-progress', this._onWaveformProgressMove);
            module.$(window).off('mouseup', '.waveform-progress', this._onWaveformProgressUp);

            module.$.each(this.waveformReq, function(key, req) {
                req.abort();
                delete that.waveformReq[key];
            });

            module.$.each(this.waveformData, function(key, val) {
                delete that.waveformData[key];
            });

            module.$.each(this.waveformRawData, function(key, val) {
                delete that.waveformRawData[key];
            });
        },

        _onWindowScroll: function(e) {
            var $scroll = module.$('.contents .scroll-y'),
                scrollTop = $scroll.scrollTop(),
                THRESHOLD = 100;

            if  (this._lastScroll && Math.abs(this.lastScroll - scrollTop) < THRESHOLD) {
                return;
            }
            this._lastScroll = scrollTop;
            this.updateWaveform();
        },

        _onWindowResize: function(e) {
            var $waveform = module.$('.waveform-progress', this._$playableFrame),
                $playingWaveform,
                $progress,
                percent,
                that = this;

// Only update waveform width changed
            if (this._waveformWidth && $waveform.width() === this._waveformWidth) {
                return;
            }

// Re calculate bin size
            module.$.each(this.waveformRawData, function(url, data) {
                var $el = module.$('.waveform-progress[data-url=\'' + url + '\']', that._$playableFrame);
                that.waveformData[url] = that._calculateBars($el, data);
            });
            $waveform.removeClass('loaded');
            this.updateWaveform(true);

// Update progressing waveform
            $progress = $waveform.find('.progress .waveform-canvas');
            if ($progress.size() > 0 && (percent = $progress.data('percent'))) {
                $playingWaveform = $progress.closest('.waveform-progress');
                this._updateWaveformProgress($playingWaveform, parseInt(percent));
            }
        },

        _calculateBars: function($el, data) {
            var barCount,
                barWidth = 2,
                marginWidth = 1,
                width = $el.width();

            barCount = Math.floor(width / (barWidth + marginWidth));
            if (barCount === 0) {
                return [];
            }

            return module.utils.downsizeArray(data, barCount);
        },

        updateWaveform: function() {
            var that = this,
                $waveform = module.$('.waveform-progress', this._$playableFrame),
                $scroll = module.$('.contents .scroll-y'),
                windowHeight = module.$(window).height();

            if ($waveform.size() === 0) {
                return;
            }

            this._waveformWidth = $waveform.width();

            $waveform.each(function(idx, el) {
                var $el = module.$(el),
                    $container = $el.closest(that._musicContainer),
                    url = $el.data('url'),
                    nonSecureUrl,
                    data,
                    offsetTop = $container.offset().top,
                    containerHeight = $container.height();

                if (that.waveformReq[url]) {
                    return;
                }

                if (offsetTop + containerHeight < -100 || offsetTop > windowHeight + 100) {
                    $el.hide();
                    return;
                } else {
                    $el.show();
                }
                if ($el.hasClass('loaded') || $el.hasClass('failed')) {
                    return;
                }

                data = that.waveformData[url];
                if (data) {
                    renderWaveform($el, data);
                    return;
                }

// XXX:
// In case IE9, ajax request through http -> https will be blocked by browser
// So our waveform request which is to AWS will be also blocked cause its https
// But thanksfully AWS supports normal http request,
// we will replace request url to http
                if (url.startsWith('https://')) {
                    nonSecureUrl = url.replace('https://', 'http://');
                }
                that.waveformReq[url] = module.$.ajax({
                    url: nonSecureUrl || url,
                    dataType:'json',
                    crossDomain: true
                }).done(function(data) {
                    that.waveformRawData[url] = data;
                    that.waveformData[url] = that._calculateBars($el, data);
                    renderWaveform($el, that.waveformData[url]);
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus === 'abort') {
                        return;
                    }
                    that.waveformData[url] = [];
                    $el.addClass('failed');
                }).always(function() {
                    delete that.waveformReq[url];
                });
            });

            function renderWaveform($el, bars) {
                var $bgCanvas,
                    bgCanvas,
                    $drop = $el.find('.drop'),
                    $dropCanvas,
                    dropCanvas,
                    width = $el.width(),
                    height = $el.height(),
                    barWidth = 2,
                    marginWidth = 1,
                    colorNormal = '#636365',
                    //colorDrop = '#cd85ff',
                    colorDrop = '#f03eb1',
                    duration,
                    dropStart,
                    dropEnd,
                    fromIdx,
                    toIdx,
                    i,
                    fromX,
                    toX,
                    context,
                    val, x, barHeight, y, delta, w;

                $bgCanvas = module.$('.bg .waveform-canvas', $el);
                if ($bgCanvas.size() === 0) {
                    $bgCanvas = module.$('<canvas class=\'waveform-canvas\'></canvas>');
                    $el.find('.bg').html($bgCanvas);
                }
                bgCanvas = $bgCanvas[0];

                $dropCanvas = module.$('.drop .waveform-canvas', $el);
                if ($dropCanvas.size() === 0) {
                    $dropCanvas = module.$('<canvas class=\'waveform-canvas\'></canvas>');
                    $el.find('.drop').html($dropCanvas);
                }
                $drop.html($dropCanvas);

                bgCanvas.width = width;
                bgCanvas.height = height;

                context = bgCanvas.getContext('2d');
                context.clearRect(0, 0, bgCanvas.width, bgCanvas.height);

                module.$.each(bars, function(idx, val) {

                    var x, barHeight, y;
                    x = idx * (barWidth + marginWidth);
                    barHeight = val * height * 0.009;
                    y = height - barHeight;
                    context.fillStyle = colorNormal;
                    context.fillRect(x, y, barWidth, barHeight);
                });
                context.save();

                if ($drop.size() > 0) {

                    duration = $el.data('duration');
                    dropStart = $el.find('.drop').data('dropStart');
                    dropEnd = $el.find('.drop').data('dropEnd');
                    fromX = Math.round((dropStart / duration) * width);
                    toX = Math.round((dropEnd / duration) * width);
                    fromIdx = Math.floor(fromX / (barWidth + marginWidth));
                    toIdx = Math.ceil(toX / (barWidth + marginWidth));

                    dropCanvas = $dropCanvas[0];
                    dropCanvas.width = width;
                    dropCanvas.height = height;

                    context = dropCanvas.getContext('2d');
                    context.globalAlpha=0.8;
                    context.clearRect(0, 0, dropCanvas.width, dropCanvas.height);

                    for(i = fromIdx; i < bars.length && i < toIdx; i++) {

                        val = bars[i];
                        x = i * (barWidth + marginWidth);
                        barHeight = val * height * 0.009;
                        y = height - barHeight;
                        context.fillStyle = colorDrop;
                        w = barWidth;
                        if (i === fromIdx) {
                            delta = x;
                            x = Math.max(x, fromX);
                            delta = x - delta;
                            w -= delta;
                        }
                        if (i === toIdx - 1) {
                            delta = Math.min(x + w, toX) - x;
                            w = delta;
                        }
                        context.fillRect(x, y, w, barHeight);
                    }
                }
                $el.addClass('loaded');
            }
        },

        _updateWaveformProgress: function($el, progress) {
            var $canvas = $el.find('.progress .waveform-canvas'),
                canvas,
                width = $el.width(),
                height = $el.height(),
                barWidth = 2,
                marginWidth = 1,
                colorNormal = '#ffffff',
                bars,
                i,
                toX,
                toIdx,
                context,
                url = $el.data('url'),
                val, x, barHeight, y, delta, w;

            if (!this.waveformData[url]) {
                return;
            }

            bars = this.waveformData[url];

            toX = Math.round((progress / 100) * width);
            toIdx = Math.ceil(toX / (barWidth + marginWidth));

            if ($canvas.size() === 0) {
                $el.find('.progress').html('<canvas class=\'waveform-canvas\'></canvas>');
                $canvas = $el.find('.progress .waveform-canvas');
            }
            $canvas.data('percent', progress);
            canvas = $canvas[0];

            canvas.width = width;
            canvas.height = height;
            context = canvas.getContext('2d');
            context.clearRect(0, 0, canvas.width, canvas.height);

            for(i = 0; i < bars.length && i < toIdx; i++) {

                val = bars[i];
                x = i * (barWidth + marginWidth);
                barHeight = val * height * 0.009;
                y = height - barHeight;
                context.fillStyle = colorNormal;
                w = barWidth;
                if (i === toIdx - 1) {
                    delta = Math.min(x + w, toX) - x;
                    w = delta;
                }
                context.fillRect(x, y, w, barHeight);
            }
        },

        _onWaveformProgressDown: function(e) {
            var playerManager = module.playerManager,
                currentMusic = playerManager.currentMusic,
                $el = module.$(e.currentTarget),
                offsetLeft, x, left, width;

            if (!currentMusic ||
                    String($el.closest(this._musicContainer).data('musicId')) !==  currentMusic.id ||
                    !playerManager.playing ||
                    playerManager.currentPlayer.section === 'drop') {
                return;
            }
            this._waveformProgressTarget = $el;
            this._waveformProgressDown = true;
            if (this._seekTimer) {
                clearTimeout(this._seekTimer);
                this._seekTimer = null;
            }

            offsetLeft = $el.offset().left;
            width = $el.width();
            x = e.pageX - offsetLeft;
            left = x * 100 / width;

            if (left < 0) {
                left = 0;
            }
            if (left > $el.width()) {
                left = 100;
            }
            this._updateWaveformProgress($el, left);
        },

        _onWaveformProgressMove: function(e) {
            var $waveform, offsetLeft, x, left, width;

            if (this._waveformProgressDown && this._waveformProgressTarget) {
                offsetLeft = this._waveformProgressTarget.offset().left;
                width = this._waveformProgressTarget.width();
                x = e.pageX - offsetLeft;
                left = x * 100 / width;

                if (left < 0) {
                    left = 0;
                }
                if (left > this._waveformProgressTarget.width()) {
                    left = 100;
                }
                $waveform = this._waveformProgressTarget;
                this._updateWaveformProgress($waveform, left);
            }
        },

        _onWaveformProgressUp: function(e) {
            var playerManager = module.playerManager,
                currentMusic = playerManager.currentMusic,
                $musicContainer,
                seekTo, offsetLeft, x, left, duration, width, curr, $waveform;

            if (this._waveformProgressDown && this._waveformProgressTarget) {
                duration = parseInt(this._waveformProgressTarget.data('duration'));

                $musicContainer = this._waveformProgressTarget.closest(this._musicContainer);
                if (!currentMusic ||
                        String($musicContainer.data('musicId')) !==  currentMusic.id ||
                        !playerManager.playing ||
                        playerManager.currentPlayer.section === 'drop' ||
                        !duration) {

                    if (duration) {
                        curr = playerManager.getCurrentPlaybackTime();
                        left = curr * 100 / duration;
                    } else {
                        left = 0;
                    }
                    $waveform = this._waveformProgressTarget;
                    this._updateWaveformProgress($waveform, left);

                    this._waveformProgressTarget = null;
                    this._waveformProgressDown = null;
                    return;
                }
                offsetLeft = this._waveformProgressTarget.offset().left;
                width = this._waveformProgressTarget.width();

                x = e.pageX - offsetLeft;
                left = x * 100 / width;

                if (left < 0) {
                    left = 0;
                }
                if (left > this._waveformProgressTarget.width()) {
                    left = 100;
                }
                seekTo = duration * left / 100;

                playerManager.seekTo(seekTo);
                this._waveformProgressTarget = null;

                this._seekTimer = setTimeout(module.$.proxy(function() {
                    this._waveformProgressDown = false;
                }, this), 1000);
            }
        },

        _onWaveformLoadingMusic: function(e, m, s) {
            this._$playableFrame.find(this._musicContainer + ' .playback-time').text('00:00');
            this._$playableFrame.find(this._musicContainer +
                ' .waveform-progress .progress canvas').remove();
        },

        _onMusicProgressUpdate: function(e, progText, music, section) {
            var $targetMusic,
                $targetPlayback,
                $targetProgress;

            if (!music) {
                return;
            }

            $targetMusic = this._$playableFrame.find(
                this._musicContainer + '[data-music-id=\'' + music.id + '\']');

            if ($targetMusic.size() === 0) {
                return;
            }

            $targetPlayback = $targetMusic.find('.playback-time');
            $targetPlayback.text(moment.duration({seconds:progText.playback}).format('mm:ss'));

            //if (progText.duration > 0) {
            //    $targetDuration = $targetMusic.find('.duration-time');
            //    $targetDuration.text(moment.duration({seconds:progText.duration}).format('mm:ss'));
            //}

            if (this._waveformProgressDown) {
                return;
            }

            if (progText.duration > 0) {
                $targetProgress = $targetMusic.find('.waveform-progress');
                this._updateWaveformProgress($targetProgress, progText.playback * 100 / progText.duration);
            }
        }
    };

// Mixin class for playable view event listen
// call simply PlayableViewMixin.bind from view's initailize func
// and call PlayableViewMixin.unbind from view's remove func
    PlayableViewMixin = {
        bind: function(context, $el, sections) {
            module.$.extend(context, this);
            context.bindPlayableView($el, sections);
        },

        unbind: function(context) {
            var publicPlaylists, playlist, playlistId;

            if (context.unbindPlayableView) {
                context.unbindPlayableView();
            }
            publicPlaylists = module.playlistManager.publicPlaylists;
            playlistId = module.playlistManager.playingPlaylistId;
            if (playlistId) {
                playlist = publicPlaylists[playlistId];
            }
            module.playlistManager.publicPlaylists = {};
            if (playlist) {
                module.playlistManager.publicPlaylists[playlist.id] = playlist;
            }
        },

        bindPlayableView: function($el, sections) {
            var that = this;

            module._.bindAll(this, '_onPlayMusicClicked', '_onAddMusicClicked',
                '_onPlayInfoUpdate', '_onTogglePlay',
                '_onDropBtnMouseEnter', '_onDropBtnClicked', '_onDropBtnMouseLeave',
                '_onMoreBtnClicked', '_onPlayerStopped', '_onPlayPlaylistBtnClicked',
                '_onPlayCtrlClicked', '_onMusicFinished', '_onShareMusicClicked',
                '_onRepostMusicClicked',
                '_onDotMenuClicked', '_onLikeMusicClicked', '_onEditMusicClicked',
                '_onDeleteMusicClicked', '_onDownloadMusicClicked', '_onLoadingMusic',
                '_onPlayMusic', '_onTrackLiked', '_onTrackDisliked');

            this._musicContainer = '.a-addable-music';
            this._$playableFrame = $el;
            this._playSections = sections || [];
            this._musicStorage = {};
            if (this.parentMusicStorage) {
                module.$.each(this.parentMusicStorage, function(key, val) {
                    that._musicStorage[key] = val;
                });
            }
            this._tracksTmpl = module._.template(module.$('#tmpl-tracklist').html());

            this._$playableFrame.on('click', '.play-music', this._onPlayMusicClicked);
            this._$playableFrame.on('click', '.add-to-playlist', this._onAddMusicClicked);
            this._$playableFrame.on('click', '.btn-share', this._onShareMusicClicked);
            this._$playableFrame.on('click', '.btn-download', this._onDownloadMusicClicked);
            this._$playableFrame.on('click', '.btn-like', this._onLikeMusicClicked);
            this._$playableFrame.on('click', '.btn-edit-track', this._onEditMusicClicked);
            this._$playableFrame.on('click', '.btn-delete-track', this._onDeleteMusicClicked);
            this._$playableFrame.on('click', '.btn-repost', this._onRepostMusicClicked);
            this._$playableFrame.on('click', '.btn-dot-menu', this._onDotMenuClicked);
            this._$playableFrame.on('click', '.listen-drop', this._onDropBtnClicked);
            this._$playableFrame.on('mouseenter', 'listen-drop', this._onDropBtnMouseEnter);
            this._$playableFrame.on('mouseleave', 'listen-drop', this._onDropBtnMouseLeave);
            this._$playableFrame.on('click', '.unfold-btn', this._onMoreBtnClicked);
            this._$playableFrame.on('click', '.play-playlist-btn', this._onPlayPlaylistBtnClicked);
            this._$playableFrame.on('click', '.ctrl-play', this._onPlayCtrlClicked);
            module.$(module).on('playInfoUpdate loadingMusic', this._onPlayInfoUpdate);
            module.$(module).on('finishedMusic playerStopped', this._onMusicFinished);
            module.$(module).on('togglePlay', this._onTogglePlay);
            module.$(module).on('loadingMusic', this._onLoadingMusic);
            module.$(module).on('musicProgressUpdate', this._onPlayMusic);
            module.$(module).on('playerStopped', this._onPlayerStopped);
            module.$(module).on('trackLiked', this._onTrackLiked);
            module.$(module).on('trackDisliked', this._onTrackDisliked);
        },

        updatePlayState: function() {
            var playerManager = module.playerManager,
                currentTrack = playerManager.getCurrentMusic(),
                $onMusic;

            this._$playableFrame.find(this._musicContainer).removeClass('on');
            if (currentTrack) {
                $onMusic = this._$playableFrame.find(this._musicContainer +
                    '[data-music-id=\'' + currentTrack.id + '\']');
                $onMusic.addClass('on');

                if (playerManager.playing) {
                    $onMusic.find('.ctrl-play').addClass('pause');
                } else {
                    $onMusic.find('.ctrl-play').removeClass('pause');
                }
            }
        },

        _onTrackLiked: function(e, music) {
            var selector,
                $el;

            if (!music) {
                return;
            }

            selector = this._musicContainer + '[data-music-id=\'' + music.id + '\']';
            $el = this._$playableFrame.find(selector);
            if ($el.size() > 0) {
                $el.find('.btn-like').addClass('liked');
            }
        },

        _onTrackDisliked: function(e, music) {
            var selector,
                $el;

            if (!music) {
                return;
            }

            selector = this._musicContainer + '[data-music-id=\'' + music.id + '\']';
            $el = this._$playableFrame.find(selector);
            if ($el.size() > 0) {
                $el.find('.btn-like').removeClass('liked');
            }
        },

        _onTogglePlay: function(e, music, section, isPlaying) {
            var $musicContainer,
                $onMusic,
                $targetMusic;

            this.onTogglePlay && this.onTogglePlay.apply(this, arguments);

            $onMusic = this._$playableFrame.find(this._musicContainer + '.on');
            if (!music) {
                if (!isPlaying) {
                    $onMusic.find('.ctrl-play').removeClass('pause');
                }
            } else {
                $targetMusic = this._$playableFrame.find(
                    this._musicContainer + '[data-music-id=\'' + music.id + '\']');
                if ($targetMusic.size() > 0) {
                    if (isPlaying) {
                        $targetMusic.find('.ctrl-play').addClass('pause');
                    } else {
                        $targetMusic.find('.ctrl-play').removeClass('pause');
                    }
                }
            }
            if (section === 'drop') {
                module.$('.play-music', this._$playableFrame).removeClass('pause');
                module.$('.listen-drop', this._$playableFrame).removeClass('pause');

                if (module.playerManager.playing) {
                    $musicContainer = module.$(this._musicContainer + '.on', this._$playableFrame);
                    $musicContainer.find('.listen-drop').addClass('pause');
                }
            }
        },

        _onPlayerStopped: function() {
            var $onMusic = this._$playableFrame.find(this._musicContainer + '.on');
            $onMusic.removeClass('on');
            this._updatePlayPlaylistBtn(false);
        },

        _onPlayPlaylistBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $music = this._$playableFrame.find(this._musicContainer).first();

            if ($btn.hasClass('playing')) {
                return;
            }
            $music.find('.play-music').click();
        },

        _updatePlayPlaylistBtn: function(isPlaying) {
            var $btn = module.$('.play-playlist-btn', this._$playableFrame);

            if (isPlaying) {
                $btn.addClass('playing');
                $btn.find('.text').html('Playing');
            } else {
                $btn.removeClass('playing');
                $btn.find('.text').html('Play');
            }
        },

        _onDownloadMusicClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                resourcePath = $btn.data('target');

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }
            $dbt.view.FollowToDownloadView.show(resourcePath);
        },

        _onMusicFinished: function(e, music, section) {
            var $onMusic = this._$playableFrame.find(this._musicContainer + '.on');
            $onMusic.removeClass('on');
            $onMusic.find('.listen-drop').removeClass('loading');
        },

        _onPlayInfoUpdate: function(e, music, section) {
            var $onMusic = this._$playableFrame.find(this._musicContainer + '.on'),
                musicData,
                $newOnMusic;

            musicData = extractMusicFromDOM(this, $onMusic);

            this._updatePlayPlaylistBtn(false);
            if (section === 'drop') {
                if (!musicData || !musicData.drop || musicData.drop.dref !== music.id) {
                    $onMusic.removeClass('on');
                    $newOnMusic = this._$playableFrame
                        .find(this._musicContainer + '[data-drop-music-id=\'' + music.id + '\']');
                    $newOnMusic.addClass('on');
                }
                this._updatePlayPlaylistBtn(false);
                return;
            }
            if (musicData && musicData.id ===  music.id) {
                this._updatePlayPlaylistBtn(true);
                return;
            }
            $onMusic.removeClass('on');
            $newOnMusic = this._$playableFrame
                .find(this._musicContainer + '[data-music-id=\'' + music.id + '\']');
            $newOnMusic.addClass('on');
            if ($newOnMusic.size() > 0 && this._playSections.indexOf(section) !== -1) {
                this._updatePlayPlaylistBtn(true);
            } else {
                this._updatePlayPlaylistBtn(false);
            }
        },

        _onMoreBtnClicked: function(e) {
            var $wrapper = module.$(e.currentTarget).closest('.btn-wrapper'),
                $folded = $wrapper.next();
            $wrapper.hide();
            $folded.show();
        },

        getPlaySection: function(e) {
            return this._playSections[0];
        },

        _onDropBtnClicked: function(e) {
            var $dropBtn = module.$(e.currentTarget),
                $musicContainer,
                musicData,
                currMusic,
                that = this,
                loading = false;

            if ($dropBtn.hasClass('loading')) {
                return;
            }

            $musicContainer = $dropBtn.closest(this._musicContainer);

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData || !musicData.drop) {
                return;
            }

            this._$playableFrame.find(this._musicContainer + '.on').removeClass('on');
            this._updatePlayPlaylistBtn(true);
            $musicContainer.addClass('on');

            currMusic = module.playerManager.getCurrentMusic();
            if (module.playerManager.currentPlayer &&
                    module.playerManager.currentPlayer.section === 'drop') {

// We should puase music
                if (!currMusic ||
                        currMusic.type !== musicData.drop.type ||
                        currMusic.id !== musicData.drop.dref ||
                        (!module.playerManager.playing && !$dropBtn.hasClass('loading'))) {
                    loading = true;
                    $dropBtn.addClass('loading');
                    module.$(module).one(
                        'playMusic',
                        function(e, music, s) {
                            bindDropTimer();
                            $dropBtn.removeClass('loading');
                        });
                }
                module.playerManager.onPlayMusic(
                    new module.music.DropMusic({
                        'id': musicData.drop.dref,
                        'title': musicData.title,
                        'type': musicData.drop.type,
                        'original': musicData
                    }), 'drop', musicData.drop.when);

                if (!loading) {
                    bindDropTimer();
                }
            } else {
                $dropBtn.addClass('loading');
                module.$(module).one(
                    'playMusic',
                    function(e, music, s) {
                        bindDropTimer();
                        $dropBtn.removeClass('loading');
                    });
                module.playerManager.onMusicClicked(
                    new module.music.DropMusic({
                        'id': musicData.drop.dref,
                        'title': musicData.title,
                        'type': musicData.drop.type,
                        'original': musicData
                    }), 'drop', { startPoint: musicData.drop.when});
            }

            function bindDropTimer() {
                if (that._dropPlayerTimer) {
                    clearTimeout(that._dropPlayerTimer);
                    that._dropPlayerTimer = null;
                }
                if (module.playerManager.playing) {
                    that._dropPlayerTimer = setTimeout(function() {
                        var currMusic;
                        that._dropPlayerTimer = null;
                        if (!module.playerManager.playing ||
                                !module.playerManager.currentPlayer ||
                                module.playerManager.currentPlayer.section !== 'drop') {
                            return;
                        }
                        currMusic = module.playerManager.getCurrentMusic();
                        if (currMusic.id === musicData.drop.dref &&
                                currMusic.type === musicData.drop.type) {
                            module.playerManager.stop('drop');
                        }
                    }, 20000);
                }
            }
        },

        _onDropBtnMouseEnter: function(e) {
            var $target = module.$(e.currentTarget),
                $wrapper = $target.closest('.title-wrapper');
            $wrapper.find('.play-music').addClass('hover');
        },

        _onDropBtnMouseLeave: function(e) {
            var $target = module.$(e.currentTarget),
                $wrapper = $target.closest('.title-wrapper');
            $wrapper.find('.play-music').removeClass('hover');
        },

        openTrackList: function($musicContainer, musicData) {
            var $wrapper,
                $tracks,
                $openedChildren,
                that = this;

            $wrapper = $musicContainer.closest('.a-addable-music-wrapper');

// Hide previous played music
            if (this._tracklistReq) {
                this._tracklistReq.abort();
                this._tracklistReq = null;
            }
            if (!$musicContainer.hasClass('opened')) {
                if (this._$currMusicContainer) {
                    hideSubTracks(this._$currMusicContainer,
                            $musicContainer,
                            function() {
                                showSubTracks($musicContainer);
                                $musicContainer.addClass('opened');
                                that._$currMusicContainer = $musicContainer;
                            });
                } else {
                    showSubTracks($musicContainer);
                    $musicContainer.addClass('opened');
                    this._$currMusicContainer = $musicContainer;
                }
            }
            if ($musicContainer.next().hasClass('tracks')) {
                $tracks = $musicContainer.next();

                $openedChildren =
                    module.$('.a-addable-music.opened', $tracks);
                $openedChildren.removeClass('opened');
                return;
            }
        },

        _onPlayCtrlClicked: function(e) {
            var $playCtrl = module.$(e.currentTarget),
                $musicContainer,
                $playlist,
                musicData,
                section = this.getPlaySection(e),
                playlistId,
                cb;

            $musicContainer = module.$(e.currentTarget).closest(this._musicContainer);

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return;
            }

            $playlist = $musicContainer.closest('.a-playable-list');

            cb = module.$.proxy(function(musicData) {
                if ($playCtrl.hasClass('pause')) {
                    module.playerManager.onPlayMusic(musicData, section);
                    return;
                }

                module.s.progressController.init(section,
                    $musicContainer, musicData.id);

                this.openTrackList($musicContainer, musicData);

                if ($playlist.size() > 0) {
                    playlistId = $playlist.data('playlistId');
                }

                module.playerManager.onMusicClicked(
                    musicData,
                    section, {
                        playlistId: playlistId
                    });
            }, this);

            if (musicData.tag === 'undefined_track' && !musicData.id) {
                this._resolveUndefinedTrack($musicContainer, musicData, cb);
            } else {
                cb(musicData);
            }
        },

        _onPlayMusicClicked: function(e, startPoint) {
            var $musicContainer,
                $playlist,
                musicData,
                section = this.getPlaySection(e),
                playlistId, cb;

            $musicContainer = module.$(e.currentTarget).closest(this._musicContainer);
            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return
            }

            $playlist = $musicContainer.closest('.a-playable-list');

            cb = module.$.proxy(function(musicData) {
                module.s.progressController.init(section,
                    $musicContainer, musicData.id);

                this.openTrackList($musicContainer, musicData);

                if ($playlist.size() > 0) {
                    playlistId = $playlist.data('playlistId');
                }
                module.playerManager.onMusicClicked(
                    musicData,
                    section, {
                        playlistId: playlistId,
                        startPoint: startPoint
                    });
            }, this);

            if (musicData.tag === 'undefined_track' && !musicData.id) {
                this._resolveUndefinedTrack($musicContainer, musicData, cb);
            } else {
                cb(musicData);
            }
        },

        _onAddMusicClicked: function(e) {
            var $musicContainer = module.$(e.currentTarget).parents(this._musicContainer),
                musicData,
                playlist,
                success, cb;

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return
            }

            cb  = module.$.proxy(function(musicData) {
                playlist = module.playlistManager.getSelectedPlaylist();
                success = playlist.add(musicData, true);

                if (success) {
                    module.s.notify(module.string('track_added'), 'success');
                    module.playlistManager.getSelectedPlaylist().sync();
                    module.log(
                        'playlist-add-from-chart',
                        'add-' + musicData.type,
                        musicData.title);
                    module.coreLog('trackAdd', musicData.title);

                    module.$(module).trigger('playlistChanged', {
                            title: module.string('track_added_to'),
                            desc: playlist.name
                    });
                }
            }, this);

            if (musicData.tag === 'undefined_track' && !musicData.id) {
                this._resolveUndefinedTrack($musicContainer, musicData, cb);
            } else {
                cb(musicData);
            }
        },

        _resolveUndefinedTrack: function($musicContainer, music, callback) {
            var that = this;

            if (this._resolveReq) {
                this._resolveReq.abort();
            }

            this._resolveRes = module.$.ajax({
                url: module.coreApi.queryIndexUrl,
                data: JSON.stringify({
                    q: music.title
                }),
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                crossDomain: true
            }).done(function(res) {
                var id, type, $parentContainer, m, key;

                if (!res || !res.success || !res.data) {
                    module.s.notify(module.string('failed_to_fetch_music_info'), 'error');
                    return;
                }
                id = res.data;
                type = 'soundcloud';
                if (!/^\d+$/.test(id)) {
                    if (id.startsWith('http')) {
                        type = 'podcast';
                    } else {
                        type = 'youtube';
                    }
                }

                key = type + '_' + id;

                m = new module.music.Music({
                        id: id,
                        type: type,
                        title: music.title
                    });

                that.setMusicToStorage(m.getStorageKey(), m);

                // remove preexisted track
                that.setMusicToStorage(music.getStorageKey(), null);

                $musicContainer.data('musicStorageKey', key);
                $musicContainer.attr('data-music-storage-key', key);
                $musicContainer.data('musicId', id);
                $musicContainer.attr('data-music-id', id);

                $parentContainer = $musicContainer.closest('.a-addable-music-wrapper');
                if ($parentContainer.size() > 0) {
                    $parentContainer.data('parentMusicId', id);
                    $parentContainer.attr('data-parent-music-id', id);
                }
                callback && callback(m);
            }).fail(function(jqXHR, errorText, err) {
                if (errorText !== 'abort') {
                    module.s.notify(module.string('failed_to_fetch_music_info'), 'error');
                }
            }).always(function() {
                that._resolveReq = null;
            });
        },

        _onShareMusicClicked: function(e) {
            var $musicContainer = module.$(e.currentTarget).parents(this._musicContainer),
                musicData,
                cb, section;

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return
            }

            section = this.getPlaySection(e);

            cb = module.$.proxy(function(musicData) {
                if (musicData.id && musicData.title && musicData.type) {
                    module.view.ShareSingleTrackView.show(musicData, section);
                }
            }, this);

            if (musicData.tag === 'undefined_track' && !musicData.id) {
                this._resolveUndefinedTrack($musicContainer, musicData, cb);
            } else {
                cb(musicData);
            }
        },

        _onEditMusicClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $musicContainer = $btn.parents(this._musicContainer),
                musicData,
                account;

            this.onEditMusicClicked && this.onEditMusicClicked(e);

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            musicData = extractMusicFromDOM(this, $musicContainer);
            if ($musicContainer.hasClass('removing')) {
                return;
            }

            account = module.context.account;
            if (!account) {
                module.view.NeedSigninView.show();
                return;
            }
            if (musicData.user.resource_name !== account.resourceName) {
                module.s.notify(module.string('you_have_no_permission'), 'error');
                return;
            }

            module.router.navigate(musicData.getResourcePath() + 'edit', {trigger: true});
        },

        _onDeleteMusicClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $musicContainer = $btn.parents(this._musicContainer),
                musicData,
                that = this;

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            musicData = extractMusicFromDOM(this, $musicContainer);

            if (this.musicDeleteReq) {
                return;
            }

            if ($musicContainer.hasClass('removing')) {
                return;
            }

            module.view.ConfirmView.show({
                title: module.string('are_you_sure'),
                text: module.string('are_you_sure_delete_%s', musicData.title),
                confirmText: module.string('yes_delete'),
                closeText: module.string('cancel')
            }, function(confirmed) {
                if (!confirmed) {
                    return;
                }

                $musicContainer.addClass('removing');
                that.musicDeleteReq = module.$.ajax({
                    url:module.api.trackUrl,
                    data:JSON.stringify({
                        track_id: musicData.uid
                    }),
                    dataType:'json',
                    type: 'DELETE'
                }).done(function(res) {
                    if (!res || !res.success) {
                        module.s.notify(module.string('failed_to_delete_track'), 'error');
                        that.onAfterDeleteMusic && that.onAfterDeleteMusic(false);
                        return;
                    }
                    if ($musicContainer.parent().hasClass('a-addable-music-wrapper')) {
                        $musicContainer.parent().remove();
                    } else {
                        $musicContainer.remove();
                    }

                    that.onAfterDeleteMusic && that.onAfterDeleteMusic(true);
                }).fail(function(jqXHR, textStatus, error) {
                    module.s.notify(module.string('failed_to_delete_track'), 'error');
                    $musicContainer.removeClass('removing');
                    that.onAfterDeleteMusic && that.onAfterDeleteMusic(false);
                }).always(function() {
                    that.musicDeleteReq = null;
                });
            });
        },

        _onLikeMusicClicked: function(e, isLiked) {
            var $btn = module.$(e.currentTarget),
                $musicContainer = $btn.parents(this._musicContainer),
                musicData,
                section,
                that = this;

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return
            }

            isLiked = isLiked || $btn.hasClass('liked');

            if (isLiked) {
                module.context.account.dislike(musicData, function(success, msg) {
                    var errorMsg;

                    if (!success) {
                        errorMsg = module.string('failed_to_update_like');
                        module.s.notify(errorMsg, 'error');
                        $btn.addClass('liked');
                        return;
                    }
                    module.notification.notify(module.string('track_disliked'), 'success');
                    $btn.removeClass('liked');
                });
            } else {
                module.context.account.like(musicData, function(success, msg) {
                    var errorMsg;

                    if (!success) {
                        errorMsg = module.string('failed_to_update_like');
                        module.s.notify(errorMsg, 'error');
                        $btn.removeClass('liked');
                        return;
                    }
                    module.s.notify(module.string('track_liked'), 'success');
                    $btn.addClass('liked');
                });
            }

            this.onLikeBtnClicked && this.onLikeBtnClicked(e);
        },

        _onRepostMusicClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $musicContainer = $btn.parents(this._musicContainer),
                musicData,
                account,
                errorMsg;

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return
            }

            account = module.context.account;
            if (!account) {
                module.view.NeedSigninView.show();
                return;
            }
            if (musicData.user && account.resourceName === musicData.user.resourceName) {
                return;
            }
            account.repost(musicData, function(success, msg) {
                if (!success) {
                    errorMsg = module.string('already_reposted');
                    module.s.notify(errorMsg, 'error');
                    return;
                }
                module.s.notify(module.string('reposted'), 'success');
            });

            this.onRepostBtnClicked && this.onRepostBtnClicked(e);
        },

        _onDotMenuClicked: function(e) {
            var $menu = module.$(e.currentTarget),
                $musicContainer = $menu.parents(this._musicContainer),
                musicData,
                _excludes = $menu.data('exclude'),
                excludes = [],
                that = this,
                isLiked;

            if (_excludes) {
                excludes = _excludes.split(',');
            }

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return
            }

            isLiked = module.context.account &&
                module.context.account.isLiked(musicData);
            module.view.PopupMusicMenuView.show({
                anchor: $menu,
                excludes: excludes,
                track: musicData,
                onClose: function(action) {
                    if ('like' === action) {
                        that._onLikeMusicClicked(e, isLiked);
                        return;
                    }
                    if ('add' === action) {
                        that._onAddMusicClicked(e);
                        return;
                    }
                    if ('repost' === action) {
                        that._onRepostMusicClicked(e);
                        return;
                    }
                    if ('share' === action) {
                        that._onShareMusicClicked(e);
                        return;
                    }
                    if ('delete' === action) {
                        that._onDeleteMusicClicked(e);
                        return;
                    }
                    if ('edit' === action) {
                        that._onEditMusicClicked(e);
                        return;
                    }
                }
            });
            return false;
        },

        _onLoadingMusic: function(e, m, s) {
            var $loadingMusic = this._$playableFrame.find(this._musicContainer + '.loading'),
                musicData,
                $targetMusic;

            if (!m) {
                $loadingMusic.removeClass('loading');
                return;
            }

            if ($loadingMusic.size() > 0) {
                musicData = extractMusicFromDOM(this, $loadingMusic);
                if (musicData.id === m.id && this._playSections.indexOf(s) > -1) {
                    return;
                }
                $loadingMusic.removeClass('loading');
            }

            $targetMusic = this._$playableFrame
                .find(this._musicContainer + '[data-music-id=\'' + m.id + '\']');
            $targetMusic.addClass('loading');

            this._playingMusic = null;
            this.onLoadingMusic && this.onLoadingMusic(e, m, s);
        },

        _onPlayMusic: function(e, progText, music, section) {
            var $loadingMusic = this._$playableFrame.find(this._musicContainer + '.loading');
            if (!music || progText.playback === 0 ||
                    (this._playingMusic &&
                    this._playingMusic.id === music.id)) {
                return;
            }
            this._playingMusic = music;
            $loadingMusic.removeClass('loading');
            this.onPlayMusic && this.onPlayMusic(e, progText, music, section);
        },

        unbindPlayableView: function() {
            if (this.tracklistReq) {
                this.tracklistReq.abort();
                this.tracklistReq = null;
            }

            if (this._resolveReq) {
                this._resolveReq.abort();
                this._resolveReq = null;
            }
            this._musicStorage = {};

            module.$(window).off('resize', this._onWindowResize);
            module.$(module).off('playInfoUpdate loadingMusic', this._onPlayInfoUpdate);
            module.$(module).off('finishedMusic playerStopped', this._onMusicFinished);
            module.$(module).off('togglePlay', this._onTogglePlay);
            module.$(module).off('loadingMusic', this._onLoadingMusic);
            module.$(module).off('musicProgressUpdate', this._onPlayMusic);
            module.$(module).off('playerStopped', this._onPlayerStopped);
            module.$(module).off('trackLiked', this._onTrackLiked);
            module.$(module).off('trackDisliked', this._onTrackDisliked);

            this._$playableFrame.off('click', '.play-music', this._onPlayMusicClicked);
            this._$playableFrame.off('click', '.add-to-playlist', this._onAddMusicClicked);
            this._$playableFrame.off('click', '.btn-share', this._onShareMusicClicked);
            this._$playableFrame.off('click', '.btn-download', this._onDownloadMusicClicked);
            this._$playableFrame.off('click', '.btn-like', this._onLikeMusicClicked);
            this._$playableFrame.off('click', '.btn-repost', this._onRepostMusicClicked);
            this._$playableFrame.off('click', '.btn-dot-menu', this._onDotMenuClicked);
            this._$playableFrame.off('click', '.btn-edit-track', this._onEditMusicClicked);
            this._$playableFrame.off('click', '.btn-delete-track', this._onDeleteMusicClicked);
            this._$playableFrame.off('click', '.listen-drop', this._onDropBtnClicked);
            this._$playableFrame.off('mouseenter', 'listen-drop', this._onDropBtnMouseEnter);
            this._$playableFrame.off('mouseleave', 'listen-drop', this._onDropBtnMouseLeave);
            this._$playableFrame.off('click', '.unfold-btn', this._onMoreBtnClicked);
            this._$playableFrame.off('click', '.play-playlist', this._onPlayPlaylistBtnClicked);
            this._$playableFrame.off('click', '.ctrl-play', this._onPlayCtrlClicked);
        },

        setMusicToStorage: function(key, val) {
            if (!val) {
                delete this._musicStorage[key];
            } else {
                this._musicStorage[key] = val;
            }
        },

        updateMusicStorage: function(map) {
            module.$.extend(this._musicStorage, map);
        },

        getMusicFromStorage: function(key) {
            return this._musicStorage[key];
        },

        clearMusicStorage: function() {
            this._musicStorage = {};
        }
    };




///////////////////////////////
// Dropbeat PageView Related //
///////////////////////////////

    Failure404PageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-404',
        initialize: function(options) {
            DropbeatPageView.prototype.initialize.call(this, options);

            module.$.extend(this._context, { path: options.path });
            this.render();
            module.$('header .header-menu .menu').removeClass('selected');
        },

        render:function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        }
    });

    AboutPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-about',
        initialize: function(options) {
            DropbeatPageView.prototype.initialize.call(this, options);

            this.render();

            module.$('header .header-menu .menu').removeClass('selected');
            module.$('header .header-menu .about-menu').addClass('selected');
        },

        render:function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        }
    });

    HomePageView = DropbeatPageView.extend({
        subViews: (function() {
            return module.$.extend ({}, DropbeatPageView.prototype.subViews);
        }()),
        tmpl: '#tmpl-page-home',
        initialize: function(options) {
            var that = this;

            module._.bindAll(this, 'onResize');

            DropbeatPageView.prototype.initialize.call(this, options);

            this.render();

            module.$('header .header-menu .menu').removeClass('selected');
            module.$('header .header-menu .home-menu').addClass('selected');

            this._subViews = [
                HomePageView.subViews.tabView
            ];

            module.$.each(this._subViews, function(idx, view) {
                view.init(that, options);
                view.show();
            });

            setTimeout(function() {
                var $fbPlugin = module.$('<div class=\'fb-page\'></div>'),
                    $xfbml = module.$('<div class=\'fb-xfbml-parse-ignore\'></div>');
                $fbPlugin.attr('data-href', 'https://www.facebook.com/godropbeat');
                $fbPlugin.attr('data-height', '1000');
                $fbPlugin.attr('data-hide-cover', 'false');
                $fbPlugin.attr('data-width', '350');
                $fbPlugin.attr('data-show-facepile', 'true');
                $fbPlugin.attr('data-show-posts', 'false');
                $xfbml.html('<blockquote cite="https://www.facebook.com/godropbeat">' +
                            '<a href="https://www.facebook.com/godropbeat">Dropbeat</a></blockquote>');
                module.$('.fb-page-wrapper').html($fbPlugin.html($xfbml));
                if (module.context.fbInitialized &&
                        window['FB'] && FB.XFBML && FB.XFBML.parse) {
                    FB.XFBML.parse(module.$('#page-home')[0]);
                }
                if (window['twttr'] && twttr.widgets) {
                    twttr.widgets.load( module.$('#page-home')[0] );
                }
                if (window['insta']) {
                    insta.init();
                }
            }, 200);

            if (!module.context.account) {
                module.$('#dropbeat .body-section .signin-btn').css('display', 'inline-block');
                module.$('#dropbeat .body-section .signup-btn').css('display', 'inline-block');
            } else {
                module.$('#dropbeat .body-section .signin-btn').css('display', 'none');
                module.$('#dropbeat .body-section .signup-btn').css('display', 'none');
            }
            if (module.context.isAndroid) {
                module.$('#dropbeat .body-section .mobile-home-menu .google-play-btn')
                    .css('display', 'block');
            } else {
                module.$('#dropbeat .body-section .mobile-home-menu .google-play-btn')
                    .css('display', 'none');
            }

            this._$bigSearchBox = module.$('.big-search-box');
            this._$headerSearchMenu = module.$('.header-section .search-menu');
            this._$leftMenus = module.$('.left-menu');
            this._$rightMenus = module.$('.right-menu');
            this._$rightMenusInner = module.$('.right-menu .inner');

            module.view.initContentsBg();
            module.$(window).on('resize', this.onResize);

            module.$('.search-input').val('');
            //module.$('.search-autocomplete').removeClass('overlay');
            module.$('.search-autocomplete').hide();
            module.$('.search-autocomplete .items').html('');

            module.$(window).on('resize', this.onScroll);

            if (!module.context.isIOS) {
                module.view.FollowDropbeatView.show(function() {
                    //$dbt.view.UpdateNewsView.show(function() {
                    if (!module.context.isMobile &&
                            !module.$.cookie('done_artist_follow') &&
                            module.context.account &&
                            module.context.account.fbId) {
                    }
                });
            }
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .import-as-playlist-btn': 'onImportAsPlaylist',
                'mousewheel .right-menu .static': 'onRightMenuMouseWheel'
            });
        },

        render:function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        onResize: function() {
            module.view.resizeContentsBg();
        },

        onImportAsPlaylist: function(e) {
            var $btn = module.$(e.currentTarget),
                title = unescape($btn.attr('data-title')),
                $target = module.$('#' + $btn.data('target')),
                $music = $target.find('.a-addable-music'),
                tracks = [],
                ids = [],
                that = this;

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }
            if ($music.size() === 0) {
                module.s.notify(
                    module.string('failed_to_import_playlist'), 'error');
                return;
            }

            $music.each(function(idx, el) {
                var $el = module.$(el),
                    musicData = extractMusicFromDOM(that, $el);

                if (!musicData || !musicData.id || musicData.id === '' ||
                        ids.indexOf(musicData.id) > -1) {
                    return true;
                }
                ids.push(musicData.id);
                tracks.push(musicData);
            });

            module.s.storage.createPlaylist(title, function(playlist) {
                if (!playlist) {
                    module.s.notifiy('Failed to create playlist', 'error');
                    return;
                }
                module.s.storage.setPlaylist(tracks, playlist.seqId,
                        function() {
                    module.playlistManager.loadPlaylists(function() {
                        var selector;

                        module.s.playlistTabs.renderAll();
                        module.playlistManager.updateCurrentPlaylistView();
                        selector = '.my-playlist .playlists ';
                        selector += '.playlist[data-playlist-id=\'';
                        selector += playlist.seqId + '\']';
                        selector += ' .nonedit-mode-view .title';
                        module.$(selector).click();

                        module.s.notify('Playlist imported', 'success');
                    });
                })
            });
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
            this._$headerSearchMenu.show();
            module.$(window).off('resize', this.onResize);
            module.$.each(this._subViews, function(idx, view) {
                view.remove && view.remove();
            });
        }
    });

    HomePageView.subViews = {};
    HomePageView.subViews.tabView = (function() {
        var view = {},
            selectedView;

        view.init = function(parentView, options) {
            module._.bindAll(this, 'onTabClicked');

            this.parentView = parentView;

            this.options = options;

            this.$el = module.$('.feed-section');
            this.$tab = module.$('.tab', this.$el);
            this.$tabContents = module.$('.tab-contents', this.$el);

            this.$el.on('click', '.tab', this.onTabClicked);
        };

        view.onTabClicked = function(e) {
            var $tab = module.$(e.currentTarget),
                target = $tab.data('target'),
                key;

            target = target.replace(/-([a-z])/g, function (g) {
                return g[1].toUpperCase();
            });

            if (selectedView && selectedView.key === target) {
                return;
            }
            if (selectedView) {
                selectedView.remove();
            }

            if (target === 'explore') {
                key = target;
                selectedView = new HomePageView.subViews.SectionedView({
                    el: '.tab-contents-' + target,
                    key: key
                });
            } else {
                key = target;
                selectedView = new HomePageView.subViews.FeedView({
                    el: '.tab-contents-' + target,
                    key: key
                });
            }
            this.$tab.removeClass('selected');
            this.$tabContents.removeClass('selected');
            $tab.addClass('selected');
            selectedView.show();
        };

        view.remove = function() {
            this.$el.off('click', '.tab', this.onTabClicked);
            selectedView && selectedView.remove();
            selectedView = null;
        };

        view.show = function() {
            var found, tab;

            tab = module.utils.gup('tab');
            if (tab) {
                this.$tab.each(function(idx, el) {
                    var $el = module.$(el);
                    if ($el.is('[data-target=\'' + tab + '\']')) {
                        found = $el;
                    }
                });
            }
            if (!found) {
                found = this.$tab.first();

                if (found.data('target') === 'feed' &&
                    (!module.context.account ||
                    module.context.account.getFollowings().length === 0)) {
                    found = found.next();
                }
            }
            found.click();
        };

        return view;
    }());

    HomePageView.subViews.feedSubViews = (function() {
        var views = {},
            LazyLoadingTrackView;

        LazyLoadingTrackView = {
            init: function(options) {
                var that = this, selector, url;

                module._.bindAll(this, 'onScroll', 'onRefreshBtnClicked',
                    'onOptionClicked', 'onSelectOption');


                if (options.replaceUrl) {
                    url = location.pathname + '?' + module.$.param({
                        tab:options.parentView.key.camelToDash(),
                        subtab: this.key.camelToDash()
                    });

                    module.router.navigate(url, {trigger: false, replace:true});
                }

                this.showRank = options.showRank;
                this.$el = module.$(options.el);
                this.context = options.context;
                this.context.hideOptions = options.hideOptions;
                this.context.genre = options.hideOptions;
                this.limitCount = options.limitCount || -1;
                this.trackCount = 0;
                this.options = options.feedOptions || [];
                this.optionMap = {};
                this.selectedOption = this.options &&
                    this.options.length > 0 ? this.options[0] : null;
                this.context.selectedOption = this.selectedOption;

                this.tmpl = module._.template(module.$(options.tmpl).html());
                this.trackTmpl = module._.template(module.$(options.trackTmpl).html());

                this.render();

                this.$tracks = module.$(options.tracks, this.$el);
                this.parentView = options.parentView;
                this.sectionId = options.sectionId;
                this.sectionName = options.sectionName;

                module.$.each(this.options, function(idx, opt) {
                    that.optionMap[opt.key] = opt;
                });
                this.optionChoiceSelector ='.option-select .selected-option';
                this.optionChoiceItemSelector = '.option-select .option';
                this.optionChoiceDropdownSelector = '.option-select .dropdown';

                if (!options.hideOptions) {
                    this.$optionChoiceText = module.$(this.optionChoiceSelector, this.$el).find('.text');
                    this.$optionDropdown = module.$(this.optionChoiceDropdownSelector, this.$el);
                    this.renderOption(this.$optionDropdown, this.options);
                }

                this.$filter = module.$('.overlay-filter');
                this.$spinner = module.$('.spinner', this.$el);

                if (!options.hideOptions) {
                    if (this.selectedOption) {
                        this.$optionChoiceText.text(this.selectedOption.name);
                    }
                    selector = this.selectedOption ?
                        '.option[data-key=\'' + this.selectedOption.key + '\']' :
                        '.option:first';

                    this.$optionDropdown.find(selector).addClass('selected');
                }

                this.itemSelector = options.itemSelector;

                WaveformViewMixin.bind(this, this.$el);
                PlayableViewMixin.bind(this, this.$el, [this.sectionId.camelToUnderscore()]);

                module.$('.contents .scroll-y').on('scroll', this.onScroll);
                module.$(window).on('resize', this.onScroll);
                this.$el.on('click', '.btn-retry', this.onRefreshBtnClicked);
                this.$el.on('click', this.optionChoiceSelector, this.onSelectOption);
                this.$el.on('click', this.optionChoiceItemSelector, this.onOptionClicked);
            },

            renderOption: function($frame, options) {
                module.$.each(options, function(idx, option) {
                    var $option = module.$('<li class=\'option\'>' + option.name + '</li>');
                    if (option.key) {
                        $option.data('key', option.key);
                        $option.attr('data-key', option.key);
                    }
                    $frame.append($option);
                });
            },

            render: function() {
                this.$el.html(this.tmpl(this.context));
            },

            show: function(forceRefresh) {
                var action, option;

                this.$tracks.empty();
                this.nextPage = 0;
                this.lastParams = null;
                this.hideFailure();
                this.updatePlaylistId();
                this.loadFeed(this.nextPage, this.getOptionType(), forceRefresh);

                if (this.sectionId === 'popularNow') {
                    action = 'popularNow';
                } else if (this.sectionId === 'followedArtists') {
                    action = 'following';
                } else if (this.sectionId === 'newRelease') {
                    action = 'newRelease';
                } else if (this.sectionId === 'newUpload') {
                    action = 'newUpload';
                } else if (this.sectionId === 'dailyChart') {
                    action = 'dailyChart';
                } else if (this.sectionId === 'channelExplore') {
                    action = 'channelExplore';
                }

                if (this.selectedOption) {
                    option = this.selectedOption.name;
                    action += '_' + option.toLowerCase().replace(' ', '');
                }

                if (action) {
                    module.log('load_feed', action, 'feed_web', 1);
                }
            },

            remove: function() {
                PlayableViewMixin.unbind(this);
                WaveformViewMixin.unbind(this);

                module.$('.contents .scroll-y').off('scroll', this.onScroll);
                module.$(window).off('resize', this.onScroll);
                this.$el.off('click', '.btn-retry', this.refresh);
                this.$el.off('click', this.optionChoiceSelector, this.onSelectOption);
                this.$el.off('click', this.optionChoiceItemSelector, this.onOptionClicked);

                if (this.req) {
                    this.req.abort();
                    this.req = null;
                }

                delete this.lastParams;
            },

            getNextPage: function() {
                return this.nextPage;
            },

            getOptionType: function() {
                return this.selectedOption ? this.selectedOption.key : null;
            },

            createReq: function() {
                throw 'not implemented';
            },

            showSpinner: function() {
                this.$spinner && this.$spinner.show();
            },

            hideSpinner: function() {
               this.$spinner && this.$spinner.hide();
            },

            showFailure: function() {
                this.$failure && this.$failure.show();
            },

            hideFailure: function() {
                this.$failure && this.$failure.hide();
            },

            getPlaylistId: function() {
                var playlistId = 'feed_' + this.sectionId.camelToUnderscore();

                if (this.selectedOption) {
                    playlistId += '_' + this.selectedOption.key;
                }
                return playlistId;
            },

            updatePlaylistId: function() {
                this.playlistId = this.getPlaylistId();

                this.$tracks.data('playlistId', this.playlistId);
                this.$tracks.attr('data-playlist-id', this.playlistId);
                this.$tracks.addClass('a-playable-list');
            },

            getParams: function(nextPage, optionType, forceRefresh) {
                var params = {
                    p: nextPage
                };

                if (optionType && optionType !== '_') {
                    params.g = optionType;
                }
                return params;
            },

            onScroll: function(e) {
                var $scroll = module.$('.contents .scroll-y'),
                    $lastItem = module.$(this.itemSelector, this.$el).last();

                if (this.nextPage &&
                        this.nextPage > 0 &&
                        $lastItem.size() > 0 &&
                        $lastItem.offset().top  < $scroll.height()) {
                    this.loadFeed(this.getNextPage(), this.getOptionType(), false);
                }
            },

            onRefreshBtnClicked: function(e) {
                this.refresh();
            },

            onSelectOption: function() {
                var that = this;
                if (this.$optionDropdown.is(':hidden')) {
                    this.$filter.show().one('click', function() {
                        that.$optionDropdown.hide();
                    });
                    this.$optionDropdown.show();
                }
            },

            onOptionClicked: function(e) {
                var $target = module.$(e.currentTarget);

                if ($target.hasClass('selected')) {
                    return;
                }

                module.$(this.optionChoiceItemSelector, this.$page).removeClass('selected');
                $target.addClass('selected');
                this.selectedOption = this.optionMap[$target.data('key')];

                this.$optionChoiceText.text($target.text());
                if (this.$filter.is(':visible')) {
                    this.$filter.click();
                }

                this.nextPage = 0;
                this.lastParams = null;
                this.show(false);
            },

            refresh: function(clearAll) {
                var playlistManager = module.playlistManager;

                if (clearAll) {
                    this.nextPage = 0;
                    this.lastParams = null;
                    this.$tracks.empty();
                    this.trackCount = 0;
                    playlistManager.publicPlaylists[this.playlistId] = null;
                }

                this.loadFeed(this.getNextPage(), this.getOptionType(), true);
            },

            loadFeed: function(nextPage, optionType, forceRefresh) {
                var that = this, params;

                if (nextPage === -1) {
                    return;
                }

                params = this.getParams(nextPage, optionType, forceRefresh);

                if (!forceRefresh && module._.isEqual(this.lastParams, params)) {
                    return;
                }

                this.lastParams = params;

                if (this.req) {
                    this.req.abort();
                }

                that.showSpinner();
                that.hideFailure();

                this.req = this.createReq(params);
                this.req.done(function(res) {
                    var data, playlist,
                        playlistTracks,
                        playlistManager = module.playlistManager,
                        q, idx, i,
                        tracks;

                    if (!res.success) {
                        that.showFailure();
                        return;
                    }

                    data = that.parseResponse(res);
                    tracks = that.getTracksFromData(data);
                    if (that.limitCount > -1) {
                        tracks.length  = Math.min(tracks.length, that.limitCount - that.trackCount);
                        that.trackCount += tracks.length;
                    }

                    if (!data) {
                        that.showFailure();
                    }
                    if (tracks.length === 0) {
                        that.nextPage = -1;
                    } else if (that.limitCount > -1 && that.trackCount === that.limitCount) {
                        that.nextPage = -1;
                    } else {
                        that.nextPage += 1;
                    }

                    if (!playlistManager.publicPlaylists[that.playlistId]) {
                        playlistManager.publicPlaylists[that.playlistId] = new module.s.Playlist({
                            id: that.playlistId,
                            name: that.sectionName,
                            tracks: []
                        });
                    }
                    playlist = playlistManager.publicPlaylists[that.playlistId];

                    playlistTracks = playlist.raw();
                    module.$.each(data, function(idx, item) {
                        playlistTracks.push(item);
                    });

                    if (module.playlistManager.playingPlaylistId === playlist.id) {
                        if (module.s.shuffleControl.isShuffle()) {
                            q = playlist.raw().slice(0);
                            module.s.shuffleControl.shuffle(q);
                            idx = -1;
                            if (module.playerManager.currentMusic) {
                                for (i = 0; i < q.length; i += 1) {
                                    if (q[i].id ===
                                            module.playerManager.getCurrentMusic().id) {
                                        idx = i;
                                    }
                                }
                            }

                            module.s.musicQ.init(q, idx);
                        } else {
                            if (module.playerManager.currentMusic) {
                                idx = playlist.findIdx(module.playerManager.currentMusic.id);
                            }
                            module.s.musicQ.init(playlist.playlist, idx);
                        }
                        module.s.playerBase.updateButton();
                    }

                    that.renderItems(data);
                    that.updateWaveform();
                    that.updatePlayState();
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus !== 'abort') {
                        that.showFailure();
                    }
                }).always(function() {
                    that.req = null;
                    that.hideSpinner();
                });
            },

            getTracksFromData: function(data) {
                return data;
            },

            renderItems: function(data) {
                var contents = '',
                    $contents,
                    playerManager = module.playerManager,
                    player = playerManager.currentPlayer,
                    that = this,
                    preIdx,
                    section = this.sectionId.camelToUnderscore(),
                    $music;

                if (!this.userTrackGenres) {
                    this.userTrackGenres = {};
                    module.$.each(this.parentView.genres['dropbeat'], function(idx, genre) {
                        that.userTrackGenres[genre.id] = genre.name;
                    });
                }

                preIdx = this.$tracks.find(this.itemSelector).size() || 0;
                module.$.each(data, function(idx, t) {
                    contents += that.trackTmpl({
                        trackInfo: t,
                        rank: that.showRank ? preIdx + idx + 1 : false,
                        editable: t.type === 'dropbeat' &&
                            module.context.account &&
                            module.context.account.resourceName === t.user.resource_name,
                        genres: that.userTrackGenres
                    });
                });

                $contents = module.$(contents);


                if (playerManager.currentMusic && player && player.section === section) {
                    $music = $contents.find(
                        '.a-addable-music[data-music-id=\'' +
                        playerManager.currentMusic.id + '\']');
                    $music.addClass('on');
                }

                this.$tracks.append($contents);
                initTooltipster($contents);
            },
            parseResponse: function() {
                throw 'not implemented';
            }
        };

        views.popularNow = (function() {
            var view = {};
            module.$.extend(view, LazyLoadingTrackView);

            view.key = 'popularNow';

            view.init = function(options) {
                var genres = [],
                    _genres = options.parentView.genres['trending'],
                    found;

                if (options.genre && options.genre === '_') {
                    genres.push({
                        name: 'NOW TRENDING',
                        key: '_'
                    });
                } else if (options.genre) {
                    module.$.each(_genres, function(idx, genre) {
                        if (genre.name.toLowerCase() === options.genre.toLowerCase()) {
                            found = genre;
                            return false;
                        }
                    });
                    if (found) {
                        genres.push(found);
                    }
                } else {
                    genres.push({
                        name: 'NOW TRENDING',
                        key: '_'
                    });
                    genres = genres.concat(_genres);
                }

                LazyLoadingTrackView.init.call(this, {
                    parentView: options.parentView,
                    showRank: true,
                    el: '.child-tab-contents-popular-now',
                    context: {},
                    tmpl: '#tmpl-feed-popular-now-section',
                    trackTmpl : options.squareView ?
                        '#tmpl-square-addable-music' :
                        '#tmpl-external-addable-music',
                    tracks: '.tracks',
                    itemSelector: '.a-addable-music-wrapper',
                    sectionName: module.string('popular_now'),
                    sectionId: 'popularNow',
                    feedOptions: genres,
                    replaceUrl: false,
                    limitCount: options.limitCount,
                    hideOptions: !!options.genre
                });
                this.$spinner = module.$('.spinner', this.$el);
                this.$failure = module.$('.failure', this.$el);
                this.$tab = module.$('.tab-popular-now');
            };

            view.show = function() {
                var $empty;

                if (this.selectedOption) {
                    LazyLoadingTrackView.show.call(this);
                    this.$el.show();
                } else {
                    this.$spinner.hide();
                    this.$failure.hide();

                    $empty = module.$('<div></div>');
                    $empty.addClass('empty');
                    $empty.text(module.string('empty_sectioned_track'));
                    this.$tracks.html($empty);
                    this.$el.hide();
                }
                this.$tab.addClass('selected');
                this.$el.addClass('selected');
            };

            view.remove = function() {
                LazyLoadingTrackView.remove.call(this);
                this.$tab.removeClass('selected');
                this.$el.removeClass('selected');
                this.$el.empty();
            };

            view.getOptionType = function() {
                return this.selectedOption ? this.selectedOption.key : null;
            };

            view.createReq = function(params) {
                return module.$.ajax({
                    url: module.coreApi.streamTrendingUrl,
                    data: params,
                    cache:false,
                    dataType:'json',
                    crossDomain: true
                });
            };

            view.parseResponse = function(res) {
                var data = res.data,
                    that = this,
                    optionType = this.getOptionType();

                if (optionType && optionType !== '_') {
                    return module.$.map(res.data, function(t, idx) {
                        var trackName = t.track_name,
                            title = t.artist_name + ' - ' + trackName,
                            track;

                        if (t.mix_type) {
                            title += ' (' + t.mix_type + ')';
                        }
                        track = new module.music.Music({
                            id: t.youtube_uid,
                            type: 'youtube',
                            title: title,
                            track_name: trackName,
                            mix_type: t.mix_type,
                            user_name: t.artist_name,
                            user_resource_name: null, //artistNameToResourceName(t.artist_name),
                            created_at: t.released,
                            artwork: t.thumbnail,
                            drop: t.drop
                        });
                        that.setMusicToStorage(track.getStorageKey(), track);
                        return track;
                    });
                }
                return module.$.map(data, function(t, idx) {
                    var title = t.artist_name + ' - ' + t.track_name,
                        track;
                    track = new module.music.Music({
                        id: t.id,
                        type: t.type,
                        title: title,
                        track_name: t.track_name,
                        desc: t.snippet,
                        user_name: t.artist_name,
                        user_resource_name: null, //artistNameToResourceName(t.artist_name),
                        drop: t.drop
                    });
                    that.setMusicToStorage(track.getStorageKey(), track);
                    return track;
                });
            };
            return view;
        }());

        views.dailyChart = (function() {
            var view = {};
            module.$.extend(view, LazyLoadingTrackView);

            view.key = 'dailyChart';

            view.init = function(options) {
                var genres = [],
                    _genres = options.parentView.genres['default'],
                    found;

                _genres = module.$.map(_genres, function (genre) {
                    return {
                        key: genre.id || genre.key,
                        name: genre.name
                    };
                });

                if (options.genre && options.genre === '_') {
                    genres.push({
                        name: 'TOP100',
                        key: 'top100'
                    });
                } else if (options.genre) {
                    module.$.each(_genres, function(idx, genre) {
                        if (genre.name.toLowerCase() === options.genre.toLowerCase()) {
                            found = genre;
                            return false;
                        }
                    });
                    if (found) {
                        genres.push(found);
                    }
                } else {
                    genres.push({
                        name: 'TOP100',
                        key: 'top100'
                    });
                    genres = genres.concat(_genres);
                }
                genres = module.$.map(genres, function(genre, idx) {
                        return {
                            name: genre.name,
                            key: genre.name.toLowerCase()
                        }
                    });

                LazyLoadingTrackView.init.call(this, {
                    parentView: options.parentView,
                    showRank: true,
                    el: '.child-tab-contents-daily-chart',
                    context: {},
                    tmpl: '#tmpl-feed-daily-chart-section',
                    trackTmpl : options.squareView ?
                        '#tmpl-square-addable-music' :
                        '#tmpl-external-addable-music',
                    tracks: '.tracks',
                    itemSelector: '.a-addable-music-wrapper',
                    sectionName: module.string('daily_chart'),
                    sectionId: 'dailyChart',
                    feedOptions: genres,
                    replaceUrl: false,
                    limitCount: options.limitCount,
                    hideOptions: !!options.genre
                });
                this.$spinner = module.$('.spinner', this.$el);
                this.$failure = module.$('.failure', this.$el);
                this.$tab = module.$('.tab-daily-chart');
            };

            view.renderOption = function($frame, options) {
                module.$.each(options, function(idx, option) {
                    var $option = module.$('<li class=\'option\'>' + option.name + '</li>');
                    $option.data('key', option.name.toLowerCase());
                    $option.attr('data-key', option.name.toLowerCase());
                    $frame.append($option);
                });
            };

            view.show = function() {
                var $empty;

                if (this.selectedOption) {
                    LazyLoadingTrackView.show.call(this);
                    this.$el.show();
                } else {
                    this.$spinner.hide();
                    this.$failure.hide();

                    $empty = module.$('<div></div>');
                    $empty.addClass('empty');
                    $empty.text(module.string('empty_sectioned_track'));
                    this.$tracks.html($empty);
                    this.$el.hide();
                }
                this.$tab.addClass('selected');
                this.$el.addClass('selected');
            };

            view.remove = function() {
                LazyLoadingTrackView.remove.call(this);
                this.$tab.removeClass('selected');
                this.$el.removeClass('selected');
                this.$el.empty();
            };

            view.onScroll = function() {
                // do nothing
            };

            view.getParams = function(nextPage, optionType, forceRefresh) {
                var params = {};

                if (optionType) {
                    params.type = optionType;
                }
                return params;
            };

            view.getOptionType = function() {
                return this.selectedOption ? this.selectedOption.key : null;
            };

            view.createReq = function(params) {
                return module.$.ajax({
                    url: module.coreApi.trendingChartUrl,
                    data: params,
                    cache:false,
                    dataType:'json',
                    crossDomain: true
                });
            };

            view.parseResponse = function(res) {
                var that = this;
                this.nextPage = -1;
                return module.$.map(res.data, function(t, idx) {
                    var trackName = t.track_name,
                        title = t.artist_name + ' - ' + trackName,
                        track;

                    if (t.mix_type) {
                        title += ' (' + t.mix_type + ')';
                    }

                    track = new module.s.Music({
                        id: t.youtube_uid,
                        type: 'youtube',
                        title: title,
                        genre: t.genre,
                        track_name: trackName,
                        mix_type: t.mix_type,
                        user_name: t.artist_name,
                        artwork: t.thumbnail,
                        drop: t.drop,
                        created_at: t.released
                    });
                    that.setMusicToStorage(track.getStorageKey(), track);
                    return track;
                });
            };
            return view;
        }());

        views.followedArtists = (function() {
            var view = {};

            module.$.extend(view, LazyLoadingTrackView);

            view.key = 'followedArtists';

            view.init = function(parentView) {
                LazyLoadingTrackView.init.call(this, {
                    parentView: parentView,
                    showRank: false,
                    el: '.child-tab-contents-followed-artists',
                    context: {},
                    tmpl: '#tmpl-feed-followed-artists-section',
                    trackTmpl : '#tmpl-external-addable-music',
                    tracks: '.tracks',
                    itemSelector: '.a-addable-music-wrapper',
                    sectionName: module.string('following_tracks'),
                    sectionId: 'followedArtists',
                    replaceUrl: true
                });

                this.internalTrackTmpl = module._.template(
                    module.$('#tmpl-useruploaded-addable-music').html());

                this.prevSelectedOption = null;

                module._.bindAll(this, 'refresh', 'onManageFollowBtnClicked');

                this.$spinner = module.$('.spinner', this.$el);
                this.$failure = module.$('.failure', this.$el);
                this.$tab = module.$('.tab-followed-artists');

                this.$el.on('click', '.btn-manage-follow', this.onManageFollowBtnClicked);
            };

            view.getParams = function(nextPage, orderType, forceRefresh) {
                var params = {
                    p: nextPage
                };

                if (orderType) {
                    params.order = orderType;
                }
                params.f = forceRefresh ? 1 : 0;
                return params;
            };

            view.show = function(forceRefresh) {
                this.$tab.addClass('selected');
                this.$el.addClass('selected');

                LazyLoadingTrackView.show.call(this);
            };

            view.remove = function() {
                LazyLoadingTrackView.remove.call(this);
                this.$tab.removeClass('selected');
                this.$el.removeClass('selected');
                this.$el.empty();

                this.$el.off('click', '.btn-manage-follow', this.onManageFollowBtnClicked);

                if (this.followInfoReq) {
                    this.followInfoReq.abort();
                    this.followInfoReq = null;
                }
            };

            view.createReq = function(params) {
                return module.$.ajax({
                    url: module.api.streamFollowingUrl,
                    cache:false,
                    data: params,
                    dataType:'json'
                });
            };

            view.parseResponse = function(res) {
                var that = this;

                return module.$.map(res.data, function(t, idx) {
                    var track;
                    if (t.unique_key) {
                        track = new module.music.DropbeatMusic(t);
                        track.desc = null;
                    } else {
                        track = new module.music.Music({
                            created_at: t.release_date || t.published_at,
                            user_name: t.artist_name || t.channel_title,
                            user_resource_name: t.resource_name,
                            type: t.type,
                            id: t.id,
                            title: t.title
                        });
                    }
                    if (t.author_resource_name && t.author_nickname) {
                        track.user = track.user || {};
                        track.user.nickname = t.author_nickname;
                        track.user.resource_name = t.author_resource_name;
                        track.user.profile_image = t.author_profile_image;
                    }
                    if (t.repost) {
                        track.repost = t.repost;
                    }
                    that.setMusicToStorage(track.getStorageKey(), track);
                    return track;
                });
            };

            view.onManageFollowBtnClicked = function() {
                module.router.navigate('/r/' +
                    module.context.account.resourceName + '/following', {trigger: true});
            };

            view.loadFeed = function(nextPage, optionType, forceRefresh) {
                if (!module.context.account) {
                    this.nextPage = -1;
                    this.hideSpinner();
                    return;
                }
                LazyLoadingTrackView.loadFeed.apply(this, arguments);
            };

            view.renderItems = function(data) {
                var contents = '',
                    $contents,
                    playerManager = module.playerManager,
                    player = playerManager.currentPlayer,
                    that = this,
                    preIdx,
                    section = this.sectionId.camelToUnderscore(),
                    tmpl,
                    $music;

                preIdx = this.$tracks.find(this.itemSelector).size() || 0;

                module.$.each(data, function(idx, t) {
                    if (t.type === 'dropbeat') {
                        tmpl = that.internalTrackTmpl;
                    } else {
                        tmpl = that.trackTmpl;
                    }
                    contents += tmpl({
                        trackInfo: t,
                        rank:false,
                        editable: t.type === 'dropbeat' &&
                            module.context.account &&
                            module.context.account.resourceName === t.user.resource_name
                    });
                });

                $contents = module.$(contents);


                if (playerManager.currentMusic && player && player.section === section) {
                    $music = $contents.find(
                        '.a-addable-music[data-music-id=\'' +
                        playerManager.currentMusic.id + '\']');
                    $music.addClass('on');
                }

                this.$tracks.append($contents);
                initTooltipster($contents);
            };
            return view;
        }());

        views.newRelease = (function() {
            var view = {};
            module.$.extend(view, LazyLoadingTrackView);

            view.key = 'newRelease';

            view.init = function(options) {
                var genres = [],
                    _genres = options.parentView.genres['default'],
                    found;

                _genres = module.$.map(_genres, function (genre) {
                    return {
                        key: genre.id || genre.key,
                        name: genre.name
                    };
                });

                if (options.genre && options.genre === '_') {
                    genres.push({
                        name: 'ALL',
                        key: '_'
                    });
                } else if (options.genre) {
                    module.$.each(_genres, function(idx, genre) {
                        if (genre.name.toLowerCase() === options.genre.toLowerCase()) {
                            found = genre;
                            return false;
                        }
                    });
                    if (found) {
                        genres.push(found);
                    }
                } else {
                    genres.push({
                        name: 'ALL',
                        key: '_'
                    });
                    genres = genres.concat(_genres);
                }

                LazyLoadingTrackView.init.call(this, {
                    parentView: options.parentView,
                    showRank: false,
                    el: '.child-tab-contents-new-release',
                    context: {},
                    tmpl: '#tmpl-feed-new-release-section',
                    trackTmpl : options.squareView ?
                        '#tmpl-square-addable-music' :
                        '#tmpl-external-addable-music',
                    tracks: '.tracks',
                    itemSelector: '.a-addable-music-wrapper',
                    sectionName: module.string('new_release'),
                    sectionId: 'newRelease',
                    feedOptions: genres,
                    replaceUrl:false,
                    limitCount: options.limitCount,
                    hideOptions: !!options.genre
                });
                this.$spinner = module.$('.spinner', this.$el);
                this.$failure = module.$('.failure', this.$el);
                this.$tab = module.$('.tab-new-release');
            };

            view.show = function() {
                var $empty;

                if (this.selectedOption) {
                    LazyLoadingTrackView.show.call(this);
                    this.$el.show();
                } else {
                    this.$spinner.hide();
                    this.$failure.hide();
                    $empty = module.$('<div></div>');
                    $empty.addClass('empty');
                    $empty.text(module.string('empty_sectioned_track'));
                    this.$tracks.html($empty);
                    this.$el.hide();
                }
                this.$tab.addClass('selected');
                this.$el.addClass('selected');
            };

            view.remove = function() {
                LazyLoadingTrackView.remove.call(this);
                this.$tab.removeClass('selected');
                this.$el.removeClass('selected');
                this.$el.empty();
            };

            view.createReq = function(params) {
                return module.$.ajax({
                    url: module.coreApi.streamNewUrl,
                    data: params,
                    cache:false,
                    dataType:'json',
                    crossDomain: true
                });
            };

            view.parseResponse = function(res) {
                var that = this;

                return module.$.map(res.data, function(t, idx) {
                    var trackName = t.track_name,
                        title = t.artist_name + ' - ' + trackName,
                        track;

                    if (t.mix_type) {
                        title += ' (' + t.mix_type + ')';
                    }

                    track = new module.music.Music({
                        id: t.id,
                        type: t.type,
                        title: title,
                        track_name: trackName,
                        mix_type: t.mix_type,
                        user_name: t.artist_name,
                        user_resource_name: null, //artistNameToResourceName(t.artist_name),
                        created_at: t.release_date,
                        artwork: t.thumbnail,
                        drop: t.drop,
                        genre: t.genre
                    });

                    that.setMusicToStorage(track.getStorageKey(), track);
                    return track;
                });
            };
            return view;
        }());

        views.newUpload = (function() {
            var view = {};
            module.$.extend(view, LazyLoadingTrackView);

            view.key = 'newUpload';

            view.init = function(parentView) {
                var orderBy = [];

                orderBy.push({
                    name: module.string('order_popular'),
                    key: 'popular'
                });
                orderBy.push({
                    name: module.string('order_recent'),
                    key: 'recent'
                });

                LazyLoadingTrackView.init.call(this, {
                    parentView: parentView,
                    showRank:true,
                    el: '.child-tab-contents-new-upload',
                    context: {},
                    tmpl: '#tmpl-feed-new-upload-section',
                    trackTmpl : '#tmpl-useruploaded-addable-music',
                    tracks: '.tracks',
                    itemSelector: '.a-addable-music-wrapper',
                    sectionName: module.string('new_upload'),
                    sectionId: 'newUpload',
                    feedOptions: orderBy,
                    replaceUrl: true
                });
                this.$spinner = module.$('.spinner', this.$el);
                this.$failure = module.$('.failure', this.$el);
                this.$tab = module.$('.tab-new-upload');
            };

            view.show = function() {
                LazyLoadingTrackView.show.call(this);
                this.$tab.addClass('selected');
                this.$el.addClass('selected');
            };

            view.remove = function() {
                LazyLoadingTrackView.remove.call(this);
                this.$tab.removeClass('selected');
                this.$el.removeClass('selected');
                this.$el.empty();
            };

            view.createReq = function(params) {
                var data = {};
                if (params.g) {
                    data['order'] = params.g;
                }
                data['p'] = params.p;
                return module.$.ajax({
                    url: module.api.userTrackNewestUrl,
                    data: data,
                    cache:false,
                    dataType:'json'
                });
            };

            view.parseResponse = function(res) {
                var that = this,
                    data = res.data,
                    tracks = [];

                if (that.selectedOption.key !== 'recent') {
                    this.nextPage = -1;
                }
                module.$.each(data, function(idx, t) {
                    var track;

                    track = new module.music.DropbeatMusic(t);
                    track.desc = null;
                    that.setMusicToStorage(track.getStorageKey(), track);
                    tracks.push(track);
                });
                return tracks;
            };

            view.renderItems = function(data) {
                var contents = '',
                    $contents,
                    playerManager = module.playerManager,
                    player = playerManager.currentPlayer,
                    that = this,
                    preIdx,
                    section = this.sectionId.camelToUnderscore(),
                    $music;

                if (!this.userTrackGenres) {
                    this.userTrackGenres = {};
                    module.$.each(this.parentView.genres['dropbeat'], function(idx, genre) {
                        that.userTrackGenres[genre.id] = genre.name;
                    });
                }

                preIdx = this.$tracks.find(this.itemSelector).size() || 0;
                module.$.each(data, function(idx, t) {
                    contents += that.trackTmpl({
                        trackInfo: t,
                        rank: that.selectedOption.key !== 'recent' &&
                            that.showRank ? preIdx + idx + 1 : false,
                        editable: t.type === 'dropbeat' &&
                            module.context.account &&
                            module.context.account.resourceName === t.user.resource_name,
                        genres: that.userTrackGenres
                    });
                });

                $contents = module.$(contents);


                if (playerManager.currentMusic && player && player.section === section) {
                    $music = $contents.find(
                        '.a-addable-music[data-music-id=\'' +
                        playerManager.currentMusic.id + '\']');
                    $music.addClass('on');
                }

                this.$tracks.append($contents);
                initTooltipster($contents);
            };
            return view;
        }());

        views.channelExplore = (function() {
            var view = {};
            module.$.extend(view, LazyLoadingTrackView);

            view.key = 'channelExplore';

            view.init = function(parentView) {
                var genres = [];

                module._.bindAll(this, 'onFollowBtnClicked');

                genres.push({
                    name: 'ALL',
                    key: ''
                });
                genres = genres.concat(parentView.genres['dropbeat']);

                this.users = {};

                LazyLoadingTrackView.init.call(this, {
                    parentView: parentView,
                    showRank:false,
                    el: '.child-tab-contents-channel-explore',
                    context: {},
                    tmpl: '#tmpl-feed-channel-explore-section',
                    trackTmpl : '#tmpl-channel-explore-addable-music',
                    tracks: '.tracks',
                    itemSelector: '.channel-track',
                    sectionName: 'Channel Explore',
                    sectionId: 'channel_expolre',
                    replaceUrl: true
                });
                this.$spinner = module.$('.spinner', this.$el);
                this.$failure = module.$('.failure', this.$el);
                this.$tab = module.$('.tab-channel-explore');
                this.$el.on('click', '.channel-track .btn-follow', this.onFollowBtnClicked);
            };

            view.show = function() {
                LazyLoadingTrackView.show.call(this);
                this.$tab.addClass('selected');
                this.$el.addClass('selected');
            };

            view.remove = function() {
                LazyLoadingTrackView.remove.call(this);
                this.$tab.removeClass('selected');
                this.$el.off('click', '.channel-track .btn-follow', this.onFollowBtnClicked);
                this.$el.removeClass('selected');
                this.$el.empty();
            };

            view.createReq = function(params) {
                return module.$.ajax({
                    url: module.coreApi.channelFeedUrl,
                    data: JSON.stringify(params),
                    cache:false,
                    dataType:'json',
                    contentType:'application/json',
                    type:'POST',
                    crossDoamin: true
                });
            };

            view.getParams = function(nextPage, orderType, forceRefresh) {
                var params = {
                    p: nextPage
                };
                return params;
            };

            view.parseResponse = function(res) {
                var that = this,
                    data = res.data;

                return module.$.map(data, function(t, idx) {
                    var track;

                    track = new module.music.Music({
                        title: t.title,
                        id: t.video_id,
                        type:'youtube',
                        cretaed_at: t.published_at,
                        user_id: t.channel_id,
                        user_name: t.channel_title,
                        user_profile_image: t.channel_image,
                        user_resource_name: t.resource_name
                    });
                    track.user.type = 'channel';
                    that.users['c' + t.channel_id] = track.user;
                    that.setMusicToStorage(track.getStorageKey(), track);

                    return track;
                });
            };

            view.renderItems = function(data) {
                var contents = '',
                    $contents,
                    playerManager = module.playerManager,
                    player = playerManager.currentPlayer,
                    that = this,
                    preIdx,
                    section = this.sectionId.camelToUnderscore(),
                    $music;

                if (!this.userTrackGenres) {
                    this.userTrackGenres = {};
                    module.$.each(this.parentView.genres['dropbeat'], function(idx, genre) {
                        that.userTrackGenres[genre.id] = genre.name;
                    });
                }

                preIdx = this.$tracks.find(this.itemSelector).size() || 0;
                module.$.each(data, function(idx, t) {
                    contents += that.trackTmpl({
                        trackInfo: t,
                        rank:preIdx + idx + 1,
                        editable: t.type === 'dropbeat' &&
                            module.context.account &&
                            module.context.account.resourceName === t.user.resource_name,
                        genres: that.userTrackGenres,
                        isFollowing: false
                    });
                });

                $contents = module.$(contents);


                if (playerManager.currentMusic && player && player.section === section) {
                    $music = $contents.find(
                        '.a-addable-music[data-music-id=\'' +
                        playerManager.currentMusic.id + '\']');
                    $music.addClass('on');
                }

                this.$tracks.append($contents);
                initTooltipster($contents);
            };

            view.onFollowBtnClicked = function(e) {
                var $btn = module.$(e.currentTarget),
                    $btnText = module.$('.text', $btn),
                    $item = $btn.closest('.channel-info'),
                    $btnGroup,
                    itemId = $item.data('id'),
                    user = this.users[itemId];

                if (!module.context.account) {
                    module.view.NeedSigninView.show();
                    return;
                }

                $btnGroup = module.$('.channel-info[data-id=\'' + itemId +
                    '\'] .btn-follow', this.$el);

                if ($btn.hasClass('progress') || !user || !user.id) {
                    return;
                }

                $btnGroup.addClass('progress');

                if ($btn.hasClass('following')) {
                    module.context.account.unfollow(user, function(success, msg) {
                            $btnGroup.removeClass('progress');
                            if (!success) {
                                module.s.notify(msg || 'Failed to unfollow artist', 'error');
                                return;
                            }
                            $btnGroup.removeClass('following');
                            $btnGroup.find('.text').text($btnText.data('textFollow'));
                        });
                } else {
                    module.context.account.follow(user, function(success, msg) {
                            $btnGroup.removeClass('progress');
                            if (!success) {
                                module.s.notify(msg || 'Failed to follow artist', 'error');
                                return;
                            }
                            $btnGroup.addClass('following');
                            $btnGroup.find('.text').text($btnText.data('textFollowing'));
                        });
                }
            };

            return view;
        }());

        return views;
    }());

    HomePageView.subViews.FeedView = DropbeatView.extend({
        initialize: function(options) {
            DropbeatView.prototype.initialize.apply(this, arguments);
            this.key = options.key;
            this.$tabs = module.$('.child-tabs', this.$el);
            this.$tabContents = module.$('.child-tab-contents-wrapper', this.$el);
            this.$spinner = module.$('.right-section > .spinner', this.$el);
            this.$failure = module.$('.right-section > .failure', this.$el);

            this.$tab = module.$('.child-tab', this.$el);
        },

        events: function() {
            var events = DropbeatView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .child-tab': 'onTabClicked',
                'click > .failure .btn-retry': 'onRetryBtnClicked'
            });
        },

        remove: function() {
            this.undelegateEvents();
            this.stopListening();
            if (this.optionReq) {
                this.optionReq.abort();
                this.optionReq = null;
            }
            if (this.selectedView) {
                this.selectedView.remove();
            }
            this.selectedView = null;
        },

        show : function() {
            var $targetTab,
                $next,
                subTabKey;

            this.$el.addClass('selected');
            this.genres = module.context.genre;
            this.$spinner.hide();

            subTabKey = module.utils.gup('subtab');
            if (subTabKey) {
                $targetTab = this.$tabs.find('.tab-' + subTabKey);
            }
            if (!$targetTab || $targetTab.size() === 0) {
                $targetTab = this.$tabs.find('.child-tab').first();
            }

            $targetTab.click();
            this.$tabs.show();
            this.$tabContents.show();
        },

        onRetryBtnClicked : function(e) {
            this.show();
        },

        onTabClicked : function(e) {
            var $tab = module.$(e.currentTarget),
                target = $tab.data('target'),
                views = HomePageView.subViews.feedSubViews;

            target = target.replace(/-([a-z])/g, function (g) {
                return g[1].toUpperCase();
            });
            if (!views[target]) {
                return;
            }

            if (this.selectedView && this.selectedView.key === target) {
                return;
            }
            if (this.selectedView) {
                this.selectedView.remove();
            }
            this.selectedView = views[target];
            this.selectedView.init(this);
            this.selectedView.show();
        }
    });

    HomePageView.subViews.SectionedView = HomePageView.subViews.FeedView.extend({

        initialize:function(options) {
            var _genres = module.context.genre,
                genres = [],
                genreKeys = {},
                genre;

            HomePageView.subViews.FeedView.prototype
                .initialize.apply(this, arguments);

            this.$optionChoiceText = module.$('.option-select .selected-option',
                this.$el).find('.text');
            this.$optionDropdown = module.$('.option-select .dropdown', this.$el);

            this.childViews = [];

            genres.push('ALL');

            module.$.each(_genres['dropbeat'], function(idx, genre) {
                if (!genreKeys[genre.name.toLowerCase()]) {
                    genres.push(genre.name);
                    genreKeys[genre.name.toLowerCase()] = genre.name;
                }
            });

            module.$.each(_genres['default'], function(idx, genre) {
                if (!genreKeys[genre.name.toLowerCase()]) {
                    genres.push(genre.name);
                    genreKeys[genre.name.toLowerCase()] = genre.name;
                }
            });

            module.$.each(_genres['trending'], function(idx, genre) {
                if (!genreKeys[genre.name.toLowerCase()]) {
                    genres.push(genre.name);
                    genreKeys[genre.name.toLowerCase()] = genre.name;
                }
            });

            this.renderOption(this.$optionDropdown, genres);
            this.$filter = module.$('.overlay-filter');

            genre = module.utils.gup('genre');
            if (genre) {
                genre = genre.replace(/\+/g, '%20');
                this.selectedOption = decodeURIComponent(genre);
            }
            if (!this.selectedOption || genres.indexOf(this.selectedOption) === -1) {
                this.selectedOption = genres[0];
            }
            this.$optionDropdown.find('.option')
                .removeClass('selected')
                .first().addClass('selected');
            this.$optionChoiceText.text(this.selectedOption);
        },

        events: function() {
            var events = HomePageView.subViews.FeedView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .option-select .option': 'onOptionClicked',
                'click .option-select .selected-option': 'onSelectOption'
            });
        },

        onSelectOption: function() {
            var that = this;
            if (this.$optionDropdown.is(':hidden')) {
                this.$filter.show().one('click', function() {
                    that.$optionDropdown.hide();
                });
                this.$optionDropdown.show();
            }
        },

        onOptionClicked: function(e) {
            var $target = module.$(e.currentTarget),
                today, url,
                mm, dd, timestamp;

            if ($target.hasClass('selected')) {
                return;
            }

            module.$('.option-select .option', this.$el).removeClass('selected');
            $target.addClass('selected');
            this.selectedOption = $target.data('key');

            this.$optionChoiceText.text($target.text());
            if (this.$filter.is(':visible')) {
                this.$filter.click();
            }

            this.show();
        },

        renderOption: function($frame, options) {
            module.$.each(options, function(idx, option) {
                var $option = module.$('<li class=\'option\'>' + option + '</li>');
                $option.data('key', option);
                $option.attr('data-key', option);
                $frame.append($option);
            });
        },

        show : function() {
            var that = this,
                $targetTabs,
                $next,
                subTabKey,
                url;

            this.$el.addClass('selected');
            this.genres = module.context.genre;
            this.$spinner.hide();

            module.$.each(this.childViews, function(idx, view) {
                view.remove();
            });
            this.childViews.length = 0;

            $targetTabs = this.$tabs.find('.child-tab');
            $targetTabs.each(function(idx, tab) {
                module.$(tab).click();
            });

            url = location.pathname + '?' + module.$.param({
                tab: 'explore',
                genre:this.selectedOption
            });

            module.router.navigate(url, {trigger: false, replace:true});
            this.$tabContents.show();
        },

        onTabClicked : function(e) {
            var $tab = module.$(e.currentTarget),
                target = $tab.data('target'),
                views = HomePageView.subViews.feedSubViews,
                view;

            target = target.replace(/-([a-z])/g, function (g) {
                return g[1].toUpperCase();
            });
            if (!views[target]) {
                return;
            }

            view = views[target];
            view.init({
                parentView: this,
                limitCount: 6,
                genre: this.selectedOption === 'ALL' ? '_' : this.selectedOption,
                squareView: true
            });
            view.show();
            this.childViews.push(view);
        },

        remove: function() {
            var ParentView = HomePageView.subViews.FeedView;

            ParentView.prototype.remove.apply(this, arguments);
            module.$.each(this.childViews, function(idx, view) {
                view.remove();
            });
            this.childViews.length = 0;
        }
    });

    ExplorePageView = DropbeatPageView.extend({

        tmpl: '#tmpl-page-explore',

        initialize: function(options) {
            var feedName = options.name.dashToCamel(),
                feedNameKey = options.name.replace('-', '_'),
                view;

            module.$('header .header-menu .menu').removeClass('selected');
            DropbeatPageView.prototype.initialize.call(this, options);

            module.$.extend(this._context, {
                path: options.path,
                name: module.string(feedNameKey),
                key:options.name
            });
            this.render();
            this.genres = module.context.genre;

            this.childView = HomePageView.subViews.feedSubViews[feedName];
            this.childView.init({
                parentView: this
            });
            this.childView.show();
        },

        render:function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
            this.childView.remove();
        }
    });

    SharedTrackPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-shared-track',
        trackTmpl: '#tmpl-shared-track',
        officialSectionTmpl: '#tmpl-shared-track-another',
        mixSectionTmpl: '#tmpl-shared-track-mix',
        trendingSectionTmpl: '#tmpl-shared-track-trending',
        initialize: function(options) {
            DropbeatPageView.prototype.initialize.call(this, options);

            module._.bindAll(this, 'onIosPlayMusicClicked');

            if (module.context.isIOS) {
                module.$('#dropbeat').addClass('ios');
            }

            this._trackUid = options.trackUid

            this.render();
            this.section = 'shared_track';
            this._trackTmpl = module._.template(module.$(this.trackTmpl).html());
            this._officialSectionTmpl =
                module._.template(module.$(this.officialSectionTmpl).html());
            this._mixSectionTmpl = module._.template(module.$(this.mixSectionTmpl).html());
            this._trendingSectionTmpl = module._.template(module.$(this.trendingSectionTmpl).html());

            this.$trackFrame = module.$('.shared-track', this.$el);
            this.$failure = module.$('.failure', this.$el);
            this.$spinner = module.$('.spinner', this.$el);
            this.$sections = module.$('.sections', this.$el);

            // Start listen playable view events
            PlayableViewMixin.bind(this, this.$el, [this.section]);
            this.loadTrack(this._trackUid);
        },
        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .open-in-app-btn': 'onOpenInAppBtnClicked'
            });
        },
        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },
        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
            if (!module.context.isIOS) {
                PlayableViewMixin.unbind(this);
            } else {
                this.$el.off('click', '.play-music,.seemore', this.onIosPlayMusicClicked);
            }

            if (this.req) {
                this.req.abort();
                this.req = null;
            }

            if (this.artistReq) {
                this.artistReq.abort();
                this.artistReq = null;
            }

            if (this.officialReq) {
                this.officialReq.abort();
                this.officialReq = null;
            }

            if (this.mixReq) {
                this.mixReq.abort();
                this.mixReq = null;
            }

            if (this.trendingReq) {
                this.trendingReq.abort();
                this.trendingReq = null;
            }
        },
        onOpenInAppBtnClicked: function(e) {
            if (module.context.isMobile) {
                module.deeplink.open(
                    'dropbeat://net.dropbeat.labs?track=' + this._trackUid);
            }
        },
        onIosPlayMusicClicked: function(e) {
        },
        loadTrack: function(uid) {
            var that = this;

            this.$spinner.show();

            if (this.req) {
                this.req.abort();
            }
            this.req = module.$.ajax({
                url:module.api.trackShareUrl,
                data: {
                    uid:uid
                },
                dataType:'json'
            }).done(function(res) {
                var track;

                if (!res || !res.success || !res.data) {
                    that.$failure.show();
                    return;
                }

                track = new module.music.Music({
                    title: res.data.track_name,
                    type: res.data.type,
                    id: res.data.ref
                });
                that.setMusicToStorage(track.getStorageKey(), track);
                that.$trackFrame.html(that._trackTmpl({track: track}));
                if (!module.context.isIOS) {
                    that.loadSections(track);
                } else {
                    that.$spinner.hide();
                }
            }).fail(function(jqXHR, errText, err) {
                if (errText !== 'abort') {
                    that.$failure.show();
                }
            }).always(function() {
                that.req = null;
            });
        },

        loadSections: function(track) {
            var that = this,
                requests = [],
                sections = [];

            requests.push(that.loadMix(track, sections, 1));
            requests.push(that.loadOfficial(track, sections, 2));
            requests.push(that.loadTrending(track, sections, 3));

            module.$.when.apply($, requests).then(function() {
                that.$spinner.hide();
                if (sections.length === 0) {
                    return;
                }
                sections.sort(function(lhs, rhs) {
                    return lhs.priority - rhs.priority;
                });
                module.$.each(sections, function(idx, section) {
                    if (section.name !== 'trending' || idx === 0) {
                        that.$sections.append(section.$el);
                    }
                });
                if (that.$sections.find('.section').size() === 1) {
                    that.$sections.find('.section').addClass('wide');
                }
            });
        },
        loadTrending: function(track, sections, priority) {
            var that = this, deferred = module.$.Deferred();

            if (this.trendingReq) {
                this.trendingReq.abort();
            }
            this.trendingReq = module.$.ajax({
                url:module.coreApi.trendingChartUrl,
                data: {
                    type:'TOP100'
                },
                crossDomain: true,
                contentType: 'application/json',
                dataType: 'json'
            }).done(function(res) {
                var tracks = [];

                if (!res || !res.success ||
                        !res.data || res.data.length === 0) {
                    return;
                }

                module.$.each(res.data, function(idx, t) {
                    var m;
                    if (!t.youtube_uid || track.id === t.id) {
                        return true;
                    }
                    m = new module.music.Music({
                        title: t.artist_name + ' - ' + t.track_name,
                        track_name: t.track_name,
                        user_name: t.artist_name,
                        id: t.youtube_uid,
                        artwork: t.thumbnail,
                        type: 'youtube'
                    });
                    that.setMusicToStorage(m.getStorageKey(), m);
                    tracks.push(m);
                });
                if (tracks.length === 0) {
                    return;
                }

                sections.push({
                    $el: module.$(that._trendingSectionTmpl({tracks: tracks})),
                    priority: priority,
                    name:'trending'
                });
            }).always(function() {
                that.trendingReq = null;
                deferred.resolve();
            });
            return deferred;
        },
        loadMix: function(track, sections, priority) {
            var that = this, deferred = module.$.Deferred();

            if (this.mixReq) {
                this.mixReq.abort();
            }
            this.mixReq = module.$.ajax({
                url: module.coreApi.searchRemixUrl,
                data: {
                    q: track.title
                },
                crossDomain: true,
                contentType: 'application/json',
                dataType: 'json'
            }).done(function(res) {
                var tracks = [];
                if (!res || !res.success ||
                        !res.data || res.data.length === 0) {
                    return;
                }

                module.$.each(res.data, function(idx, t) {
                    var m;
                    if (t.id === track.id) {
                        return true;
                    }
                    m = new module.music.Music(t);
                    that.setMusicToStorage(m.getStorageKey(), m);
                    tracks.push(m);
                });

                if (tracks.length == 0) {
                    return;
                }

                module.$.each(tracks, function(idx, t) {
                    var thumbUrl;
                    if (t.tag === 'undefined_track') {
                        if (track.type === 'youtube') {
                            thumbUrl = 'http://img.youtube.com/vi/' + track.id + '/mqdefault.jpg';
                        } else {
                            thumbUrl = '/images/default_playlist_thumb.png';
                        }
                        t.thumb = thumbUrl;
                    }
                });

                sections.push({
                    $el: module.$(that._mixSectionTmpl({tracks: tracks})),
                    priority: priority,
                    name:'trending'
                });
            }).always(function() {
                that.mixReq = null;
                deferred.resolve();
            });
            return deferred;
        },
        loadOfficial: function(track, sections, priority) {
            var that = this, deferred = module.$.Deferred();

            if (this.officialReq) {
                this.officialReq.abort();
            }

            this.officialReq = module.$.ajax({
                url: module.coreApi.searchAnotherTracksUrl,
                data: {
                    q: track.title
                },
                crossDomain: true,
                contentType: 'application/json',
                dataType: 'json'
            }).done(function(res) {
                var $el, artists, tracks;

                if (!res || !res.success || !res.data) {
                    return;
                }

                tracks = res.data;

                artists = Object.keys(res.data);
                module.$.each(artists, function(idx, artist) {
                    var _tracks = [];
                    module.$.each(tracks[artist], function(idx, t) {
                        var m;
                        if (t.id === track.id) {
                            return true;
                        }
                        m = new module.music.Music(t);
                        that.setMusicToStorage(m.getStorageKey(), m);
                        _tracks.push(m);
                    });
                    if (_tracks.length > 0) {
                        tracks[artist] = _tracks;
                    } else {
                        delete tracks[artists];
                    }
                });
                artists = Object.keys(tracks);
                if (artists.length === 0) {
                    return;
                }

                $el = module.$(that._officialSectionTmpl({
                        artists: artists,
                        tracks:tracks
                    }));

                sections.push({
                    $el: $el,
                    priority: priority,
                    name:'another'
                });
            }).always(function() {
                that.officialReq = null;
                deferred.resolve();
            });

            return deferred;
        }
    });

    MobileSharedTrackPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-mobile-shared-track',
        trackTmpl: '#tmpl-mobile-shared-track',
        initialize: function(options) {
            var ua = navigator.userAgent;

            DropbeatPageView.prototype.initialize.call(this, options);

            this._trackUid = options.trackUid


            if (module.context.isMobile &&
                ((!module.context.isIOS ||
                (!ua.match(/CriOS/) &&
                (!ua.match(/Safari/) || !ua.match(/Version\/9/)))))) {
                module.deeplink.open('dropbeat://net.dropbeat.labs?track=' +
                    this._trackUid, function() {});
            }
            this.render();
            this.section = 'shared_track';
            this._trackTmpl = module._.template(module.$(this.trackTmpl).html());

            this.$failure = module.$('.failure', this.$el);
            this.$spinner = module.$('.spinner', this.$el);

            // Start listen playable view events
            PlayableViewMixin.bind(this, this.$el, [this.section]);
            this.loadTrack(this._trackUid);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .open-in-app-btn': 'onOpenInAppBtnClicked'
            });
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
            PlayableViewMixin.unbind(this);

            if (this.req) {
                this.req.abort();
                this.req = null;
            }
        },

        onTogglePlay: function(e, music, section, isPlaying) {
            var $btn;
            if (!this.track || section !== this.section || !music || music.id !== this.track.id) {
                return;
            }
            $btn = module.$('.a-addable-music .btn-play');
            if (!isPlaying) {
                $btn.find('i').removeClass('loading')
                    .removeClass('glyphicons-pause')
                    .addClass('glyphicons-play');
                $btn.find('.text').text(module.string('play'));
            } else if (!$btn.hasClass('loading')) {
                $btn.find('i').removeClass('loading')
                    .addClass('glyphicons-pause')
                    .removeClass('glyphicons-play');
                $btn.find('.text').text(module.string('pause'));
            }
        },

        onLoadingMusic: function(e, m, s) {
            var $btn = module.$('.a-addable-music .btn-play');

            $btn.find('i').addClass('loading')
                .removeClass('glyphicons-pause')
                .removeClass('glyphicons-play');
            $btn.find('.text').text(module.string('loading'));
            $btn.addClass('loading');
        },

        onPlayMusic: function(e, m, s) {
            var $btn = module.$('.a-addable-music .btn-play');

            $btn.find('i').removeClass('loading')
                .addClass('glyphicons-pause')
                .removeClass('glyphicons-play');
            $btn.find('.text').text(module.string('pause'));
            $btn.removeClass('loading');
        },

        onOpenInAppBtnClicked: function(e) {
            module.deeplink.open(
                'dropbeat://net.dropbeat.labs?track=' + this._trackUid);
        },

        loadTrack: function(uid) {
            var that = this;

            this.$spinner.show();

            if (this.req) {
                this.req.abort();
            }
            this.req = module.$.ajax({
                url:module.api.trackShareUrl,
                data: {
                    uid:uid
                },
                dataType:'json'
            }).done(function(res) {
                var track;

                if (!res || !res.success || !res.data) {
                    that.$failure.show();
                    return;
                }

                track = new module.music.Music({
                    title: res.data.track_name,
                    type: res.data.type,
                    id: res.data.ref
                });

                that.setMusicToStorage(track.getStorageKey(), track);
                that.track = track;
                that.$el.html(that._trackTmpl({track: track}));

                if (navigator.userAgent.indexOf('FBIOS') > -1) {
                    module.s.notify(module.string('fbios_mute_notice'), 'info', {
                        autoHide:false
                    });
                    module.s.notify(module.string('fbios_applink_notice'), 'info', {
                        autoHide:false
                    });
                }
            }).fail(function(jqXHR, errText, err) {
                if (errText !== 'abort') {
                    that.$failure.show();
                }
            }).always(function() {
                that.req = null;
                that.$spinner.hide();
            });
        }
    });


    SharedPlaylistPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-shared-playlist',
        playlistTmpl: '#tmpl-shared-playlist',
        initialize: function(options) {
            var ua = navigator.userAgent;

            module._.bindAll(this, 'onLoadFinished');

            DropbeatPageView.prototype.initialize.call(this, options);

            this._playlistUid = options.playlistUid;

            if (module.context.isMobile &&
                ((!module.context.isIOS ||
                (!ua.match(/CriOS/) &&
                (!ua.match(/Safari/) || !ua.match(/Version\/9/)))))) {
                module.deeplink.open('dropbeat://net.dropbeat.labs?playlist=' +
                    this._playlistUid, function() {});
            }

            this.render();
            if (module.context.isMobile) {
                this.$el.addClass('mobile');
            }
            this.section = 'shared_playlist';
            this._playlistTmpl = module._.template(module.$(this.playlistTmpl).html());

            this._$playlistFrame = module.$('.shared-playlist', this.$el);
            this._$failure = module.$('.failure', this.$el);
            this._$spinner = module.$('.spinner', this.$el);
            this._$playlistTitle = module.$('.playlist-title', this.$el);
            this._$playlistDesc= module.$('.playlist-desc', this.$el);
            this._$playlistDesc= module.$('.playlist-desc', this.$el);

            module.playlistManager.loadSharedPlaylist(
                this._playlistUid, this.onLoadFinished);

            // Start listen playable view events
            PlayableViewMixin.bind(this, this._$playlistFrame, [this.section]);
        },
        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .import-as-playlist-btn': 'onImportAsPlaylist',
                'click .open-in-app-btn': 'onOpenInAppBtnClicked'
            });
        },
        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },
        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
            PlayableViewMixin.unbind(this);
        },
        onOpenInAppBtnClicked: function(e) {
            if (module.context.isMobile) {
                module.deeplink.open(
                    'dropbeat://net.dropbeat.labs?playlist=' + this._playlistUid);
            }
        },
        onImportAsPlaylist: function(e) {
            var $btn = module.$(e.currentTarget),
                title = unescape($btn.attr('data-title')),
                $target = module.$('#' + $btn.data('target')),
                $music = $target.find('.a-addable-music'),
                tracks = [],
                ids = [],
                that = this;

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            if ($music.size() === 0) {
                module.s.notify(
                    module.string('failed_to_import_playlist'), 'error');
                return;
            }

            $music.each(function(idx, el) {
                var $el = module.$(el),
                    musicData = extractMusicFromDOM(that, $el);

                if (!musicData || !musicData.id || musicData.id === '' ||
                        ids.indexOf(musicData.id) > -1) {
                    return true;
                }
                ids.push(musicData.id);
                tracks.push(musicData);
            });

            module.s.storage.createPlaylist(title, function(playlist) {
                if (!playlist) {
                    module.s.notifiy(module.string('failed_to_create_playlist'), 'error');
                    return;
                }
                module.s.storage.setPlaylist(tracks, playlist.seqId,
                        function() {
                    module.playlistManager.loadPlaylists(function() {
                        var selector;

                        module.s.playlistTabs.renderAll();
                        module.playlistManager.updateCurrentPlaylistView();
                        selector = '.my-playlist .playlists ';
                        selector += '.playlist[data-playlist-id=\'';
                        selector += playlist.seqId + '\']';
                        selector += ' .nonedit-mode-view .title';
                        module.$(selector).click();

                        module.s.notify('Playlist imported', 'success');
                    });
                })
            });
        },
        onLoadFinished: function(success, playlists) {
            var playlist, that = this;

            this._$spinner.hide();
            if (!success || playlists.length === 0) {
                this._$failure.show();
                return;
            }

            playlist = playlists[0];

            module.$.each(playlist.playlist, function(idx, music) {
                that.setMusicToStorage(music.getStorageKey(), music);
            });

            module.playlistManager.publicPlaylists[playlist.id] = playlist;
            if (this._updatePlayPlaylistBtn) {
                this._updatePlayPlaylistBtn(
                    module.playlistManager.playingPlaylistId === playlist.id);
            }
            this._playlistId = playlist.id;

            module.$.extend(this._context, {playlist: playlist});
            this._$playlistFrame.html(this._playlistTmpl(this._context));
            this._$playlistFrame.show();

            if (module.context.isMobile && navigator.userAgent.indexOf('FBIOS') > -1) {
                module.s.notify(module.string('fbios_mute_notice'), 'info', {
                    autoHide:false
                });
                module.s.notify(module.string('fbios_applink_notice'), 'info', {
                    autoHide:false
                });
            }
        }
    });

    SearchResultPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-search-result',
        subViews: (function() {
            return module.$.extend ({}, DropbeatPageView.prototype.subViews);
        }()),
        initialize: function(options) {
            var that = this,
                $tab;

            module._.bindAll(this, 'onScroll', 'onSearchInputKeyUp',
                'handleSearchResult');
            DropbeatPageView.prototype.initialize.call(this, options);

            module.$('header .header-menu .menu').removeClass('selected');

            module.$.extend(this._context, {keyword: options.keyword});

            module.$('.search-input').val(options.keyword);
            this._podcastReq = null;
            this._eventReq = null;
            this.render();
            this.subViews.keywordView.init();

            this._$searchSpinner = module.$('.body-section .search-spinner');

// Start listen playable view events
            this._$searchResults = module.$('.search-result-section', this.$el);
            PlayableViewMixin.bind(this, this._$searchResults, ['search', 'drop']);
            WaveformViewMixin.bind(this, this.$el);

            if (options.keyword) {
                this._originalKeyword = options.keyword;
                this.onSubmit(options.keyword);
            }

            this._$bigSearchBox = module.$('#page-search-result .search-menu');
            this._$headerSearchMenu = module.$('.header-section .search-menu');

            module.$('.search-input').on('keyup', this.onSearchInputKeyUp);
            module.$('.contents .scroll-y').on('scroll', this.onScroll);
            module.$(window).on('resize', this.onScroll);
            setTimeout(function() {
                if (module.$('.contents .scroll-y').scrollTop() > that._$bigSearchBox.outerHeight()) {
                    that._$headerSearchMenu.show();
                    that._headerVisible = true;
                } else {
                    that._$headerSearchMenu.hide();
                    module.$('.search-input', that._$bigSearchBox).focus();
                    that._headerVisible = false;
                }
            }, 0);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .search-result-section .btn-follow': 'onFollowBtnClicked'
            });
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        onScroll: function() {
            var $scroll = module.$('.contents .scroll-y'),
                scrollTop = $scroll.scrollTop();

            if (scrollTop - this._$bigSearchBox.outerHeight() > 0 &&
                    !this._headerVisible) {
                this._$headerSearchMenu.fadeIn(100);
                this._headerVisible = true;
            } else if (scrollTop - this._$bigSearchBox.height() < 0 &&
                    this._headerVisible) {
                this._$headerSearchMenu.fadeOut(100);
                this._headerVisible = false;
            }
        },

        onFollowBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $btnText = module.$('.text', $btn),
                $item = $btn.closest('.artist-wrapper'),
                itemId = $item.data('id'),
                user = this.users[itemId];

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            if ($btn.hasClass('progress') || !itemId || itemId === '') {
                return;
            }

            $btn.addClass('progress');

            if ($btn.hasClass('following')) {
                module.context.account.unfollow(user, function(success, msg) {
                        $btn.removeClass('progress');
                        if (!success) {
                            module.s.notify(module.string('failed_to_unfollow'), 'error');
                            return;
                        }
                        $btn.removeClass('following');
                        $btnText.text($btnText.data('textFollow'));
                    });
            } else {
                module.context.account.follow(user, function(success, msg) {
                        $btn.removeClass('progress');
                        if (!success) {
                            module.s.notify(module.string('failed_to_follow'), 'error');
                            return;
                        }
                        $btn.addClass('following');
                        $btnText.text($btnText.data('textFollowing'));
                    });
            }
        },

        onSearchInputKeyUp: function(e) {
            setTimeout(module.$.proxy(function() {
                var keyword = module.$(e.currentTarget).val(),
                    $label = module.$('.search-keyword-label', this._$bigSearchBox);
                module.$('.search-input').each(function(idx, el) {
                    if (module.$(this).val() !== keyword) {
                        module.$(this).val(keyword);
                    }
                });
                if (keyword === this._originalKeyword) {
                    $label.text('Search results for');
                } else {
                    $label.text('Start new search for');
                }
            }, this), 0);
        },

        onSubmit: function(q, callback) {
            var that = this;
            module.feature.search.box.reset();
            if (this.searchReq) {
                this.searchReq.abort();
            }
            this.searchReq = module.feature.search.box.doSearch(
                q, function() {
                    that._$searchResults.show();
                    that._$searchSpinner.hide();
                    that.handleSearchResult.apply(this, arguments);
                });
            if (this.searchReq) {
                this._$searchResults.hide();
                this._$searchSpinner.show();
                this.searchReq.always(function() {
                    that.searchReq = null;
                });
            }
            this.subViews.keywordView.show(q);
        },

        remove: function() {
            var currentPlayer;

            DropbeatPageView.prototype.remove.apply(this, arguments);
            module.feature.search.box.reset();
            module.$('.search-input').off('keyup', this.onSearchInputKeyUp);
            module.$('.contents').off('scroll', this.onScroll);
            module.$(window).off('resize', this.onScroll);

            if (module.playerManager.playing) {
                currentPlayer = module.playerManager.currentPlayer;
                if (currentPlayer.section === 'drop') {
                    module.playerManager.onPlayMusic(
                        null, currentPlayer.section);
                }
            }

            PlayableViewMixin.unbind(this);
            WaveformViewMixin.unbind(this);

            if (this.searchReq) {
                this.searchReq.abort();
                this.searchReq = null;
            }
        },

        handleSearchResult: function(success, data) {
            var that = this,
                results = {},
                tracks,
                track,
                followables,
                artists = [],
                $results,
                html = '',
                artistTmpl,
                internalTrackTmpl,
                externalTrackTmpl;

            if (!success) {
                if (data) {
                    this._$searchResults.html(
                        '<div class=\'failure\'>Failed to fetch search result :(</div>');
                    this._$searchResults.hide();
                } else {
                    this._$searchResults.html('');
                    this._$searchResults.show();
                }
                return;
            }
            this._$searchResults.show();

            followables = data.artists || [];
            tracks = data.tracks || [];

            module.$.each(followables, function(idx, followable) {
                var user, localId;

                if (followable.user_type === 'user') {
                    localId = 'u' + followable.id;
                } else if (followable.user_type === 'artist') {
                    localId = 'a' + followable.id;
                } else if (followable.user_type === 'channel') {
                    localId = 'c' + followable.id;
                } else {
                    return true;
                }

                user = {
                    id: followable.id,
                    nickname:followable.name,
                    resource_name: followable.resource_name,
                    profile_image: followable.image,
                    type: followable.user_type,
                    localId: localId
                };

                artists.push(user);
            });

            this.resultEscape(tracks);

            artistTmpl = module._.template(module.$('#tmpl-search-artist').html());
            externalTrackTmpl = module._.template(module.$('#tmpl-external-addable-music').html());
            internalTrackTmpl = module._.template(module.$('#tmpl-useruploaded-addable-music').html());

            if (tracks.length === 0 && artists.length === 0) {
                html = '<div class=\'no-search-result\'>There is no search result</div>';
                $results = this._$searchResults;
                $results.html(html);
                setTimeout(module.$.proxy(function() {
                    this._$searchResults.scrollTop(0);
                }, this), 0);
                return;
            }

            this.users = {};
            if (artists.length > 0) {
                html += '<div class=\'artists-section section-item\'>';
                html += '<div class=\'section-title\'>People</div>';

                module.$.each(artists, function(idx, artist) {
                    that.users[artist.localId] = artist;

                    html += artistTmpl({
                            user:artist
                        });
                });

                html += '</div>';
            }

            if (tracks.length > 0) {
                if (artists.length === 0) {
                    track = new module.music.Music(tracks.shift());
                    if (track.type !== 'dropbeat') {
                        html += '<div class=\'top-match-section section-item\'>';
                        html += '<div class=\'section-title\'>Top match</div>';

                        html += externalTrackTmpl({
                            trackInfo: track
                        });
                        html += '</div>';
                    }
                }

                if (tracks.length > 0) {
                    html += '<div class=\'relevant-section section-item\'>';
                    html += '<div class=\'section-title\'>Tracks</div>';

                    module.$.each(tracks, function(idx, t) {
                        var track;
                        if (t.type === 'dropbeat') {
                            track = new module.music.DropbeatMusic(t.snippet);
                            html += internalTrackTmpl({
                                trackInfo: track
                            });
                        } else {
                            track = new module.music.Music(t);
                            html += externalTrackTmpl({
                                trackInfo: track
                            });
                        }
                        that.setMusicToStorage(track.getStorageKey(), track);
                    });
                    html += '</div>';
                }
            }

            $results = this._$searchResults;
            $results.html(html);
            this.updateWaveform();
            this.updatePlayState();
            setTimeout(module.$.proxy(function() {
                module.$(this._$searchResults).scrollTop(0);
            }, this), 0);
        },

        resultEscape: function (resp) {
            var i;

            for (i = 0; i < resp.length; i += 1) {
                resp[i].title = module.escapes.title(resp[i].title);
            }
        }

    });

    SearchResultPageView.prototype.subViews.keywordView = (function() {
        var view = {},
            template = '#tmpl-keywords';

        view._req = null;
        view.init = function() {
            this.$el = module.$('.body-section .recommended-keywords');
            this._tmpl = module._.template(module.$(template).html());
            this.hide();
            this.$el.on('click', '.keyword', function() {
                module.log('search-section', 'search-with-recommend-keyword',
                    module.$(this).text());

                var q = module.$(this).text(),
                    path;

                path = '/?q=' + encodeURIComponent(q);
                if (path === window.location.pathname) {
                    return;
                }
                module.router.navigate(path, {trigger: true});
            });
        };

        view.reset = function() {
            if(this._req) {
                this._req.abort();
            }
            this.$el.html('');
        };

        view.hide = function() {
            this.$el.hide();
        };

        view.show = function(keyword) {
            var that = this;
            this._req = module.$.ajax({
                url:module.coreApi.searchRelatedUrl,
                data: {
                    q: keyword
                },
                crossDomain: true,
                dataType: 'json'
            }).done(function(resp) {
                if (!resp || !resp.success ||
                        !resp.data || resp.data.length === 0) {
                    view.hide();
                    return;
                }
                that.$el.html(that._tmpl({keywords: resp.data}));
                that.$el.show();
            }).always(function() {
                that._req = null;
            });
        };
        return view;
    }());


    IframePageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-iframe',
        subViews: (function() {
            return module.$.extend ({}, DropbeatPageView.prototype.subViews);
        }()),
        initialize: function(options) {

            DropbeatPageView.prototype.initialize.call(this, options);
            module.$.extend(this._context, { src: options.src});
            this.render();
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
        }
    });

    AppDownloadPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-app-download',
        initialize: function(options) {
            var that = this;
            DropbeatPageView.prototype.initialize.call(this, options);
            module.$.extend(this._context, {os:options.os});

            this.render(options.redirect);

            module.$('#search-result-player').remove();
            module.$('.player-section').remove();
            module.$('.playlist-section').remove();
            module.$('#dropbeat .contents').css('marginBottom', 0);
            module.$('#dropbeat .contents .contents-inner').css('paddingBottom', 0);
            module.$('header .search-menu').remove();
            module.$('header .social-plugins').remove();
            module.$('header .header-menu').remove();
            module.$('header .account-menus .import-playlist-menu').remove();
            module.$('header .account-menus .get-app-menu').remove();
            module.$('header .account-menus .signin-btn').remove();
            module.$('header .account-menus .signup-btn').remove();
            module.view.initContentsBg();

        },
        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .open-in-app-btn': 'onOpenInAppBtnClicked'
            });
        },
        render: function(path) {
            var contents;

            contents = this._tmpl(this._context);
            this._$pageTarget.html(contents);
            this.$el = module.$('.page', this._$pageTarget);
        }
    });

    UploadPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-upload',
        initialize: function(options) {
            var that = this;

            module._.bindAll(this,
                'onDropSelectorUp', 'onDropSelectorMove', 'onAudioPrepared',
                'onAudioError');

            DropbeatPageView.prototype.initialize.call(this, options);

            module.$('header .header-menu .menu').removeClass('selected');
            module.$('header .header-menu .upload-menu').addClass('selected');
            this._failureTmpl = module._.template(module.$('#tmpl-page-upload-not-supported').html());

            this.render();
            this.credentialCallbacks = [];

            this.$uploadForm = module.$('.upload-form', this.$el);

            this.$soundUploadForm = module.$('#form-upload-track', this.$el);
            this.$soundProgressFrame = module.$('.sound-progress-wrapper', this.$el);
            this.$soundUploadBtn = module.$('.btn-upload-sound', this.$soundUploadForm);
            this.$soundCloudImportBtn = module.$('.btn-soundcloud-import', this.$soundUploadForm);
            this.$uploadMethodOr = module.$('.or', this.$soundUploadForm);
            this.$soundUploadCancelBtn = module.$('.track-sound-uploader .btn-cancel', this.$el);

            this.$dropProgressFrame = module.$('.drop-progress-wrapper', this.$el);
            this.$dropProgress = module.$('.progress', this.$dropProgressFrame);
            this.$dropProgressPercent = module.$('.progress .progress-label', this.$dropProgressFrame);

            this.$soundProgress = module.$('.progress', this.$soundProgressFrame);
            this.$soundProgressPercent = module.$('.progress .progress-label', this.$soundProgressFrame);

            this.$trackInfoUploader = module.$('.track-info-uploader', this.$el);
            this.$trackInfoForm = module.$('#form-upload-track-info', this.$el);
            this.$trackInfoTitle = module.$('.input-title', this.$trackInfoUploader);
            this.$trackInfoDesc = module.$('.input-description', this.$trackInfoUploader);
            this.$trackInfoTrackUrl = module.$('.input-track-url', this.$trackInfoUploader);
            this.$trackInfoTrackKey = module.$('.input-unique-key', this.$trackInfoUploader);
            this.$trackInfoGenreId = module.$('.input-genre', this.$trackInfoUploader);
            this.$trackInfoCoverArtImg = module.$('.thumbnail .preview img', this.$trackInfoUploader);
            this.$trackInfoCoverArtUrl = module.$('.input-coverart-url', this.$trackInfoUploader);
            this.$trackInfoWaveformUrl = module.$('.input-waveform-url', this.$trackInfoUploader);
            this.$trackInfoResourceName = module.$('.input-resource-name', this.$trackInfoUploader);
            this.$trackInfoDropUrl = module.$('.input-drop-url', this.$trackInfoUploader);
            this.$trackInfoDropStart = module.$('.input-drop-start', this.$trackInfoUploader);
            this.$trackInfoDropDuration = module.$('.input-drop-duration', this.$trackInfoUploader);
            this.$trackInfoDuration = module.$('.input-duration', this.$trackInfoUploader);
            this.$trackInfoDownloadable = module.$('.input-downloadable', this.$trackInfoUploader);
            this.$trackInfoTrackType = module.$('.input-track-type', this.$trackInfoUploader);
            this.$trackInfoUploaderSubmitBtn = module.$('.btn-save', this.$el);

            this.$genreSelectFrame = module.$('.genre-select-wrapper', this.$el);
            this.$genreLoader = module.$('.spinner', this.$genreSelectFrame);
            this.$genreSelect = module.$('.genre-select', this.$genreSelectFrame);
            this.$genreFailure = module.$('.failure', this.$genreSelectFrame);
            this.$genreDropdown = module.$('.dropdown', this.$genreSelectFrame);
            this.$filter = module.$('.overlay-filter');

            this.$downloadableChoice = module.$('.downloadable-input-wrapper .choice', this.$el);
            this.$trackTypeChoice = module.$('.track-type-input-wrapper .choice', this.$el);

            this.$dropInfoUploaderFrame = module.$('.drop-info-uploader-wrapper', this.$el);
            this.$dropInfoUploader = module.$('.drop-info-uploader', this.$dropInfoUploaderFrame);
            this.$waveformFrame = module.$('.waveforms', this.$dropInfoUploader);
            this.$dropSelector = module.$('.drop-selectors', this.$dropInfoUploader);
            this.$dropSelectorBar = module.$('.drop-selector', this.$dropSelector);

            this.$waveformProgressFrame = module.$('.waveform-progress-wrapper', this.$dropInfoUploaderFrame);
            this.$waveformProgress = module.$('.progress', this.$waveformProgressFrame);
            this.$waveformCanvasFrame = module.$('.canvas-wrapper', this.$dropInfoUploaderFrame);
            this.$waveformProgressPercent = module.$('.progress .progress-label', this.$waveformProgressFrame);

            this.$dropBtn = module.$('.listen-drop', this.$dropInfoUploader);
            this.$dropBtnText = module.$('.listen-drop .text', this.$dropInfoUploader);

            this.genre = module.context.genre['dropbeat'];
            module.$.each(this.genre, function(id, genre) {
                that.$genreDropdown.append(
                    '<li class=\'genre\' data-key=\'' + genre.id + '\'>' + genre.name + '</li>');
            });
            this.$genreSelect.show();
            this.$genreLoader.hide();

            module.$(window).on('mouseup', this.onDropSelectorUp);
            module.$(window).on('mousemove', this.onDropSelectorMove);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .btn-soundcloud-import' : 'onSoundcloudImportBtnClicked',
                'change #form-upload-track input[type=\'file\']': 'onSoundUploadStart',
                'click #form-upload-track .btn-upload-sound': 'onSoundUploadBtnClicked',

                'click .btn-upload-thumbnail': 'onThumbnailUploadBtnClicked',

                'click .uploader .btn-save': 'onTrackInfoSaveBtnClicked',
                'click .uploader .btn-cancel': 'onCancelBtnClicked',
                'click .uploader .btn-cancel-drop': 'onCancelDropBtnClicked',

                'click .genre-select .selected-genre': 'onSelectGenre',
                'click .genre-select .genre': 'onGenreClicked',

                'click .downloadable-input-wrapper .choice': 'onToggleDownloadable',
                'click .track-type-input-wrapper .choice': 'onTrackTypeSelected',

                'click .drop-info-uploader .listen-drop': 'onDropBtnClicked',

                'mousedown .drop-selectors' : 'onDropSelectorDown'
            });
        },

        render: function() {
            if (this.canUpload()) {
                this._$pageTarget.html(this._tmpl(this._context));
            } else {
                this._$pageTarget.html(this._failureTmpl(this._context));
            }
            this.$el = module.$('.page', this._$pageTarget);
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
            module.$(window).off('mouseup', this.onDropSelectorUp);
            module.$(window).off('mousemove', this.onDropSelectorMove);

            module.removeBeforeunloadCallback('upload');
            this.cancelUploader();

        },

        onSoundcloudImportBtnClicked: function() {
            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }
            module.router.navigate('/soundcloud-import', {trigger: true});
        },

        canUpload: function() {
            var xhr;
            if (window['XMLHttpRequest']) {
                xhr = new XMLHttpRequest();
                if (!('withCredentials' in xhr) && typeof XDomainRequest === 'undefined') {
                    return false;
                }
            }
            if (!window['AudioContext'] && !window['webkitAudioContext']) {
                return false;
            }
            if (module.context.isMobile) {
                return false;
            }
            return true;
        },

        onToggleDownloadable: function(e) {
            var currVal = this.$trackInfoDownloadable.val(),
                $ic = this.$downloadableChoice.find('i');

            if (currVal === 'true') {
                this.$downloadableChoice.addClass('selected');
                this.$trackInfoDownloadable.val(false);
                this.$trackInfoDownloadable.attr('value', false);
                $ic.removeClass('glyphicons-unchecked').addClass('glyphicons-check');
            } else {
                this.$downloadableChoice.removeClass('selected');
                this.$trackInfoDownloadable.val(true);
                this.$trackInfoDownloadable.attr('value', true);
                $ic.addClass('glyphicons-unchecked').removeClass('glyphicons-check');
            }
        },

        onTrackTypeSelected: function(e) {
            var currVal = this.$trackInfoTrackType.val(),
                $choice = module.$(e.currentTarget),
                key = $choice.data('key'),
                $ic = $choice.find('i'),
                audio,
                $dropText;

            if ($choice.hasClass('selected')) {
                return;
            }

            this.$trackTypeChoice.removeClass('selected');

            this.$trackTypeChoice.find('i')
                .removeClass('glyphicons-check')
                .addClass('glyphicons-unchecked');

            $choice.addClass('selected');
            $ic.removeClass('glyphicons-unchecked').addClass('glyphicons-check');

            this.$trackInfoTrackType.val(key);
            this.$trackInfoTrackType.attr('value', key);

            if (key === 'TRACK') {
                this.$dropInfoUploader.removeClass('disabled');
            } else {
                if (this.soundPlayer) {
                    this.soundPlayer.pause();
                }

                this.$dropBtn.removeClass('stop');
                $dropText = this.$dropBtn.find('.text');
                $dropText.text($dropText.data('textPlay'));
                this.$dropBtn.find('i').addClass('glyphicons-play')
                    .removeClass('glyphicons-stop');

                this.$dropInfoUploader.addClass('disabled');
            }
        },

        onDropBtnClicked : function(e) {
            var drop,
                $btn = module.$(e.currentTarget),
                $text = $btn.find('.text'),
                $ic= $btn.find('i'),
                that = this;

            if (!this.soundPlayer) {
                return;
            }
            drop = this.$trackInfoDropStart.val();
            if (drop !== 0 && !drop) {
                return;
            }

            if ($btn.hasClass('stop')) {
                // do stop
                this.soundPlayer && this.soundPlayer.pause();
                $btn.removeClass('stop');
                $text.text($text.data('textPlay'));
                $ic.addClass('glyphicons-play').removeClass('glyphicons-stop');
                return;
            }

            this.soundPlayer.setPosition(drop * 1000);
            this.soundPlayer.play();
            $btn.addClass('stop');
            $btn.find('.text');
            $text.text($text.data('textStop'));
            $ic.removeClass('glyphicons-play').addClass('glyphicons-stop');

            if (this.dropTimer) {
                clearTimeout(this.dropTimer);
            }

            this.dropTimer = setTimeout(function() {
                if (that.$dropBtn.hasClass('stop')) {
                    that.$dropBtn.click();
                }
                that.dropTimer = null;
            }, 20000);
        },

        onDropSelectorDown:function(e) {
            if (this.$dropSelectorBar.is(':hidden')) {
                return;
            }
            var $selector = module.$(e.currentTarget);
            this._dropSelectorDown = true;
            this._dropSelectorDownPosition = e.pageX - this.$dropSelector.offset().left;
        },

        onDropSelectorUp:function(e) {
            var drop, audio,
                that = this;

            if (!this._dropSelectorDown) {
                return;
            }
            this._dropSelectorDown = false;
            this._dropSelectorDownPosition = 0;

            drop = this.$trackInfoDropStart.val();

            if (this.$dropBtn.hasClass('stop') && drop >= 0) {
                if (this.soundPlayer) {
                     this.soundPlayer.setPosition(drop * 1000);
                }

                if (this.dropTimer) {
                    clearTimeout(this.dropTimer);
                }

                this.dropTimer = setTimeout(function() {
                    if (that.$dropBtn.hasClass('stop')) {
                        that.$dropBtn.click();
                    }
                    that.dropTimer = null;
                }, 20000);
            }
        },

        onDropSelectorMove: function(e) {
            var x, offsetLeft, left, duration, dropWidth,
                canvasWidth = this.$waveformFrame.width(),
                dropSec = 20;

            if (!this._dropSelectorDown) {
                return;
            }

            duration = this.$trackInfoDuration.val();
            if (!duration) {
                return;
            }

            dropWidth = dropSec * (canvasWidth / duration);

            offsetLeft = this.$waveformFrame.offset().left;
            x = e.pageX - offsetLeft
            left = x - this._dropSelectorDownPosition;

            if (left < 0) {
                left = 0;
            }
            if (left > canvasWidth) {
                left = canvasWidth;
            }
            if (left > canvasWidth - dropWidth) {
                left = canvasWidth - dropWidth;
            }

            this.$dropSelector.css({
                left:left + 'px',
                width: dropWidth + 'px'
            });
            this.updateDropArea();
        },

        renderInitialDropSelector: function() {
            var duration, dropWidth,
                canvasWidth = this.$waveformFrame.width(),
                dropSec = 20,
                left,
                leftStr;

            duration = this.$trackInfoDuration.val();
            if (!duration) {
                this.$dropSelector.hide();
                return;
            }

            dropWidth = dropSec * (canvasWidth / duration);

            leftStr = this.$dropSelector.css('left');
            if (leftStr.indexOf('%') > -1) {
                left = parseInt(leftStr) * canvasWidth / 100;
            } else {
                left = parseInt(leftStr);
            }

            if (left < 0) {
                left = 0;
            }
            if (left > canvasWidth - dropWidth) {
                left = canvasWidth - dropWidth;
            }

            this.$dropSelector.css({
                left:left + 'px',
                width: dropWidth + 'px'
            });
            this.updateDropArea();
        },

        updateDropArea: function() {
            var that = this,
                left, w, tmp,
                width = this.$waveformFrame.width(),
                colorPlaying = '#fff',
                //colorDrop = '#cd85ff',
                colorDrop = '#f03eb1',
                $canvas,
                canvasWidth = width,
                canvasHeight = 80,
                blockCount,
                marginWidth = 1,
                barWidth = 2,
                duration,
                dropStart,
                context,
                fromIdx,
                toIdx,
                i,
                fromX,
                toX,
                val, x, barHeight, y, delta;

            blockCount = Math.floor(canvasWidth / (marginWidth + barWidth));

            duration = this.$trackInfoDuration.val();
            if (!duration) {
                this.$dropSelector.hide();
                return;
            }
            this.$dropSelector.show();

            left = this.$dropSelector.css('left');
            if (left.endsWith('%')) {
                left = parseInt(left);
            } else {
                left = (parseInt(left) * 100) / width;
            }

            w = this.$dropSelector.css('width');
            if (w.endsWith('%')) {
                w = parseInt(w);
            } else {
                w = (parseInt(w) * 100) / width;
            }

            dropStart = duration * left / 100;
            this.setDropInput(dropStart, 20);

            if (!this.waveformData) {
                return;
            }
            if (!this.canvas) {
                $canvas = module.$('<canvas class=\'waveform-drop-canvas\' width=\'' +
                    canvasWidth + '\' height=\'' + canvasHeight + '\'/>');
                this.$waveformCanvasFrame.append($canvas);
                this.canvas = this.$waveformCanvasFrame.find('.waveform-drop-canvas')[0];
            }
            context = this.canvas.getContext('2d');
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            context.save();

            fromX = Math.round(canvasWidth * left / 100);
            toX = Math.round(canvasWidth * (left + w) / 100);
            fromIdx = Math.floor(fromX / (barWidth + marginWidth));
            toIdx = Math.ceil(toX / (barWidth + marginWidth));

            for(i = fromIdx; i < blockCount && i < toIdx; i++) {

                val = that.waveformData[i];
                x = i * (barWidth + marginWidth);
                barHeight = val * canvasHeight * 0.009;
                y = canvasHeight - barHeight;
                context.fillStyle = colorDrop;
                w = barWidth;
                if (i === fromIdx) {
                    delta = x;
                    x = Math.max(x, fromX);
                    delta = x - delta;
                    w -= delta;
                }
                if (i === toIdx - 1) {
                    delta = Math.min(x + w, toX) - x;
                    w = delta;
                }
                context.fillRect(x, y, w, barHeight);
            }
            context.restore();
        },

        onSoundUploadBtnClicked: function(e) {
            var $btn;

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return false;
            }
            $btn = this.$soundUploadForm.find('input[type=\'file\']');
            $btn.click();
        },

        onThumbnailUploadBtnClicked: function(e) {
            var that = this,
                $btn,
                browser = module.context.browserSupport.currentBrowser,
                isIE9 = browser[0] === 'MSIE' && browser[1] < 10;

            if (isIE9) {
                module.s.notify(module.string('image_uploading_not_supported'), 'error');
                return;
            }

            module.view.ImageUploadCropView.show(
                {aspectRatio:1, imageType:'c', width:600, height: 600},
                function(url) {
                    if (!url) {
                        return;
                    }
                    that.setThumbUrlInput(url);
                });
        },

        onTrackInfoSaveBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                formArray = this.$trackInfoForm.serializeArray(),
                data = {},
                that = this;

            if ($btn.attr('disabled')) {
                return;
            }

            module.$.each(formArray, function(idx, s) {
                var val = s.value;
                if (val === 'false') {
                    val = false;
                } else if (val === 'true') {
                    val = true;
                }
                data[s.name] = val;
            });

            if (!validate(data)) {
                module.s.notify(module.string('failed_to_submit'), 'error');
                return;
            }

            if (data.track_type === 'MIX') {
                data.drop_start = -1;
                data.drop_duration = -1;
                data.drop_url = null;
                submit(data);
            } else if (data.drop_url) {
                submit(data);
                return;
            } else {
                if (data.drop_start < 0) {
                    module.s.notify('Drop should be selected', 'error');
                    return;
                }
                this.uploadDrop(
                    this.credentialKey, this.uploadHost, data.unique_key, data.drop_start)
                    .done(function(dropUrl) {
                        that.setDropUrlInput(dropUrl);
                        data.drop_url = dropUrl;
                        submit(data);
                    })
                    .fail(function(msg) {
                        if (msg === 'canceled') {
                            return;
                        }
                        module.s.notify(msg || module.string('failed_to_upload'), 'error');
                    });
            }

            function submit(data) {

                that.$trackInfoForm.find('input textarea').attr('disabled');
                $btn.addClass('progress');

                if (that.uploadTrackInfoReq) {
                    return;
                }

                that.postSoundProgress(-1, 'Uploading track information.. (2/2)');

                that.uploadTrackInfoReq = module.$.ajax({
                    url: module.api.trackUrl,
                    data: JSON.stringify(data),
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function(res) {
                    var resourcePath;
                    if (!res || !res.success || !res.obj) {
                        module.s.notify(res.error || module.string('failed_to_upload'), 'error');
                        that.$dropProgressFrame.hide();
                        return;
                    }

                    resourcePath = '/r/' + res.obj.user_resource_name + '/' + res.obj.resource_path;
                    module.removeBeforeunloadCallback('upload');
                    module.router.navigate(resourcePath, {trigger: true});
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus === 'abort') {
                        return;
                    }
                    module.s.notify(module.string('failed_to_upload'), 'error');
                    that.$dropProgressFrame.hide();
                }).always(function() {
                    that.uploadTrackInfoReq = null;
                    that.$trackInfoForm.find('input textarea').removeAttr('disabled');
                    $btn.removeClass('progress');
                });
            }

            function validate(data) {
                var errors = {},
                    $frames = {
                        'name': that.$trackInfoTitle,
                        'unique_key': that.$trackInfoTrackKey,
                        'duration': that.$trackInfoDropDuration,
                        'drop_start' : that.$trackInfoDropStart,
                        'drop_duration' : that.$trackInfoDropDuration,
                        'genre_id': that.$trackInfoGenreId,
                        'description': that.$trackInfoDesc,
                        'waveform_url': that.$trackInfoWaveformUrl,
                        'coverart_url': that.$trackInfoCoverArtUrl,
                        'track_url': that.$trackInfoTrackUrl,
                        'track_type': that.$trackInfoTrackType
                    };

                if (!data.name) {
                    errors['name'] = module.string('required_field');
                } else if (data.name.length > 80) {
                    errors['name'] = module.string('input_must_be_less_than_%d', 80);
                }

                if (!data.duration) {
                    errors['duration'] = module.string('required_field');
                }

                if (!data.drop_start) {
                    errors['drop_start'] = module.string('required_field');
                }

                if (!data.drop_duration) {
                    errors['drop_duration'] = module.string('required_field');
                }

                if (!data.genre_id) {
                    errors['genre_id'] = module.string('required_field');
                }

                if (!data.description) {
                    errors['description'] = module.string('required_field');
                }

                if (!data.waveform_url) {
                    errors['waveform_url'] = module.string('required_field');
                }

                if (!data.track_url) {
                    errors['track_url'] = module.string('required_field');
                }

                if (!data.coverart_url) {
                    errors['coverart_url'] = module.string('required_field');
                    module.$('.track-info-uploader .thumbnail .errors').text(
                        module.string('required_field'));
                }

                if (!data.track_type) {
                    errors['track_type'] = module.string('required_field');
                }

                if (['MIX', 'TRACK'].indexOf(data.track_type) === -1) {
                    errors['track_type'] = module.string('track_type_is_not_proper');
                }

                module.$.each($frames, function(key, input) {
                    var $el = module.$(input),
                        $wrapper = $el.closest('.input-wrapper'),
                        $errors;

                    if ($wrapper.size() === 0) {
                        return true;
                    }
                    $errors = $wrapper.find('.errors');
                    if (errors[key]) {
                        $errors.text(errors[key]);
                        $wrapper.addClass('error');
                    } else {
                        $errors.text('');
                        $wrapper.removeClass('error');
                    }
                });
                return Object.keys(errors).length === 0;
            }
        },

        onCancelBtnClicked: function(e) {
            if (confirm($dbt.string('are_you_sure_cancel_upload'))) {
                module.removeBeforeunloadCallback('upload');
                this.cancelUploader();
            }
        },

        onCancelDropBtnClicked: function(e) {
            if (this.dropUploadReq) {
                this.dropUploadReq.abort();
            }
        },

        cancelUploader: function() {
            if (this.uploadReq) {
                this.uploadReq.abort();
                this.uploadReq = null;
            }

            if (this.preprocessReq) {
                this.preprocessReq.abort();
                this.preprocessReq = null;
            }

            if (this.DropUploadReq) {
                this.DropUploadReq.abort();
                this.DropUploadReq = null;
            }

            if (this.imageUploadReq) {
                this.imageUploadReq.abort();
                this.imageUploadReq = null;
            }

            if (this.waveformUploadReq) {
                this.waveformUploadReq.abort();
                this.waveformUploadReq = null;
            }

            if (this.uploadTrackInfoReq) {
                this.uploadTrackInfoReq.abort();
                this.uploadTrackInfoReq = null;
            }

            if (this.soundPlayer) {
                soundManager.destroySound(this.soundPlayer.id);
                this.soundPlayer = null;
            }

            this.credentialCallbacks = {};

            this.setSoundUrlInput();
            this.setSoundDurationInput();
            this.setThumbUrlInput();
            this.mayEnableSaveBtn();
            this.resetAudioPlayer();
            this.resetWaveformRenderer();
            this.setDropInput(-1);

            this.$soundUploadCancelBtn.hide();
            this.$soundUploadBtn.css('display', 'inline-block');
            this.resetSoundFileUploader();
            this.$soundCloudImportBtn.show();
            this.$uploadMethodOr.show();
            this.$soundUploadBtn.show();
            this.file = null;
            this.$uploadForm.removeClass('track-uploaded');
            this.$soundProgressFrame.hide();
            this.$dropProgressFrame.hide();

            module.removeBeforeunloadCallback('upload');
        },

        onSoundUploadStart: function(e) {
            var that = this,
                $form = this.$soundUploadForm,
                $file = $form.find('input[type=\'file\']')[0],
                file,
                filename,
                resourceName,
                idx,
                ext;

            if (!$file.files || $file.files.length === 0) {
                return;
            }
            file = $file.files[0];
            if (file.size > 1000 * 1000 * 1000) {
                module.s.notify(module.string('file_length_limit'), 'error');
                return;
            }

            module.playerManager.pause();
            this.file = file;

            this.$soundCloudImportBtn.hide();
            this.$uploadMethodOr.hide();
            this.$soundUploadBtn.hide();
            this.$uploadForm.addClass('track-uploaded');

            filename = file.name;
            idx = filename.lastIndexOf('.');
            if (idx > -1 && idx < filename.length - 1) {
                ext = filename.substring(idx + 1, filename.length);
                filename = filename.substring(0, idx);
            }

            if (!ext) {
                module.s.notify(module.string('no_sound_ext'), 'error');
                this.cancelUploader();
                return;
            }
            ext = ext.toLowerCase();
            if (['mp3', 'wav'].indexOf(ext) === -1) {
                module.s.notify(module.string('invalid_sound_ext'), 'error');
                this.cancelUploader();
                return;
            }

            module.addBeforeunloadCallback('upload', function() {
                return module.string('are_you_sure_cancel_upload');
            });

            this.setSoundDurationInput(null);
            this.postSoundProgress(-1, 'Requesting authentication.. (1/3)');

            this.$trackInfoTitle.val(filename);
            this.uploadHost = null;
            this.credentialKey = null;

            this.$soundProgressFrame.show();
            this.$soundProgressPercent.removeClass('error');
            this.$soundUploadCancelBtn.css('display', 'inline-block');

            module.utils.getUploadCredential()
                .then(function(key, host) {
                    that.uploadHost = host;
                    that.credentialKey = key;
                    return [key, host];
                })
                .then(module.$.proxy(this.preprocess, this))
                .then(module.$.proxy(this.uploadSound, this))
                .done(function(token, trackUrl, waveform) {
                    that.$soundProgressFrame.hide();
                    that.prepareAudioPlayer(trackUrl);
                    that.renderWaveform(waveform);
                    that.setSoundUrlInput(trackUrl);
                    that.setUniqueKeyInput(token);
                })
                .fail(function(msg) {
                    that.cancelUploader();
                    if (msg === 'canceled') {
                        return;
                    }
                    module.s.notify(
                        msg || module.string('failed_to_upload'), 'error');
                });
            return false;
        },

        postSoundProgress: function(percent, message) {
            this.$soundProgressPercent.text(message);
            this.$soundProgress.progressbar({
                max:100,
                value: percent < 0 || percent === 100 ? false : percent
            });
            this.$dropProgressPercent.text(message);
            this.$dropProgress.progressbar({
                max:100,
                value: percent < 0 || percent === 100 ? false : percent
            });
        },

        resetSoundFileUploader: function() {
            this.$soundUploadForm.find('.track-input-wrapper').html(
                '<input class=\'visuallyhidden\' type=\'file\' name=\'sound\' accept=\'audio/*\'/>');
            this.file = null;
        },

        setSoundUrlInput: function(url) {
            if (!url) {
                this.$trackInfoTrackUrl.removeAttr('value');
                this.$trackInfoTrackUrl.val('');
                this.mayEnableSaveBtn();
            } else {
                this.$trackInfoTrackUrl.attr('value', url);
                this.$trackInfoTrackUrl.val(url);
                this.mayEnableSaveBtn();
            }
        },

        setDropUrlInput: function(url) {
            if (!url) {
                this.$trackInfoDropUrl.removeAttr('value');
                this.$trackInfoDropUrl.val('');
                this.mayEnableSaveBtn();
            } else {
                this.$trackInfoDropUrl.attr('value', url);
                this.$trackInfoDropUrl.val(url);
                this.mayEnableSaveBtn();
            }
        },

        setUniqueKeyInput: function(url) {
            if (!url) {
                this.$trackInfoTrackKey.removeAttr('value');
                this.$trackInfoTrackKey.val('');
                this.mayEnableSaveBtn();
            } else {
                this.$trackInfoTrackKey.attr('value', url);
                this.$trackInfoTrackKey.val(url);
                this.mayEnableSaveBtn();
            }
        },

        setSoundDurationInput: function(duration) {
            if (!duration) {
                this.$trackInfoDuration.removeAttr('value');
                this.$trackInfoDuration.val('');
                this.mayEnableSaveBtn();
                this.$dropSelector.hide();
            } else {
                duration = parseInt(duration);
                this.$trackInfoDuration.attr('value', duration);
                this.$trackInfoDuration.val(duration);
                this.mayEnableSaveBtn();
                this.renderInitialDropSelector();
            }
        },

        setThumbUrlInput: function(url) {
            if (!url) {
                this.$trackInfoCoverArtImg.attr('src', '/images/default_cover_big.png');
                this.$trackInfoCoverArtUrl.val('');
                this.$trackInfoCoverArtUrl.removeAttr('value');
                this.mayEnableSaveBtn();
            } else {
                this.$trackInfoCoverArtImg.attr('src', url);
                this.$trackInfoCoverArtUrl.val(url);
                this.$trackInfoCoverArtUrl.attr('value', url);
                this.mayEnableSaveBtn();
            }
        },

        mayEnableSaveBtn: function() {
            var enable = true;
            if (!this.$trackInfoTrackUrl.val()) {
            }
            if (!this.$trackInfoCoverArtUrl.val()) {
                enable = false;
            }
            if (!this.$trackInfoDuration.val()) {
                enable = false;
            }
            if (!this.$trackInfoWaveformUrl.val()) {
                enable = false;
            }
            if (!enable) {
                this.$trackInfoUploaderSubmitBtn.attr('disabled', 'disabled');
            } else {
                this.$trackInfoUploaderSubmitBtn.removeAttr('disabled');
            }
        },

        prepareAudioPlayer: function(url) {
            if (this.soundPlayer) {
                soundManager.destroySound(this.soundPlayer.id);
                this.soundPlayer = null;
            }
            this.soundPlayer = soundManager.createSound({
                id:url,
                url: url,
                volume:100,
                onload: this.onAudioPrepared,
                onerror: this.onAudioError,
                autoLoad: true
            });
        },

        onAudioPrepared: function(e) {
            var duration = Math.round(this.soundPlayer.duration / 1000);
            this.setSoundDurationInput(duration);
            this.$dropBtn.show();
        },

        onAudioError: function(e) {
            this.$soundUploadCancelBtn.click();
            this.$dropBtn.hide();
            module.s.notify(module.string('failed_to_load_audio_data'), 'error');
        },

        resetAudioPlayer: function() {
            if (this.soundPlayer) {
                soundManager.destroySound(this.soundPlayer.id);
                this.soundPlayer = null;
            }
        },

        resetWaveformRenderer: function() {
            if (this.waveformUploadReq) {
                this.waveformUploadReq.abort();
                this.waveformUploadReq = null;
            }
            this.$waveformProgressPercent.removeClass('error');
            this.$waveformCanvasFrame.html('');
            this.canvas = null;
            this.$dropInfoUploaderFrame.hide();
            this.$dropInfoUploaderFrame.removeClass('prepared');
            this.$waveformProgress.hide();
            this.setWaveformDataInput([]);
        },

        setDropInput: function(start, duration) {
            if (start >= 0) {
                start = parseInt(start);
                duration = parseInt(duration);
                this.$trackInfoDropStart.attr('value', start);
                this.$trackInfoDropDuration.attr('value', duration);
            } else {
                this.$trackInfoDropStart.removeAttr('value');
                this.$trackInfoDropDuration.removeAttr('value');
            }
        },

        renderWaveform: function(data) {
            var that = this,
                context,
                canvasWidth,
                canvasHeight,
                canvas,
                $canvas,
                reader;

            this.$dropInfoUploaderFrame.show();
            this.$waveformProgress.show();
            this.$waveformProgressPercent.text('Preparing waveform renderer..');
            this.$waveformProgressPercent.removeClass('error');
            this.$waveformProgress.progressbar({
                max:100,
                value: false
            });

            // Canvas
            canvasWidth = this.$waveformFrame.width();
            canvasHeight = 80;

            $canvas = module.$('<canvas class=\'waveform-bg-canvas\' width=\'' + canvasWidth + '\' height=\'' + canvasHeight + '\'/>');
            this.$waveformCanvasFrame.html($canvas);
            canvas = this.$waveformCanvasFrame.find('.waveform-bg-canvas')[0];

            context = canvas.getContext('2d');
            canvas = canvas;

// because we will create sprite waveform, we multiply height 3
            canvas.width = canvasWidth;
            canvas.height = canvasHeight;

            displayBuffer(data);

            function displayBuffer(buff) {
                var barCount = 1000,
                    bars = [],
                    blocks = [],
                    blockCount,
                    marginWidth = 1,
                    barWidth = 2,
                    colorNormal = '#636365',
                    colorPlaying = '#fff',
                    //colorDrop = '#cd85ff',
                    colorDrop = '#f03eb1',
                    dataURL,
                    blob;

                blockCount =  Math.floor(canvasWidth / (barWidth + marginWidth));
                if (blockCount === 0) {
                    return;
                }

                bars = module.utils.downsizeArray(buff, barCount);
                blocks = module.utils.downsizeArray(bars, blockCount);

                context.save();

                that.waveformData = blocks;
                that.updateDropArea();

                module.$.each(blocks, function(idx, val) {
                    var x, barHeight, y;
                    x = idx * (barWidth + marginWidth);
                    barHeight = val * canvasHeight * 0.009;
                    y = canvasHeight - barHeight;
                    context.fillStyle = colorNormal;
                    context.fillRect(x, y, barWidth, barHeight);
                });
                context.save();

                module.utils.getUploadCredential().done(function(key, host) {
                    var file,
                        str = '[' + bars.join(',') + ']';

                    file = new Blob([str], {type: 'text/plain'});
                    that.waveformUploadReq = module.utils.uploadImage(
                            host, key, file, 'w', function() {},
                        function(success, url) {
                            that.$waveformProgress.hide();
                            that.$dropInfoUploaderFrame.addClass('prepared');
                            that.setWaveformDataInput(url);
                        });
                    that.waveformUploadReq.always(function() {
                        that.waveformUploadReq = null;
                    });
                }).fail(function(msg) {
                    if (msg !== 'canceled') {
                        module.s.notify(msg, 'error');
                    }
                    that.cancelUploader();
                });
            }
        },

        setWaveformDataInput: function(data) {
            if (!data) {
                this.$trackInfoWaveformUrl.val('');
                this.$trackInfoWaveformUrl.removeAttr('value');
                this.mayEnableSaveBtn();
            } else {
                this.$trackInfoWaveformUrl.val(data);
                this.$trackInfoWaveformUrl.attr('value', data);
                this.mayEnableSaveBtn();
            }
            this.updateDropArea();
        },

        uploadDrop: function(key, host, token, start) {
            var that = this,
                url = module.uploadApi.postProcessUrl(host),
                formData,
                deferred = module.$.Deferred();

            formData = new FormData();
            formData.append('token', token);
            formData.append('drop_start', start);

            this.$dropProgressFrame.show();
            this.$dropProgressPercent.removeClass('error');

            if (this.dropUploadReq) {
                this.dropUploadReq.abort();
            }

            this.postSoundProgress(-1, 'Generating drop file.. (1/2)');

            this.dropUploadReq = module.$.ajax({
                cache:false,
                url:url,
                type:'post',
                data: formData,
                dataType:'json',
                contentType:false,
                processData: false,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                    xhr.setRequestHeader('dbt_key', key);
                }
            }).done(function(res) {
                if (!res || !res.drop_url) {
                    deferred.reject();
                    that.$dropProgressFrame.hide();
                    return;
                }
                that.$dropSelectorBar.hide();
                deferred.resolve(res.drop_url);
            }).fail(function(jqXHR, textStatus, error) {
                that.$dropProgressFrame.hide();
                deferred.reject(textStatus === 'abort' ? 'canceled' : null);
            }).always(function() {
                that.dropUploadReq = null;
            });

            return deferred;
        },

        preprocess: function(credential) {
            var that = this,
                key = credential[0],
                host = credential[1],
                url = module.uploadApi.preProcessUrl(host),
                file = this.file,
                formData = new FormData(),
                filename,
                ext,
                idx,
                deferred = module.$.Deferred();

            if (!file) {
                deferred.reject(module.string('no_file_to_upload'));
                return deferred;
            }

            filename = file.name;
            idx = filename.lastIndexOf('.');
            ext = filename.substring(idx + 1, filename.length);
            filename = filename.substring(0, idx).replace(/[^a-zA-Z0-9\-\s]/g,'');

            if (filename.length === 0) {
                filename = module.utils.makeId();
            }
            filename = filename + '.' + ext;

            formData.append('filename', filename);
            formData.append('type', 0);
            formData.append('content', file);

            if (this.preprocessReq) {
                this.preprocessReq.abort();
            }

            this.postSoundProgress(-1, 'Preprocessing.. (2/3)');

            this.preprocessReq = module.$.ajax({
                xhr: function() {
                    var x = module.utils.createCORSRequest('post', url);
                    if (!x) {
                        return null;
                    }
                    x.upload.addEventListener('progress', function(e) {
                        if (e.lengthComputable) {
                            var percentage;
                            percentage = Math.min(Math.round(
                                (e.loaded * 100) / file.size), 100);
                            that.postSoundProgress(percentage,
                                'Preprocessing.. ' + percentage + '% (2/3)');
                        }
                    }, false);
                    x.withCredentials = true;
                    return x;
                },
                cache:false,
                url:url,
                type:'post',
                data: formData,
                dataType:'json',
                contentType:false,
                processData: false,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                    xhr.setRequestHeader('dbt_key', key);
                }
            }).done(function(res) {
                if (!res || !res.token) {
                    deferred.reject();
                    return;
                }
                deferred.resolve(res.token);
            }).fail(function(jqXHR, textStatus, error) {
                deferred.reject(textStatus === 'abort' ? 'canceled' : null);
            }).always(function() {
                that.preprocessReq = null;
            });
            return deferred;
        },

        uploadSound: function(token) {
            var that = this,
                url = module.uploadApi.uploadSoundUrl(this.uploadHost),
                deferred = module.$.Deferred();

            if (this.uploadReq) {
                this.uploadReq.abort();
            }

            this.postSoundProgress(-1, 'Uploading sound file.. (3/3)');

            this.uploadReq = module.$.ajax({
                url:url,
                data: {
                    token : token
                },
                crossDomain: true,
                dataType:'json',
                type:'post',
                cache:false,
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                    xhr.setRequestHeader('dbt_key', that.credentialKey);
                }
            }).done(function(res) {
                var waveformData;

                if (!res) {
                    deferred.reject();
                    return;
                }
                try {
                    waveformData = JSON.parse(res.waveform_data);
                    deferred.resolve(token, res.track_url, waveformData);
                } catch(e) {
                    deferred.reject();
                }
            }).fail(function(jqXHR, textStatus, error) {
                deferred.reject(textStatus === 'abort' ? 'canceled' : null);
            }).always(function() {
                that.uploadReq = null;
            });

            return deferred;
        },

        onSelectGenre: function() {
            var that = this;
            if (this.$genreDropdown.is(':hidden')) {
                this.$filter.show().one('click', function() {
                    that.$genreDropdown.hide();
                });
                this.$genreDropdown.show();
            }
        },

        onGenreClicked: function(e) {
            var $target = module.$(e.currentTarget),
                today, url,
                mm, dd, timestamp, selectedGenre;

            if ($target.hasClass('selected')) {
                return;
            }

            module.$('.genre', this.$genreDropdown).removeClass('selected');
            $target.addClass('selected');

            selectedGenre = $target.data('key');

            module.$('.selected-genre .text', this.$genreSelect).text($target.text());
            if (this.$filter.is(':visible')) {
                this.$filter.click();
            }
            this.$trackInfoGenreId.val(selectedGenre);
            this.$trackInfoGenreId.attr('value', selectedGenre);
        }
    });

    StatisticsSubView = DropbeatView.extend({
        initialize: function(options) {
            this.key = options.key;
            this.parentView = options.parentView;
            this.dateFrom = options.dateFrom;
            this.dateTo = options.dateTo;
            this.success = options.onSuccess;
            this.failure = options.onFailure;
            this.$el = options.$el;

            DropbeatView.prototype.initialize.apply(this, arguments);
        },

        loadData: function() {
            throw 'Not implemented';
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .btn-back': 'onBackBtnClicked'
            });
        },

        remove: function() {
            this.undelegateEvents();
            this.$el.empty();
            this.stopListening();
            return this;
        },

        onBackBtnClicked: function() {
            if (this.key === 'sub-location') {
                this.parentView.loadPage('location');
            } else if (this.key === 'single-track') {
                this.parentView.loadPage('track');
            }
        }
    });

    StatisticsPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-statistics',

        subViews : (function () {
            var views = {};
            views.LocationView = StatisticsSubView.extend({
                tmpl: '#tmpl-statistics-location-page',
                initialize: function(options) {
                    this.country = options.country;
                    this.tablePageIdx = 0;
                    this.tableData = [];
                    this._tableTmpl = this.getTableTmpl();

                    StatisticsSubView.prototype.initialize.call(this, options);
                    module.$.extend(this._context, {country: this.country || false});

                    this.render();
                    this.loadData();
                },

                render: function() {
                    this.$el.html(this._tmpl(this._context));
                },

                remove: function() {
                    StatisticsSubView.prototype.remove.apply(this, arguments);
                    if (this.req) {
                        this.req.abort();
                        this.req = null;
                    }
                },

                events: function() {
                    var events = StatisticsSubView.prototype.events;
                    if (module._.isFunction(events)) {
                        events = events.call(this);
                    }

                    return module.$.extend ({}, events, {
                        'click .arrow.prev': 'onTablePrevBtnClicked',
                        'click .arrow.next': 'onTableNextBtnClicked',
                        'click .drilldown-link': 'onDrilldownClicked'
                    });
                },

                loadData: function() {
                    var that = this,
                        deferreds = [],
                        dataDeferred = module.$.Deferred(),
                        googleLoadDeferred = module.$.Deferred(),
                        data;

                    google.load('visualization', '1', {
                        packages:['geochart'],
                        callback: function() {
                            googleLoadDeferred.resolve();
                        }
                    });

                    if (!this.data) {
                        this.req = module.$.ajax({
                            url:module.api.statisticsPlaybackUrl,
                            data: {
                                type: 'location',
                                date_from: this.dateFrom.format('YYYY-MM-DD'),
                                date_to: moment(this.dateTo.toDate())
                                    .add(1, 'day').format('YYYY-MM-DD'),
                                diff: parseInt(moment().format('Z'))
                            },
                            dataType:'json',
                            cache:false
                        }).done(function(res) {
                            var data;

                            if (!res || !res.success) {
                                dataDeferred.reject();
                                return;
                            }

                            that.data = res.data;
                            if (!that.country) {
                                data = module.$.map(that.data.country, function(el, i) {
                                    var str = el[0],
                                        val = el[1],
                                        idx = str.lastIndexOf('-'),
                                        country = str.substring(0, idx),
                                        code = str.substring(idx + 1, str.length);

                                    if (code === 'UK') {
                                        code = 'GB';
                                    }

                                    return {
                                        name: country,
                                        value: val,
                                        targetKey: code,
                                        targetName: country
                                    };
                                });
                            } else {
                                data = [];
                                module.$.each(that.data.city, function(i, el) {
                                    var str = el[0],
                                        value = el[1],
                                        idx = str.lastIndexOf('-'),
                                        name,
                                        code;

                                    if (idx === -1) {
                                        return true;
                                    }

                                    name = str.substring(0, idx);
                                    code = str.substring(idx + 1, str.length);
                                    if (code === 'UK') {
                                        code = 'GB';
                                    }

                                    if (that.country.code === code) {
                                        data.push({
                                            name:name,
                                            value: value
                                        });
                                    }
                                });
                            }
                            dataDeferred.resolve(data);
                        }).fail(function(jqXHR, textStatus, error) {
                            if (textStatus !== 'abort') {
                                that.failure();
                            }
                            dataDeferred.reject();
                        }).always(function() {
                            that.req = null;
                        });
                    } else {
                        if (!that.country) {
                            data = module.$.map(this.data.country, function(el, i) {
                                var str = el[0],
                                    val = el[1],
                                    idx = str.lastIndexOf('-'),
                                    country = str.substring(0, idx),
                                    code = str.substring(idx + 1, str.length);

                                return {
                                    countryCode: code,
                                    countryName: country,
                                    value: val
                                };
                            });
                        } else {
                            data = [];
                            module.$.each(that.data.city, function(i, el) {
                                var str = el[0],
                                    value = el[1],
                                    idx = str.lastIndexOf('-'),
                                    name,
                                    code;

                                if (idx === -1) {
                                    return true;
                                }

                                name = str.substring(0, idx);
                                code = str.substring(idx + 1, str.length);

                                if (that.country.code === code) {
                                    data.push({
                                        name:name,
                                        value: value
                                    });
                                }
                            });
                        }
                        dataDeferred.resolve(data);
                    }


                    deferreds.push(dataDeferred);
                    deferreds.push(googleLoadDeferred);

                    module.$.when.apply(this, deferreds).done(function(data) {
                        var chartData,
                            chartOptions,
                            geochart,
                            raw,
                            tableData;

                        function compare(a, b) {
                            return b.value - a.value;
                        }

                        that.success();

                        tableData = data.sort(compare);

                        // show Table
                        that.$chartContainer = module.$('.chart-container', that.$el);
                        that.$tableContainer = module.$('.table-container', that.$el);
                        that.tableData = tableData;
                        that.renderTablePage(0);

                        // show graph

                        raw = [];
                        if (that.country) {
                            raw.push(['Regions', 'PlayCount']);
                        } else {
                            raw.push(['City', 'PlayCount']);
                        }
                        module.$.each(tableData, function(idx, val) {
                            raw.push([val.name, val.value]);
                        });
                        chartData = google.visualization.arrayToDataTable(raw);

                        geochart = new google.visualization.GeoChart(that.$chartContainer[0]);

                        chartOptions = {
                            colorAxis: { colors: ['#ffffff', '#00dde5', '#7800a0'] },
                            backgroundColor:'#2e2f33',
                            datalessRegionColor: '#88898c',
                            defaultColor:'#ccc'
                        };

                        if (that.country) {
                            chartOptions.region = that.country.code;
                            chartOptions.displayMode = 'markers';
                        } else {
                            chartOptions.region = 'world';
                            chartOptions.displayMode = 'regions';
                        }
                        geochart.draw(chartData, chartOptions);
                    })
                    .fail(function() {
                        that.failure();
                    });
                },

                getTableTmpl: function() {
                    return module._.template(module.$('#tmpl-statistics-location-table').html());
                },

                renderTablePage: function(pageIdx) {
                    var PAGE_ITEM_SIZE = 15,
                        startIdx = pageIdx * PAGE_ITEM_SIZE,
                        endIdx = startIdx + PAGE_ITEM_SIZE;

                    this.tablePageIdx = pageIdx;

                    this.$tableContainer.html(this._tableTmpl({
                        data: this.tableData.slice(startIdx, endIdx),
                        offset: startIdx,
                        hasPrev: startIdx > 0,
                        hasNext: this.tableData.length > endIdx
                    }));
                },

                onTableNextBtnClicked: function(e) {
                    var $btn =module.$(e.currentTarget);
                    if ($btn.hasClass('disabled')) {
                        return;
                    }
                    this.renderTablePage(this.tablePageIdx + 1);
                },

                onTablePrevBtnClicked: function(e) {
                    var $btn =module.$(e.currentTarget);
                    if ($btn.hasClass('disabled')) {
                        return;
                    }
                    this.renderTablePage(this.tablePageIdx - 1);
                },

                onDrilldownClicked: function(e) {
                    var $link = module.$(e.currentTarget),
                        targetKey = $link.data('targetKey'),
                        targetName = $link.data('targetName');

                    this.parentView.loadPage('sub-location', {
                        country: {
                            code: targetKey,
                            name: targetName
                        },
                        data: this.data
                    });
                }
            });

            views.TrackView = StatisticsSubView.extend({
                tmpl: '#tmpl-statistics-track-page',
                initialize: function(options) {
                    this.trackTablePageIdx = 0;
                    this.trackTableData = [];
                    this._trackTableTmpl = this.getTrackTableTmpl();
                    this._acquisitionTableTmpl = this.getAcquisitionTableTmpl();
                    StatisticsSubView.prototype.initialize.call(this, options);

                    this.render();
                    this.loadData();
                },

                render: function() {
                    this.$el.html(this._tmpl(this._context));
                },

                remove: function() {
                    StatisticsSubView.prototype.remove.apply(this, arguments);
                    if (this.req) {
                        this.req.abort();
                        this.req = null;
                    }

                    if (this.trackReq) {
                        this.trackReq.abort();
                        this.trackReq = null;
                    }
                },

                events: function() {
                    var events = StatisticsSubView.prototype.events;
                    if (module._.isFunction(events)) {
                        events = events.call(this);
                    }

                    return module.$.extend ({}, events, {
                        'click .section-most-played-track .arrow.prev': 'onTrackTablePrevBtnClicked',
                        'click .section-most-played-track .arrow.next': 'onTrackTableNextBtnClicked',
                        'click .section-acquisition .arrow.prev': 'onAcquisitionTablePrevBtnClicked',
                        'click .section-acquisition .arrow.next': 'onAcquisitionTableNextBtnClicked',
                        'click .section-most-played-track .drilldown-link': 'onTrackDrilldownClicked'
                    });
                },

                loadData: function() {
                    var that = this,
                        deferreds = [],
                        trackDeferred = module.$.Deferred(),
                        dataDeferred = module.$.Deferred(),
                        googleLoadDeferred = module.$.Deferred();

                    google.load('visualization', '1', {
                        packages:['corechart', 'bar'],
                        callback: function() {
                            googleLoadDeferred.resolve();
                        }
                    });

                    this.req = module.$.ajax({
                        url:module.api.statisticsPlaybackUrl,
                        data: {
                            type: 'track',
                            date_from: this.dateFrom.format('YYYY-MM-DD'),
                            date_to: moment(this.dateTo.toDate()).add(1, 'day').format('YYYY-MM-DD'),
                            diff: parseInt(moment().format('Z'))
                        },
                        dataType:'json',
                        cache:false
                    }).done(function(res) {
                        if (!res || !res.success) {
                            dataDeferred.reject();
                            return;
                        }
                        dataDeferred.resolve(res.data);
                    }).fail(function(jqXHR, textStatus, error) {
                        dataDeferred.reject();
                    }).always(function() {
                        that.req = null;
                    });

                    this.trackReq = module.$.ajax({
                        url:module.api.resolveObjectUrl,
                        data: {
                            url: 'http://dropbeat.net/r/' + module.context.account.resourceName
                        },
                        dataType:'json',
                        cache:false
                    }).done(function(res) {
                        var tracks = {};
                        if (!res || !res.success || !res.data || !res.data.tracks) {
                            trackDeferred.reject();
                            return;
                        }
                        module.$.each(res.data.tracks, function(idx, track) {
                            tracks[track.unique_key] = new module.music.DropbeatMusic(track);
                        });
                        trackDeferred.resolve(tracks);
                    }).fail(function(jqXHR, textStatus, error) {
                        trackDeferred.reject();
                    }).always(function() {
                        that.trackReq = null;
                    });

                    deferreds.push(dataDeferred);
                    deferreds.push(trackDeferred);
                    deferreds.push(googleLoadDeferred);

                    module.$.when.apply(this, deferreds).done(function(data, trackData) {
                        var playChartData,
                            playChartOptions,
                            playChart,
                            dateDiff,
                            m = {},
                            profileChartData,
                            profileChartOptions,
                            profileChart,
                            trackTableData,
                            acqusitionTableData,
                            date,
                            i;
                            //startColor = parseInt('#f03eb1'.replace(/^#/, ''), 16),
                            //endColor = parseInt('#a003ec'.replace(/^#/, ''), 16);

                        that.success();

                        that.$playChartContainer = module.$('.section-play .chart-container', that.$el);
                        that.$profilChartContainer = module.$('.section-user-profiles .chart-container', that.$el);
                        that.$trackTableContainer = module.$('.section-most-played-track .table-container', that.$el);
                        that.$acquisitionTableContainer = module.$('.section-acquisition .table-container', that.$el);

                        // show track Table
                        trackTableData = [];
                        module.$.each(data.playback_count_by_track || [], function(idx, info) {
                            var key = info[0],
                                val = info[1],
                                track = trackData[key];
                            if (track) {
                                trackTableData.push({
                                    name: track.track_name,
                                    value: val,
                                    trackType: track.track_type,
                                    targetKey: track.resource_name,
                                    targetName: track.track_name
                                });
                            }
                        });

                        that.trackTableData = trackTableData;
                        that.renderTrackTablePage(0);

                        // show acquisition Table
                        acqusitionTableData = module.$.map(data.referer_count || [],
                                function(el, idx) {
                            return {
                                name: el[0],
                                value: el[1]
                            };
                        });

                        that.acquisitionTableData = acqusitionTableData;
                        that.renderAcquisitionTablePage(0);

                        // show graph
                        playChartData = new google.visualization.DataTable();
                        playChartData.addColumn('date', 'Date');
                        playChartData.addColumn('number', 'Count');
                        playChartData.addColumn({type: 'string', role: 'style'});

                        module.$.each(data.playback_count, function(idx, row) {
                            m[row[0]] = row[1];
                        });

                        dateDiff = that.dateTo.diff(that.dateFrom, 'days') + 1;
                        for (i = 0;  i < dateDiff; i++) {
                            date = moment(that.dateTo.toDate()).subtract(i, 'day'),
                                key = date.format('YYYY-MM-DD');
                            if (!m[key]) {
                                m[key] = 0;
                            }
                        }

                        module.$.each(m, function(key, val) {
                            playChartData.addRow([moment(key).toDate(), val, 'color:#cd85ff']);
                        });


                        playChart = new google.visualization.ColumnChart(that.$playChartContainer[0]);

                        playChartOptions = {
                            title: '',
                            hAxis: {
                                title:'',
                                format: 'yyyy-MM-dd',
                                minValue: that.dateFrom.toDate(),
                                maxValue: that.dateTo.toDate(),
                                gridlines: {
                                    color:'#2e2f33'
                                },
                                textStyle: {
                                    color:'#dcdde3'
                                }
                            },
                            baselineColor:'#88898c',
                            vAxis: {
                                format:'#',
                                viewWindowMode:'explicit',
                                viewWindow: {
                                    min:0
                                },
                                gridlines: {
                                    color:'#2e2f33'
                                },
                                textStyle: {
                                    color:'#dcdde3'
                                }
                            },
                            bar: {groupWidth:'80%'},
                            legend: {position:'none'},
                            chartArea: {
                                width: 1020,
                                height: 320
                            },
                            backgroundColor:'#2e2f33',
                            datalessRegionColor: '#88898c',
                            defaultColor:'#1b1c1f'
                        };

                        playChart.draw(playChartData, playChartOptions);

                        /*
                        profileChartData = new google.visualization.DataTable();
                        profileChartData.addColumn('string', 'Genre');
                        profileChartData.addColumn('number', 'Count');
                        profileChartData.addColumn({type: 'string', role: 'style'});

                        var colorDiff = startColor - endColor;
                        var colorAmount = colorDiff / data.userProfile.length;
                        module.$.each(data.userProfile, function(idx, row) {
                            var color = (startColor + colorAmount * idx).toString(16);
                            profileChartData.addRow([row.name, row.value, 'color:#' + color]);
                        });

                        profileChartOptions = {
                            title: '',
                            backgroundColor:'#2e2f33',
                            pieHole: 0.5,
                            pieSliceTextStyle: {
                                color: '#dcdde3',
                            },
                            legend: 'none'
                        };

                        profileChart = new google.visualization.PieChart(that.$profilChartContainer[0]);
                        profileChart.draw(profileChartData, profileChartOptions);
                        */
                    })
                    .fail(function() {
                        that.failure();
                    });
                },

                getTrackTableTmpl: function() {
                    return module._.template(module.$('#tmpl-statistics-track-table').html());
                },

                renderTrackTablePage: function(pageIdx) {
                    var PAGE_ITEM_SIZE = 10,
                        startIdx = pageIdx * PAGE_ITEM_SIZE,
                        endIdx = startIdx + PAGE_ITEM_SIZE;

                    this.trackTablePageIdx = pageIdx;
                    this.$trackTableContainer.html(this._trackTableTmpl({
                        data: this.trackTableData.slice(startIdx, endIdx),
                        offset: startIdx,
                        hasPrev: startIdx > 0,
                        hasNext: this.trackTableData.length > endIdx
                    }));
                },

                onTrackTableNextBtnClicked: function(e) {
                    var $btn =module.$(e.currentTarget);
                    if ($btn.hasClass('disabled')) {
                        return;
                    }
                    this.renderTrackTablePage(this.trackTablePageIdx + 1);
                },

                onTrackTablePrevBtnClicked: function(e) {
                    var $btn =module.$(e.currentTarget);
                    if ($btn.hasClass('disabled')) {
                        return;
                    }
                    this.renderTrackTablePage(this.trackTablePageIdx - 1);
                },

                onTrackDrilldownClicked: function(e) {
                    var $link = module.$(e.currentTarget),
                        targetKey = $link.data('targetKey'),
                        targetName = $link.data('targetName'),
                        trackType = $link.data('trackType');

                    if (trackType === 'MIX') {
                        module.s.notify(module.string('not_support_mixset_track_drilldown'), 'warn');
                        return;
                    }

                    this.parentView.loadPage('single-track', {
                        track: {
                            name: targetName,
                            resourceName: targetKey
                        }
                    });
                },

                getAcquisitionTableTmpl: function() {
                    return module._.template(module.$('#tmpl-statistics-acquisition-table').html());
                },

                renderAcquisitionTablePage: function(pageIdx) {
                    var PAGE_ITEM_SIZE = 10,
                        startIdx = pageIdx * PAGE_ITEM_SIZE,
                        endIdx = startIdx + PAGE_ITEM_SIZE;

                    this.acquisitionTablePageIdx = pageIdx;
                    this.$acquisitionTableContainer.html(this._acquisitionTableTmpl({
                        data: this.acquisitionTableData.slice(startIdx, endIdx),
                        offset: startIdx,
                        hasPrev: startIdx > 0,
                        hasNext: this.acquisitionTableData.length > endIdx
                    }));
                },

                onAcquisitionTableNextBtnClicked: function(e) {
                    var $btn =module.$(e.currentTarget);
                    if ($btn.hasClass('disabled')) {
                        return;
                    }
                    this.renderAcuisitionTablePage(this.acquisitionTablePageIdx + 1);
                },

                onAcquisitionTablePrevBtnClicked: function(e) {
                    var $btn =module.$(e.currentTarget);
                    if ($btn.hasClass('disabled')) {
                        return;
                    }
                    this.renderAcuisitionTablePage(this.acquisitionTablePageIdx - 1);
                }
            });

            views.SingleTrackView = StatisticsSubView.extend({
                tmpl: '#tmpl-statistics-single-track-page',
                initialize: function(options) {
                    StatisticsSubView.prototype.initialize.call(this, options);

                    this.track = options.track;
                    module.$.extend(this._context, { trackInfo: this.track });
                    this.render();
                    this.loadData();
                },

                render: function() {
                    this.$el.html(this._tmpl(this._context));
                },

                remove: function() {
                    StatisticsSubView.prototype.remove.apply(this, arguments);
                    if (this.req) {
                        this.req.abort();
                        this.req = null;
                    }
                },

                loadData: function() {
                    var that = this,
                        deferreds = [],
                        dataDeferred = module.$.Deferred(),
                        googleLoadDeferred = module.$.Deferred();

                    google.load('visualization', '1', {
                        packages:['line'],
                        callback: function() {
                            googleLoadDeferred.resolve();
                        }
                    });

                    this.req = module.$.ajax({
                        url:module.api.statisticsPlaybackUrl,
                        data: {
                            type: 'track_detail',
                            resource_path: this.track.resourceName,
                            date_from: this.dateFrom.format('YYYY-MM-DD'),
                            date_to: moment(this.dateTo.toDate()).add(1, 'day').format('YYYY-MM-DD'),
                            diff: parseInt(moment().format('Z'))
                        },
                        dataType:'json',
                        cache:false
                    }).done(function(res) {
                        var data = [],
                            width;
                        if (!res || !res.success) {
                            dataDeferred.reject();
                            return;
                        }
                        width = res.data.width;
                        module.$.each(res.data.data, function(idx, d) {
                            data.push({
                                point:width * idx + width / 2,
                                value:d
                            });
                        });
                        dataDeferred.resolve({
                            data: data
                        });
                    }).fail(function(jqXHR, textStatus, error) {
                        dataDeferred.reject();
                    }).always(function() {
                        that.req = null;
                    });

                    deferreds.push(dataDeferred);
                    deferreds.push(googleLoadDeferred);

                    module.$.when.apply(this, deferreds).done(function(data) {
                        var playChartData,
                            playChartOptions,
                            playChart;

                        that.success();

                        that.$playChartContainer =
                            module.$('.section-most-played .chart-container', that.$el);

                        // show graph
                        playChartData = new google.visualization.DataTable();
                        playChartData.addColumn('number', 'Time (sec)');
                        playChartData.addColumn('number', 'Play Count');
                        playChartData.addColumn({type: 'string', role: 'style'});

                        module.$.each(data.data, function(idx, row) {
                            playChartData.addRow([row.point, row.value, 'color:#cd85ff']);
                        });

                        playChart = new google.visualization.LineChart(that.$playChartContainer[0]);

                        playChartOptions = {
                            title: '',
                            series: {
                                0: {color:'#cd85ff' }
                            },
                            pointSize:2,
                            hAxis: {
                                title:'Second',
                                titleTextStyle:{
                                    color:'#dcdde3'
                                },
                                gridlines: {
                                    color:'#2e2f33'
                                },
                                textStyle: {
                                    color:'#dcdde3'
                                }
                            },
                            vAxis: {
                                title: 'Play count',
                                titleTextStyle:{
                                    color:'#dcdde3'
                                },
                                gridlines: {
                                    color:'#2e2f33'
                                },
                                textStyle: {
                                    color:'#dcdde3'
                                }
                            },
                            legend: {position:'none'},
                            chartArea: {
                                width: 1020,
                                height: 320,
                                backgroundColor:'#2e2f33'
                            },
                            backgroundColor:'#2e2f33'
                        };

                        playChart.draw(playChartData, playChartOptions);
                    })
                    .fail(function() {
                        that.failure();
                    });
                }
            });

            return module.$.extend(views, DropbeatPageView.prototype.subViews);
        }()),

        initialize: function(options) {
            var that = this;


            module.$('.contents').addClass('desktop-only').addClass('gray-bg');
            module.$('header .header-menu .menu').removeClass('selected');
            module.$('header .header-menu .statistics-menu').addClass('selected');
            DropbeatPageView.prototype.initialize.call(this, options);

            this.mapScriptLoaded = false;
            module.$.getScript('https://www.google.com/jsapi')
                    .done(function() {
                        that.mapScriptLoaded = true;
                        that.loadPage();
                    })
                    .fail(function(jqXHR, textStatus, error) {
                        that.$spinner.hide();
                        that.$failure.show();
                        that.mapScriptLoaded = false;
                    });

            this.render();
            this.$dateFrom = module.$('#date-from', this.$el);
            this.$dateTo = module.$('#date-to', this.$el);
            this.$periodSelect = module.$('.period-select', this.$el);

            this.$failure = module.$('.failure', this.$el);
            this.$spinner = module.$('.spinner', this.$el);
            this.$statisticsContents = module.$('.statistics-contents', this.$el);
            this.$chartContainer = module.$('.chart-container', this.$statisticsContents);
            this.$tableContainer = module.$('.table-container', this.$statisticsContents);

            this.dateFrom = moment(moment().subtract(8, 'day').format('MM/DD/YYYY'));
            this.dateTo = moment(moment().format('MM/DD/YYYY'));

            this.$dateFrom.val(this.dateFrom.format('MM/DD/YYYY'));
            this.$dateTo.val(this.dateTo.format('MM/DD/YYYY'));
            this.$pageTabs = module.$('.page-tabs', this.$el);

            this.$dateFrom.datepicker({
                defaultDate: this.dateFrom.toDate(),
                changeMonth : true,
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    var newDate;
                    that.$dateTo.datepicker('option', 'minDate', selectedDate);
                    newDate = moment(selectedDate);
                    if (that.dateFrom.format('MM/DD/YYYY') !== newDate.format('MM/DD/YYYY')) {
                        that.dateFrom = newDate;
                        that.onDateChanged();
                    }
                }
            });

            this.$dateTo.datepicker({
                defaultDate: this.dateTo.toDate(),
                changeMonth : true,
                numberOfMonths: 1,
                onClose: function(selectedDate) {
                    var newDate;

                    that.$dateFrom.datepicker('option', 'maxDate', selectedDate);
                    newDate = moment(selectedDate);
                    if (that.dateTo.format('MM/DD/YYYY') !== newDate.format('MM/DD/YYYY')) {
                        that.dateTo = newDate;
                        that.onDateChanged();
                    }
                }
            });

            this.$spinner.show();

            setTimeout(module.$.proxy(function() {
                this.$pageTabs.find('.page-tab').first().click();
            }, this), 0);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .period-select .btn-apply': 'onApplyBtnClicked',
                'click .page-tabs .page-tab': 'onPageTabClicked',
                'click .failure .btn-retry': 'onApplyBtnClicked'
            });
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        onDateChanged : function() {
            this.$periodSelect.addClass('modified');
        },

        onApplyBtnClicked: function() {
            this.loadPage();
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
            module.$('.contents').removeClass('desktop-only').removeClass('gray-bg');
        },

        loadPage: function(key, options) {
            var that = this,
                View;

            if (!this.mapScriptLoaded) {
                return;
            }

            if (!key && !this.pageTabKey) {
                return;
            }
            if (!key) {
                key = this.pageTabKey;
                options = this.pageOptions;
            }
            options = options || {};

            this.pageTabKey = key;
            this.pageOptions = options;

            this.$periodSelect.removeClass('modified');


            this.$statisticsContents.hide();

            if (this.view) {
                this.view.remove();
            }

            if (key === 'location') {
                View = this.subViews.LocationView;
            } else if (key === 'sub-location') {
                View = this.subViews.LocationView;
            } else if (key === 'track') {
                View = this.subViews.TrackView;
            } else if (key === 'single-track') {
                View = this.subViews.SingleTrackView;
            } else {
                this.$spinner.hide();
                this.$failure.show();
                return;
            }

            this.$spinner.show();
            this.$failure.hide();

            this.view = new View(module.$.extend({
                $el: that.$statisticsContents,
                key: key,
                parentView: this,
                dateFrom:this.dateFrom,
                dateTo:this.dateTo,
                onSuccess: function() {
                    that.$statisticsContents.show();
                    that.$spinner.hide();
                },
                onFailure: function() {
                    that.$statisticsContents.hide();
                    that.$spinner.hide();
                    that.$failure.show();
                }
            }, options));
        },

        onPageTabClicked: function(e) {
            var $tab = module.$(e.currentTarget);
            if ($tab.hasClass('selected')) {
                return;
            }
            this.$pageTabs.find('.page-tab').removeClass('selected');
            $tab.addClass('selected');
            this.pageTabKey = $tab.data('target');
            this.loadPage();
        }
    });

    SoundcloudImportPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-soundcloud-import',
        initialize: function(options) {
            module.$('header .header-menu .menu').removeClass('selected');
            module.$('.contents').addClass('desktop-only')
                .addClass('gray-bg')
                .addClass('fixed-width');
            DropbeatPageView.prototype.initialize.call(this, options);
            this.render();

            this.$urlInputFrame = module.$('.url-input-wrapper', this.$el);
            this.$urlInput = module.$('.input-sc-url', this.$urlInputFrame);
            this.$inputBox = module.$('.input-box', this.$el);

            this.$landingSection = module.$('.landing-section', this.$el);
            this.$placeholderSection = module.$('.placeholder-section', this.$el);
            this.$uploadSection = module.$('.upload-section', this.$el);
            this.$uploaderFrame = module.$('.upload-section .page-wrapper', this.$el);
            this.$spinnerSection = module.$('.spinner-section', this.$el);
            this.$typeSelectSection = module.$('.upload-type-select-section', this.$el);
            this.$massUploadSection = module.$('.mass-upload-section', this.$el);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }
            return module.$.extend({}, events, {
                'submit #form-sc-url': 'onScUrlSubmit',
                'click .upload-section .tracks .track': 'onTrackSelected',
                'click .upload-type-select-section .btn-import-single': 'onSingleUploadTypeClicked',
                'click .upload-type-select-section .btn-import-multiple': 'onMultipleUploadTypeClicked',
                'click .btn-search' : 'onSearchBtnClicked'
            });
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        remove: function() {
            this._uploadView && this._uploadView.remove();
            DropbeatPageView.prototype.remove.call(this, arguments);
            module.$('.contents').removeClass('desktop-only')
                .removeClass('gray-bg')
                .removeClass('fixed-width');

            if (this.scResolveReq) {
                this.scResolveReq.abort();
                this.scResolveReq = null;
            }
        },

        onSearchBtnClicked: function(e) {
            module.$('#form-sc-url').submit();
        },

        onScUrlSubmit: function(e) {
            var that = this,
                url = this.$urlInput.val();

            if (!url) {
                return false;
            }

            if (!module.utils.isURL(url)) {
                that.showError('Invalid URL. URL should start with \'http://soundcloud.com\'');
                return false;
            }

            if (this.scResolveReq) {
                return false;
            }

            this.$urlInput.attr('disabled', 'disabled');

            this.showError('');
            this.$spinnerSection.show();
            this.$placeholderSection.hide();

            this.scResolveReq = module.$.getScript("//connect.soundcloud.com/sdk-2.0.0.js").done(function() {
                SC.initialize({
                    client_id:module.clientKeys['soundcloud']
                });
                SC.get('/resolve', {url: url}, function(res, error) {
                    if (error || !res || res.kind !== 'user' || !res.id) {
                        that.showError('Invalid URL. URL should start with \'http://soundcloud.com\'');
                        that.$placeholderSection.show();
                        that.$spinnerSection.hide();
                        that.$urlInput.removeAttr('disabled');
                        that.scResolveReq = null;
                        return;
                    }
                    that.resolveToUs(res.id);
                });
            });

            return false;
        },

        showError: function(msg) {
            var $errors = this.$urlInputFrame.find('.errors');
            if (msg) {
                $errors.show();
            } else {
                $errors.hide();
            }
            $errors.html(msg);
        },

        resolveToUs: function(userId) {
            var that = this;

            if (this.ourResolveReq) {
                this.ourResolveReq.abort();
                this.ourResolveReq = null;
            }

            this.ourResolveReq = module.$.ajax({
                url:module.api.importSoundcloudTrackUrl,
                data: {
                    sc_user_id: userId
                },
                dataType:'json'
            }).done(function(res) {
                if (!res || !res.success) {
                    that.showError(module.string('failed_to_resolve_url'));
                    that.$placeholderSection.show();
                    return;
                }

                if (res.data.length === 0) {
                    that.showError('No track exist');
                    that.$placeholderSection.show();
                    return;
                }
                res.data.reverse();
                that.tracks = res.data;
                that.$urlInput.removeAttr('disabled');
                that.$inputBox.hide();
                that.showTypeSelect();
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus !== 'abort') {
                    that.showError('Failed to resolve URL');
                    that.$placeholderSection.show();
                    that.$urlInput.removeAttr('disabled');
                }
            }).always(function() {
                that.$spinnerSection.hide();
                that.ourResolveReq = null;
                that.scResolveReq = null;
                that.$urlInput.removeAttr('disabled');
            });
        },

        onSingleUploadTypeClicked: function(e) {
            this.$typeSelectSection.hide();
            this.showSingleUpload();
        },

        onMultipleUploadTypeClicked: function(e) {
            this.$typeSelectSection.hide();
            this.showMassUpload();
        },

        showTypeSelect: function() {
            this.$typeSelectSection.show();
        },

        showSingleUpload: function() {
            var $tracklist = this.$uploadSection.find('.tracks');
            $tracklist.empty();
            module.$.each(this.tracks, function(idx, track) {
                var $track = module.$('<a class=\'track\'></a>');

                $track.attr('data-stream-url', track.stream_url);
                $track.attr('data-title', track.title);
                $track.html(track.title);
                $tracklist.append($track);
            });
            this.$uploadSection.show();
            $tracklist.find('.track').first().click();
        },

        showMassUpload: function() {
            if (this._uploadView) {
                this._uploadView.remove();
            }
            this._uploadView = new SoundcloudMassUploadView({
                $frame: this.$massUploadSection,
                tracks: this.tracks
            });
            this.$massUploadSection.show();
        },

        onTrackSelected: function(e) {
            var $track = module.$(e.currentTarget),
                title = $track.data('title'),
                streamUrl = $track.data('streamUrl');

            if (this._uploadView && this._uploadView.uploading &&
                    !confirm(module.string('are_you_sure_cancel_upload'))) {
                return;
            }

            if (this._uploadView) {
                this._uploadView.remove();
            }

            this.$uploadSection.find('.track').removeClass('selected');
            $track.addClass('selected');
            this.$uploaderFrame.show();

            this._uploadView = new SoundcloudUploadView({
                $frame: this.$uploaderFrame.find('.uploader-wrapper'),
                title: title,
                streamUrl: streamUrl
            });
        }
    });

    ScImportTask = function() {
        this._uploadDrop = function(token, dropStart) {
            var that = this,
                url = module.uploadApi.postProcessUrl(this.uploadHost),
                deferred = module.$.Deferred(),
                formData = new FormData(),
                trackType;

            if (this.dropUploadReq) {
                this.dropUploadReq.abort();
            }

            this._postProgress(false, 'Generating drop file.. (5/6)');

            this.dropUploadReq = module.$.ajax({
                url:url,
                data: {
                    drop_start: dropStart,
                    token:token
                },
                crossDomain: true,
                dataType:'json',
                type:'POST',
                cache:false,
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                    xhr.setRequestHeader('dbt_key', that.credentialKey);
                }
            }).done(function(res) {
                deferred.resolve(res.drop_url);
            }).fail(function(jqXHR, textStatus, error) {
                deferred.reject('Failed to import track');
            }).always(function() {
                that.dropUploadReq = null;
            });

            return deferred;
        };

        this._preprocess = function(scUrl) {
            var that = this,
                url = module.uploadApi.preProcessUrl(this.uploadHost),
                deferred = module.$.Deferred(),
                formData = new FormData(),
                filename,
                ext,
                idx,
                trackType;

            trackType = this.formData.track_type;

            filename = this.formData.name.replace(/[^a-zA-Z0-9\-\s]/g, '').toLowerCase() + '.mp3'

            formData.append('filename', filename);
            formData.append('url', scUrl);

            if (this.preprocessReq) {
                this.preprocessReq.abort();
            }

            this._postProgress(false, 'Preprocessing.. (2/6)');

            this.preprocessReq = module.$.ajax({
                url:url,
                data: {
                    filename: filename,
                    url:scUrl,
                    type: 1
                },
                crossDomain: true,
                dataType:'json',
                type:'POST',
                cache:false,
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                    xhr.setRequestHeader('dbt_key', that.credentialKey);
                }
            }).done(function(res) {
                if (!res || !res.token) {
                    deferred.reject();
                } else {
                    deferred.resolve(res.token);
                }
            }).fail(function(jqXHR, textStatus, error) {
                deferred.reject('Failed to import track');
            }).always(function() {
                that.preprocessReq = null;
            });

            return deferred;
        };

        this._uploadSound = function(token) {
            var that = this,
                url = module.uploadApi.uploadSoundUrl(this.uploadHost),
                deferred = module.$.Deferred();

            if (this.uploadReq) {
                this.uploadReq.abort();
            }

            this._postProgress(false, 'Uploading sound file.. (3/6)');

            this.uploadReq = module.$.ajax({
                url:url,
                data: {
                    token: token
                },
                crossDomain: true,
                dataType:'json',
                type:'POST',
                cache:false,
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                    xhr.setRequestHeader('dbt_key', that.credentialKey);
                }
            }).done(function(res) {
                var waveformData;

                if (!res) {
                    deferred.reject();
                    return;
                }
                try {
                    waveformData = JSON.parse(res.waveform_data);
                    deferred.resolve(token, res.track_url, waveformData);
                } catch(e) {
                    deferred.reject();
                }
            }).fail(function(jqXHR, textStatus, error) {
                deferred.reject('Failed to import track');
            }).always(function() {
                that.uploadReq = null;
            });

            return deferred;
        };

        this._uploadWaveform = function(data) {
            var deferred = module.$.Deferred(),
                that = this,
                bars, file, str;

            try {
                bars = module.utils.downsizeArray(data, 1000);
                str = '[' + bars.join(',') + ']';
                file = new Blob([str], {type: 'text/plain'});

                this._postProgress(false, 'Uploading waveform file.. (4/6)');
                this.waveformUploadReq = module.utils.uploadImage(
                        this.uploadHost,
                        this.credentialKey,
                        file,
                        'w',
                        postWaveformProgress,
                        function(success, url) {
                            if (!success) {
                                deferred.reject();
                            }
                            deferred.resolve(url);
                        });

                this.waveformUploadReq.always(function() {
                    that.waveformUploadReq = null;
                });

            } catch(e) {
                deferred.reject();
            }

            function postWaveformProgress() {
            }

            return deferred;
        };

        this._uploadCover = function(url) {
            var COVER_SIZE = {
                    w: 600,
                    h: 600
                },
                img,
                canvas,
                context,
                deferred = module.$.Deferred(),
                that = this,
                hqImageUrl;

            hqImageUrl = url;
            hqImageUrl = hqImageUrl.replace('-large', '-t500x500');

            this.$el.find('.coverart-canvas').remove();
            this.$el.append('<canvas class=\'visuallyhidden coverart-canvas\'></canvas>');
            canvas = this.$el.find('.coverart-canvas')[0];
            context = canvas.getContext('2d');
            img = new Image();
            img.onload = function () {

                canvas.width = COVER_SIZE.w;
                canvas.height = COVER_SIZE.h;
                context.drawImage(img, 0, 0, img.width, img.height,
                    0, 0, COVER_SIZE.w, COVER_SIZE.h);

                module.utils.canvasToBlob(canvas)
                    .done(function(dataBlob) {
                        uploadImage(dataBlob, 'c');
                    })
                    .fail(function() {
                        deferred.reject('Failed to import coverart');
                    });
            };
            img.crossOrigin = 'anonymous';
            img.onerror = function () {
                deferred.reject('Failed to import coverart');
            };
            img.src = hqImageUrl;

            function uploadImage(blob, type) {
                that._postProgress(false, 'Uploading coverart.. (1/6)');
                that.imageUploadReq = module.utils.uploadImage(
                    that.uploadHost, that.credentialKey, blob, type, postImageProgress,
                    function(success, url) {
                        if (!success) {
                            deferred.reject('Failed to upload coverart');
                            return;
                        }
                        deferred.resolve(url);
                    });
                that.imageUploadReq.always(function() {
                    that.imageUploadReq = null;
                    that.$el.find('.coverart-canvas').remove();
                });
            }

            function postImageProgress(e) {
            }

            return deferred;
        };

        this._postProgress = function(percent, text) {
            this.$progress.progressbar({
                mx: 100,
                value: percent
            });
            this.$progressLabel.text(text);
            this.onprogress && this.onprogress(percent);
        };

        this._changeState = function(state, msg) {
            var that = this,
                text,
                states = ['init', 'started', 'failed', 'canceled', 'success'];

            module.$.each(states, function(idx, s) {
                if (s !== state) {
                    that.$el.removeClass('state-' + s);
                }
            });
            that.$el.addClass('state-' + state);

            if (state === 'init') {
                text = 'Ready';
            } else if (state === 'started') {
                text = 'Started';
            } else if (state === 'failed') {
                if (msg && (typeof msg === 'string')) {
                    this.$error.text(msg);
                }
                text = 'Failed';
                this._clear();
            } else if (state === 'canceled') {
                text = 'Canceled';
                this._clear();
            } else if (state === 'success') {
                text = 'Done';
                this._clear();
            }
            this.$status.text(text);
            this.onchangestate && this.onchangestate(this, state);
        };

        this._getDropPosition = function(data, duration) {
            var SIZE = 5,
                IGNORE_DURATION = 20,
                binSize = Math.floor(SIZE * data.length /  duration),
                i,
                idx, curr, prev,
                maxVal = -1,
                bins = [],
                binCount = Math.ceil(data.length / binSize);

            bins = module.utils.downsizeArray(data, binCount);

            for (i = Math.floor(IGNORE_DURATION / SIZE) ; i < bins.length; i++) {
                prev = bins[i - 1];
                curr = bins[i];
                if (maxVal < curr - prev) {
                    idx = i - 1;
                    maxVal = curr - prev;
                }
            }
            return idx * SIZE;
        };

        this._clear = function() {
            if (this.imageUploadReq) {
                this.imageUploadReq.abort();
                this.imageUploadReq = null;
            }

            if (this.uploadReq) {
                this.uploadReq.abort();
                this.uploadReq = null;
            }

            if (this.preprocessReq) {
                this.preprocessReq.abort();
                this.preprocessReq = null;
            }

            if (this.dropUploadReq) {
                this.dropUploadReq.abort();
                this.dropUploadReq = null;
            }

            if (this.waveformUploadReq) {
                this.waveformUploadReq.abort();
                this.waveformUploadReq = null;
            }

            if (this.uploadTrackInfoReq) {
                this.uploadTrackInfoReq.abort();
                this.uploadTrackInfoReq = null;
            }
        };

        this._submit = function(data) {
            var that = this;

            this._postProgress(false, 'Uploading track information.. (6/6)');
            if (this.uploadTrackInfoReq) {
                this.uploadTrackInfoReq.abort();
            }
            this.uploadTrackInfoReq = module.$.ajax({
                url: module.api.trackUrl,
                data: JSON.stringify(data),
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json'
            }).done(function(res) {
                var resourcePath,
                    uploadResult = {};
                if (!res || !res.success || !res.obj) {
                    that._changeState('failed', res.error);
                    return;
                }

                resourcePath = '/r/' + res.obj.user_resource_name + '/' + res.obj.resource_path;
                uploadResult = {
                    trackName: that.title,
                    trackUrl: resourcePath
                };
                that._changeState('success');

            }).fail(function(jqXHR, textStatus, error) {
                that._changeState('failed');
            }).always(function() {
                that.uploadTrackInfoReq = null;
            });
        };

        this._unescape = function(str) {
            var $el = module.$('<div></div>');
            $el.html(str);
            return $el.text();
        };

        this.init = function($el, track, key, host, idx) {
            var $frame = $el,
                tmpl = module._.template(module.$('#tmpl-import-track-task').html()),
                $track;

            $track = module.$(tmpl({
                task: {
                    key: track.stream_url,
                    title: track.title,
                    status: 'Ready'
                }
            }));

            $track.addClass('ready');
            $frame.append($track);

            this.idx = idx;
            this.credentialKey = key;
            this.uploadHost = host;
            this.$el = $track;
            this.readyToSubmit = false;
            this.$status = module.$('.status', this.$el);
            this.$progress = module.$('.progress', this.$el);
            this.$progressLabel = module.$('.progress .progress-label', this.$el);
            this.$error = module.$('.error', this.$el);
            this.key = track.stream_url;
            this.track = track;
            this.track.stream_url = track.stream_url;
            this.track.resource_url = track.stream_url.replace('/stream','');
            this._changeState('init');
        };

        this.start = function() {
            var that = this;

            this._changeState('started');
            this._postProgress(false, 'Loading');

            SC.get('/resolve', {url: this.track.resource_url}, function(res, error) {
                var duration,
                    dropDuration = 20,
                    deferreds = [],
                    fakeDeferred = module.$.Deferred(),
                    artworkUrl;

                if (error) {
                    that._changeState('failed');
                    return;
                }


                duration = Math.round(res.duration / 1000);

                that.formData = {
                    description : that._unescape(res.description),
                    duration : duration,
                    downloadable : true,
                    drop_duration : dropDuration,
                    name : that._unescape(res.title),
                    track_type: res.kind === 'track' && duration < 20 * 60 ? 'TRACK' : 'MIX',
                    genre_id: 6,
                    drop_url: null
                };

                artworkUrl = res.artwork_url;
                if (!artworkUrl && res.user) {
                    artworkUrl = res.user.avatar_url;
                }
                if (artworkUrl) {
                    deferreds.push(that._uploadCover(artworkUrl));
                } else {
                    deferreds.push(fakeDeferred);
                    fakeDeferred.resolve('http://dropbeat.net/images/default_cover_big.png');
                }

                module.$.when.apply(that, deferreds).done(function(coverUrl) {

                    that.formData.coverart_url = coverUrl;

                    that._preprocess(that.track.stream_url)

                        .then(module.$.proxy(that._uploadSound, that))

                        .then(function(token, trackUrl, waveformData) {
                            var deferred = module.$.Deferred();

                            that._uploadWaveform(waveformData)
                                .done(function(url) {
                                    that.formData.waveform_url = url;
                                    if (that.formData.duration < 20 * 60) {
                                        that.formData.drop_start = that._getDropPosition(waveformData,
                                            that.formData.duration);
                                    } else {
                                        that.formData.drop_start = -1;
                                    }
                                    deferred.resolve(token, trackUrl);
                                })
                                .fail(function() {
                                    deferred.reject.apply(this, arguments);
                                });
                            return deferred;
                        })

                        .then(function(token, trackUrl) {
                            that.formData.track_url = trackUrl;
                            that.formData.unique_key = token;
                            that.formData.drop_start == that.formData.drop_start || -1;

                            if (that.formData.drop_start > 0) {
                                that._uploadDrop(that.formData.unique_key, that.formData.drop_start)
                                    .done(function(dropUrl) {
                                        that.formData.drop_url = dropUrl;

                                        that.submit = function() {
                                            that.readyToSubmit = false;
                                            that._submit(that.formData)
                                        };
                                        that.readyToSubmit = true;
                                        module.$(that).trigger('ready', [that]);

                                    }).fail(function(msg) {
                                        if (!that.isCanceled) {
                                            that._changeState('failed', msg);
                                        }
                                    });
                            } else {
                                that.submit = function() {
                                    that.readyToSubmit = false;
                                    that._submit(that.formData)
                                };
                                that.readyToSubmit = true;
                                module.$(that).trigger('ready', [that]);
                            }

                        }).fail(function(msg) {
                            if (!that.isCanceled) {
                                that._changeState('failed', msg);
                            }
                        });

                }).fail(function(msg) {
                    if (!that.isCanceled) {
                        that._changeState('failed', msg);
                    }
                });
            });
        };

        this.submit = null;

        this.cancel = function() {
            this.isCanceled = true;
            this._changeState('canceled');
        };
    };

    SoundcloudMassUploadView = DropbeatView.extend({
        initialize: function(options) {
            var that = this,
                idx;

            module._.bindAll(this, 'onTaskStateChanged', 'onTaskReady');

            DropbeatView.prototype.initialize.call(this, options);

            this.credentialCallbacks = [];

            this.$el = options.$frame;
            this.tracks = options.tracks;

            this.$tracklist = this.$el.find('.tracks');
            this.$tracklist.empty();

            this.$cancelBtn = this.$el.find('.btn-cancel');
            this.$cancelAllBtn = this.$el.find('.btn-cancel-all');
            this.$startBtn = this.$el.find('.btn-start-import');

            this.taskQ = [];
            this.failureQ = [];
            this.successQ = [];
            this.workingQ = {};

            module.utils.getUploadCredential().done(function(key, host) {
                module.$.each(that.tracks, function(idx, track) {
                    var task = new ScImportTask()
                    task.init(that.$tracklist, track, key, host, idx);
                    task.onchangestate = that.onTaskStateChanged;
                    that.taskQ.push(task);
                    module.$(task).on('ready', that.onTaskReady);
                });
            }).fail(function(msg) {
                if (msg !== 'canceled') {
                    module.s.notify(module.string('failed_to_authenticate'), 'error');
                    return;
                }
            });
        },

        events: function() {
            var events = DropbeatView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .btn-cancel': 'onCancelBtnClicked',
                'click .btn-cancel-all': 'onCancelAllBtnClicked',
                'click .btn-start-import': 'onStartBtnClicked'
            });
        },

        onTaskReady: function(e, task) {
            var wait = false;
            module.$.each(this.workingQ, function(key, t) {
                if (t.key !== task.key && t.idx < task.idx) {
                    wait = true;
                    return false;
                }
            });

            if (!wait) {
                task.submit();
            }
        },

        remove: function() {
            module.removeBeforeunloadCallback('upload');

            this.undelegateEvents();
            this.stopListening();

            this.taskQ.length = 0;
            module.$.each(this.workingQ, function(key, task) {
                task.cancel();
            });

            return false;
        },

        startUpload: function() {
            var that = this,
                MAX_ON_THE_FLY_COUNT = 3,
                roomCount =  MAX_ON_THE_FLY_COUNT - Object.keys(this.workingQ).length,
                i, task,
                topPriorityTask;

            if (roomCount === 0) {
                return;
            }
            for (i = 0; i < roomCount; i++) {
                task = this.taskQ.shift();
                if (!task) {
                    break;
                }
                task.start();
                that.workingQ[task.key] = task;
            }

            module.$.each(this.workingQ, function(key, t) {
                if (!topPriorityTask || t.idx < topPriorityTask.idx) {
                    topPriorityTask = t;
                }
            });

            if (topPriorityTask && topPriorityTask.readyToSubmit) {
                topPriorityTask.submit();
            }
        },

        onTaskStateChanged: function(task, state) {
            if (['failed', 'success', 'canceled'].indexOf(state) === -1) {
                return;
            }
            delete this.workingQ[task.key];

            if (state === 'failed') {
                this.failureQ.push(task);
            } else if (state === 'success') {
                this.successQ.push(task);
            }
            this.startUpload();
            if (this.taskQ.length === 0 &&
                    Object.keys(this.workingQ).length === 0) {
                module.removeBeforeunloadCallback('upload');
                this.$cancelAllBtn.hide();
            }
        },

        onStartBtnClicked: function(e) {
            this.startUpload();
            this.$startBtn.hide();
            this.$cancelAllBtn.css('display', 'inline-block');

            module.addBeforeunloadCallback('upload', function() {
                return module.string('are_you_sure_cancel_upload');
            });
        },

        onCancelAllBtnClicked: function(e) {
            this.taskQ.length = 0;
            module.$.each(this.workingQ, function(key, task) {
                task.cancel();
            });
        },

        onCancelBtnClicked: function(e) {
            var $task = module.$(e.currentTarget).closest('.track-task'),
                key = unescape($task.data('key')),
                task;

            task = this.workingQ[key];
            if (!task) {
                return;
            }

            task.cancel();
        }

    });


    SoundcloudUploadView = DropbeatView.extend({
        tmpl: '#tmpl-track-uploader',
        initialize: function(options) {
            var that = this,
                idx;

            module._.bindAll(this,
                'onDropSelectorUp', 'onDropSelectorMove', 'onAudioPrepared', 'onAudioError',
                'onStartUploaderBtnClicked');

            DropbeatView.prototype.initialize.call(this, options);

            module.$('header .header-menu .menu').removeClass('selected');
            this._failureTmpl = module._.template(module.$('#tmpl-page-upload-not-supported').html());
            this._successTmpl = module._.template(module.$('#tmpl-track-import-result').html());

            this.$startBtnFrame = module.$('.uploader-frame .start-btn-wrapper');

            this.render(options.$frame);
            this.credentialCallbacks = [];

            this.$artworkProgress = module.$('.track-info-uploader .thumbnail .progress', this.$el);
            this.$artworkProgressPercent = module.$('.progress-label', this.$artworkProgress);

            this.$soundProgressFrame = module.$('.sound-progress-wrapper', this.$el);
            this.$soundUploadCancelBtn = module.$('.track-sound-uploader .btn-cancel', this.$el);

            this.$soundProgress = module.$('.progress', this.$soundProgressFrame);
            this.$soundProgressPercent = module.$('.progress .progress-label',
                this.$soundProgressFrame);

            this.$dropProgressFrame = module.$('.drop-progress-wrapper', this.$el);
            this.$dropProgress = module.$('.progress', this.$dropProgressFrame);
            this.$dropProgressPercent = module.$('.progress .progress-label', this.$dropProgressFrame);

            this.$trackInfoUploader = module.$('.track-info-uploader', this.$el);
            this.$trackInfoForm = module.$('#form-upload-track-info', this.$el);
            this.$trackInfoTitle = module.$('.input-title', this.$trackInfoUploader);
            this.$trackInfoDesc = module.$('.input-description', this.$trackInfoUploader);
            this.$trackInfoTrackUrl = module.$('.input-stream-url', this.$trackInfoUploader);
            this.$trackInfoTrackKey = module.$('.input-unique-key', this.$trackInfoUploader);
            this.$trackInfoGenreId = module.$('.input-genre', this.$trackInfoUploader);
            this.$trackInfoCoverArtImg = module.$('.thumbnail .preview img', this.$trackInfoUploader);
            this.$trackInfoCoverArtUrl = module.$('.input-coverart-url', this.$trackInfoUploader);
            this.$trackInfoWaveformUrl = module.$('.input-waveform-url', this.$trackInfoUploader);
            this.$trackInfoResourceName = module.$('.input-resource-name', this.$trackInfoUploader);
            this.$trackInfoDropUrl = module.$('.input-drop-url', this.$trackInfoUploader);
            this.$trackInfoDropStart = module.$('.input-drop-start', this.$trackInfoUploader);
            this.$trackInfoDropDuration = module.$('.input-drop-duration', this.$trackInfoUploader);
            this.$trackInfoDuration = module.$('.input-duration', this.$trackInfoUploader);
            this.$trackInfoDownloadable = module.$('.input-downloadable', this.$trackInfoUploader);
            this.$trackInfoTrackType = module.$('.input-track-type', this.$trackInfoUploader);
            this.$trackInfoUploaderSubmitBtn = module.$('.btn-save', this.$el);

            this.$genreSelectFrame = module.$('.genre-select-wrapper', this.$el);
            this.$genreLoader = module.$('.spinner', this.$genreSelectFrame);
            this.$genreSelect = module.$('.genre-select', this.$genreSelectFrame);
            this.$genreFailure = module.$('.failure', this.$genreSelectFrame);
            this.$genreDropdown = module.$('.dropdown', this.$genreSelectFrame);
            this.$filter = module.$('.overlay-filter');

            this.$downloadableChoice = module.$('.downloadable-input-wrapper .choice', this.$el);
            this.$trackTypeChoice = module.$('.track-type-input-wrapper .choice', this.$el);

            this.$dropInfoUploaderFrame = module.$('.drop-info-uploader-wrapper', this.$el);
            this.$dropInfoUploader = module.$('.drop-info-uploader', this.$dropInfoUploaderFrame);
            this.$waveformFrame = module.$('.waveforms', this.$dropInfoUploader);
            this.$dropSelector = module.$('.drop-selectors', this.$dropInfoUploader);

            this.$waveformProgressFrame = module.$('.waveform-progress-wrapper', this.$dropInfoUploaderFrame);
            this.$waveformProgress = module.$('.progress', this.$waveformProgressFrame);
            this.$waveformCanvasFrame = module.$('.canvas-wrapper', this.$dropInfoUploaderFrame);
            this.$waveformProgressPercent = module.$('.progress .progress-label', this.$waveformProgressFrame);

            this.$dropBtn = module.$('.listen-drop', this.$dropInfoUploader);
            this.$dropBtnText = module.$('.listen-drop .text', this.$dropInfoUploader);

            this.genre = module.context.genre['dropbeat'];
            module.$.each(this.genre, function(id, genre) {
                that.$genreDropdown.append(
                    '<li class=\'genre\' data-key=\'' + genre.id + '\'>' + genre.name + '</li>');
            });
            this.$genreSelect.show();
            this.$genreLoader.hide();

            module.$(window).on('mouseup', this.onDropSelectorUp);
            module.$(window).on('mousemove', this.onDropSelectorMove);

            this.$trackInfoTrackUrl.remove();
            this.$trackInfoTitle.val(this._unescape(options.title));
            this.$trackInfoTitle.attr('value', this._unescape(options.title));
            this.streamUrl = options.streamUrl;
            this.resourceResolveUrl = options.streamUrl.replace('/stream','');
            this.title = options.title;
        },

        events: function() {
            var events = DropbeatView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {

                'click .track-sound-uploader .btn-cancel': 'onSoundUploadCancelBtnClicked',
                'click .track-drop-uploader .btn-cancel-drop': 'onSoundUploadCancelBtnClicked',

                'click .btn-upload-thumbnail': 'onThumbnailUploadBtnClicked',

                'click .btns .btn-save': 'onTrackInfoSaveBtnClicked',
                'click .btns .btn-cancel': 'onTrackInfoCancelBtnClicked',

                'click .genre-select .selected-genre': 'onSelectGenre',
                'click .genre-select .genre': 'onGenreClicked',

                'click .downloadable-input-wrapper .choice': 'onToggleDownloadable',
                'click .track-type-input-wrapper .choice': 'onTrackTypeSelected',

                'click .drop-info-uploader .listen-drop': 'onDropBtnClicked',

                'mousedown .drop-selectors' : 'onDropSelectorDown'
            });
        },

        render: function($frame) {
            if (this.canUpload()) {
                $frame.html(this._tmpl(this._context));
                this.$startBtnFrame.show();
                this.$startBtnFrame.on('click', '.btn-start-uploader',
                    this.onStartUploaderBtnClicked);
            } else {
                this.$startBtnFrame.hide();
                $frame.html(this._failureTmpl(this._context));
            }
            this.$el = $frame.find('.uploader');
        },

        remove: function() {
            DropbeatView.prototype.remove.apply(this, arguments);
            module.$(window).off('mouseup', this.onDropSelectorUp);
            module.$(window).off('mousemove', this.onDropSelectorMove);

            if (this.soundPlayer) {
                soundManager.destroySound(this.soundPlayer.id);
                this.soundPlayer = null;
            }

            module.removeBeforeunloadCallback('upload');
            this.$startBtnFrame.off('click',
                '.btn-start-uploader', this.onStartUploaderBtnClicked);

            if (this.req) {
                this.req.abort();
                this.req = null;
            }

            if (this.preprocessReq) {
                this.preprocessReq.abort();
                this.preprocessReq = null;
            }

            if (this.resourceResolveReq) {
                this.resourceResolveReq.abort();
                this.resourceResolveReq = null;
            }

            if (this.uploadReq) {
                this.uploadReq.abort();
                this.uploadReq = null;
            }

            if (this.dropUploadReq) {
                this.dropUploadReq.abort();
                this.dropUploadReq = null;
            }

            if (this.imageUploadReq) {
                this.imageUploadReq.abort();
                this.imageUploadReq = null;
            }

            if (this.waveformReq) {
                this.waveformReq.abort();
                this.waveformReq = null;
            }

            if (this.waveformUploadReq) {
                this.waveformUploadReq.abort();
                this.waveformUploadReq = null;
            }

            if (this.uploadTrackInfoReq) {
                this.uploadTrackInfoReq.abort();
                this.uploadTrackInfoReq = null;
            }

            if (this.imageDownloadReq) {
                this.imageDownloadReq.abort();
                this.imageDownloadReq = null;
            }
        },

        _unescape: function(str) {
            var $el = module.$('<div></div>');
            $el.html(str);
            return $el.text();
        },

        onStartUploaderBtnClicked: function() {
            var that = this;
            this.uploading = true;
            this.$startBtnFrame.hide();

            module.playerManager.pause();

            module.addBeforeunloadCallback('upload', function() {
                return module.string('are_you_sure_cancel_upload');
            });

            this.resolve(this.resourceResolveUrl, function(track) {
                var artworkUrl = track.artwork_url || track.user.avatar_url;

                that.setSoundDurationInput(Math.round(track.duration / 1000));
                that.prepareAudioPlayer(that.streamUrl);
                that.renderWaveform(track.waveform_url);
                that.$trackInfoDesc.val(that._unescape(track.description));

                artworkUrl = artworkUrl.replace('-large', '-t500x500');
                that.importArtwork(artworkUrl);
            });

        },

        resolve: function(url, callback) {
            var that = this;

            this.resourceResolveReq = module.$.ajax({
                url:url,
                dataType:'json',
                crossDomain:true
            }).done(function(res) {
                if (!res.waveform_url) {
                    callback && callback(false);
                }
                callback && callback(res);
            }).fail(function(jqXHR, textStatus, error) {
                callback && callback(false);
            }).always(function() {
                that.resourceResolveReq = null;
            });
        },

        importArtwork: function(url) {
            var that = this,
                img,
                COVER_SIZE = {
                    w: 600,
                    h: 600
                };

            function postProgress(percent, text) {
                that.$artworkProgressPercent.text('Downloading..');
                that.$artworkProgress.progressbar({
                    max:100,
                    value: percent < 0 ? false : Math.min(percent, 100)
                });
            }

            function postImageProgress(percent) {
                postProgress(percent, 'Uploading.. (' + percent + '%)');
            }

            if (!url) {
                return;
            }
            this.$artworkProgress.show();
            postProgress(-1, 'Downloading..');

            img = new Image();
            img.onload = function () {
                var deferreds = [],
                    type = 'c',
                    canvas,
                    context;

                postProgress(-1, 'Converting..');
                canvas = document.createElement('canvas');

                canvas.width = COVER_SIZE.w;
                canvas.height = COVER_SIZE.h;
                context = canvas.getContext('2d');
                context.drawImage(img, 0, 0, img.width, img.height,
                    0, 0, COVER_SIZE.w, COVER_SIZE.h);

                deferreds.push(module.utils.canvasToBlob(canvas));
                deferreds.push(module.utils.getUploadCredential());

                module.$.when.apply(this, deferreds)
                    .done(function(dataBlob, credential) {
                        var key = credential[0],
                            host = credential[1];

                        postProgress(-1, 'Uploading..');

                        that.imageUploadReq = module.utils.uploadImage(
                            host, key, dataBlob, type, postImageProgress,
                            function(success, url) {
                                if (!success) {
                                    module.s.notify('Failed to upload coverart', 'error');
                                    return;
                                }
                                that.setThumbUrlInput(url);
                            });
                        that.imageUploadReq.always(function() {
                            that.imageUploadReq = null;
                            that.$artworkProgress.hide();
                        });

                    })
                    .fail(function() {
                        that.$artworkProgress.hide();
                        module.s.notify('Failed to import coverart', 'error');
                    });
            };
            img.crossOrigin = 'anonymous';
            img.onerror = function () {
                module.s.notify('Failed to import coverart', 'error');
            };
            img.src = url;
        },

        fetchWaveform: function(waveformUrl, callback) {
            var that = this;

            this.waveformReq = module.$.ajax({
                url: 'http://www.waveformjs.org/w',
                dataType:'jsonp',
                data: {
                    url: waveformUrl
                }
            }).done(function(res) {
                callback && callback(true, res);
            }).fail(function(jqXHR, textStatus, error) {
                callback && callback(false);
            }).always(function() {
                that.waveformReq = null;
            });
        },

        startUploadFile: function(scUrl, callback) {
            var xhr, blob, that = this,
                resourceName;

            this.setSoundUrlInput(null);
            this.setSoundKeyInput(null);

            this.$soundProgressFrame.show();
            this.$soundProgressPercent.removeClass('error');
            this.$soundUploadCancelBtn.css('display', 'inline-block');

            this.postSoundProgress(-1, 'Requesting authentication.. (1/5)');
            module.utils.getUploadCredential()
                .then(function(key, host) {
                    var trackType, filename,
                        url = module.uploadApi.preProcessUrl(host),
                        deferred = module.$.Deferred();

                    trackType = that.$trackInfoTrackType.val();
                    filename = that.title.replace(/[^a-zA-Z0-9\-\s]/g, '').toLowerCase() + '.mp3';

                    that.uploadHost = host;
                    that.credentialKey = key;

                    that.postSoundProgress(-1, 'Preprocessing.. (2/5)');

                    if (that.preprocessReq) {
                        that.preprocessReq.abort();
                        that.preprocessReq = null;
                    }

                    that.preprocessReq = module.$.ajax({
                        url:url,
                        data: {
                            filename: filename,
                            url:scUrl,
                            type:1
                        },
                        crossDomain: true,
                        dataType:'json',
                        type:'POST',
                        cache:false,
                        xhrFields: {
                            withCredentials: true
                        },
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                            xhr.setRequestHeader('dbt_key', that.credentialKey);
                        }
                    }).done(function(res) {
                        if (!res || !res.token) {
                            deferred.reject();
                            return;
                        }
                        deferred.resolve(res.token);
                    }).fail(function(jqXHR, textStatus, error) {
                        deferred.reject(textStatus === 'abort' ? 'canceled' : null);
                    });
                    return deferred;

                }).then(function(token) {
                    var url = module.uploadApi.uploadSoundUrl(that.uploadHost);
                    that.postSoundProgress(-1, 'Uploading sound file.. (3/5)');

                    that.uploadReq = module.$.ajax({
                        xhr: function() {
                            var x = module.utils.createCORSRequest('post', url);
                            if (!x) {
                                return null;
                            }
                            x.withCredentials = true;
                            return x;
                        },
                        url:url,
                        data: {
                            token:token
                        },
                        crossDomain: true,
                        dataType:'json',
                        type:'POST',
                        cache:false,
                        xhrFields: {
                            withCredentials: true
                        },
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                            xhr.setRequestHeader('dbt_key', that.credentialKey);
                        }
                    }).done(function(res) {
                        callback && callback(true, res.track_url, token);
                    }).fail(function(jqXHR, textStatus, error) {
                        callback && callback(false, textStatus == 'abort' ? 'canceled' : null);
                    }).always(function() {
                        that.uploadReq = null;
                    });

                }).fail(function(msg) {
                    callback && callback(false, msg);
                    return;
                });
        },

        canUpload: function() {
            var xhr;
            if (window['XMLHttpRequest']) {
                xhr = new XMLHttpRequest();
                if (!('withCredentials' in xhr) && typeof XDomainRequest === 'undefined') {
                    return false;
                }
            }
            if (!window['AudioContext'] && !window['webkitAudioContext']) {
                return false;
            }
            if (module.context.isMobile) {
                return false;
            }
            return true;
        },

        onToggleDownloadable: function(e) {
            var currVal = this.$trackInfoDownloadable.val(),
                $ic = this.$downloadableChoice.find('i');

            if (currVal === 'true') {
                this.$downloadableChoice.addClass('selected');
                this.$trackInfoDownloadable.val(false);
                this.$trackInfoDownloadable.attr('value', false);
                $ic.removeClass('glyphicons-unchecked').addClass('glyphicons-check');
            } else {
                this.$downloadableChoice.removeClass('selected');
                this.$trackInfoDownloadable.val(true);
                this.$trackInfoDownloadable.attr('value', true);
                $ic.addClass('glyphicons-unchecked').removeClass('glyphicons-check');
            }
        },

        onTrackTypeSelected: function(e) {
            var currVal = this.$trackInfoTrackType.val(),
                $choice = module.$(e.currentTarget),
                key = $choice.data('key'),
                $ic = $choice.find('i'),
                audio,
                $dropText;

            if ($choice.hasClass('selected')) {
                return;
            }

            this.$trackTypeChoice.removeClass('selected');
            this.$trackTypeChoice.find('i').removeClass('glyphicons-check').addClass('glyphicons-unchecked');
            $choice.addClass('selected');
            $ic.removeClass('glyphicons-unchecked').addClass('glyphicons-check');

            this.$trackInfoTrackType.val(key);
            this.$trackInfoTrackType.attr('value', key);

            if (key === 'TRACK') {
                this.$dropInfoUploader.removeClass('disabled');
            } else {
                if (this.soundPlayer) {
                    this.soundPlayer.stop();
                }

                this.$dropBtn.removeClass('stop');
                $dropText = this.$dropBtn.find('.text');
                $dropText.text($dropText.data('textPlay'));
                this.$dropBtn.find('i').addClass('glyphicons-play')
                    .removeClass('glyphicons-stop');

                this.$dropInfoUploader.addClass('disabled');
            }
        },

        onDropBtnClicked : function(e) {
            var drop,
                $btn = module.$(e.currentTarget),
                $text = $btn.find('.text'),
                $ic= $btn.find('i'),
                that = this;

            if (!this.soundPlayer) {
                return;
            }
            drop = parseInt(this.$trackInfoDropStart.val());
            if (drop !== 0 && !drop) {
                return;
            }

            if ($btn.hasClass('stop')) {
                // do stop
                this.soundPlayer && this.soundPlayer.pause();
                $btn.removeClass('stop');
                $text.text($text.data('textPlay'));
                $ic.addClass('glyphicons-play').removeClass('glyphicons-stop');
                return;
            }

            this.soundPlayer.setPosition(drop * 1000);
            this.soundPlayer.play();
            $btn.addClass('stop');
            $btn.find('.text');
            $text.text($text.data('textStop'));
            $ic.removeClass('glyphicons-play').addClass('glyphicons-stop');

            if (this.dropTimer) {
                clearTimeout(this.dropTimer);
            }

            this.dropTimer = setTimeout(function() {
                if (that.$dropBtn.hasClass('stop')) {
                    that.$dropBtn.click();
                }
                that.dropTimer = null;
            }, 20000);
        },

        onDropSelectorDown:function(e) {
            var $selector = module.$(e.currentTarget);
            this._dropSelectorDown = true;
            this._dropSelectorDownPosition = e.pageX - this.$dropSelector.offset().left;
        },

        onDropSelectorUp:function(e) {
            var drop, audio,
                that = this;

            if (!this._dropSelectorDown) {
                return;
            }
            this._dropSelectorDown = false;
            this._dropSelectorDownPosition = 0;

            drop = this.$trackInfoDropStart.val();

            if (this.$dropBtn.hasClass('stop') && drop >= 0) {
                if (this.soundPlayer) {
                     this.soundPlayer.setPosition(drop * 1000);
                }

                if (this.dropTimer) {
                    clearTimeout(this.dropTimer);
                }

                this.dropTimer = setTimeout(function() {
                    if (that.$dropBtn.hasClass('stop')) {
                        that.$dropBtn.click();
                    }
                    that.dropTimer = null;
                }, 20000);
            }
        },

        onDropSelectorMove: function(e) {
            var x, offsetLeft, left, duration, dropWidth,
                canvasWidth = this.$waveformFrame.width(),
                dropSec = 20;

            if (!this._dropSelectorDown) {
                return;
            }

            duration = this.$trackInfoDuration.val();
            if (!duration) {
                return;
            }

            dropWidth = dropSec * (canvasWidth / duration);

            offsetLeft = this.$waveformFrame.offset().left;
            x = e.pageX - offsetLeft
            left = x - this._dropSelectorDownPosition;

            if (left < 0) {
                left = 0;
            }
            if (left > canvasWidth) {
                left = canvasWidth;
            }
            if (left > canvasWidth - dropWidth) {
                left = canvasWidth - dropWidth;
            }

            this.$dropSelector.css({
                left:left + 'px',
                width: dropWidth + 'px'
            });
            this.updateDropArea();
        },

        renderInitialDropSelector: function() {
            var duration, dropWidth,
                canvasWidth = this.$waveformFrame.width(),
                dropSec = 20,
                left,
                leftStr;


            duration = this.$trackInfoDuration.val();
            if (!duration) {
                this.$dropSelector.hide();
                return;
            }

            dropWidth = dropSec * (canvasWidth / duration);

            leftStr = this.$dropSelector.css('left');
            if (leftStr.indexOf('%') > -1) {
                left = parseInt(leftStr) * canvasWidth / 100;
            } else {
                left = parseInt(leftStr);
            }

            if (left < 0) {
                left = 0;
            }
            if (left > canvasWidth - dropWidth) {
                left = canvasWidth - dropWidth;
            }

            this.$dropSelector.css({
                left:left + 'px',
                width: dropWidth + 'px'
            });
            this.updateDropArea();
        },

        updateDropArea: function() {
            var that = this,
                left, w, tmp,
                width = this.$waveformFrame.width(),
                colorPlaying = '#fff',
                //colorDrop = '#cd85ff',
                colorDrop = '#f03eb1',
                $canvas,
                canvasWidth = width,
                canvasHeight = 80,
                blockCount,
                marginWidth = 1,
                barWidth = 2,
                duration,
                dropStart,
                context,
                fromIdx,
                toIdx,
                i,
                fromX,
                toX,
                val, x, barHeight, y, delta;

            blockCount = Math.floor(canvasWidth / (marginWidth + barWidth));

            duration = this.$trackInfoDuration.val();
            if (!duration) {
                this.$dropSelector.hide();
                return;
            }
            this.$dropSelector.show();

            left = this.$dropSelector.css('left');
            if (left.endsWith('%')) {
                left = parseInt(left);
            } else {
                left = (parseInt(left) * 100) / width;
            }

            w = this.$dropSelector.css('width');
            if (w.endsWith('%')) {
                w = parseInt(w);
            } else {
                w = (parseInt(w) * 100) / width;
            }

            dropStart = duration * left / 100;
            this.setDropInput(dropStart, 20);

            if (!this.waveformData) {
                return;
            }
            if (!this.canvas) {
                $canvas = module.$('<canvas class=\'waveform-drop-canvas\' width=\'' + canvasWidth + '\' height=\'' + canvasHeight + '\'/>');
                this.$waveformCanvasFrame.append($canvas);
                this.canvas = this.$waveformCanvasFrame.find('.waveform-drop-canvas')[0];
            }
            context = this.canvas.getContext('2d');
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            context.save();

            fromX = Math.round(canvasWidth * left / 100);
            toX = Math.round(canvasWidth * (left + w) / 100);
            fromIdx = Math.floor(fromX / (barWidth + marginWidth));
            toIdx = Math.ceil(toX / (barWidth + marginWidth));

            for(i = fromIdx; i < blockCount && i < toIdx; i++) {

                val = that.waveformData[i];
                x = i * (barWidth + marginWidth);
                barHeight = val * canvasHeight * 0.009;
                y = canvasHeight - barHeight;
                context.fillStyle = colorDrop;
                w = barWidth;
                if (i === fromIdx) {
                    delta = x;
                    x = Math.max(x, fromX);
                    delta = x - delta;
                    w -= delta;
                }
                if (i === toIdx - 1) {
                    delta = Math.min(x + w, toX) - x;
                    w = delta;
                }
                context.fillRect(x, y, w, barHeight);
            }
            context.restore();
        },

        onSoundUploadCancelBtnClicked: function(e) {
            if (this.preprocessReq) {
                this.preprocessReq.abort();
            }

            if (this.uploadReq) {
                this.uploadReq.abort();
            }

            if (this.dropUploadReq) {
                this.dropUploadReq.abort();
            }

            this.setSoundUrlInput(null);
            this.setSoundKeyInput(null);
            this.$soundProgressFrame.hide();
            this.$soundUploadCancelBtn.hide();
            this.$trackInfoUploaderSubmitBtn.removeClass('progress');
            this.$trackInfoUploaderSubmitBtn.removeAttr('disabled');

            module.removeBeforeunloadCallback('upload');
        },

        onThumbnailUploadBtnClicked: function(e) {
            var that = this,
                $btn,
                browser = module.context.browserSupport.currentBrowser,
                isIE9 = browser[0] === 'MSIE' && browser[1] < 10;

            if (isIE9) {
                module.s.notify(module.string('image_uploading_not_supported'), 'error');
                return;
            }

            module.view.ImageUploadCropView.show(
                {aspectRatio:1, imageType:'c', width:600, height:600},
                function(url) {
                    if (!url) {
                        return;
                    }
                    that.setThumbUrlInput(url);
                });
        },

        onTrackInfoSaveBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                formArray = this.$trackInfoForm.serializeArray(),
                data = {},
                that = this;


            if ($btn.attr('disabled')) {
                return;
            }

            module.$.each(formArray, function(idx, s) {
                var val = s.value;
                if (val === 'false') {
                    val = false;
                } else if (val === 'true') {
                    val = true;
                }
                data[s.name] = val;
            });

            if (!validate(data)) {
                module.s.notify(module.string('failed_to_submit'), 'error');
                return;
            }

            this.startUploadFile(this.streamUrl, function(success) {
                if (!success) {
                    if (arguments[1] === 'canceled') {
                        return;
                    }
                    module.s.notify(arguments[1] || module.string('failed_to_upload'), 'error');
                    that.$soundProgressFrame.hide();
                    that.$soundUploadCancelBtn.hide();
                    return;
                }

                data.track_url = arguments[1];
                data.unique_key = arguments[2];

                if (!data.track_url || !data.unique_key) {
                    module.s.notify(module.string('failed_to_upload'), 'error');
                    that.$soundProgressFrame.hide();
                    that.$soundUploadCancelBtn.hide();
                    return;
                }

                that.setSoundUrlInput(data.track_url);
                that.setSoundKeyInput(data.unique_key);

                if (data.track_type === 'MIX') {
                    data.drop_start = -1;
                    data.drop_duration = -1;
                    data.drop_url = null;
                    submit(data);
                } else {
                    that.$soundProgressFrame.hide();
                    if (data.drop_start < 0) {
                        module.s.notify('Drop should be selected', 'error');
                        that.$soundProgressFrame.hide();
                        that.$soundUploadCancelBtn.hide();
                        return;
                    }
                    that.uploadDrop(that.uploadHost, that.credentialKey,
                            data.unique_key, data.drop_start, function(success) {
                        if (!success) {
                            that.$dropProgressFrame.hide();

                            that.$soundProgressFrame.hide();
                            that.$soundUploadCancelBtn.hide();
                            if (arguments[1] === 'canceled') {
                                return;
                            }
                            module.s.notify(arguments[1] || module.string('failed_to_upload'), 'error');
                            return;
                        }

                        data.drop_url = arguments[1] || null;
                        submit(data);
                    });
                }
            });

            function submit(data) {
                that.postSoundProgress(-1, 'Uploading track info.. (5/5)');
                that.$trackInfoForm.find('input textarea').attr('disabled');
                $btn.addClass('progress');
                if (that.uploadTrackInfoReq) {
                    that.uploadTrackInfoReq.abort();
                }
                that.uploadTrackInfoReq = module.$.ajax({
                    url: module.api.trackUrl,
                    data: JSON.stringify(data),
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function(res) {
                    var resourcePath;
                    if (!res || !res.success || !res.obj) {
                        module.s.notify(res.error || module.string('failed_to_upload'), 'error');
                        return;
                    }

                    resourcePath = '/r/' + res.obj.user_resource_name + '/' + res.obj.resource_path;
                    that.$el.html(that._successTmpl({
                        trackName: that.title,
                        trackUrl: resourcePath
                    }));
                    that.uploading = false;
                    module.removeBeforeunloadCallback('upload');
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus === 'abort') {
                        return;
                    }
                    module.s.notify(module.string('failed_to_upload'), 'error');
                }).always(function() {
                    that.uploadTrackInfoReq = null;

                    that.$dropProgressFrame.hide();
                    that.$soundProgressFrame.hide();
                    that.$soundUploadCancelBtn.hide();

                    that.$trackInfoForm.find('input textarea').removeAttr('disabled');
                    $btn.removeClass('progress');
                });
            }

            function validate(data) {
                var errors = {},
                    $frames = {
                        'name': that.$trackInfoTitle,
                        'duration': that.$trackInfoDropDuration,
                        'genre_id': that.$trackInfoGenreId,
                        'description': that.$trackInfoDesc,
                        'waveform_url': that.$trackInfoWaveformUrl,
                        'coverart_url': that.$trackInfoCoverArtUrl,
                        'track_type': that.$trackInfoTrackType
                    };

                if (!data.name) {
                    errors['name'] = module.string('required_field');
                } else if (data.name.length > 80) {
                    errors['name'] = module.string('input_must_be_less_than_%d', 80);
                }

                if (!data.duration) {
                    errors['duration'] = module.string('required_field');
                }

                if (!data.genre_id) {
                    errors['genre_id'] = module.string('required_field');
                }

                if (!data.description) {
                    errors['description'] = module.string('required_field');
                }

                if (!data.waveform_url) {
                    errors['waveform_url'] = module.string('required_field');
                }

                if (!data.coverart_url) {
                    errors['coverart_url'] = module.string('required_field');
                    module.$('.track-info-uploader .thumbnail .errors').text(
                        module.string('required_field'));
                }

                if (!data.track_type) {
                    errors['track_type'] = module.string('required_field');
                }

                if (['MIX', 'TRACK'].indexOf(data.track_type) === -1) {
                    errors['track_type'] = module.string('track_type_is_not_proper');
                }

                module.$.each($frames, function(key, input) {
                    var $el = module.$(input),
                        $wrapper = $el.closest('.input-wrapper'),
                        $errors;

                    if ($wrapper.size() === 0) {
                        return true;
                    }
                    $errors = $wrapper.find('.errors');
                    if (errors[key]) {
                        $errors.text(errors[key]);
                        $wrapper.addClass('error');
                    } else {
                        $errors.text('');
                        $wrapper.removeClass('error');
                    }
                });
                return Object.keys(errors).length === 0;
            }
        },

        uploadDrop: function(host, key, token, start, callback) {
            var that = this,
                url = module.uploadApi.postProcessUrl(host),
                formData = new FormData();

            formData.append('drop_start', start);
            formData.append('token', token);

            this.$dropProgressFrame.show();
            this.$dropProgressPercent.removeClass('error');

            if (this.dropUploadReq) {
                this.dropUploadReq.abort();
            }

            this.postSoundProgress(-1, 'Generating drop file.. (4/5)');

            this.dropUploadReq = module.$.ajax({
                cache:false,
                url:url,
                type:'post',
                data: formData,
                dataType:'json',
                contentType:false,
                processData: false,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                    xhr.setRequestHeader('dbt_key', key);
                }
            }).done(function(res) {
                if (!res) {
                    callback && callback(false);
                    that.$dropProgressFrame.hide();
                    return;
                }
                callback(true, res.drop_url);
            }).fail(function(jqXHR, textStatus, error) {
                that.$dropProgressFrame.hide();
                callback && callback(false, textStatus === 'abort' ? 'canceled' : null);
            }).always(function() {
                that.dropUploadReq  = null;
            });

        },
        onTrackInfoCancelBtnClicked: function(e) {
            module.router.navigate('/', {trigger: true});
        },

        postSoundProgress: function(percent, message) {
            this.$soundProgressPercent.text(message);
            this.$soundProgress.progressbar({
                max:100,
                value: percent < 0 || percent === 100 ? false : percent
            });
            this.$dropProgressPercent.text(message);
            this.$dropProgress.progressbar({
                max:100,
                value: percent < 0 || percent === 100 ? false : percent
            });
        },

        setSoundUrlInput: function(url) {
            if (!url) {
                this.$trackInfoTrackUrl.removeAttr('value');
                this.$trackInfoTrackUrl.val('');
                this.mayEnableSaveBtn();
            } else {
                this.$trackInfoTrackUrl.attr('value', url);
                this.$trackInfoTrackUrl.val(url);
                this.mayEnableSaveBtn();
            }
        },

        setSoundKeyInput: function(url) {
            if (!url) {
                this.$trackInfoTrackKey.removeAttr('value');
                this.$trackInfoTrackKey.val('');
                this.mayEnableSaveBtn();
            } else {
                this.$trackInfoTrackKey.attr('value', url);
                this.$trackInfoTrackKey.val(url);
                this.mayEnableSaveBtn();
            }
        },

        setSoundDurationInput: function(duration) {
            if (!duration) {
                this.$trackInfoDuration.removeAttr('value');
                this.$trackInfoDuration.val('');
                this.mayEnableSaveBtn();
                this.$dropSelector.hide();
            } else {
                duration = parseInt(duration);
                this.$trackInfoDuration.attr('value', duration);
                this.$trackInfoDuration.val(duration);
                this.mayEnableSaveBtn();
                this.renderInitialDropSelector();
            }
        },

        setThumbUrlInput: function(url) {
            if (!url) {
                this.$trackInfoCoverArtImg.attr('src', '/images/default_cover_big.png');
                this.$trackInfoCoverArtUrl.val('');
                this.$trackInfoCoverArtUrl.removeAttr('value');
                this.mayEnableSaveBtn();
            } else {
                this.$trackInfoCoverArtImg.attr('src', url);
                this.$trackInfoCoverArtUrl.val(url);
                this.$trackInfoCoverArtUrl.attr('value', url);
                this.mayEnableSaveBtn();
            }
        },

        mayEnableSaveBtn: function() {
            var enable = true;
            if (!this.$trackInfoCoverArtUrl.val()) {
                enable = false;
            }
            if (!this.$trackInfoDuration.val()) {
                enable = false;
            }
            if (!this.$trackInfoWaveformUrl.val()) {
                enable = false;
            }
            if (!enable) {
                this.$trackInfoUploaderSubmitBtn.attr('disabled', 'disabled');
            } else {
                this.$trackInfoUploaderSubmitBtn.removeAttr('disabled');
            }
        },

        prepareAudioPlayer: function(url) {
            if (this.soundPlayer) {
                soundManager.destroySound(this.soundPlayer.id);
                this.soundPlayer = null;
            }
            this.soundPlayer = soundManager.createSound({
                id:url,
                url: url,
                volume:100,
                onload: this.onAudioPrepared,
                onerror: this.onAudioError,
                autoLoad: true
            });
        },

        onAudioPrepared: function(e) {
            var duration = Math.round(this.soundPlayer.duration / 1000);
            this.setSoundDurationInput(duration);
            this.$dropBtn.show();
        },

        onAudioError: function(e) {
            this.$soundUploadCancelBtn.click();
            this.$dropBtn.hide();
            module.s.notify(module.string('failed_to_load_audio_data'), 'error');
        },

        resetAudioPlayer: function() {
            if (this.soundPlayer) {
                soundManager.destroySound(this.soundPlayer.id);
                this.soundPlayer = null;
            }
        },

        resetWaveformRenderer: function() {
            if (this.waveformUploadReq) {
                this.waveformUploadReq.abort();
                this.waveformUploadReq = null;
            }
            this.$waveformProgressPercent.removeClass('error');
            this.$waveformCanvasFrame.html('');
            this.canvas = null;
            this.$dropInfoUploaderFrame.hide();
            this.$dropInfoUploaderFrame.removeClass('prepared');
            this.$waveformProgress.hide();
            this.setWaveformDataInput([]);
        },

        setDropInput: function(start, duration) {
            if (start >= 0) {
                start = parseInt(start);
                duration = parseInt(duration);
                this.$trackInfoDropStart.attr('value', start);
                this.$trackInfoDropDuration.attr('value', duration);
            } else {
                this.$trackInfoDropStart.removeAttr('value');
                this.$trackInfoDropDuration.removeAttr('value');
            }
        },

        renderWaveform: function(url) {
            var that = this,
                context,
                canvasWidth,
                canvasHeight,
                canvas,
                $canvas,
                reader;

            this.$dropInfoUploaderFrame.show();
            this.$waveformProgress.show();
            this.$waveformProgressPercent.text('Preparing waveform renderer..');
            this.$waveformProgressPercent.removeClass('error');
            this.$waveformProgress.progressbar({
                max:100,
                value: false
            });


            // Canvas
            canvasWidth = this.$waveformFrame.width();
            canvasHeight = 80;

            $canvas = module.$('<canvas class=\'waveform-bg-canvas\' width=\'' + canvasWidth + '\' height=\'' + canvasHeight + '\'/>');
            this.$waveformCanvasFrame.html($canvas);
            canvas = this.$waveformCanvasFrame.find('.waveform-bg-canvas')[0];

            context = canvas.getContext('2d');
            canvas = canvas;

// because we will create sprite waveform, we multiply height 3
            canvas.width = canvasWidth;
            canvas.height = canvasHeight;

            this.fetchWaveform(url, function(success, data) {
                if (!success) {
                    that.$waveformProgressPercent
                        .text(module.string('failed_to_read_sound_file'))
                        .addClass('error').text(module.string('failed_to_read'));

                    that.$waveformProgress.progressbar({
                        max:100,
                        value: 0
                    });
                    return;
                }
                displayBuffer(data);
            });

            function displayBuffer(buff) {
                var barCount = 1000,
                    bars = [],
                    blocks = [],
                    blockCount,
                    i,
                    marginWidth = 1,
                    barWidth = 2,
                    colorNormal = '#636365',
                    colorPlaying = '#fff',
                    //colorDrop = '#cd85ff',
                    colorDrop = '#f03eb1',
                    dataURL,
                    blob;

                blockCount = Math.floor(canvasWidth / (marginWidth + barWidth));

                if (blockCount === 0) {
                    return;
                }

                bars = module.utils.downsizeArray(buff, barCount);
                blocks = module.utils.downsizeArray(bars, blockCount);

                that.waveformData = blocks;

                context.save();
                module.$.each(blocks, function(idx, val) {
                    var x, barHeight, y;
                    x = idx * (barWidth + marginWidth);
                    barHeight = val * canvasHeight * 0.009;
                    y = canvasHeight - barHeight;
                    context.fillStyle = colorNormal;
                    context.fillRect(x, y, barWidth, barHeight);
                });
                context.save();

                module.utils.getUploadCredential()
                    .done(function(key, host) {
                        var file,
                            str = '[' + bars.join(',') + ']';

                        file = new Blob([str], {type: 'text/plain'});
                        that.waveformUploadReq = module.utils.uploadImage(
                            host, key, file, 'w', function() {},
                            function(success, url) {
                                that.$waveformProgress.hide();
                                that.$dropInfoUploaderFrame.addClass('prepared');
                                that.setWaveformDataInput(url);
                            });
                        that.waveformUploadReq.always(function() {
                            that.waveformUploadReq = null;
                        });
                    }).fail(function(msg) {
                        module.s.notify(module.string('failed_to_authenticate'), 'error');
                    });
            }
        },

        setWaveformDataInput: function(data) {
            if (!data) {
                this.$trackInfoWaveformUrl.val('');
                this.$trackInfoWaveformUrl.removeAttr('value');
                this.mayEnableSaveBtn();
                this.$dropSelector.hide();;
            } else {
                this.$trackInfoWaveformUrl.val(data);
                this.$trackInfoWaveformUrl.attr('value', data);
                this.mayEnableSaveBtn();
                this.updateDropArea();
            }
        },

        onSelectGenre: function() {
            var that = this;
            if (this.$genreDropdown.is(':hidden')) {
                this.$filter.show().one('click', function() {
                    that.$genreDropdown.hide();
                });
                this.$genreDropdown.show();
            }
        },

        onGenreClicked: function(e) {
            var $target = module.$(e.currentTarget),
                today, url,
                mm, dd, timestamp, selectedGenre;

            if ($target.hasClass('selected')) {
                return;
            }

            module.$('.genre', this.$genreDropdown).removeClass('selected');
            $target.addClass('selected');

            selectedGenre = $target.data('key');

            module.$('.selected-genre .text', this.$genreSelect).text($target.text());
            if (this.$filter.is(':visible')) {
                this.$filter.click();
            }
            this.$trackInfoGenreId.val(selectedGenre);
            this.$trackInfoGenreId.attr('value', selectedGenre);
        }
    });

    StatisticsSubView = DropbeatView.extend({
        initialize: function(options) {
            this.key = options.key;
            this.parentView = options.parentView;
            this.dateFrom = options.dateFrom;
            this.dateTo = options.dateTo;
            this.success = options.onSuccess;
            this.failure = options.onFailure;
            this.$el = options.$el;

            DropbeatView.prototype.initialize.apply(this, arguments);
        },

        loadData: function() {
            throw 'Not implemented';
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .btn-back': 'onBackBtnClicked'
            });
        },

        remove: function() {
            this.undelegateEvents();
            this.$el.empty();
            this.stopListening();
            return this;
        },

        onBackBtnClicked: function() {
            if (this.key === 'sub-location') {
                this.parentView.loadPage('location');
            } else if (this.key === 'single-track') {
                this.parentView.loadPage('track');
            }
        }
    });

    ProfilePageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-profile',
        initialize: function(options) {
            var that = this,
                browser = module.context.browserSupport.currentBrowser,
                isIE9 = browser[0] === 'MSIE' && browser[1] < 10;

            this.tabSubViews = {};
            this.musicStorage = {};
            this.genreFavoriteIds = [];
            this.credentialCallbacks = [];

            DropbeatPageView.prototype.initialize.call(this, options);
            module.$.extend(this._context, {
                account: module.context.account
            });

            module.$('header .header-menu .menu').removeClass('selected');
            this._contentTmpl = module._.template(module.$('#tmpl-profile-contents').html());
            this._trackTmpl = module._.template(module.$('#tmpl-useruploaded-addable-music').html());
            this.render();

            this.$coverview = module.$('.coverview');
            this.$coverFilter = module.$('.cover-filter', this.$coverview);
            this.$coverPlaceholder = module.$('.cover-placeholder', this.$coverview);
            this.$coverUploadBtn = module.$('.btn-upload-cover', this.$coverview);

            this.$spinner = module.$('.page >.spinner', this.$el);
            this.$failure = module.$('.page >.failure', this.$el);
            this.$profileContents = module.$('.profile-contents', this.$el);

            if (isIE9) {
                this.showNotification(module.string('image_upload_not_supported_notice'), 'error');
            }
            setTimeout(function() {
                that.loadUserPage(module.context.account.resourceName);
            }, 0);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .btn-retry': 'onRetryClicked',
                'click .btn-edit': 'onEditBtnClicked',
                'click .section-tabs .section-tab': 'onTabClicked',

                'click .btn-upload-thumbnail': 'onThumbnailUploadBtnClicked',
                'click .btn-upload-cover': 'onCoverUploadBtnClicked'
            });
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page-wrapper', this._$pageTarget);
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
            if (this.req) {
                this.req.abort();
                this.req = null;
            }

            if (this.profileReq) {
                this.profileReq.abort();
                this.profileReq = null;
            }

            if (this.coverReq) {
                this.coverReq.abort();
                this.coverReq = null;
            }

            this.musicStorage = {};
        },

        onCoverUploadBtnClicked: function(e) {
            var that = this,
                $btn,
                browser = module.context.browserSupport.currentBrowser,
                isIE9 = browser[0] === 'MSIE' && browser[1] < 10;

            if (isIE9) {
                module.s.notify(module.string('image_uploading_not_supported'), 'error');
                return;
            }

            module.view.ImageUploadCropView.show(
                {aspectRatio:4, imageType:'pc', width:1200, height:300},
                function(url) {
                    if (!url) {
                        return;
                    }
                    that.updateCoverUrl(url);
                });
        },

        updateCoverUrl: function(url) {
            var that = this;

            if (this.coverReq) {
                this.coverReq.abort();
            }
            this.coverReq = module.$.ajax({
                url: module.api.changeCoverImageUrl,
                data: JSON.stringify({
                    url: url
                }),
                contentType: 'application/json',
                dataType: 'json',
                type:'POST'
            }).done(function(res) {
                that.$coverPlaceholder.css('backgroundImage', 'url(\'' + url + '\')');
                that.$coverFilter.hide();
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus === 'abort') {
                    return;
                }
                module.s.notify(module.string('failed_to_upload'), 'error');
            }).always(function() {
                that.coverReq = null;
            });
        },

        onThumbnailUploadBtnClicked: function(e) {
            var that = this,
                $btn,
                browser = module.context.browserSupport.currentBrowser,
                isIE9 = browser[0] === 'MSIE' && browser[1] < 10;

            if (isIE9) {
                module.s.notify(module.string('image_uploading_not_supported'), 'error');
                return;
            }

            module.view.ImageUploadCropView.show(
                {aspectRatio:1, imageType:'p', width:600, height:600},
                function(url) {
                    if (!url) {
                        return;
                    }
                    that.updateProfileUrl(url);
                });
        },

        updateProfileUrl: function(url) {
            var that = this;

            if (this.profileReq) {
                this.profileReq.abort();
            }
            this.profileReq = module.$.ajax({
                url: module.api.changeProfileImageUrl,
                data: JSON.stringify({
                    url: url
                }),
                contentType: 'application/json',
                dataType: 'json',
                type:'POST'
            }).done(function(res) {
                that.$thumbnail.find('img').attr('src', url);
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus === 'abort') {
                    return;
                }
                module.s.notify(module.string('failed_to_upload'), 'error');
            }).always(function() {
                that.profileReq = null;
            });
        },

        onTabClicked: function(e) {
            var $taretTab = module.$(e.currentTarget),
                $tab = module.$('.section-tabs .section-tab', this.$el),
                view,
                resourceView,
                target = $taretTab.data('target'),
                targetContents = '.section-content.' + target;

            target = target.dashToCamel();
            resourceView = UserResourceSubViews;

            view = resourceView[target];
            if (!view) {
                return;
            }
            if (this.currentSubView && this.currentSubView.key === target) {
                return;
            }

            if (this.currentSubView && this.currentSubView.scrollUnbind) {
                this.currentSubView.scrollUnbind();
            }

            $tab.removeClass('selected');
            module.$('.section-tabs .section-tab.' + target).addClass('selected');
            module.$('.section-contents .section-content').removeClass('selected');
            module.$(targetContents).addClass('selected');

            if (!this.tabSubViews[target]) {
                this.tabSubViews[target] = new view({
                    el: targetContents,
                    key: target,
                    user: this.user,
                    parentMusicStorage : this.musicStorage,
                    parentView: this
                });
            }
            this.currentSubView = this.tabSubViews[target];
            if (this.currentSubView && this.currentSubView.scrollBind) {
                this.currentSubView.scrollBind();
            }
        },

        onRetryClicked: function(e) {
            this.loadUserPage(module.context.account.resourceName);
        },

        onEditBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget);
            if ($btn.hasClass('btn-nickname-edit')) {
                module.view.NicknameUpdateView.show({
                    nickname: $btn.data('default')
                });
            } else if ($btn.hasClass('btn-user-url-edit')) {
                module.view.UserUrlUpdateView.show({
                    resourceName: $btn.data('default')
                });
            } else if ($btn.hasClass('btn-desc-edit')) {
                module.view.UserDescUpdateView.show({
                    desc: $btn.data('default')
                });
            } else if ($btn.hasClass('btn-genre-edit')) {
                module.view.GenreSelectView.show({
                    favoriteGenreIds:this.favoriteGenreIds,
                    genres: this.userGenres
                });
            }
        },

        loadGenreInfo: function() {
            var that = this;

            if (this.genreReq) {
                this.genreReq.abort();
            }
            this.genreReq = module.$.ajax({
                url:module.api.genreFavoritesUrl,
                dataType:'json',
                cache:false
            }).done(function(res) {
                var genres,
                    html = '';

                if (!res || !res.success || !res.data) {
                    module.s.notify(module.string('failed_to_laod_genre_taste'), 'error');
                    that.$genresFrame.hide();
                    return;
                }

                that.favoriteGenreIds = res.data;

                genres = {};
                that.userGenres = module.context.genre.dropbeat;
                module.$.each(module.context.genre.dropbeat, function(idx, genre) {
                    genres[genre.id] = genre.name;
                });

                if (res.data.length === 0) {
                    html = '<span class=\'empty-message\'>' +
                         module.string('has_no_favorite_genres') +'</span>';
                } else {
                    module.$.each(res.data, function(idx, genreId) {
                        var genre = genres[genreId];
                        if (genre) {
                            html += '<span class=\'genre\'>#' + genre + '</span>';
                        }
                    });
                }

                that.$genres.html(html);
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus !== 'abort') {
                    module.s.notify(module.string('failed_to_load_genre_taste'), 'error');
                }
                that.$genresFrame.hide();
            }).always(function() {
                that.genreReq = null;
            });
        },

        loadUserPage: function(resourceName) {
            var that = this,
                resourceUrl = 'http://dropbeat.net/r/' + resourceName + '/',
                resolveResponse,
                resolveDeferred = module.$.Deferred();

            this.$failure.hide();
            this.$spinner.show();
            this.$coverview.hide();

            if (this.req) {
                this.req.abort();
            }

            resolveResponse = module.context.resolveResponse &&
                module.context.resolveResponse[resourceUrl];
            if (resolveResponse) {
// We should delete resolve cache for ajax load
// when user returned to this page
                delete module.context.resolveResponse[resourceUrl];
                resolveDeferred.resolve({
                    success:true,
                    data:resolveResponse
                });
            } else {
                this.req = module.$.ajax({
                    url:module.api.resolveObjectUrl,
                    data: {
                        url:resourceUrl
                    },
                    dataType:'json',
                    cache:false
                });
                resolveDeferred = this.req;
            }

            resolveDeferred.done(function(res) {
                var html, userData = {},
                    coverImageUrl,
                    $wrapper;

                if (!res || !res.success || !res.data) {
                    that.$failure.show();
                    return;
                }

                that.genres = {};
                module.$.each(module.context.genre['dropbeat'], function(idx, genre) {
                    that.genres[genre.id] = genre.name;
                });

                that.user = res.data.user;
                that.user.type = 'user';
                userData['user'] = that.user;
                userData['tracks'] = []

                if (res.data.tracks) {
                    module.$.each(res.data.tracks, function(idx, t) {
                        var track = new module.music.DropbeatMusic(t);
                        track.desc = null;
                        userData['tracks'].push(track);
                        that.musicStorage[track.getStorageKey()] = track;
                    });
                }

                html = that._contentTmpl({
                    userData: userData,
                    trackTmpl: that._trackTmpl,
                    genres: that.genres
                });
                that.$profileContents.html(html);

                that.$profileContents.find('.section-tabs .section-tab').first().click();

                that.$thumbnail = module.$('.personal-info .thumbnail', that.$el);

                that.$genresFrame = module.$('.genres-wrapper', that.$el);
                that.$genres = module.$('.genres', that.$el);

// set page cover view
                coverImageUrl = userData.user.profile_cover_image || '/images/default_user_cover.png';
                if (!userData.user.profile_cover_image) {
                    that.$coverFilter.show();
                }
                that.$coverPlaceholder.css({
                    backgroundImage: 'url(\'' + coverImageUrl + '\')'
                });

                $wrapper = module.$('.following-box-wrapper', that.$el);
                if ($wrapper.size() > 0) {
                    that.subViews['followingSectionView'] = new FollowingSectionView({
                        $wrapper: $wrapper,
                        user: that.user
                    });
                }

                $wrapper = module.$('.followers-box-wrapper', that.$el);
                if ($wrapper.size() > 0) {
                    that.subViews['followersSectionView'] = new FollowersSectionView({
                        $wrapper: $wrapper,
                        user: that.user
                    });
                }


                that.loadGenreInfo();
                that.$coverview.show();
            }).fail(function(jqXHR, textStatus, error) {
                if (!textStatus || textStatus !== 'abort') {
                    that.$failure.show();
                }
            }).always(function() {
                that.req = null;
                that.$spinner.hide();
            });
        }
    });

    LoadResourceView = DropbeatView.extend({
        initialize: function(options) {
            var sectionName;

            module._.bindAll(this, 'onScroll');

            DropbeatView.prototype.initialize.apply(this, arguments);

            this.$el = module.$(options.el);
            this.$el.show();
            this.key = options.key;
            this.user = options.user;
            this.parentView = options.parentView;
            this.parentMusicStorage = options.parentMusicStorage;
            this.$spinner = module.$('.spinner', this.$el);
            this.$failure = module.$('.failure', this.$el);
            this.$tracks = module.$('.tracks', this.$el);
            this.$emptyTracks = module.$('.empty-tracks', this.$el);
            this.playableBound = false;

            this.nextPage = 0;

            if (!module.context.isMobile) {
                WaveformViewMixin.bind(this, this.$tracks);
            }

            sectionName = this.getSectionName();

            PlayableViewMixin.bind(this, this.$tracks, [sectionName]);
            this.playableBound = true;

            module.playlistManager.publicPlaylists[this.getPlaylistId()] =
                new module.playlist.Playlist({
                    id: this.getPlaylistId(),
                    name: this.getPlaylistName(),
                    tracks: []
                });
            this.$tracks.attr('data-playlist-id', this.getPlaylistId());
            this.$tracks.data('playlistId', this.getPlaylistId());
            this.$tracks.addClass('a-playable-list');
            this.playlist = module.playlistManager.publicPlaylists[this.getPlaylistId()];
            this.loadResources();
        },

        events: function() {
            var events = DropbeatView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .failure .retry-btn': 'loadResources'
            });
        },

        remove: function() {
            DropbeatView.prototype.remove.apply(this, arguments);
            if (this.playableBound) {
                PlayableViewMixin.unbind(this);
            }
            if (!module.context.isMobile) {
                WaveformViewMixin.unbind(this);
            }

            this.scrollUnbind();
            if (this.req) {
                this.req.abort();
                this.req = null;
            }
        },

        scrollBind: function() {
            module.$('.contents .scroll-y').on('scroll', this.onScroll);
            module.$(window).on('resize', this.onScroll);
        },

        scrollUnbind: function() {
            module.$('.contents .scroll-y').off('scroll', this.onScroll);
            module.$(window).off('resize', this.onScroll);
        },

        onScroll: function() {
            var $scroll = module.$('.contents .scroll-y'),
                scrollTop = $scroll.scrollTop(),
                $lastItem = this.$tracks.find('.a-addable-music-wrapper').last();
            if (this.nextPage &&
                    this.nextPage > 0 &&
                    $lastItem.size() > 0 &&
                    $lastItem.offset().top  < $scroll.height()) {
                this.loadResources();
            }
        },

        getSectionName : function() {
            throw 'Not implemented';
        },

        getPlaylistName : function() {
            throw 'Not implemented';
        },

        getPlaylistId : function() {
            throw 'Not implemented';
        },

        loadResources : function() {
            throw 'Not implemented';
        },

        updatePlaylist: function() {
            var p = this.playlist,
                idx, q, i,
                that = this;

            if (!this.playlist) {
                return;
            }
            this.$tracks.find('.a-addable-music-wrapper > .a-addable-music')
                .each(function(idx, el) {
                    var $el = module.$(el),
                        music = extractMusicFromDOM(that, $el);

                    if (music) {
                        p.raw().push(music);
                    }
                });

            if (module.playlistManager.playingPlaylistId === p.id) {
                if (module.s.shuffleControl.isShuffle()) {
                    q = p.raw().slice(0);
                    module.s.shuffleControl.shuffle(q);
                    idx = -1;
                    if (module.playerManager.currentMusic) {
                        for (i = 0; i < q.length; i += 1) {
                            if (q[i].id ===
                                    module.playerManager.getCurrentMusic().id) {
                                idx = i;
                            }
                        }
                    }

                    module.s.musicQ.init(q, idx);
                } else {
                    if (module.playerManager.currentMusic) {
                        idx = p.findIdx(module.playerManager.currentMusic.id);
                    }
                    module.s.musicQ.init(p.playlist, idx);
                }
                module.s.playerBase.updateButton();
            }
        }
    });

    ArtistResourceSubViews = (function() {
        var views = {};

        // dummy view
        views.official = LoadResourceView.extend({
            initialize: function(options) {
                LoadResourceView.prototype.initialize.apply(this, arguments);
                this.updatePlaylist();
                this.$emptyTracks.show();
            },

            getSectionName : function() {
                return 'artist_official';
            },

            getPlaylistName : function() {
                return this.user.nickname + ' offical';
            },

            getPlaylistId : function() {
                return 'official_' + this.user.id;
            },

            loadResources : function() {
                this.updateWaveform && this.updateWaveform();
                this.updatePlayState();
            }
        });

        views.podcast = LoadResourceView.extend({
            initialize: function(options) {
                this._trackTmpl = module._.template(module.$('#tmpl-external-addable-music').html());
                LoadResourceView.prototype.initialize.apply(this, arguments);
            },

            getSectionName : function() {
                return 'artist_podcast';
            },

            getPlaylistName : function() {
                return this.user.nickname + ' podcast';
            },

            getPlaylistId : function() {
                return 'podcast_' + this.user.id;
            },

            loadResources : function() {
                var that = this,
                    reqParams;

                if (!this.user) {
                    return;
                }

                if (this.nextPage < 0) {
                    return;
                }

                reqParams = {
                    p: this.nextPage,
                    q: this.user.nickname
                };

                if (this.req) {
                    return;
                }

                this.$spinner.show();

                this.req = module.$.ajax({
                    url:module.coreApi.podcastUrl,
                    data: reqParams,
                    crossDomain: true,
                    dataType: 'json'
                }).done(function(res) {
                    if (!res || !res.success || !res.data) {
                        return;
                    }
                    if (res.data.length === 0) {
                        that.nextPage = -1;
                    } else {
                        that.nextPage += 1;
                    }
                    module.$.each(res.data, function(idx, t) {
                        var track = new module.music.PodcastMusic(t);
                        track.user = module.$.extend({}, that.user, track.user);
                        that.$tracks.append(that._trackTmpl({
                            trackInfo: track,
                            genres:that.parentView.genres
                        }));
                        that.setMusicToStorage(track.getStorageKey(), track);
                    });
                    that.updateWaveform && that.updateWaveform();
                    that.updatePlayState();
                    that.updatePlaylist();

                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus !== 'abort') {
                        that.$failure.show();
                    }
                }).always(function() {
                    that.$spinner.hide();
                    that.req = null;
                });
            }
        });

        views.liveset = LoadResourceView.extend({
            initialize: function(options) {
                this._trackTmpl = module._.template(module.$('#tmpl-external-addable-music').html());
                LoadResourceView.prototype.initialize.apply(this, arguments);
            },

            getSectionName : function() {
                return 'artist_liveset';
            },

            getPlaylistName : function() {
                return this.user.nickname + ' liveset';
            },

            getPlaylistId : function() {
                return 'liveset_' + this.user.id;
            },

            loadResources : function() {
                var that = this,
                    reqParams;

                if (!this.user) {
                    return;
                }

                reqParams = {
                    q: this.user.nickname
                };

                if (this.req) {
                    return;
                }

                this.$spinner.show();

                this.req = module.$.ajax({
                    url:module.coreApi.searchLivesetUrl,
                    data: reqParams,
                    crossDomain: true,
                    dataType: 'json'
                }).done(function(res) {
                    if (!res || !res.success || !res.data) {
                        return;
                    }
                    that.nextPage = -1;
                    module.$.each(res.data, function(idx, t) {
                        var track = new module.music.Music(t);
                        track.user = module.$.extend({}, that.user, track.user);
                        that.$tracks.append(that._trackTmpl({
                            trackInfo: track,
                            genres:that.parentView.genres
                        }));
                        that.setMusicToStorage(track.getStorageKey(), track);
                    });

                    that.updateWaveform && that.updateWaveform();
                    that.updatePlayState();
                    that.updatePlaylist();
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus !== 'abort') {
                        that.$failure.show();
                    }
                }).always(function() {
                    that.$spinner.hide();
                    that.req = null;
                });
            }
        });

        views.events = LoadResourceView.extend({
            initialize: function(options) {
                this._eventInfoTmpl = module._.template(module.$('#tmpl-event-info').html());
                LoadResourceView.prototype.initialize.apply(this, arguments);
                this.$events = module.$('.events', this.$el);
            },

            getSectionName: function() {
                return 'events';
            },

            getPlaylistName: function() {
                return 'Events';
            },

            getPlaylistId: function() {
                return 'events';
            },

            loadResources : function() {
                var that = this,
                    reqParams;

                if (!this.user) {
                    return;
                }

                reqParams = {
                    q: this.user.nickname
                };

                if (this.req) {
                    return;
                }

                this.$spinner.show();

                this.req = module.$.ajax({
                    url:module.coreApi.eventInfoUrl,
                    data: reqParams,
                    crossDomain: true,
                    dataType: 'json'
                }).done(function(res) {
                    if (!res || !res.success || !res.data || res.data.length === 0) {
                        that.$failure.show();
                        return;
                    }
                    module.$.each(res.data, function(idx, eventInfo) {
                        var name = eventInfo.info.toLowerCase(),
                            keyword,
                            _idx;
                        _idx = name.indexOf(that.user.nickname.toLowerCase());
                        if (_idx > -1) {
                            keyword = eventInfo.info.substring(_idx, that.user.nickname.length);
                            name = eventInfo.info.replace(keyword, '');
                        } else {
                            name = eventInfo.info;
                        }
                        that.$events.append(that._eventInfoTmpl({
                            eventInfo: {
                                fromDate: moment(eventInfo.date).format('MMMM Do YYYY, h:mm a'),
                                name: name,
                                url: eventInfo.url,
                                venue: eventInfo.venue,
                                detail: eventInfo.detail
                            }
                        }));
                    });
                    that.nextPage = -1;
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus !== 'abort') {
                        that.$failure.show();
                    }
                }).always(function() {
                    that.$spinner.hide();
                    that.req = null;
                });
            }
        });
        return views;
    }());

    UserResourceSubViews = (function() {
        var views = {};

        // dummy view
        views.uploads = LoadResourceView.extend({
            initialize: function(options) {
                LoadResourceView.prototype.initialize.apply(this, arguments);
                this.updatePlaylist();
                this.$emptyTracks.show();
            },

            getSectionName : function() {
                return 'user_uploads';
            },

            getPlaylistName : function() {
                return this.user.nickname + ' uploads';
            },

            getPlaylistId : function() {
                return 'uploads_' + this.user.id;
            },

            loadResources : function() {
                this.updateWaveform && this.updateWaveform();
                this.updatePlayState();
            }
        });

        views.reposts = LoadResourceView.extend({
            initialize: function(options) {
                LoadResourceView.prototype.initialize.apply(this, arguments);
                this._externalTrackTmpl = module._.template(module.$('#tmpl-external-addable-music').html());
                this._internalTrackTmpl = module._.template(module.$('#tmpl-useruploaded-addable-music').html());
            },

            getSectionName : function() {
                return 'user_reposts';
            },

            getPlaylistName : function() {
                return this.user.nickname + ' reposts';
            },

            getPlaylistId : function() {
                return 'reposts_' + this.user.id;
            },

            loadResources : function() {
                var that = this,
                    reqParams;

                if (this.nextPage < 0) {
                    return;
                }

                if (this.req) {
                    return;
                }

                this.$spinner.show();

                this.req = module.$.ajax({
                    url:module.api.repostUrl,
                    data: {
                        user_id:that.user.id
                    },
                    dataType: 'json'
                }).done(function(res) {
                    if (!res || !res.success || !res.data) {
                        return;
                    }
                    that.nextPage = -1;

                    if (res.data.length === 0) {
                        that.$emptyTracks.show();
                        that.$tracks.hide();
                    } else {
                        that.$emptyTracks.hide();
                        that.$tracks.show();
                        module.$.each(res.data, function(idx, t) {
                            var track, tmpl, editable = false;
                            if (!t.type || t.type === 'dropbeat') {
                                track = new module.music.DropbeatMusic(t);
                                track.desc = null;
                                tmpl = that._internalTrackTmpl;
                                editable = module.context.account &&
                                    module.context.account.resourceName ===
                                        track.user.resource_name;
                            } else {
                                track = new module.music.Music({
                                    id:t.id,
                                    type: t.type,
                                    title: t.title,
                                    user_name: t.author_nickname,
                                    user_resource_name: t.author_resource_name,
                                    user_profile_image: t.author_profile_image
                                });
                                tmpl = that._externalTrackTmpl;
                            }
                            that.setMusicToStorage(track.getStorageKey(), track);
                            that.$tracks.append(tmpl({
                                trackInfo: track,
                                editable: editable,
                                genres:that.parentView.genres
                            }));
                        });
                    }
                    that.updateWaveform && that.updateWaveform();
                    that.updatePlayState();
                    that.updatePlaylist();
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus !== 'abort') {
                        that.$failure.show();
                    }
                }).always(function() {
                    that.$spinner.hide();
                    that.req = null;
                });
            }
        });

        views.likes = LoadResourceView.extend({
            initialize: function(options) {
                LoadResourceView.prototype.initialize.apply(this, arguments);
                this._externalTrackTmpl = module._.template(module.$('#tmpl-external-addable-music').html());
                this._internalTrackTmpl = module._.template(module.$('#tmpl-useruploaded-addable-music').html());
            },

            getSectionName : function() {
                return 'user_likes';
            },

            getPlaylistName : function() {
                return this.user.nickname + ' likes';
            },

            getPlaylistId : function() {
                return 'likes_' + this.user.id;
            },

            loadResources : function() {
                var that = this,
                    reqParams;

                if (this.nextPage < 0) {
                    return;
                }

                if (this.req) {
                    return;
                }

                this.$spinner.show();

                this.req = module.$.ajax({
                    url:module.api.userLikeUrl,
                    data: {
                        user_id:that.user.id
                    },
                    dataType: 'json'
                }).done(function(res) {
                    if (!res || !res.success || !res.data) {
                        return;
                    }
                    that.nextPage = -1;

                    if (res.data.length === 0) {
                        that.$emptyTracks.show();
                        that.$tracks.hide();
                    } else {
                        that.$emptyTracks.hide();
                        that.$tracks.show();
                        module.$.each(res.data, function(idx, t) {
                            var track, tmpl, editable = false;
                            if (t.type === 'user_track') {
                                track = new module.music.DropbeatMusic(t.data);
                                track.desc = null;
                                tmpl = that._internalTrackTmpl;
                                editable = module.context.account &&
                                    module.context.account.resourceName ===
                                        track.user.resource_name;
                            } else {
                                track = new module.music.Music(t.data);
                                tmpl = that._externalTrackTmpl;
                            }
                            that.setMusicToStorage(track.getStorageKey(), track);
                            that.$tracks.append(tmpl({
                                trackInfo: track,
                                editable: editable,
                                genres:that.parentView.genres
                            }));
                        });
                    }
                    that.updateWaveform && that.updateWaveform();
                    that.updatePlayState();
                    that.updatePlaylist();
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus !== 'abort') {
                        that.$failure.show();
                    }
                }).always(function() {
                    that.$spinner.hide();
                    that.req = null;
                });
            }
        });
        return views;
    }());

    ChannelResourceSubViews = (function() {
        var views = {},
            GooglePlaylistView;

        GooglePlaylistView = LoadResourceView.extend({
            initialize: function(options) {
                this._trackTmpl = module._.template(
                    module.$('#tmpl-external-addable-music').html());
                this.googlePlaylistItemUrl =
                    'https://www.googleapis.com/youtube/v3/playlistItems/';

                LoadResourceView.prototype.initialize.apply(this, arguments);
            },

            onScroll: function() {
                var $scroll = module.$('.contents .scroll-y'),
                    scrollTop = $scroll.scrollTop(),
                    $lastItem = this.$tracks.find('.a-addable-music-wrapper').last();
                if (this.nextPage &&
                        this.nextPage !== -1 &&
                        $lastItem.size() > 0 &&
                        $lastItem.offset().top  < $scroll.height()) {
                    this.loadResources();
                }
            },

            getResourceId: function() {
                throw 'Not implemented';
            },

            getResourceName: function() {
                throw 'Not implemented';
            },

            getSectionName : function() {
                return 'channel_detail';
            },

            getPlaylistName : function() {
                return this.user.nickname + ' ' + this.getResourceName();
            },

            getPlaylistId : function() {
                return 'channel_' + this.user.id + '_' + this.getResourceId();
            },

            loadResources : function() {
                var that = this,
                    reqParams, resourceId;

                resourceId = this.getResourceId();
                if (!this.user || !resourceId) {
                    return;
                }

                if (this.req) {
                    if (this.prevResourceId && this.prevResourceId === resourceId) {
                        return;
                    }
                    this.req.abort();
                }
                this.prevResourceId = resourceId;

                this.$spinner.show();
                reqParams = {
                    uid: resourceId,
                };

                if (this.nextPage !== 0) {
                    reqParams.pageToken = this.nextPage;
                }

                this.req = module.$.ajax({
                    url: module.coreApi.channelGproxy,
                    data: reqParams,
                    crossDomain: true,
                    dataType: 'json',
                    maxReqCount: 0
                }).done(function(res) {
                    var tracks = [];

                    if (!res || !res.items) {
                        that.$failure.show();
                        return;
                    }

                    module.$.each(res.items, function(idx, t) {
                        var track;
                        if (t.unique_key) {
                            track = new module.music.DropbeatMusic(t);
                            track.desc = null;
                        } else {
                            track = new module.music.Music({
                                created_at: t.release_date || t.published_at,
                                user_name: t.artist_name || t.channel_title,
                                user_resource_name: t.resource_name,
                                type: t.type,
                                id: t.id,
                                title: t.title
                            });
                        }
                        track.user = module.$.extend({}, that.user, track.user);
                        tracks.push(track);
                    });

                    if (res.next_page_token) {
                        that.nextPage = res.next_page_token;
                    } else {
                        that.nextPage = -1;
                    }

                    module.$.each(tracks, function(idx, t) {
                        that.$tracks.append(that._trackTmpl({
                            trackInfo: t,
                            genres:that.parentView.genres
                        }));
                        that.setMusicToStorage(t.getStorageKey(), t);
                    });

                    that.updateWaveform && that.updateWaveform();
                    that.updatePlayState();
                    that.updatePlaylist();
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus !== 'abort') {
                        that.$failure.show();
                    }
                }).always(function() {
                    that.$spinner.hide();
                    that.req = null;
                });
            }
        });

        // dummy view
        views.recent = GooglePlaylistView.extend({
            initialize: function(options) {
                this.resourceId = options.user.uploads;
                GooglePlaylistView.prototype.initialize.apply(this, arguments);
            },

            getResourceId: function() {
                return this.resourceId;
            },

            getResourceName: function() {
                return 'Recent';
            }
        });


        views.channel_sections = GooglePlaylistView.extend({
            initialize: function(options) {
                this.resourceItems = options.user.playlist;

                GooglePlaylistView.prototype.initialize.apply(this, arguments);
                this.$filter = module.$('.overlay-filter');

                this.$playlistDropdown = module.$('.playlist-select-wrapper .dropdown', this.$el);
                setTimeout(module.$.proxy(function() {
                    this.$playlistDropdown.find('.playlist').removeClass('selected');
                    this.$playlistDropdown.find('.playlist').first().click();
                }, this), 0);
            },

            events: function() {
                var events = GooglePlaylistView.prototype.events;
                if (module._.isFunction(events)) {
                    events = events.call(this);
                }

                return module.$.extend ({}, events, {
                    'click a.plalyist': 'onPlaylistClicked',
                    'click .playlist-select-wrapper .selected-playlist': 'onSelectPlaylistClicked',
                    'click .playlist-select-wrapper .dropdown .playlist': 'onPlaylistClicked'
                });
            },

            getResourceId: function() {
                return this.selectedResourceItem ? this.selectedResourceItem.uid : null;
            },

            getResourceName: function() {
                return this.selectedResourceItem ? this.selectedResourceItem.title : null;
            },

            onSelectPlaylistClicked: function() {
                var that = this;
                if (this.$playlistDropdown.is(':hidden')) {
                    this.$filter.show().one('click', function() {
                        that.$playlistDropdown.hide();
                    });
                    this.$playlistDropdown.show();
                }
            },

            onPlaylistClicked: function(e) {
                var that = this,
                    $target = module.$(e.currentTarget),
                    today, uid, found,
                    mm, dd, timestamp;

                if ($target.hasClass('selected')) {
                    return;
                }

                module.$('.playlist-select-wrapper .playlist', this.$el).removeClass('selected');
                $target.addClass('selected');

                uid = $target.data('uid');

                module.$('.selected-playlist .text').text($target.text());
                if (this.$filter.is(':visible')) {
                    this.$filter.click();
                }

                module.$.each(this.user.playlist, function(idx, playlist) {
                    if (playlist.uid === uid) {
                        found = playlist;
                        return false;
                    }
                });

                if (!found) {
                    return;
                }

                this.selectedResourceItem = found;
                this.nextPage = 0;
                this.$tracks.empty();
                this.clearMusicStorage();
                this.loadResources();
            }
        });
        return views;
    }());

    TrackEditPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-edit-track',
        initialize: function(options) {
            var that = this,
                browser = module.context.browserSupport.currentBrowser,
                isIE9 = browser[0] === 'MSIE' && browser[1] < 10;

            module._.bindAll(this, 'onDropSelectorUp', 'onDropSelectorMove',
                'onAudioPrepared', 'onAudioError');

            module.$('header .header-menu .menu').removeClass('selected');
            module.$('.contents').addClass('desktop-only')
                .addClass('gray-bg');
            DropbeatPageView.prototype.initialize.call(this, options);

            module.$('header .header-menu .menu').removeClass('selected');
            this._resourceName = options.userName;
            this._trackName = options.trackName;
            this._contentsTmpl = module._.template(module.$('#tmpl-edit-track-contents').html());

            this.credentialCallbacks = [];

            this.render();
            this.$spinner = module.$('> .spinner', this.$el);
            this.$failure = module.$('> .failure', this.$el);
            this.$404 = module.$('> .failure-404', this.$el);
            this.$contents = module.$('.resource-contents', this.$el);

            this.loadResource(this._resourceName, this._trackName);

            if (isIE9) {
                this.showNotification(module.string('image_upload_not_supported_notice'), 'error');
            }

            module.playerManager.pause();
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .page-track .btn-retry': 'onRetryBtnClicked',

                'click .btn-upload-thumbnail': 'onThumbnailUploadBtnClicked',

                'click .uploader .btn-save': 'onTrackInfoSaveBtnClicked',
                'click .uploader .btn-cancel': 'onCancelBtnClicked',
                'click .uploader .btn-cancel-drop': 'onCancelDropBtnClicked',

                'click .genre-select .selected-genre': 'onSelectGenre',
                'click .genre-select .genre': 'onGenreClicked',

                'click .downloadable-input-wrapper .choice': 'onToggleDownloadable',

                'click .drop-info-uploader .listen-drop': 'onDropBtnClicked',

                'mousedown .drop-selectors' : 'onDropSelectorDown'
            });
        },

        onRetryBtnClicked: function(e) {
            this.loadResource(this._resourceName, this._trackName);
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
            module.$(window).off('mouseup', this.onDropSelectorUp);
            module.$(window).off('mousemove', this.onDropSelectorMove);

            module.removeBeforeunloadCallback('upload');

            module.$('.contents').removeClass('desktop-only')
                .removeClass('gray-bg');

            if (this.req) {
                this.req.abort();
                this.req = null;
            }

            this.cancelUploader();
        },

        onCancelBtnClicked: function(e) {
            if (confirm(module.string('are_you_sure_discard_changes'))) {
                module.removeBeforeunloadCallback('upload');
                this.cancelUploader();
                window.history.back();
            }
        },

        onCancelDropBtnClicked: function(e) {
            if (this.dropUploadReq) {
                this.dropUploadReq.abort();
            }
        },

        cancelUploader: function() {

            if (this.waveformReq) {
                this.waveformReq.abort();
                this.waveformReq = null;
            }

            if (this.uploadTrackInfoReq) {
                this.uploadTrackInfoReq.abort();
                this.uploadTrackInfoReq = null;
            }

            if (this.DropUploadReq) {
                this.DropUploadReq.abort();
                this.DropUploadReq = null;
            }

            if (this.soundPlayer) {
                soundManager.destroySound(this.soundPlayer.id);
                this.soundPlayer = null;
            }
        },

        loadResource: function(resourceName, trackName) {
            var that = this;

            this.$failure.hide();
            this.$spinner.show();

            if (this.req) {
                this.req.abort();
            }

            this.req = module.$.ajax({
                url:module.api.resolveObjectUrl,
                data: {
                    url: 'http://dropbeat.net/r/' + resourceName + '/' + trackName
                },
                dataType:'json',
                cache:false
            }).done(function(res) {
                var html,
                    track,
                    user,
                    genres;

                if (!res || !res.success || !res.data) {
                    that.$failure.show();
                    return;
                }

                genres = {};
                module.$.each(module.context.genre.dropbeat, function(idx, genre) {
                    genres[genre.id] = genre.name;
                });
                that.genres = genres;

                track = new module.music.DropbeatMusic(res.data);
                that.track = track;

                html = that._contentsTmpl({
                    trackInfo: that.track
                });
                that.$contents.html(html);

                that.initUploader();
                that.updateFormValue();
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus && textStatus !== 'abort' &&
                        jqXHR && jqXHR.status === 400) {
                    that.$404.show();
                    return;
                }
                that.$failure.show();
            }).always(function() {
                that.$spinner.hide();
                that.req = null;
            });
        },

        initUploader : function() {
            var that = this;

            module.addBeforeunloadCallback('upload', function() {
                return module.string('are_you_sure_discard_changes');
            });

            this.$dropProgressFrame = module.$('.drop-progress-wrapper', this.$el);
            this.$dropProgress = module.$('.progress', this.$dropProgressFrame);
            this.$dropProgressPercent = module.$('.progress .progress-label', this.$dropProgressFrame);

            this.$trackInfoUploader = module.$('.track-info-uploader', this.$el);
            this.$trackInfoForm = module.$('#form-upload-track-info', this.$el);

            this.$trackInfoDesc = module.$('.input-description', this.$trackInfoUploader);
            this.$trackInfoGenreId = module.$('.input-genre', this.$trackInfoUploader);
            this.$trackInfoCoverArtImg = module.$('.thumbnail .preview img', this.$trackInfoUploader);
            this.$trackInfoCoverArtUrl = module.$('.input-coverart-url', this.$trackInfoUploader);
            this.$trackInfoDropUrl = module.$('.input-drop-url', this.$trackInfoUploader);
            this.$trackInfoDropStart = module.$('.input-drop-start', this.$trackInfoUploader);
            this.$trackInfoDropDuration = module.$('.input-drop-duration', this.$trackInfoUploader);
            this.$trackInfoDownloadable = module.$('.input-downloadable', this.$trackInfoUploader);
            this.$trackInfoTrackType = module.$('.input-track-type', this.$trackInfoUploader);
            this.$trackInfoUploaderSubmitBtn = module.$('.btn-save', this.$el);

            this.$genreSelectFrame = module.$('.genre-select-wrapper', this.$el);
            this.$genreLoader = module.$('.spinner', this.$genreSelectFrame);
            this.$genreSelect = module.$('.genre-select', this.$genreSelectFrame);
            this.$genreFailure = module.$('.failure', this.$genreSelectFrame);
            this.$genreDropdown = module.$('.dropdown', this.$genreSelectFrame);
            this.$filter = module.$('.overlay-filter');

            this.$downloadableChoice = module.$('.downloadable-input-wrapper .choice', this.$el);
            this.$trackTypeChoice = module.$('.track-type-input-wrapper .choice', this.$el);

            this.$dropInfoUploaderFrame = module.$('.drop-info-uploader-wrapper', this.$el);
            this.$dropInfoUploader = module.$('.drop-info-uploader', this.$dropInfoUploaderFrame);
            this.$waveformFrame = module.$('.waveforms', this.$dropInfoUploader);
            this.$dropSelector = module.$('.drop-selectors', this.$dropInfoUploader);

            this.$waveformProgressFrame = module.$('.waveform-progress-wrapper', this.$dropInfoUploaderFrame);
            this.$waveformProgress = module.$('.progress', this.$waveformProgressFrame);
            this.$waveformCanvasFrame = module.$('.canvas-wrapper', this.$dropInfoUploaderFrame);
            this.$waveformProgressPercent = module.$('.progress .progress-label', this.$waveformProgressFrame);

            this.$dropBtn = module.$('.listen-drop', this.$dropInfoUploader);
            this.$dropBtnText = module.$('.listen-drop .text', this.$dropInfoUploader);

            this.genre = module.context.genre['dropbeat'];
            module.$.each(this.genre, function(id, genre) {
                that.$genreDropdown.append(
                    '<li class=\'genre\' data-key=\'' + genre.id + '\'>' + genre.name + '</li>');
            });
            this.$genreSelect.show();
            this.$genreLoader.hide();

            module.$(window).on('mouseup', this.onDropSelectorUp);
            module.$(window).on('mousemove', this.onDropSelectorMove);


// Remove unnecessory fields
            module.$('.title-input-wrapper', this.$trackInfoUploader).remove();
            module.$('.input-track-url', this.$trackInfoUploader).remove();
            module.$('.input-unique-key', this.$trackInfoUploader).remove();
            module.$('.input-resource-name', this.$trackInfoUploader).remove();
            module.$('.input-duration', this.$trackInfoUploader).remove();
            module.$('.track-type-input-wrapper', this.$trackInfoUploader).remove();
            module.$('.input-waveform-url', this.$trackInfoUploader).remove();
        },

        updateFormValue: function() {
            var $dropText;

            this.$trackInfoDesc.val(this.track.desc);
            this.$trackInfoDesc.attr('value', this.track.desc);

            this.$trackInfoCoverArtImg.attr('src', this.track.artwork);

            this.$trackInfoCoverArtUrl.attr('value', this.track.artwork);
            this.$trackInfoCoverArtUrl.val(this.track.artwork);

            this.$trackInfoDropDuration.attr('value', 20);
            this.$trackInfoDropDuration.val(20);
            this.$trackInfoDropStart.attr('value', this.track.drop.start);
            this.$trackInfoDropStart.val(this.track.drop.start);

            if (this.track.downloadable) {
                this.$downloadableChoice.removeClass('selected');
                this.$trackInfoDownloadable.val(true);
                this.$trackInfoDownloadable.attr('value', true);
                this.$downloadableChoice.find('i')
                    .addClass('glyphicons-unchecked')
                    .removeClass('glyphicons-check');
            } else {
                this.$downloadableChoice.addClass('selected');
                this.$trackInfoDownloadable.val(false);
                this.$trackInfoDownloadable.attr('value', false);
                this.$downloadableChoice.find('i')
                    .removeClass('glyphicons-unchecked')
                    .addClass('glyphicons-check');
            }

            if (this.track.track_type !== 'TRACK') {
                this.$dropInfoUploaderFrame.remove();
            } else {
                this.$trackInfoDropUrl.val(this.track.drop.url);
                this.$trackInfoDropUrl.attr('value', this.track.drop.url);
                this.renderWaveformWithData(this.track.waveform_url);
            }

            this.$genreDropdown.find(
                '.genre[data-key=\'' + this.track.genre_id + '\']').click();

            this.prepareAudioPlayer(this.track.stream_url);


            this.mayEnableSaveBtn();
        },

        onToggleDownloadable: function(e) {
            var currVal = this.$trackInfoDownloadable.val(),
                $ic = this.$downloadableChoice.find('i');

            if (currVal === 'true') {
                this.$downloadableChoice.addClass('selected');
                this.$trackInfoDownloadable.val(false);
                this.$trackInfoDownloadable.attr('value', false);
                $ic.removeClass('glyphicons-unchecked').addClass('glyphicons-check');
            } else {
                this.$downloadableChoice.removeClass('selected');
                this.$trackInfoDownloadable.val(true);
                this.$trackInfoDownloadable.attr('value', true);
                $ic.addClass('glyphicons-unchecked').removeClass('glyphicons-check');
            }
        },

        onDropBtnClicked : function(e) {
            var drop,
                $btn = module.$(e.currentTarget),
                $text = $btn.find('.text'),
                $ic= $btn.find('i'),
                that = this;

            if (!this.soundPlayer) {
                return;
            }
            drop = this.$trackInfoDropStart.val();
            if (drop !== 0 && !drop) {
                return;
            }

            if ($btn.hasClass('stop')) {
                // do stop
                this.soundPlayer && this.soundPlayer.pause();
                $btn.removeClass('stop');
                $text.text($text.data('textPlay'));
                $ic.addClass('glyphicons-play').removeClass('glyphicons-stop');
                return;
            }

            this.soundPlayer.setPosition(drop * 1000);
            this.soundPlayer.play();
            $btn.addClass('stop');
            $btn.find('.text');
            $text.text($text.data('textStop'));
            $ic.removeClass('glyphicons-play').addClass('glyphicons-stop');

            if (this.dropTimer) {
                clearTimeout(this.dropTimer);
            }

            this.dropTimer = setTimeout(function() {
                if (that.$dropBtn.hasClass('stop')) {
                    that.$dropBtn.click();
                }
                that.dropTimer = null;
            }, 20000);
        },

        onDropSelectorDown:function(e) {
            var $selector = module.$(e.currentTarget);
            this._dropSelectorDown = true;
            this._dropSelectorDownPosition = e.pageX - this.$dropSelector.offset().left;
        },

        onDropSelectorUp:function(e) {
            var drop, audio,
                that = this;

            if (!this._dropSelectorDown) {
                return;
            }
            this._dropSelectorDown = false;
            this._dropSelectorDownPosition = 0;

            drop = this.$trackInfoDropStart.val();

            if (this.$dropBtn.hasClass('stop') && drop >= 0) {
                if (this.soundPlayer) {
                     this.soundPlayer.setPosition(drop * 1000);
                }

                if (this.dropTimer) {
                    clearTimeout(this.dropTimer);
                }

                this.dropTimer = setTimeout(function() {
                    if (that.$dropBtn.hasClass('stop')) {
                        that.$dropBtn.click();
                    }
                    that.dropTimer = null;
                }, 20000);
            }
        },

        onDropSelectorMove: function(e) {
            var x, offsetLeft, left, duration, dropWidth,
                canvasWidth,
                dropSec = 20,
                innerX;

            if (!this._dropSelectorDown) {
                return;
            }
            canvasWidth = this.$waveformFrame.width();

            duration = this.track.duration;
            if (!duration) {
                return;
            }

            dropWidth = dropSec * (canvasWidth / duration);

            offsetLeft = this.$waveformFrame.offset().left;
            x = e.pageX - offsetLeft
            left = x - this._dropSelectorDownPosition;

            if (left < 0) {
                left = 0;
            }
            if (left > canvasWidth) {
                left = canvasWidth;
            }
            if (left > canvasWidth - dropWidth) {
                left = canvasWidth - dropWidth;
            }

            this.$dropSelector.css({
                left:left + 'px',
                width: dropWidth + 'px'
            });
            this.updateDropArea();
        },

        renderInitialDropSelector: function() {
            var duration, dropWidth,
                canvasWidth = this.$waveformFrame.width(),
                dropSec = 20,
                left,
                drop;

            duration = this.track.duration;
            if (!duration) {
                this.$dropSelector.hide();
                return;
            }

            dropWidth = dropSec * (canvasWidth / duration);

            drop = this.track.drop.start;
            left = drop * canvasWidth / duration;

            if (left < 0) {
                left = 0;
            }
            if (left > canvasWidth - dropWidth) {
                left = canvasWidth - dropWidth;
            }

            this.$dropSelector.css({
                left:left + 'px',
                width: dropWidth
            });
            this.updateDropArea(true);
        },

        updateDropArea: function(preserveInput) {
            var that = this,
                left, w, tmp,
                width = this.$waveformFrame.width(),
                colorPlaying = '#fff',
                //colorDrop = '#cd85ff',
                colorDrop = '#f03eb1',
                $canvas,
                canvasWidth = width,
                canvasHeight = 80,
                blockCount,
                marginWidth = 1,
                barWidth = 2,
                duration,
                dropStart,
                context,
                fromIdx,
                toIdx,
                i,
                fromX,
                toX,
                val, x, barHeight, y, delta;

            blockCount = Math.floor(canvasWidth / (barWidth + marginWidth));

            duration = this.track.duration;
            if (!duration) {
                this.$dropSelector.hide();
                return;
            }
            this.$dropSelector.show();

            left = this.$dropSelector.css('left');
            if (left.endsWith('%')) {
                left = parseInt(left);
            } else {
                left = (parseInt(left) * 100) / width;
            }

            w = this.$dropSelector.css('width');
            if (w.endsWith('%')) {
                w = parseInt(w);
            } else {
                w = (parseInt(w) * 100) / width;
            }

            if (!preserveInput) {
                dropStart = duration * left / 100;
                this.setDropInput(dropStart, 20);
            }

            if (!this.waveformData) {
                return;
            }
            if (!this.canvas) {
                $canvas = module.$('<canvas class=\'waveform-drop-canvas\' width=\'' + canvasWidth + '\' height=\'' + canvasHeight + '\'/>');
                this.$waveformCanvasFrame.append($canvas);
                this.canvas = this.$waveformCanvasFrame.find('.waveform-drop-canvas')[0];
            }
            context = this.canvas.getContext('2d');
            context.clearRect(0, 0, this.canvas.width, this.canvas.height);
            context.save();

            fromX = Math.round(canvasWidth * left / 100);
            toX = Math.round(canvasWidth * (left + w) / 100);
            fromIdx = Math.floor(fromX / (barWidth + marginWidth));
            toIdx = Math.ceil(toX / (barWidth + marginWidth));

            for(i = fromIdx; i < blockCount && i < toIdx; i++) {

                val = that.waveformData[i];
                x = i * (barWidth + marginWidth);
                barHeight = val * canvasHeight * 0.009;
                y = canvasHeight - barHeight;
                context.fillStyle = colorDrop;
                w = barWidth;
                if (i === fromIdx) {
                    delta = x;
                    x = Math.max(x, fromX);
                    delta = x - delta;
                    w -= delta;
                }
                if (i === toIdx - 1) {
                    delta = Math.min(x + w, toX) - x;
                    w = delta;
                }
                context.fillRect(x, y, w, barHeight);
            }
            context.restore();
        },

        onThumbnailUploadBtnClicked: function(e) {
            var that = this,
                $btn,
                browser = module.context.browserSupport.currentBrowser,
                isIE9 = browser[0] === 'MSIE' && browser[1] < 10;

            if (isIE9) {
                module.s.notify(module.string('image_uploading_not_supported'), 'error');
                return;
            }

            module.view.ImageUploadCropView.show(
                {aspectRatio:1, imageType:'c', width:600, height:600},
                function(url) {
                    if (!url) {
                        return;
                    }
                    that.setThumbUrlInput(url);
                });
        },

        onTrackInfoSaveBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                formArray = this.$trackInfoForm.serializeArray(),
                data = {},
                that = this;


            if ($btn.attr('disabled')) {
                return;
            }

            module.$.each(formArray, function(idx, s) {
                var val = s.value;
                if (val === 'false') {
                    val = false;
                } else if (val === 'true') {
                    val = true;
                }
                data[s.name] = val;
            });

            if (!validate(data)) {
                module.s.notify('Failed to submit', 'error');
                return;
            }

            data.track_id = this.track.uid;
            if (this.track.track_type !== 'TRACK') {
                data.drop_start = -1;
                data.drop_duration = -1;
                data.drop_url = null;
                submit(data);
            } else {
                if (data.drop_start < 0) {
                    module.s.notify('Drop should be selected', 'error');
                    return;
                }
                if (parseInt(data.drop_start) === this.track.drop.start) {
                    data.drop_url = this.track.drop.dref;
                    submit(data);
                    return;
                }

                module.utils.getUploadCredential().done(function(key, host) {
                    that.uploadDrop(key, host, that.track.stream_url, data.drop_start)
                        .done(function(dropUrl) {
                            data.drop_url = dropUrl;
                            submit(data);
                        })
                        .fail(function(msg) {
                            if (msg !== 'canceled') {
                                module.s.notify(msg || module.string('failed_to_upload'), 'error');
                            }
                        });
                });
            }

            function submit(data) {
                that.$trackInfoForm.find('input textarea').attr('disabled');
                $btn.addClass('progress');

                if (that.uploadTrackInfoReq) {
                    that.uploadTrackInfoReq.abort();
                }
                that.$dropProgressPercent.text('Uploading track information.. (2/2)');

                that.uploadTrackInfoReq = module.$.ajax({
                    url: module.api.trackUrl,
                    data: JSON.stringify(data),
                    type: 'PUT',
                    dataType: 'json',
                    contentType: 'application/json'
                }).done(function(res) {
                    var resourcePath;
                    if (!res || !res.success) {
                        module.s.notify(res.error || module.string('failed_to_upload'), 'error');
                        that.$dropProgressFrame.hide();
                        return;
                    }

                    resourcePath = that.track.getResourcePath();
                    module.removeBeforeunloadCallback('upload');
                    module.router.navigate(resourcePath, {trigger: true});
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus === 'abort') {
                        return;
                    }
                    module.s.notify(module.string('failed_to_upload'), 'error');
                    that.$dropProgressFrame.hide();
                }).always(function() {
                    that.uploadTrackInfoReq = null;
                    that.$trackInfoForm.find('input textarea').removeAttr('disabled');
                    $btn.removeClass('progress');
                });

            }

            function validate(data) {
                var errors = {},
                    $frames = {
                        'drop_start' : that.$trackInfoDropStart,
                        'drop_duration' : that.$trackInfoDropDuration,
                        'genre_id': that.$trackInfoGenreId,
                        'description': that.$trackInfoDesc,
                        'coverart_url': that.$trackInfoCoverArtUrl
                    };

                if (!data.genre_id) {
                    errors['genre_id'] = module.string('required_field');
                }

                if (!data.description) {
                    errors['description'] = module.string('required_field');
                }

                if (!data.coverart_url) {
                    errors['coverart_url'] = module.string('required_field');
                    module.$('.track-info-uploader .thumbnail .errors').text(
                        module.string('required_field'));
                }

                module.$.each($frames, function(key, input) {
                    var $el = module.$(input),
                        $wrapper = $el.closest('.input-wrapper'),
                        $errors;

                    if ($wrapper.size() === 0) {
                        return true;
                    }
                    $errors = $wrapper.find('.errors');
                    if (errors[key]) {
                        $errors.text(errors[key]);
                        $wrapper.addClass('error');
                    } else {
                        $errors.text('');
                        $wrapper.removeClass('error');
                    }
                });
                return Object.keys(errors).length === 0;
            }
        },

        setThumbUrlInput: function(url) {
            if (!url) {
                this.$trackInfoCoverArtImg.attr('src', '/images/default_cover_big.png');
                this.$trackInfoCoverArtUrl.val('');
                this.$trackInfoCoverArtUrl.removeAttr('value');
                this.mayEnableSaveBtn();
            } else {
                this.$trackInfoCoverArtImg.attr('src', url);
                this.$trackInfoCoverArtUrl.val(url);
                this.$trackInfoCoverArtUrl.attr('value', url);
                this.mayEnableSaveBtn();
            }
        },

        mayEnableSaveBtn: function() {
            var enable = true;
            if (!this.$trackInfoCoverArtUrl.val()) {
                enable = false;
            }
            if (!enable) {
                this.$trackInfoUploaderSubmitBtn.attr('disabled', 'disabled');
            } else {
                this.$trackInfoUploaderSubmitBtn.removeAttr('disabled');
            }
        },

        prepareAudioPlayer: function(url) {
            if (this.soundPlayer) {
                soundManager.destroySound(this.soundPlayer.id);
                this.soundPlayer = null;
            }
            this.soundPlayer = soundManager.createSound({
                id:url,
                url: url,
                volume:100,
                onload: this.onAudioPrepared,
                onerror: this.onAudioError,
                autoLoad: true
            });
        },

        onAudioPrepared: function(e) {
            var duration = Math.round(this.soundPlayer.duration / 1000);
            this.$dropBtn.show();
        },

        onAudioError: function(e) {
            this.$dropBtn.hide();
            module.s.notify(module.string('failed_to_load_audio_data'), 'error');
        },

        setDropInput: function(start, duration) {
            if (start >= 0) {
                start = parseInt(start);
                duration = parseInt(duration);
                this.$trackInfoDropStart.attr('value', start);
                this.$trackInfoDropDuration.attr('value', duration);
            } else {
                this.$trackInfoDropStart.removeAttr('value');
                this.$trackInfoDropDuration.removeAttr('value');
            }
        },

        uploadDrop: function(key, host, track_url, start) {
            var that = this,
                url = module.uploadApi.postProcessUrl(host),
                formData,
                deferred = module.$.Deferred();

            formData = new FormData();
            formData.append('url', track_url);
            formData.append('drop_start', start);

            this.$dropProgressFrame.show();
            this.$dropProgressPercent.removeClass('error');

            if (this.dropUploadReq) {
                this.dropUploadReq.abort();
            }

            this.$dropProgress.progressbar({
                max:100,
                value:false
            });
            this.$dropProgressPercent.text('Generating drop file.. (1/2)');

            this.dropUploadReq = module.$.ajax({
                cache:false,
                url:url,
                type:'post',
                data: formData,
                dataType:'json',
                contentType:false,
                processData: false,
                crossDomain: true,
                xhrFields: {
                    withCredentials: true
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('Access-Control-Request-Methods', 'POST');
                    xhr.setRequestHeader('dbt_key', key);
                }
            }).done(function(res) {
                if (!res) {
                    deferred.reject();
                    that.$dropProgressFrame.hide();
                    return;
                }
                deferred.resolve(res.drop_url);
            }).fail(function(jqXHR, textStatus, error) {
                that.$dropProgressFrame.hide();
                deferred.reject(textStatus === 'abort' ? 'canceled': null);
            }).always(function() {
                that.dropUploadReq = null;
            });

            return deferred;
        },

        renderWaveformWithData: function(url) {
            var that = this,
                context,
                canvasWidth,
                canvasHeight,
                canvas,
                $canvas,
                reader;

            this.$dropInfoUploaderFrame.show();
            this.$waveformProgress.show();
            this.$waveformProgressPercent.text('Preparing waveform renderer..');
            this.$waveformProgressPercent.removeClass('error');
            this.$waveformProgress.progressbar({
                max:100,
                value: false
            });

            // Canvas
            canvasWidth = this.$waveformFrame.width();
            canvasHeight = 80;

            $canvas = module.$('<canvas class=\'waveform-bg-canvas\' width=\'' +
                canvasWidth + '\' height=\'' + canvasHeight + '\'/>');
            this.$waveformCanvasFrame.html($canvas);
            canvas = this.$waveformCanvasFrame.find('.waveform-bg-canvas')[0];

            context = canvas.getContext('2d');
            canvas = canvas;

// because we will create sprite waveform, we multiply height 3
            canvas.width = canvasWidth;
            canvas.height = canvasHeight;

            if (this.waveformReq) {
                this.waveformReq.abort();
            }
            this.waveformReq = module.$.ajax({
                url:url,
                dataType:'json'
            }).done(function(data) {
                displayBuffer(data);
            }).fail(function(jqXHR, textStatus ,error) {
                if (textStatus !== 'abort') {
                    onError();
                }
            }).always(function() {
                that.waveformReq = null;
            });

            function onError() {

                that.$waveformProgressPercent
                    .text('Failed to read sound file.')
                    .addClass('error').text(module.string('failed_to_read'));

                that.$waveformProgress.progressbar({
                    max:100,
                    value: 0
                });
            }

            function displayBuffer(bars) {
                var barCount = bars.length,
                    blocks = [],
                    blockCount,
                    marginWidth = 1,
                    barWidth = 2,
                    colorNormal = '#636365',
                    colorPlaying = '#fff',
                    //colorDrop = '#cd85ff',
                    colorDrop = '#f03eb1',
                    len = bars.length;

                blockCount =  Math.floor(canvasWidth / (barWidth + marginWidth));
                blocks = module.utils.downsizeArray(bars, blockCount);

                context.save();

                that.waveformData = blocks;
                that.renderInitialDropSelector();

                module.$.each(blocks, function(idx, val) {
                    var x, barHeight, y;
                    x = idx * (barWidth + marginWidth);
                    barHeight = val * canvasHeight * 0.009;
                    y = canvasHeight - barHeight;
                    context.fillStyle = colorNormal;
                    context.fillRect(x, y, barWidth, barHeight);
                });
                context.save();

                that.$waveformProgress.hide();
                that.$dropInfoUploaderFrame.addClass('prepared');
            }
        },

        onSelectGenre: function() {
            var that = this;
            if (this.$genreDropdown.is(':hidden')) {
                this.$filter.show().one('click', function() {
                    that.$genreDropdown.hide();
                });
                this.$genreDropdown.show();
            }
        },

        onGenreClicked: function(e) {
            var $target = module.$(e.currentTarget),
                today, url,
                mm, dd, timestamp, selectedGenre;

            if ($target.hasClass('selected')) {
                return;
            }

            module.$('.genre', this.$genreDropdown).removeClass('selected');
            $target.addClass('selected');

            selectedGenre = $target.data('key');

            module.$('.selected-genre .text', this.$genreSelect).text($target.text());
            if (this.$filter.is(':visible')) {
                this.$filter.click();
            }
            this.$trackInfoGenreId.val(selectedGenre);
            this.$trackInfoGenreId.attr('value', selectedGenre);
        }
    });

    TrackPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-track',
        initialize: function(options) {
            var that = this,
                ua = navigator.userAgent;

            DropbeatPageView.prototype.initialize.call(this, options);

            module.$('header .header-menu .menu').removeClass('selected');
            this._resourceName = options.userName;
            this._trackName = options.trackName;

            if (module.context.isMobile &&
                ((!module.context.isIOS ||
                (!ua.match(/CriOS/) &&
                (!ua.match(/Safari/) || !ua.match(/Version\/9/)))))) {
                module.deeplink.open('dropbeat://net.dropbeat.labs/r/' +
                    this._resourceName + '/' + this._trackName + '/', function() {});
            }

            this._userTrackTmpl = module._.template(
                module.$('#tmpl-useruploaded-track-contents').html());
            this._mobileUserTrackTmpl = module._.template(
                module.$('#tmpl-mobile-useruploaded-track-contents').html());
            //this._externalTrackTmpl = module._.template(module.$('#tmpl-external-track-contents').html());
            this._commentTmpl = module._.template(module.$('#tmpl-track-comment').html());

            this.render();
            if (module.context.isMobile) {
                this.$el.addClass('mobile');
                module.$('.footer-section').hide();
                module.$('.scroll-y').removeClass('scroll-y').addClass('_scroll-y');
            }

            this.section = 'useruploaded_single_track';
            this.followingIds = [];
            this.$spinner = module.$('.spinner', this.$el);
            this.$failure = module.$('.failure', this.$el);
            this.$404 = module.$('.failure-404', this.$el);
            this.$contents = module.$('.resource-contents', this.$el);

            WaveformViewMixin.bind(this, this.$contents);
            PlayableViewMixin.bind(this, this.$contents, [this.section]);
            setTimeout(function() {
                that.loadResource(that._resourceName, that._trackName);
            }, 0);
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .page-track .btn-retry': 'onRetryBtnClicked',
                'click .btn-follow': 'onFollowBtnClicked',
                'submit #form-comment': 'onCommentSubmit',
                'click .comment .btn-delete': 'onCommentDeleteBtnClicked',
                'click .open-in-app-btn': 'onOpenInAppBtnClicked'
            });
        },

        onOpenInAppBtnClicked: function(e) {
            if (module.context.isMobile) {
                module.deeplink.open(
                    'dropbeat://net.dropbeat.labs/r/' + this._resourceName + '/' +
                        this._trackName + '/');
            }
        },

        onAfterDeleteMusic: function(success) {
            if (!success) {
                return;
            }
            module.router.navigate('/r/' + this._resourceName, {trigger: true});
        },

        onCommentDeleteBtnClicked: function(e) {
            var that = this,
                $btn = module.$(e.currentTarget),
                $comment = $btn.closest('.comment'),
                commentId = $comment.data('id'),
                commentUserId = $comment.data('userId');

            if (!commentId) {
                return;
            }
            if (!module.context.account ||
                    module.context.account.id !== commentUserId) {
                return;
            }

            if ($comment.hasClass('temp-comment')) {
                return;
            }

            if (this.commentDeleteReq) {
                this.commentDeleteReq.abort();
            }
            $comment.addClass('temp-comment');

            this.commentDeleteReq = module.$.ajax({
                url: module.api.userTrackCommentUrl,
                data: JSON.stringify({
                    comment_id: commentId
                }),
                contentType: 'application/json',
                dataType: 'json',
                type: 'DELETE'
           }).done(function(res) {
               if (!res || !res.success) {
                   module.s.notify(module.string('failed_to_delete_comment'), 'error');
                   return;
               }
               that.loadComments(that.track.uid);
           }).fail(function(jqXHR, textStatus, error) {
               if (textStatus !== 'abort') {
                   module.s.notify(module.string('failed_to_delete_comment'), 'error');
               }
               $comment.removeClass('temp-comment');
           }).always(function() {
               that.commentDeleteReq = null;
           });

        },

        onCommentSubmit: function(e) {
            var that = this,
                $form = module.$('#form-comment'),
                $input = $form.find('input[name=\'comment\']'),
                text = $input.val(),
                account,
                $tmpComment,
                profileImage;

            if (!text) {
                return false;
            }

            if (this.commentReq) {
                return false;
            }

            account = module.context.account;
            if (!account) {
                module.view.NeedSigninView.show();
                return false;
            }

            profileImage = account.profileImage;
            if (!profileImage && account.fbId) {
                profileImage = 'http://graph.facebook.com/' + account.fbId +
                    '/picture?type=square';
            }

            $tmpComment = module.$(this._commentTmpl({
                comment: {
                    id: '',
                    content: module.utils.toHtml(text),
                    user: {
                        id: account.id,
                        profile_image: profileImage,
                        resource_name:account.resourceName,
                        nickname:account.nickname
                    },
                    track_uid: this.track.unique_key
                },
                editable:false
            }));
            $tmpComment.addClass('temp-comment');

            this.$comments.prepend($tmpComment);

            this.commentReq = module.$.ajax({
                url: module.api.userTrackCommentUrl,
                data: JSON.stringify({
                    track_id: that.track.uid,
                    content:text
                }),
                contentType: 'application/json',
                dataType: 'json',
                type: 'POST'
            }).done(function(res) {
                if (!res || !res.success) {
                    that.$comments.find('.temp-comment').remove();
                    if (res.error) {
                        module.s.notify(res.error, 'error');
                        return;
                    }
                    module.s.notify(module.string('failed_to_save_comment'), 'error');
                    return;
                }
                that.loadComments(that.track.uid);
                $input.val('');
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus !== 'abort') {
                    module.s.notify(module.string('failed_to_save_comment'), 'error');
                    return;
                }
                that.$comments.find('.temp-comment').remove();
            }).always(function() {
                that.commentReq = null;
            });

            return false;
        },

        onRetryBtnClicked: function(e) {
            this.loadResource(this._resourceName, this._trackName);
        },

        onFollowBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $btnText = module.$('.text', $btn),
                user = this.user;

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            if ($btn.hasClass('progress') || !user || !user.id) {
                return;
            }

            $btn.addClass('progress');


            if ($btn.hasClass('following')) {
                module.context.account.unfollow(this.user, function(success, msg) {
                    $btn.removeClass('progress');
                    if (!success) {
                        module.s.notify(module.string('failed_to_unfollow'), 'error');
                        return;
                    }
                    $btn.removeClass('following');
                    $btnText.text($btnText.data('textFollow'));
                });
            } else {
                module.context.account.follow(this.user, function(success, msg) {
                    $btn.removeClass('progress');
                    if (!success) {
                        module.s.notify(module.string('failed_to_follow'), 'error');
                        return;
                    }
                    $btn.addClass('following');
                    $btnText.text($btnText.data('textFollowing'));
                });
            }
        },

        onTogglePlay: function(e, music, section, isPlaying) {
            var $btn;
            if (!this.track || section !== this.section || !music || music.id !== this.track.id) {
                return;
            }
            $btn = module.$('.a-addable-music .btn-play');
            if (!isPlaying) {
                $btn.find('i').removeClass('loading')
                    .removeClass('glyphicons-pause')
                    .addClass('glyphicons-play');
                $btn.find('.text').text(module.string('play'));
            } else if (!$btn.hasClass('loading')) {
                $btn.find('i').removeClass('loading')
                    .addClass('glyphicons-pause')
                    .removeClass('glyphicons-play');
                $btn.find('.text').text(module.string('pause'));
            }
        },

        onLoadingMusic: function(e, m, s) {
            var $btn = module.$('.a-addable-music .btn-play');

            $btn.find('i').addClass('loading')
                .removeClass('glyphicons-pause')
                .removeClass('glyphicons-play');
            $btn.find('.text').text(module.string('loading'));
            $btn.addClass('loading');
        },

        onPlayMusic: function(e, m, s) {
            var $btn = module.$('.a-addable-music .btn-play');

            $btn.find('i').removeClass('loading')
                .addClass('glyphicons-pause')
                .removeClass('glyphicons-play');
            $btn.find('.text').text(module.string('pause'));
            $btn.removeClass('loading');
        },

        remove: function() {
            module.$('._scroll-y').addClass('scroll-y').removeClass('_scroll-y');
            DropbeatPageView.prototype.remove.apply(this, arguments);
            WaveformViewMixin.unbind(this);
            PlayableViewMixin.unbind(this);
            if (this.req) {
                this.req.abort();
                this.req = null;
            }

            if (this.ownerReq) {
                this.ownerReq.abort();
                this.ownerReq = null;
            }

            if (this.followReq) {
                this.followReq.abort();
                this.followReq = null;
            }

            if (this.commentReq) {
                this.commentReq.abort();
                this.commentReq = null;
            }

            if (this.commentListReq) {
                this.commentListReq.abort();
                this.commentListReq = null;
            }

            if (this.commentDeleteReq) {
                this.commentDeleteReq.abort();
                this.commentDeleteReq = null;
            }

        },

        loadResource: function(resourceName, trackName) {
            var that = this,
                resourceUrl,
                resolveDeferred = module.$.Deferred(),
                deferred = module.$.Deferred(),
                deferreds = [],
                resolveResponse;

            resourceUrl = 'http://dropbeat.net/r/' + resourceName +
                '/' + trackName + '/';

            if (this.req) {
                this.req.abort();
            }
            this.$failure.hide();
            this.$spinner.show();

            resolveResponse = module.context.resolveResponse &&
                module.context.resolveResponse[resourceUrl];
            if (resolveResponse) {
// We should delete resolve cache for ajax load
// when user returned to this page
                delete module.context.resolveResponse[resourceUrl];
                resolveDeferred.resolve([{
                    success:true,
                    data:resolveResponse
                }]);
                deferreds.push(resolveDeferred);
            } else {
                this.req = module.$.ajax({
                    url:module.api.resolveObjectUrl,
                    data: {
                        url: resourceUrl
                    },
                    dataType:'json',
                    cache:false
                }).always(function() {
                    that.req = null;
                });
                deferreds.push(this.req);
            }


            this.ownerReq = module.$.ajax({
                url:module.api.resolveObjectUrl,
                data: {
                    url: 'http://dropbeat.net/r/' + resourceName
                },
                dataType:'json',
                cache:false
            }).always(function() {
                that.ownerReq = null;
            });
            deferreds.push(this.ownerReq);


            module.$.when.apply(this, deferreds)
                .done(function(trackObj, ownerObj) {
                    var html,
                        trackType,
                        track,
                        user,
                        trackRes = trackObj[0],
                        ownerRes = ownerObj[0],
                        userIds = [],
                        genres;


                    if (!trackRes || !trackRes.success || !trackRes.data) {
                        that.$failure.show();
                        return;
                    }
                    if (!ownerRes || !ownerRes.success || !ownerRes.data) {
                        that.$failure.show();
                        return;
                    }

                    genres = {};
                    module.$.each(module.context.genre.dropbeat, function(idx, genre) {
                        genres[genre.id] = genre.name;
                    });
                    that.genres = genres;

                    user = ownerRes.data.user;
                    user.isFollowing = userIds.indexOf(user.id) > -1;

                    track = new module.music.DropbeatMusic(trackRes.data);
                    that.track = track;
                    that.user = user;
                    that.user.type = 'user';
                    that.userType = 'user';

                    // we only support single track now
                    trackType = 'user_track';

                    if (trackType === 'user_track') {
                        html = that.renderUserTrackContents(track, user);
                    } else if (trackType === 'external_track') {
                        // not handle
                    }
                    if (!html) {
                        that.$failure.show();
                        return;
                    }
                    that.setMusicToStorage(track.getStorageKey(), track);
                    that.$contents.html(html);
                    that.$comments = module.$('.comments-wrapper .comments', that.$el);
                    that.$commentSpinner = module.$('.comments-wrapper .comment-spinner', that.$el);
                    that.$commentFailure = module.$('.comments-wrapper .comment-failure', that.$el);
                    that.loadComments(that.track.uid);
                    that.updateWaveform(track.id);
                    that.updatePlayState();

                    if (module.context.isMobile && navigator.userAgent.indexOf('FBIOS') > -1) {
                        module.s.notify(module.string('fbios_mute_notice'), 'info', {
                            autoHide:false
                        });
                        module.s.notify(module.string('fbios_applink_notice'), 'info', {
                            autoHide:false
                        });
                    }
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus && textStatus !== 'abort' &&
                            jqXHR && jqXHR.status === 400) {
                        that.$404.show();
                        return;
                    }
                    that.$failure.show();
                }).always(function() {
                    that.$spinner.hide();
                });
        },

        loadComments: function(trackId, showSpinner) {
            var that = this;

            if (this.commentListReq) {
                this.commentListReq.abort();
                this.commentListReq = null;
            }
            if (this.$comments.find('.comment').size() === 0) {
                this.$commentSpinner.show();
            }
            this.commentListReq = module.$.ajax({
                url: module.api.userTrackCommentUrl,
                data: {
                    track_id: trackId
                },
                contentType: 'application/json',
                dataType: 'json'
            }).done(function(res) {
                var html = '';
                if (!res || !res.success || !res.data) {
                    that.$comments.hide();
                    that.$commentFailure.show();
                    return;
                }

                if (res.data.length === 0) {
                    html += '<div class=\'empty-message\'>' +
                        module.string('no_comment') + '</div>';
                } else {
                    res.data.reverse();
                    module.$.each(res.data, function(idx, comment) {
                        html += that._commentTmpl({
                            comment: {
                                id: comment.id,
                                created_at: comment.created_at,
                                content: module.utils.toHtml(comment.content),
                                user: {
                                    id: comment.user_id,
                                    profile_image: comment.user_profile_image,
                                    resource_name:comment.user_resource_name,
                                    nickname:comment.user_name
                                },
                                track_uid: comment.track_uid
                            },
                            editable : module.context.account &&
                                module.context.account.id === comment.user_id
                        });
                    });
                }

                that.$comments.html(html);
                that.$comments.show();
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus !== 'abort') {
                    that.$comments.hide();
                    that.$commentFailure.show();
                }
            }).always(function() {
                that.commentListReq = null;
                that.$commentSpinner.hide();
            });
        },

        renderUserTrackContents: function(track, user) {
            var html, that = this,
                tmpl;
            if (module.context.isMobile) {
                tmpl = this._mobileUserTrackTmpl;
            } else {
                tmpl = this._userTrackTmpl;
            }
            html = tmpl({
                trackInfo: track,
                user: user,
                genres: that.genres,
                editable: module.context.account &&
                    module.context.account.email === user.email
            });
            return html;
        },

        renderExternalTrackContents: function(resourceName, res) {
            return '';
        }
    });

    ResourcePageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-resource',
        initialize: function(options) {
            var that = this;

            DropbeatPageView.prototype.initialize.call(this, options);

            module.$('header .header-menu .menu').removeClass('selected');

            this.tabSubViews = {};
            this.musicStorage = {};

            this._resourceName = options.resourceName;
            this._channelResourceTmpl = module._.template(module.$('#tmpl-channel-resource-contents').html());
            this._externalUserResourceTmpl = module._.template(module.$('#tmpl-external-user-resource-contents').html());
            this._userResourceTmpl = module._.template(module.$('#tmpl-user-resource-contents').html());
            this._userTrackTmpl = module._.template(module.$('#tmpl-useruploaded-addable-music').html());
            this._externalTrackTmpl = module._.template(module.$('#tmpl-external-addable-music').html());

            this.render();

            if (module.context.isMobile) {
                this.$el.addClass('mobile');
                module.$('.footer-section').hide();
            }

            this.$spinner = module.$('>.spinner', this.$el);
            this.$failure = module.$('>.failure', this.$el);
            this.$404 = module.$('>.failure-404', this.$el);
            this.$contents = module.$('.resource-contents', this.$el);

            this.$coverview = module.$('.coverview');
            this.$coverFilter = module.$('.cover-filter', this.$coverview);
            this.$coverPlaceholder = module.$('.cover-placeholder', this.$coverview);

            setTimeout(function() {
                that.loadResource(that._resourceName);
            }, 0);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .page-resource .btn-retry': 'onRetryBtnClicked',
                'click .section-tabs .section-tab': 'onTabClicked',
                'click .btn-follow': 'onFollowBtnClicked'
            });
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        remove: function() {
            var that = this;

            DropbeatPageView.prototype.remove.apply(this, arguments);
            module.$.each(this.tabSubViews, function(key, view) {
                view.remove();
                delete that.tabSubViews[key];
            });

            if (this.req) {
                this.req.abort();
                this.req = null;
            }

            if (this.followReq) {
                this.followReq.abort();
                this.followReq = null;
            }

            this.musicStorage = {};
        },

        onRetryBtnClicked: function(e) {
            this.loadResource(this._resourceName);
        },

        onFollowBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $btnText = module.$('.text', $btn),
                user = this.user,
                count, $follower;

            $follower = module.$('.overview .overview-tab.followers .count');

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            if ($btn.hasClass('progress') || !user || !user.id) {
                return;
            }

            $btn.addClass('progress');


            if ($btn.hasClass('following')) {
                module.context.account.unfollow(this.user, function(success, msg) {
                    $btn.removeClass('progress');
                    if (!success) {
                        module.s.notify(module.string('failed_to_unfollow'), 'error');
                        return;
                    }
                    if ($follower.size() > 0) {
                        count = parseInt($follower.text());
                        $follower.text(count -1);
                    }
                    $btn.removeClass('following');
                    $btnText.text($btnText.data('textFollow'));
                });
            } else {
                module.context.account.follow(this.user, function(success, msg) {
                    $btn.removeClass('progress');
                    if (!success) {
                        module.s.notify(module.string('failed_to_follow'), 'error');
                        return;
                    }
                    if ($follower.size() > 0) {
                        count = parseInt($follower.text());
                        $follower.text(count + 1);
                    }
                    $btn.addClass('following');
                    $btnText.text($btnText.data('textFollowing'));
                });
            }
        },

        onTabClicked: function(e) {
            var $taretTab = module.$(e.currentTarget),
                $tab = module.$('.section-tabs .section-tab', this.$el),
                target = $taretTab.data('target'),
                resourceView,
                view,
                targetContents = '.section-content.' + target;
            target = target.dashToCamel();

            if (this.userType === 'artist') {
                resourceView = ArtistResourceSubViews;
            } else if (this.userType === 'user') {
                resourceView = UserResourceSubViews;
            } else if (this.userType === 'channel') {
                resourceView = ChannelResourceSubViews;
            } else {
                return;
            }

            view = resourceView[target];
            if (!view) {
                return;
            }
            if (this.currentSubView && this.currentSubView.key === target) {
                return;
            }

            if (this.currentSubView && this.currentSubView.scrollUnbind) {
                this.currentSubView.scrollUnbind();
            }

            $tab.removeClass('selected');
            module.$('.section-tabs .section-tab.' + target).addClass('selected');
            module.$('.section-contents .section-content').removeClass('selected');
            module.$(targetContents).addClass('selected');

            if (!this.tabSubViews[target]) {
                this.tabSubViews[target] = new view({
                    el: targetContents,
                    key: target,
                    user: this.user,
                    parentMusicStorage : this.musicStorage,
                    parentView: this
                });
            }
            this.currentSubView = this.tabSubViews[target];
            if (this.currentSubView && this.currentSubView.scrollBind) {
                this.currentSubView.scrollBind();
            }
        },

        loadResource: function(resourceName) {
            var that = this,
                resourceUrl = 'http://dropbeat.net/r/' + resourceName + '/',
                resolveResponse,
                resolveDeferred = module.$.Deferred();

            this.$failure.hide();
            this.$spinner.show();
            this.$coverview.hide();

            if (this.req) {
                this.req.abort();
            }

            resolveResponse = module.context.resolveResponse &&
                module.context.resolveResponse[resourceUrl];
            if (resolveResponse) {
// We should delete resolve cache for ajax load
// when user returned to this page
                delete module.context.resolveResponse[resourceUrl];
                resolveDeferred.resolve({
                    success:true,
                    data:resolveResponse
                });
            } else {
                this.req = module.$.ajax({
                    url:module.api.resolveObjectUrl,
                    data: {
                        url: 'http://dropbeat.net/r/' + resourceName
                    },
                    dataType:'json',
                    cache:false
                });
                resolveDeferred = this.req;
            }

            resolveDeferred.done(function(res) {
                var html,
                    u,
                    coverImageUrl,
                    $wrapper;

                if (!res || !res.success || !res.data) {
                    that.$failure.show();
                    return;
                }

                that.genres = {};
                module.$.each(module.context.genre['dropbeat'], function(idx, genre) {
                    that.genres[genre.id] = genre.name;
                });

                that.userType = res.data.user_type;

                if (that.userType === 'user') {
                    that.user = res.data.user;
                    that.user.type = 'user';
                    html = that.renderUserContent(res);

                    coverImageUrl = that.user.profile_cover_image || '/images/default_user_cover.png';
                    if (!that.user.profile_cover_image) {
                        that.$coverFilter.show();
                    } else {
                        that.$coverFilter.hide();
                    }

                } else if (that.userType === 'artist') {
                    u = res.data.user;
                    that.user = {
                        id:u.artist_id,
                        nickname: u.artist_name,
                        has_event: u.has_event,
                        has_podcast: u.has_podcast,
                        profile_image: u.artist_image,
                        resource_name:that._resourceName,
                        type:'artist'
                    };
                    html = that.renderExternalUserContent(res);

                    coverImageUrl = u.artist_image || '/images/default_user_cover.png';
                    that.$coverFilter.show();

                } else if (that.userType === 'channel') {
                    u = res.data.user;

                    that.user = {
                        id:u.channel_id,
                        uid: u.channel_uid,
                        nickname: u.channel_name,
                        playlist: u.playlist || [],
                        uploads: u.uploads,
                        genre: u.genre,
                        profile_image: u.channel_thumbnail,
                        description: u.channel_description,
                        resource_name:that._resourceName,
                        type:'channel'
                    };
                    html = that.renderChannelUserContent(that.user);

                    coverImageUrl = u.channel_thumbnail || '/images/default_user_cover.png';
                    that.$coverFilter.show();

                }
                if (!html) {
                    that.$failure.show();
                    return;
                }

                that.$contents.html(html);
                that.$contents.find('.section-tabs .section-tab').first().click();
                if (that.userType === 'user') {
                    if (module.$('.section-contents.uploads .a-addable-music').size === 0 ||
                            that.user.resource_name === 'dropbeat') {
                        that.$contents.find('.section-tabs .section-tab.likes').click();
                    }
                }

                $wrapper = module.$('.following-box-wrapper', that.$el);
                if ($wrapper.size() > 0) {
                    that.subViews['followingSectionView'] = new FollowingSectionView({
                        $wrapper: $wrapper,
                        user: that.user
                    });
                }

                $wrapper = module.$('.followers-box-wrapper', that.$el);
                if ($wrapper.size() > 0) {
                    that.subViews['followersSectionView'] = new FollowersSectionView({
                        $wrapper: $wrapper,
                        user: that.user
                    });
                }

// set page cover view
                that.$coverPlaceholder.css({
                    backgroundImage: 'url(\'' + coverImageUrl + '\')'
                });
                that.$coverview.show();
                if (module.context.isMobile && navigator.userAgent.indexOf('FBIOS') > -1) {
                    module.s.notify(module.string('fbios_mute_notice'), 'info', {
                        autoHide:false
                    });
                    module.s.notify(module.string('fbios_applink_notice'), 'info', {
                        autoHide:false
                    });
                }
            })
            .fail(function(jqXHR, textStatus, error) {
                if (jqXHR.status && textStatus && textStatus !== 'abort' &&
                    jqXHR.status === 400) {
                    that.$404.show();
                    return;
                }
                that.$failure.show();
            }).always(function() {
                that.$spinner.hide();
                that.req = null;
            });
        },

        renderUserContent: function(res) {
            var that = this,
                data = {}, html;
            data['user'] = that.user;
            data['tracks'] = []

            if (res.data.tracks) {
                module.$.each(res.data.tracks, function(idx, t) {
                    var track = new module.music.DropbeatMusic(t);
                    data['tracks'].push(track);
                    that.musicStorage[track.getStorageKey()] = track;
                });
            }

            html = this._userResourceTmpl({
                userData: data,
                trackTmpl: this._userTrackTmpl,
                genres: this.genres
            });
            return html;
        },

        renderExternalUserContent: function(res) {
            var that = this,
                data = {}, html;

            data['user'] = that.user;
            data['tracks'] = []

            if (res.data.tracks) {
                module.$.each(res.data.tracks, function(idx, t) {
                    var track = new module.music.Music(t);
                    track.user = module.$.extend({}, that.user, track.user);
                    data['tracks'].push(track);
                    that.musicStorage[track.getStorageKey()] = track;
                });
            }

            html = this._externalUserResourceTmpl({
                userData: data,
                trackTmpl: this._externalTrackTmpl,
                genres: this.genres
            });
            return html;
        },

        renderChannelUserContent: function(user) {
            return this._channelResourceTmpl({
                userData: {
                    user: user
                },
                genres: this.genres
            });
        }
    });

    ResourceFollowersPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-resource',
        initialize: function(options) {
            var that = this;

            DropbeatPageView.prototype.initialize.call(this, options);

            module.$('header .header-menu .menu').removeClass('selected');

            this._resourceName = options.resourceName;
            this._userResourceTmpl = module._.template(module.$('#tmpl-user-resource-followers-contents').html());
            this._followItemTmpl = module._.template(module.$('#tmpl-user-follow-item').html());

            this.render();
            this.$spinner = module.$('.spinner', this.$el);
            this.$failure = module.$('.failure', this.$el);
            this.$404 = module.$('.failure-404', this.$el);
            this.$contents = module.$('.resource-contents', this.$el);

            this.$coverview = module.$('.coverview');
            this.$coverFilter = module.$('.cover-filter', this.$coverview);
            this.$coverPlaceholder = module.$('.cover-placeholder', this.$coverview);

            this.loadResource(this._resourceName);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .page-resource .btn-retry': 'onRetryBtnClicked',
                'click .section-tabs .section-tab': 'onTabClicked',
                'click .btn-follow': 'onFollowBtnClicked'
            });
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        remove: function() {
            var that = this;

            DropbeatPageView.prototype.remove.apply(this, arguments);

            if (this.req) {
                this.req.abort();
                this.req = null;
            }

            if (this.otherFollowerReq) {
                this.otherFollowerReq.abort();
                this.otherFollowerReq = null;
            }

            if (this.followReq) {
                this.followReq.abort();
                this.followReq = null;
            }

            this.musicStorage = {};
        },

        onRetryBtnClicked: function(e) {
            this.loadResource(this._resourceName);
        },

        onFollowBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $btnText = module.$('.text', $btn),
                user = this.user,
                count, $follower;

            $follower = module.$('.overview .overview-tab.followers .count');

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            if ($btn.hasClass('progress') || !user || !user.id) {
                return;
            }

            $btn.addClass('progress');


            if ($btn.hasClass('following')) {
                module.context.account.unfollow(this.user, function(success, msg) {
                    $btn.removeClass('progress');
                    if (!success) {
                        module.s.notify(module.string('failed_to_unfollow'), 'error');
                        return;
                    }
                    if ($follower.size() > 0) {
                        count = parseInt($follower.text());
                        $follower.text(count -1);
                    }
                    $btn.removeClass('following');
                    $btnText.text($btnText.data('textFollow'));
                });
            } else {
                module.context.account.follow(this.user, function(success, msg) {
                    $btn.removeClass('progress');
                    if (!success) {
                        module.s.notify(module.string('failed_to_follow'), 'error');
                        return;
                    }
                    if ($follower.size() > 0) {
                        count = parseInt($follower.text());
                        $follower.text(count + 1);
                    }
                    $btn.addClass('following');
                    $btnText.text($btnText.data('textFollowing'));
                });
            }
        },

        loadResourceFollowers: function(userId) {
            var that = this,
                html = '';

            this.$sectionContents.find('.failure').hide();
            this.$sectionContents.find('.spinner').show();

            this.otherFollowersReq = module.$.ajax({
                url:module.api.followersUserUrl,
                data:{
                    user_id:userId
                },
                contentType: 'application/json',
                dataType: 'json',
                type: 'GET'

            }).done(function(res) {
                if (!res || !res.success) {
                    that.$sectionContents.find('.failure').show();
                    return;
                }

                module.$.each(res.data, function(idx, follow) {
                    html += that._followItemTmpl({
                        follow: follow,
                        isFollowing: false
                    });
                });
                that.$sectionContents.html(html);
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus !== 'abort') {
                    that.$sectionContents.find('.failure').show();
                }
            }).always(function() {
                that.otherFollowersReq = null;
                that.$sectionContents.find('.spinner').hide();
            });
        },

        loadResource: function(resourceName) {
            var that = this,
                $wrapper;

            this.$failure.hide();
            this.$spinner.show();
            this.$coverview.hide();

            if (this.req) {
                this.req.abort();
            }

            this.req = module.$.ajax({
                url:module.api.resolveObjectUrl,
                data: {
                    url: 'http://dropbeat.net/r/' + resourceName
                },
                dataType:'json',
                cache:false
            }).done(function(res) {
                var html,
                    coverImageUrl;

                if (!res || !res.success || !res.data) {
                    that.$failure.show();
                    return;
                }

                that.genres = {};
                module.$.each(module.context.genre['dropbeat'], function(idx, genre) {
                    that.genres[genre.id] = genre.name;
                });

                that.userType = res.data.user_type;

                if (that.userType === 'user') {
                    that.user = res.data.user;
                    that.user.type = 'user';
                    html = that.renderUserContent(that.user);

                    coverImageUrl = that.user.profile_cover_image || '/images/default_user_cover.png';
                    if (!that.user.profile_cover_image) {
                        that.$coverFilter.show();
                    } else {
                        that.$coverFilter.hide();
                    }

                } else {
                    that.$404.show();
                    // not this version
                    return;
                }

                if (!html) {
                    that.$failure.show();
                    return;
                }

                that.$contents.html(html);
                that.$sectionContents = module.$('.section-contents', that.$contents);
                that.loadResourceFollowers(that.user.id);

                $wrapper = module.$('.following-box-wrapper', that.$el);
                if ($wrapper.size() > 0) {
                    that.subViews['followingSectionView'] = new FollowingSectionView({
                        $wrapper: $wrapper,
                        user: that.user
                    });
                }

                $wrapper = module.$('.followers-box-wrapper', that.$el);
                if ($wrapper.size() > 0) {
                    that.subViews['followersSectionView'] = new FollowersSectionView({
                        $wrapper: $wrapper,
                        user: that.user
                    });
                }

// set page cover view
                that.$coverPlaceholder.css({
                    backgroundImage: 'url(\'' + coverImageUrl + '\')'
                });
                that.$coverview.show();
            })
            .fail(function(jqXHR, textStatus, error) {
                if (jqXHR.status && textStatus && textStatus !== 'abort' &&
                    jqXHR.status === 400) {
                    that.$404.show();
                    return;
                }
                that.$failure.show();
            }).always(function() {
                that.$spinner.hide();
                that.req = null;
            });
        },

        renderUserContent: function(user) {
            var data = {}, html;
            data['user'] = user;

            html = this._userResourceTmpl({
                userData: data,
                genres: this.genres
            });
            return html;
        }
    });

    ResourceFollowingPageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-resource',
        initialize: function(options) {
            var that = this;

            DropbeatPageView.prototype.initialize.call(this, options);

            module.$('header .header-menu .menu').removeClass('selected');

            this._resourceName = options.resourceName;
            this._userResourceTmpl = module._.template(module.$('#tmpl-user-resource-following-contents').html());
            this._followItemTmpl = module._.template(module.$('#tmpl-user-follow-item').html());

            this.render();
            this.$spinner = module.$('.spinner', this.$el);
            this.$failure = module.$('.failure', this.$el);
            this.$404 = module.$('.failure-404', this.$el);
            this.$contents = module.$('.resource-contents', this.$el);

            this.$coverview = module.$('.coverview');
            this.$coverFilter = module.$('.cover-filter', this.$coverview);
            this.$coverPlaceholder = module.$('.cover-placeholder', this.$coverview);

            this.loadResource(this._resourceName);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .page-resource .btn-retry': 'onRetryBtnClicked',
                'click .section-tabs .section-tab': 'onTabClicked',
                'click .btn-follow': 'onFollowBtnClicked'
            });
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        remove: function() {
            var that = this;

            DropbeatPageView.prototype.remove.apply(this, arguments);

            if (this.req) {
                this.req.abort();
                this.req = null;
            }

            if (this.otherFollowingReq) {
                this.otherFollowingReq.abort();
                this.otherFollowingReq = null;
            }

            if (this.followReq) {
                this.followReq.abort();
                this.followReq = null;
            }

            this.musicStorage = {};
        },

        onRetryBtnClicked: function(e) {
            this.loadResource(this._resourceName);
        },

        onFollowBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $btnText = module.$('.text', $btn),
                user = this.user,
                count, $follower;

            $follower = module.$('.overview .overview-tab.followers .count');

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            if ($btn.hasClass('progress') || !user || !user.id) {
                return;
            }

            $btn.addClass('progress');


            if ($btn.hasClass('following')) {
                module.context.account.unfollow(this.user, function(success, msg) {
                    $btn.removeClass('progress');
                    if (!success) {
                        module.s.notify(module.string('failed_to_unfollow'), 'error');
                        return;
                    }
                    if ($follower.size() > 0) {
                        count = parseInt($follower.text());
                        $follower.text(count -1);
                    }
                    $btn.removeClass('following');
                    $btnText.text($btnText.data('textFollow'));
                });
            } else {
                module.context.account.follow(this.user, function(success, msg) {
                    $btn.removeClass('progress');
                    if (!success) {
                        module.s.notify(module.string('failed_to_follow'), 'error');
                        return;
                    }
                    if ($follower.size() > 0) {
                        count = parseInt($follower.text());
                        $follower.text(count + 1);
                    }
                    $btn.addClass('following');
                    $btnText.text($btnText.data('textFollowing'));
                });
            }
        },

        loadResourceFollowing: function(userId) {
            var that = this,
                html = '';

            this.$sectionContents.find('.failure').hide();
            this.$sectionContents.find('.spinner').show();


            this.otherFollowingReq = this.loadFollowing(userId,
                function(success, followings) {
                    if (!success) {
                        that.$sectionContents.find('.failure').hide();
                        return;
                    }

                    module.$.each(followings, function(idx, follow) {
                        html += that._followItemTmpl({
                            follow: follow,
                            isFollowing: false
                        });
                    });
                    that.$sectionContents.html(html);
                });

            this.otherFollowingReq.always(function() {
                that.otherFollowingReq = null;
                that.$sectionContents.find('.spinner').hide();
            });
        },

        loadFollowing: function(userId, callback) {
            var that = this, url,
                items = [],
                req;

            req = module.$.ajax({
                url: module.api.followingUserUrl,
                data:{
                    user_id:userId
                },
                contentType: 'application/json',
                dataType: 'json',
                type: 'GET'
            }).done(function(res) {
                var data;

                if (!res || !res.success || !res.data) {
                    module.s.notify(module.string('failed_to_fetch_following_info'), 'error');
                    callback(false);
                    return;
                }

                callback(true, res.data);
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus !== 'abort') {
                    module.s.notify(module.string('failed_to_fetch_following_info'), 'error');
                }
                callback(false);
            });

            return req;
        },

        loadResource: function(resourceName) {
            var that = this;

            this.$failure.hide();
            this.$spinner.show();
            this.$coverview.hide();

            if (this.req) {
                this.req.abort();
            }

            this.req = module.$.ajax({
                url:module.api.resolveObjectUrl,
                data: {
                    url: 'http://dropbeat.net/r/' + resourceName
                },
                dataType:'json',
                cache:false
            }).done(function(res) {
                var html,
                    coverImageUrl,
                    $wrapper;

                if (!res || !res.success || !res.data) {
                    that.$failure.show();
                    return;
                }

                that.genres = {};
                module.$.each(module.context.genre['dropbeat'], function(idx, genre) {
                    that.genres[genre.id] = genre.name;
                });

                that.userType = res.data.user_type;

                if (that.userType === 'user') {
                    that.user = res.data.user;
                    that.user.type = 'user';
                    html = that.renderUserContent(that.user);

                    coverImageUrl = that.user.profile_cover_image || '/images/default_user_cover.png';
                    if (!that.user.profile_cover_image) {
                        that.$coverFilter.show();
                    } else {
                        that.$coverFilter.hide();
                    }

                } else {
                    that.$404.show();
                    // not this version
                    return;
                }

                if (!html) {
                    that.$failure.show();
                    return;
                }

                that.$contents.html(html);
                that.$sectionContents = module.$('.section-contents', that.$contents);
                that.loadResourceFollowing(that.user.id);

                $wrapper = module.$('.following-box-wrapper', that.$el);
                if ($wrapper.size() > 0) {
                    that.subViews['followingSectionView'] = new FollowingSectionView({
                        $wrapper: $wrapper,
                        user: that.user
                    });
                }

                $wrapper = module.$('.followers-box-wrapper', that.$el);
                if ($wrapper.size() > 0) {
                    that.subViews['followersSectionView'] = new FollowersSectionView({
                        $wrapper: $wrapper,
                        user: that.user
                    });
                }

// set page cover view
                that.$coverPlaceholder.css({
                    backgroundImage: 'url(\'' + coverImageUrl + '\')'
                });
                that.$coverview.show();
            })
            .fail(function(jqXHR, textStatus, error) {
                if (jqXHR.status && textStatus && textStatus !== 'abort' &&
                    jqXHR.status === 400) {
                    that.$404.show();
                    return;
                }
                that.$failure.show();
            }).always(function() {
                that.$spinner.hide();
                that.req = null;
            });
        },

        renderUserContent: function(user) {
            var data = {}, html;
            data['user'] = user;

            html = this._userResourceTmpl({
                userData: data,
                genres: this.genres
            });
            return html;
        }
    });

    FollowingSectionView = DropbeatView.extend({
        tmpl: '#tmpl-section-following',
        initialize: function(options) {
            var that = this;

            module._.bindAll(this, 'loadResources');

            DropbeatView.prototype.initialize.call(this, options);
            this.userId = options.user.id;
            this.itemTmpl = module._.template(module.$('#tmpl-followable-item').html());
            this.users = {};
            this._context.user = options.user;

            this.render(options.$wrapper);
            this.$list = module.$('.following-list', this.$el);
            this.$spinner = module.$('.spinner', this.$el);
            this.$failure = module.$('.failure', this.$el);
            setTimeout(this.loadResources, 0);
        },

        events: function() {
            var events = DropbeatView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .btn-retry' : 'onRetryBtnClicked',
                'click .btn-follow' : 'onFollowBtnClicked'
            });
        },

        render: function($wrapper) {
            this.$el = module.$(this._tmpl(this._context));
            $wrapper.html(this.$el);
        },

        remove: function() {
            DropbeatView.prototype.remove.call(this);
            if (this.req) {
                this.req.abort();
                this.req = null;
            }
        },

        onRetryBtnClicked: function(e) {
            this.$failure.hide();
            this.loadResources();
        },

        onFollowBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $btnText = module.$('.text', $btn),
                $item = $btn.closest('.item'),
                $btnGroup,
                itemId = $item.data('id'),
                user = this.users[itemId];

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            $btnGroup = module.$('.item[data-id=\'' + itemId +
                '\'] .btn-follow', this.$el);

            if ($btn.hasClass('progress') || !user || !user.id) {
                return;
            }

            $btnGroup.addClass('progress');

            if ($btn.hasClass('following')) {
                module.context.account.unfollow(user, function(success, msg) {
                        $btnGroup.removeClass('progress');
                        if (!success) {
                            module.s.notify(msg || 'Failed to unfollow', 'error');
                            return;
                        }
                        $btnGroup.removeClass('following');
                        $btnGroup.find('.text').text($btnText.data('textFollow'));
                    });
            } else {
                module.context.account.follow(user, function(success, msg) {
                        $btnGroup.removeClass('progress');
                        if (!success) {
                            module.s.notify(msg || 'Failed to follow', 'error');
                            return;
                        }
                        $btnGroup.addClass('following');
                        $btnGroup.find('.text').text($btnText.data('textFollowing'));
                    });
            }
        },

        loadResources: function() {
            var that = this;

            this.$spinner.show();

            if (this.req) {
                this.req.abort();
            }

            this.req = module.$.ajax({
                url: module.api.followingUserUrl,
                data: {
                    user_id: module.context.account.id
                },
                contentType: 'application/json',
                dataType: 'json',
                type: 'GET'
            }).done(function(res) {
                var html = '';

                if (!res || !res.success) {
                    that.$failure.show();
                    return;
                }

                module.$.each(res.data.reverse(), function(idx, follow) {
                    var prefix, id;

                    if (idx > 2) {
                        return false;
                    }

                    if (follow.user_type === 'user') {
                        prefix = 'u';
                    } else if (follow.user_type === 'channel') {
                        prefix = 'c';
                    } else if (follow.user_type === 'artist') {
                        prefix = 'a';
                    } else {
                        return true;
                    }

                    id = prefix + follow.id;

                    that.users[id] = follow;

                    html += that.itemTmpl({
                        id: id,
                        item: follow
                    });
                });
                that.$list.html(html);
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus !== 'abort') {
                    that.$failure.show();
                }
            }).always(function() {
                that.req = null;
                that.$spinner.hide();
            });
        }
    });


    FollowersSectionView = DropbeatView.extend({
        tmpl: '#tmpl-section-followers',
        initialize: function(options) {
            var that = this;

            module._.bindAll(this, 'loadResources');

            DropbeatView.prototype.initialize.call(this, options);
            this.userId = options.user.id;
            this.itemTmpl = module._.template(module.$('#tmpl-followable-item').html());
            this.users = {};
            this._context.user = options.user;

            this.render(options.$wrapper);
            this.$list = module.$('.followers', this.$el);
            this.$spinner = module.$('.spinner', this.$el);
            this.$failure = module.$('.failure', this.$el);
            setTimeout(this.loadResources, 0);
        },

        events: function() {
            var events = DropbeatView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .btn-retry' : 'onRetryBtnClicked',
                'click .btn-follow' : 'onFollowBtnClicked'
            });
        },

        render: function($wrapper) {
            this.$el = module.$(this._tmpl(this._context));
            $wrapper.html(this.$el);
        },

        remove: function() {
            DropbeatView.prototype.remove.call(this);
            if (this.req) {
                this.req.abort();
                this.req = null;
            }
        },

        onRetryBtnClicked: function(e) {
            this.$failure.hide();
            this.loadResources();
        },

        onFollowBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $btnText = module.$('.text', $btn),
                $item = $btn.closest('.item'),
                $btnGroup,
                itemId = $item.data('id'),
                user = this.users[itemId];

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }

            $btnGroup = module.$('.item[data-id=\'' + itemId +
                '\'] .btn-follow', this.$el);

            if ($btn.hasClass('progress') || !user || !user.id) {
                return;
            }

            $btnGroup.addClass('progress');

            if ($btn.hasClass('following')) {
                module.context.account.unfollow(user, function(success, msg) {
                        $btnGroup.removeClass('progress');
                        if (!success) {
                            module.s.notify(msg || 'Failed to unfollow', 'error');
                            return;
                        }
                        $btnGroup.removeClass('following');
                        $btnGroup.find('.text').text($btnText.data('textFollow'));
                    });
            } else {
                module.context.account.follow(user, function(success, msg) {
                        $btnGroup.removeClass('progress');
                        if (!success) {
                            module.s.notify(msg || 'Failed to follow', 'error');
                            return;
                        }
                        $btnGroup.addClass('following');
                        $btnGroup.find('.text').text($btnText.data('textFollowing'));
                    });
            }
        },


        loadResources: function() {
            var that = this;

            this.$spinner.show();

            if (this.req) {
                this.req.abort();
            }

            this.req = module.$.ajax({
                url:module.api.followersUserUrl,
                data:{
                    user_id:this.userId
                },
                contentType: 'application/json',
                dataType: 'json',
                type: 'GET'
            }).done(function(res) {
                var html = '';

                if (!res || !res.success) {
                    that.$failure.show();
                    return;
                }

                module.$.each(res.data.reverse(), function(idx, follow) {
                    var prefix, id;

                    if (idx > 2) {
                        return false;
                    }

                    if (follow.user_type === 'user') {
                        prefix = 'u';
                    } else if (follow.user_type === 'channel') {
                        prefix = 'c';
                    } else if (follow.user_type === 'artist') {
                        prefix = 'a';
                    } else {
                        return true;
                    }

                    id = prefix + follow.id;

                    that.users[id] = follow;

                    html += that.itemTmpl({
                        id: id,
                        item: follow
                    });
                });
                that.$list.html(html);
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus !== 'abort') {
                    that.$failure.show();
                }
            }).always(function() {
                that.req = null;
                that.$spinner.hide();
            });
        }
    });


/////////////////
// Modal Views //
/////////////////

    module.view.FeedbackView = (function() {
        var FeedbackView, view, subModule = {};

        FeedbackView = DropbeatView.extend({
            tmpl: '#tmpl-modal-feedback',
            initialize: function(options) {
                DropbeatView.prototype.initialize.call(this, options);
                module._.bindAll(this, 'show', 'hide');
                this._modal = this.tmpl.replace('tmpl-', '');
            },
            show: function() {
                var that = this;

                this.$modal = module.$(this._tmpl(module.$.extend({}, this._context, {
                    title: this._title,
                    text: this._text,
                    confirmText: this._confirmText
                })));
                module.$('#modal').append(this.$modal);
                this.$modal.find('textarea').autogrow();
                this.$modal.modal({ show: true, backdrop:'static'})
                    .on('hidden.bs.modal', function() {
                        if (view) {
                            view.close ? view.close() : view.remove();
                            view = null;
                        }
                    })
                    .on('click', '.submit-btn', function() {
                        that.doSubmit();
                    })
            },
            hide: function(callback) {
                module.$(this._modal)
                    .on('hidden.bs.modal', function() {
                        callback && callback();
                    });
                module.$(this._modal).modal('hide');
            },
            remove: function() {
                DropbeatView.prototype.remove.call(this);
                module.$(this._modal).remove();
            },
            doSubmit: function() {
                var that = this,
                    $btn = module.$(this._modal + ' .submit-btn'),
                    $textarea = module.$(this._modal + ' .feedback-input'),
                    senderEmail,
                    content,
                    errors = [];

                if ($btn.attr('disabled')) {
                    return;
                }
                if (module.context.account) {
                    senderEmail = module.context.account.email;
                } else {
                    senderEmail =
                        module.$(this._modal + ' .sender-email-input').val();
                }

                content = $textarea.val();

                if (!validateEmail(senderEmail)) {
                    errors.push({msg:'Invalid email format'});
                }

                if (content === '') {
                    errors.push({msg:'Feedback content is required.'});
                }

                if (errors.length > 0) {
                    that.showError(errors);
                    return;
                }

                $textarea.attr('readonly', 'readonly');
                $btn.attr('disabled', 'true')
                    .addClass('progress');

                module.$.ajax({
                    url: module.api.feedbackUrl,
                    contentType: 'application/json',
                    dataType: 'json',
                    data: JSON.stringify({
                        sender: senderEmail,
                        content: content
                    }),
                    type: 'POST'
                }).done(function(res) {
                    that.hide();
                    module.s.notify('Feedback registered', 'success');
                }).fail(function(res) {
                    if (res.responseJSON && res.responseJSON.error) {
                        that.showError(res.responseJSON.error);
                    } else {
                        that.showError('undefined error');
                    }
                }).always(function(res) {
                    $btn.removeAttr('disabled');
                    $btn.removeClass('progress');
                    module.$('.text', $btn).html('Submit');

                    $textarea.removeAttr('readonly');
                });
            },
            showError: function(errors) {
                var errorArray,
                    $errorPrompt = module.$('.error-prompt', this.$modal);
                if (!(errors instanceof Array)) {
                    errorArray = [{ msg: errors}];
                } else {
                    errorArray = errors;
                }
                $errorPrompt.html('');
                module.$.each(errorArray, function(idx, el) {
                    $errorPrompt.append(el.msg + '<br/>');
                    if (el.$target) {
                        el.$target.addClass('error');
                    }
                });
                $errorPrompt.slideDown();
            }
        });

        subModule.show = module.$.proxy(function() {
            if (view) {
                this.hide(module.$.proxy(function() {
                    this.show();
                }, this));
                return;
            }
            view = new FeedbackView();
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    }());

    module.view.AlertView = (function() {
        var AlertView = DropbeatView.extend({
                tmpl: '#tmpl-modal-alert',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._title = options.title || '';
                    this._text = options.text || '';
                    this._confirmText = options.confirmText || module.string('confirm');
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(module.$.extend({}, this._context, {
                            title: this._title,
                            text: this._text,
                            confirmText: this._confirmText
                        })));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new AlertView({
                    title: options.title,
                    text: options.text,
                    confirmText: options.confirmText,
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.ConfirmView = (function() {
        var ConfirmView = DropbeatView.extend({
                tmpl: '#tmpl-modal-confirm',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAlways = options.onAlways;
                    this._title = options.title || '';
                    this._text = options.text || '';
                    this._confirmText = options.confirmText || module.string('confirm');
                    this._closeText = options.closeText || module.string('close');
                    this._isConfirmed = false;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(module.$.extend({}, this._context, {
                            title: this._title,
                            text: this._text,
                            closeText: this._closeText,
                            confirmText: this._confirmText
                        })));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('click', '.btn-confirm', function() {
                            that._isConfirmed = true;
                            that.hide();
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways(that._isConfirmed);
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new ConfirmView({
                    title: options.title,
                    text: options.text,
                    confirmText: options.confirmText,
                    closeText: options.closeText,
                    onAlways: function(confirmed) {
                        callback && callback(confirmed);
                        if (view) {
                            view.close ? view.close() : view.remove();
                            view = null;
                        }
                    }
                });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.NicknameUpdateView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-nickname-update',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._nickname = options.nickname;
                    this._onAlways = options.onAlways;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    module.view.placeholderCompat();

                    module.$('.input-nickname').val(this._nickname);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('submit', '#form-nickname-update', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('click', '.btn-submit', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    $modal.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    $modal.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                    if (this.req) {
                        this.req.abort();
                    }
                },

                validate: function(data) {
                    var nickname = data.nickname,
                        isValid = true,
                        $modal = module.$('#modal-nickname-update'),
                        $nicknameFrame = module.$('.nickname-input-wrapper', $modal),
                        error;

                    if (!nickname) {
                        error = 'Required field';
                    } else if (nickname.length < 3) {
                        error = module.string('input_must_be_greater_than_%d', 3);
                    } else if (nickname.length > 20) {
                        error = module.string('input_must_be_less_than_%d', 20);
                    }

                    module.$('.input-wrapper', $modal).removeClass('error');
                    module.$('.input-wrapper .errors', $modal).empty();
                    if (error) {
                        $nicknameFrame.addClass('error');
                        $nicknameFrame.find('.errors').html(error);
                        isValid = false;
                    }

                    return isValid;
                },

                trySubmit: function() {
                    var that = this,
                        $modal = module.$('#modal-nickname-update'),
                        $form = module.$('#form-nickname-update', $modal),
                        $nicknameFrame = module.$('.nickname-input-wrapper', $modal),
                        $submitBtn = module.$('.btn-signup', $modal),
                        formArray = $form.serializeArray(),
                        data = {};


                    if ($submitBtn.hasClass('progress')) {
                        return;
                    }

                    module.$.each(formArray, function(idx, s) {
                        data[s.name] = s.value;
                    });

                    if (!this.validate(data)) {
                        return;
                    }

                    $submitBtn.addClass('progress');
                    $modal.find('input').attr('readonly', 'readonly');


                    if (this.req) {
                        this.req.abort();
                    }

                    this.req = module.$.ajax({
                        url: module.api.changeNickname,
                        data: JSON.stringify(data),
                        dataType:'json',
                        type: 'post',
                        contentType: 'application/json'
                    }).done(function(res) {
                        if (!res || !res.success) {
                            $nicknameFrame.addClass('error');
                            $nicknameFrame.find('.errors').html('Username already exists');
                            return;
                        }
                        window.location.reload();
                    }).fail(function(jqXHR, textStatus, error) {
                        if (textStatus !== 'abort') {
                            module.s.notify(module.string('failed_to_submit'), 'error');
                        }
                    }).always(function() {
                        that.req = null;
                        $submitBtn.removeClass('progress');
                        $modal.find('input').removeAttr('readonly');
                    });
                }

            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new View({
                nickname: options.nickname,
                onAlways: function() {
                    callback && callback();
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.UserUrlUpdateView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-user-url-update',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._resourceName = options.resourceName;
                    this._onAlways = options.onAlways;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    module.view.placeholderCompat();

                    module.$('.input-user-url').val(this._resourceName);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('submit', '#form-user-url-update', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('click', '.btn-submit', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    $modal.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    $modal.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                    if (this.req) {
                        this.req.abort();
                    }
                },

                validate: function(data) {
                    var resourceName = data.resource_name,
                        isValid = true,
                        $modal = module.$('#modal-user-url-update'),
                        $urlFrame = module.$('.user-url-input-wrapper', $modal),
                        error;

                    if (!resourceName) {
                        error = 'Required field';
                    } else if (resourceName.length < 4) {
                        error = module.string('input_must_be_greater_than_%d', 4);
                    } else if (resourceName.length > 20) {
                        error = module.string('input_must_be_less_than_%d', 20);
                    } else if (resourceName.search(/^[a-z0-9]+$/) === -1) {
                        error = module.string('invalid_character');
                    }

                    module.$('.input-wrapper', $modal).removeClass('error');
                    module.$('.input-wrapper .errors', $modal).empty();
                    if (error) {
                        $urlFrame.addClass('error');
                        $urlFrame.find('.errors').html(error);
                        isValid = false;
                    }

                    return isValid;
                },

                trySubmit: function() {
                    var that = this,
                        $modal = module.$('#modal-user-url-update'),
                        $form = module.$('#form-user-url-update', $modal),
                        $urlFrame = module.$('.user-url-input-wrapper', $modal),
                        $submitBtn = module.$('.btn-submit', $modal),
                        formArray = $form.serializeArray(),
                        data = {};


                    if ($submitBtn.hasClass('progress')) {
                        return;
                    }

                    module.$.each(formArray, function(idx, s) {
                        data[s.name] = s.value;
                    });

                    if (!this.validate(data)) {
                        return;
                    }

                    $submitBtn.addClass('progress');
                    $modal.find('input').attr('readonly', 'readonly');


                    if (this.req) {
                        this.req.abort();
                    }

                    this.req = module.$.ajax({
                        url: module.api.changeResourceNameUrl,
                        data: JSON.stringify(data),
                        dataType:'json',
                        type: 'post',
                        contentType: 'application/json'
                    }).done(function(res) {
                        if (!res || !res.success) {
                            $urlFrame.addClass('error');
                            $urlFrame.find('.errors').html(module.string('url_already_exists'));
                            return;
                        }
                        window.location = '/r/' + data.resource_name;
                    }).fail(function(jqXHR, textStatus, error) {
                        if (textStatus !== 'abort') {
                            module.s.notify('Failed to submit', 'error');
                        }
                    }).always(function() {
                        that.req = null;
                        $submitBtn.removeClass('progress');
                        $modal.find('input').removeAttr('readonly');
                    });
                }

            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new View({
                resourceName: options.resourceName,
                onAlways: function() {
                    callback && callback();
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.GenreSelectView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-genre-select',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._originalIds = options.favoriteIds.slice(0);
                    this._favoriteIds = options.favoriteIds.slice(0);
                    this._genres = options.genres;
                    module.$.extend(this._context, {
                        genres: this._genres,
                        favoriteIds: this._favoriteIds
                    });

                    this._onAlways = options.onAlways;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal,
                        $genres;

                    $modal = module.$(this._tmpl(this._context)),
                    module.$('#modal').append($modal);
                    module.view.placeholderCompat();

                    $genres = module.$('.genres', $modal);

                    $modal.modal({ show: true, backdrop:'static'})
                        .on('click', '.genre', function(e) {
                            var $genre = module.$(e.currentTarget),
                                id = $genre.data('id'),
                                idx;

                            idx = that._favoriteIds.indexOf(id);

                            if (idx > -1) {
                                that._favoriteIds.splice(idx, 1);
                                $genre.removeClass('selected');
                                $genre.find('.check i')
                                    .addClass('glyphicons-unchecked')
                                    .removeClass('glyphicons-check');
                            } else {
                                that._favoriteIds.push(id);
                                $genre.addClass('selected');
                                $genre.find('.check i')
                                    .removeClass('glyphicons-unchecked')
                                    .addClass('glyphicons-check');
                            }
                        })
                        .on('click', '.btn-submit', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    $modal.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    $modal.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                    if (this.req) {
                        this.req.abort();
                    }
                },

                validate: function() {
                    var $errors = module.$('#modal-genre-select .errors');

                    if (this._favoriteIds.length === 0) {
                        $errors.show();
                        $errors.html(module.string('at_least_one_genre'));
                        return false;
                    }
                    $errors.empty();
                    $errors.hide();

                    return true;
                },

                trySubmit: function() {
                    var that = this,
                        $submitBtn = module.$('.btn-submit', $modal),
                        $modal = module.$('#modal-genre-select'),
                        addIds = [],
                        delIds = [],
                        requests = [];

                    if ($submitBtn.hasClass('progress')) {
                        return;
                    }

                    module.$.each(this._favoriteIds, function(idx, id) {
                        if (that._originalIds.indexOf(id) === -1) {
                            addIds.push(id);
                        }
                    });

                    module.$.each(this._originalIds, function(idx, id) {
                        if (that._favoriteIds.indexOf(id) === -1) {
                            delIds.push(id);
                        }
                    });

                    if (!this.validate()) {
                        return;
                    }

                    $submitBtn.addClass('progress');

                    if (this.addReq) {
                        this.addReq.abort();
                    }

                    if (this.delReq) {
                        this.delReq.abort();
                    }

                    if (addIds.length > 0) {
                        this.addReq = module.$.ajax({
                            url: module.api.genreAddFavoriteUrl,
                            data: JSON.stringify({
                                ids: addIds
                            }),
                            dataType:'json',
                            type: 'post',
                            contentType: 'application/json'
                        });
                        requests.push(this.addReq);
                    }

                    if (delIds.length > 0) {
                        this.delReq = module.$.ajax({
                            url: module.api.genreDelFavoriteUrl,
                            data: JSON.stringify({
                                ids: delIds
                            }),
                            dataType:'json',
                            type: 'post',
                            contentType: 'application/json'
                        });
                        requests.push(this.delReq);
                    }

                    if (requests.length === 0) {
                        this.hide();
                        return;
                    }
                    module.$.when.apply(this, requests)
                        .done(function() {
                            var error,
                                params = Array.prototype.slice.call(arguments),
                                resArray = [],
                                success = true;

                            if (requests.length === 1) {
                                resArray.push(params[0]);
                            } else {
                                module.$.each(params, function(idx, obj) {
                                    resArray.push(obj[0]);
                                });
                            }
                            module.$.each(resArray, function(idx, res) {
                                if (!res.success) {
                                    error = error || res.error;
                                    success = false;
                                }
                            });
                            if (!success) {
                                module.s.notify(error || module.string('failed_to_submit'), 'error');
                                return;
                            }
                            window.location.reload();
                        }).fail(function(jqXHR, textStatus, error) {
                            if (textStatus !== 'abort') {
                                module.s.notify(module.string('failed_to_submit'), 'error');
                            }
                        }).always(function() {
                            that.addReq = null;
                            that.delReq = null;
                            $submitBtn.removeClass('progress');
                        });
                }

            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                this.hide(module.$.proxy(function() {
                    this.show(options, callback);
                }, this));
                return;
            }

            view = new View({
                genres: options.genres,
                favoriteIds: options.favoriteGenreIds,
                onAlways: function() {
                    callback && callback();
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.UserDescUpdateView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-user-desc-update',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._desc = options.desc;
                    this._onAlways = options.onAlways;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    module.view.placeholderCompat();

                    module.$('.input-desc').val(this._desc);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('submit', '#form-desc-update', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('click', '.btn-submit', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    $modal.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    $modal.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                    if (this.req) {
                        this.req.abort();
                    }
                },

                validate: function(data) {
                    var desc = data.desc,
                        isValid = true,
                        $modal = module.$('#modal-user-desc-update'),
                        $descFrame = module.$('.desc-input-wrapper', $modal),
                        error;

                    if (desc.length > 2000) {
                        error = module.string('input_must_be_less_than_%d', 2000);
                    }

                    module.$('.input-wrapper', $modal).removeClass('error');
                    module.$('.input-wrapper .errors', $modal).empty();
                    if (error) {
                        $descFrame.addClass('error');
                        $descFrame.find('.errors').html(error);
                        isValid = false;
                    }

                    return isValid;
                },

                trySubmit: function() {
                    var that = this,
                        $modal = module.$('#modal-user-desc-update'),
                        $descFrame = module.$('.desc-input-wrapper', $modal),
                        $submitBtn = module.$('.btn-submit', $modal),
                        desc = $descFrame.find('.input-desc').val(),
                        data = {};


                    if ($submitBtn.hasClass('progress')) {
                        return;
                    }

                    data.desc = desc;

                    if (!this.validate(data)) {
                        return;
                    }

                    $submitBtn.addClass('progress');
                    $modal.find('input').attr('readonly', 'readonly');

                    if (this.req) {
                        this.req.abort();
                    }

                    this.req = module.$.ajax({
                        url: module.api.changeUserDescUrl,
                        data: JSON.stringify(data),
                        dataType:'json',
                        type: 'post',
                        contentType: 'application/json'
                    }).done(function(res) {
                        if (!res || !res.success) {
                            $descFrame.addClass('error');
                            $descFrame.find('.errors').html(res.error);
                            return;
                        }
                        window.location.reload();
                    }).fail(function(jqXHR, textStatus, error) {
                        if (textStatus !== 'abort') {
                            module.s.notify('Failed to submit', 'error');
                        }
                    }).always(function() {
                        that.req = null;
                        $submitBtn.removeClass('progress');
                        $modal.find('input').removeAttr('readonly');
                    });
                }

            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new View({
                desc: options.desc,
                onAlways: function() {
                    callback && callback();
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.FollowToDownloadView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-follow-to-download',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);

                    this._resourcePath = options.resourcePath;
                    this._onAlways = options.onAlways;
                    module._.bindAll(this, 'show', 'hide', 'onFollowBtnClicked', 'startDownload');
                },

                show: function() {
                    var that = this,
                        p,
                        idx,
                        userResourcePath;

                    p = this._resourcePath;
                    if (p.endsWith('/')) {
                        p = p.substring(0, p.length -1);
                    }

                    idx = p.lastIndexOf('/');
                    if (idx === -1) {
                        module.s.notify(module.string('invalid_resource_path'), 'error');
                        this._onAlways && this._onAlways();
                        return;
                    }

                    userResourcePath = p.substring(0, idx);

                    this.followingReq = module.$.ajax({
                        url: module.api.followingUserUrl,
                        data: {
                            user_id: module.context.account.id
                        },
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'GET',
                        cache:false
                    }).always(function() {
                        that.followingReq = null;
                    });

                    this.artistReq = module.$.ajax({
                        url:module.api.resolveObjectUrl,
                        data: {
                            url: 'http://dropbeat.net' + userResourcePath
                        },
                        dataType:'json'
                    }).always(function() {
                        that.artistReq = null;
                    });

                    this.trackReq = module.$.ajax({
                        url:module.api.resolveObjectUrl,
                        data: {
                            url: 'http://dropbeat.net' + this._resourcePath
                        },
                        dataType:'json'
                    }).always(function() {
                        that.trackReq = null;
                    });
                    module.$.when(this.followingReq, this.artistReq, this.trackReq)
                        .done(function(followingObj, artistObj, trackObj) {
                            var $modal,
                                followingRes = followingObj[0],
                                artistRes = artistObj[0],
                                trackRes = trackObj[0],
                                userIds = [],
                                isFollowing,
                                user,
                                track,
                                downloadable = false;

                            if (!followingRes.success ||
                                !artistRes.success ||
                                !trackRes.success) {
                                module.s.notify('Failed to load data', 'error');
                                that._onAlways && that._onAlways();
                                return;
                            }

                            module.$.each(followingRes.data, function(idx, follow) {
                                if (follow.id && follow.user_type === 'user') {
                                    userIds.push(follow.id);
                                }
                            });

                            user = artistRes.data.user;
                            isFollowing = userIds.indexOf(user.id) > -1;
                            track = new module.music.DropbeatMusic(trackRes.data);

                            that.user = user;
                            that.user.type = 'user';
                            that.track = track;

                            if (isFollowing || module.context.account.id === user.id) {
                                downloadable = true;
                            }

                            $modal = that._tmpl(module.$.extend(that._context, {
                                user: user,
                                trackInfo: track,
                                downloadable: downloadable
                            }));

                            module.$('#modal').append($modal);

                            that.$el = module.$('#modal').find('#modal-follow-to-download');
                            that.$followSection = module.$('.follow-section', that.$el);
                            that.$downloadSection = module.$('.download-section', that.$el);
                            that.$followingNotice =
                                module.$('.download-section .following-notice', that.$el);

                            that.$el.modal({ show: true, backdrop:'static'})
                                .on('hidden.bs.modal', function() {
                                    that._onAlways && that._onAlways();
                                })
                                .on('click', '.btn-download', that.startDownload)
                                .on('click', '.btn-follow', that.onFollowBtnClicked);
                        })
                        .fail(function(jqXHR, textStatus, error) {
                            if (textStatus !== 'abort') {
                                module.s.notify(module.string('failed_to_load_data'), 'error');
                            }
                            that._onAlways && that._onAlways();
                        });
                },

                startDownload: function() {
                    var downloadUrl;

                    downloadUrl = this.track.stream_url;
                    downloadUrl = downloadUrl.replace('dl=0', 'dl=1');
                    window.open(downloadUrl);
                    this._onAlways && this._onAlways();

// Log download to us
                    module.coreLog('download', this.track);
                },

                onFollowBtnClicked: function(e) {
                    var $btn = module.$(e.currentTarget),
                        user = this.user,
                        that = this;

                    if (!module.context.account) {
                        module.view.NeedSigninView.show();
                        return;
                    }

                    if ($btn.hasClass('progress') || !user || !user.id) {
                        return;
                    }

                    $btn.addClass('progress');


                    module.context.account.follow(this.user, function(success, msg) {
                        $btn.removeClass('progress');
                        if (!success) {
                            module.s.notify('Failed to follow', 'error');
                            that.hide();
                            return;
                        }
                        that.$followSection.removeClass('selected');
                        that.$downloadSection.addClass('selected');
                        that.$followingNotice.show();
                    });
                },

                hide: function(callback) {
                    this.$el.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    this.$el.modal('hide');
                },

                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    if (this.$el) {
                        this.$el.off('click', '.btn-download', this.startDownload)
                        this.$el.off('click', '.btn-follow', this.onFollowBtnClicked);
                    }
                    if (this.trackReq) {
                        this.trackReq.abort();
                        this.trackReq = null;
                    }
                    if (this.artistReq) {
                        this.artistReq.abort();
                        this.artistReq = null;
                    }
                    if (this.followingReq) {
                        this.followingReq.abort();
                        this.followingReq = null;
                    }
                    if (this.followReq) {
                        this.followReq.abort();
                        this.followReq = null;
                    }
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(resourcePath, callback) {
            if (view) {
                return;
            }
            view = new View({
                resourcePath: resourcePath,
                onAlways: function(url) {
                    callback && callback(url);
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());


    module.view.ImageUploadCropView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-image-uploader',
                initialize: function(options) {
                    var $modal, description;

                    module._.bindAll(this, 'postImageProgress');
                    DropbeatView.prototype.initialize.call(this, options);

                    this._imageType = options.imageType;
                    this._aspectRatio = options.aspectRatio;
                    this._width = options.width;
                    this._height = options.height;
                    if (!this._aspectRatio && this._width && this._height) {
                        this._aspectRatio = this._width  / this._height;
                    }

                    this._onAlways = options.onAlways;
                    module._.bindAll(this, 'show', 'hide');

                    description = options.desc || '';

                    module.$.extend(this._context, {
                        title: options.title || 'Upload Image',
                        desc: description,
                        imageUrl: options.imageUrl || ''
                    });
                    $modal = this._tmpl(this._context);
                    module.$('#modal').append($modal);
                    this.$el = module.$('#modal').find('#modal-image-uploader');

                    this.$fileSelectSection = module.$('.file-select-section', this.$el);
                    this.$imageFileInputWrapper = module.$('.image-input-wrapper', this.$fileSelectSection);
                    this.$imageUploadBtn = module.$('.btn-upload-image', this.$fileSelectSection);

                    this.$uploadSection = module.$('.upload-section', this.$el);
                    this.$imageUploadCancelBtn = module.$('.btn-cancel', this.$uploadSection);
                    this.$imageProgressFrame = module.$('.image-progress-wrapper', this.$uploadSection);

                    this.$imageProgress = module.$('.progress', this.$imageProgressFrame);
                    this.$imageProgressPercent = module.$('.progress .progress-label',
                        this.$imageProgressFrame);

                    this.$cropper = module.$('#cropper-frame', this.$el);
                    this.$cropperImg = module.$('img', this.$cropper);
                },

                events: function() {
                    var events = DropbeatView.prototype.events;
                    if (module._.isFunction(events)) {
                        events = events.call(this);
                    }

                    return module.$.extend ({}, events, {
                        'change .file-select-section input[type=\'file\']': 'onImageCropStart',
                        'click .file-select-section .btn-upload-image': 'onImageUploadBtnClicked',
                        'click .btn-submit' : 'onImageSubmitBtnClicked'
                    });
                },

                onImageUploadBtnClicked: function(e) {
                    var $btn = this.$imageFileInputWrapper.find('input[type=\'file\']');
                    $btn.click();
                },

                onImageSubmitBtnClicked: function(e) {
                    var that = this,
                        c,
                        deferreds = [];

                    c = this.$cropperImg.cropper('getCroppedCanvas', {
                        width: this._width,
                        height: this._height
                    });

                    this.switchDialogMode('upload');

                    this.$imageProgressFrame.show();
                    this.$imageUploadBtn.hide();
                    this.$imageProgressPercent.removeClass('error');

                    deferreds.push(module.utils.canvasToBlob(c));
                    deferreds.push(module.utils.getUploadCredential());

                    module.$.when.apply(this, deferreds).done(function(dataBlob, credential) {
                        var key = credential[0],
                            host = credential[1];

                        that.imageUploadReq = module.utils.uploadImage(
                                host, key, dataBlob, that._imageType, that.postImageProgress,
                            function(success, url) {
                                if (!success) {
                                    if (!url || url !== 'abort') {
                                        that.resetImageFileUploader();
                                        that.$imageUploadBtn.css('display', 'inline-block');
                                        that.$imageProgressPercent
                                            .addClass('error').text('Failed to upload');
                                        that.$imageProgress.progressbar({
                                            max:100,
                                            value: 0
                                        });
                                    }
                                    return;
                                }
                                that.uploadedUrl = url;
                                that.$el.modal('hide');
                            });
                        that.imageUploadReq.always(function() {
                            that.imageUploadReq = null;
                        });
                    }).fail(function(msg) {
                        if (msg !== 'canceled') {
                            module.s.notify(module.string('failed_to_authenticate'), 'error');
                            return;
                        }
                    });
                },

                onImageUploadCancelBtnClicked: function(e) {
                    if (this.imageUploadReq) {
                        this.imageUploadReq.abort();
                    }
                    this.resetImageFileUploader();
                    this.$imageUploadCancelBtn.hide();
                    this.$imageUploadBtn.css('display', 'inline-block');
                    this.$imageProgressFrame.hide();
                },

                onImageCropStart: function(e) {
                    var that = this,
                        $form = this.$imageFileInputWrapper,
                        $file = $form.find('input[type=\'file\']')[0],
                        file,
                        reader;

                    if (!$file.files || $file.files.length === 0) {
                        return;
                    }

                    file = $file.files[0];
                    reader = new FileReader();
                    reader.onloadend = onImageLoaded;
                    reader.onerror = onImageError;
                    reader.readAsDataURL(file);

                    this.switchDialogMode('prepare');

                    function onImageLoaded() {
                        var result = reader.result,
                            img = that.$cropperImg[0];

                        img.onload = function() {
                            that.startCropper();
                        };
                        that.$cropperImg.attr('src', result);
                        that.$cropperImg.prop('src', result);
                    }

                    function onImageError() {
                        module.s.notify('Failed to load image', 'error');
                        that.resetImageFileUploader();
                        that.switchDialogMode('select');
                    }
                },

                startCropper: function() {
                    this.switchDialogMode('crop');
                    this.$cropperImg.cropper({
                        aspectRatio: this._aspectRatio,
                        strict: true,
                        responsive: false,
                        checkImageOrigin: false,
                        modal: true,
                        guides: true,
                        center: true,
                        highlight: true,
                        background:false,
                        autoCrop: true,
                        dragCrop: true,
                        movable: true,
                        rotatable: true,
                        scalable: true,
                        zoomable: true,
                        mouseWheelZoom: true,
                        touchDragZoom: false,
                        cropBoxMovable: true,
                        cropBoxResizable: true,
                        doubleClickToggle: false,
                        crop: function(e) {
                        }
                    });
                },

                switchDialogMode: function(mode) {
                    var $dialog = this.$el.find('.modal-dialog');

                    $dialog.removeClass('modal-sm')
                        .removeClass('modal-lg')
                        .removeClass('select-mode')
                        .removeClass('crop-mode')
                        .removeClass('prepare-mode')
                        .removeClass('upload-mode');

                    if (mode === 'prepare') {
                        $dialog.addClass('prepare-mode')
                            .addClass('modal-sm');

                    } else if (mode === 'select') {
                        $dialog.addClass('select-mode');

                    } else if (mode === 'upload') {
                        $dialog.addClass('upload-mode')
                            .addClass('modal-sm');
                    } else if (mode === 'crop') {
                        $dialog.addClass('crop-mode')
                            .addClass('modal-lg');
                    }
                },

                postImageProgress: function(percent) {
                    if (percent < 100 && percent > 0) {
                        this.$imageProgressPercent.text(percent + '%');
                    } else if (percent > 0) {
                        this.$imageProgressPercent.text('Processing..');
                    } else {
                        this.$imageProgressPercent.text('preparing..');
                    }
                    this.$imageProgress.progressbar({
                        max:100,
                        value: percent < 0 || percent === 100 ? false : percent
                    });
                },

                resetImageFileUploader: function() {
                    this.$imageFileInputWrapper.html(
                        '<input class=\'visuallyhidden\' type=\'file\' name=\'image\' accept=\'image/*\'/>');
                },

                show: function() {
                    var that = this;

                    this.$el.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways(that.uploadedUrl);
                        });
                },

                hide: function(callback) {
                    this.$el.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    this.$el.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    if (this.req) {
                        this.req.abort();
                        this.req = null;
                    }
                    if (this.imageUploadReq) {
                        this.imageUploadReq.abort();
                        this.imageUploadReq = null;
                    }
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            options = options || {};
            if (view) {
                return;
            }
            view = new View({
                imageType: options.imageType,
                width: options.width,
                height: options.height,
                aspectRatio: options.aspectRatio,
                onAlways: function(url) {
                    callback && callback(url);
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.NeedSigninView = (function() {
        var NeedSigninView = DropbeatView.extend({
                tmpl: '#tmpl-modal-need-signin',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAlways = options.onAlways;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('click', '.btn-signin-with-facebook', function() {
                            module.requestFacebookSignin('modal');
                        })
                        .on('click', '.btn-signup-with-email', function() {
                            $modal.modal('hide');
                            module.view.SignupWithEmailView.show();
                        })
                        .on('click', '.btn-signin-with-email', function() {
                            $modal.modal('hide');
                            module.view.SigninWithEmailView.show();
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    $modal.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    $modal.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new NeedSigninView({
                onAlways: function() {
                    callback && callback();
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.SigninView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-signin',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAlways = options.onAlways;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('click', '.btn-signin-with-facebook', function() {
                            module.requestFacebookSignin('modal');
                        })
                        .on('click', '.btn-signin-with-email', function() {
                            module.view.SigninWithEmailView.show();
                            $modal.modal('hide');
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    $modal.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    $modal.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new View({
                onAlways: function() {
                    callback && callback();
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.SignupView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-signup',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAlways = options.onAlways;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('click', '.btn-signin-with-facebook', function() {
                            module.requestFacebookSignin('modal');
                        })
                        .on('click', '.btn-signup-with-email', function() {
                            module.view.SignupWithEmailView.show();
                            $modal.modal('hide');
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    $modal.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    $modal.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new View({
                onAlways: function() {
                    callback && callback();
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.SignupWithEmailView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-signup-with-email',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAlways = options.onAlways;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    module.view.placeholderCompat();

                    $modal.modal({ show: true, backdrop:'static'})
                        .on('submit', '#signup-form', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('click', '.btn-signup', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    $modal.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    $modal.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                    if (this.req) {
                        this.req.abort();
                    }
                },
                validate: function(data) {
                    var email = data.email,
                        nickname = data.nickname,
                        firstname = data.first_name,
                        lastname = data.last_name,
                        password = data.password,
                        passwordConfirm = data.password_confirm,
                        errors = {},
                        isValid = true,
                        $modal = module.$('#modal-signup-with-email'),
                        $frames = {
                            email: module.$('.email-input-wrapper', $modal),
                            nickname : module.$('.nickname-input-wrapper', $modal),
                            firstname : module.$('.firstname-input-wrapper', $modal),
                            lastname : module.$('.lastname-input-wrapper', $modal),
                            password : module.$('.password-input-wrapper', $modal),
                            passwordConfirm : module.$('.password-confirm-input-wrapper', $modal)
                        };

                    if (!email) {
                        errors['email'] = module.string('required_field');
                    } else if (!validateEmail(email)) {
                        errors['email'] = 'Invalid email format';
                    }

                    if (!nickname) {
                        errors['nickname'] = module.string('required_field');
                    } else if (nickname.length < 4) {
                        errors['nickname'] = module.string('input_must_be_greater_than_%d', 4);
                    } else if (nickname.length > 20) {
                        errors['nickname'] = module.string('input_must_be_less_than_%d', 20);
                    }

                    if (!lastname) {
                        errors['lastname'] = module.string('required_field');
                    } else if (lastname.length > 20) {
                        errors['lastname'] = module.string('input_must_be_less_than_%d', 20);
                    }

                    if (!firstname) {
                        errors['firstname'] = module.string('required_field');
                    } else if (firstname.length > 20) {
                        errors['firstname'] = module.string('input_must_be_less_than_%d', 20);
                    }

                    if (!password) {
                        errors['password'] = module.string('required_field');
                    }

                    if (!passwordConfirm) {
                        errors['passwordConfirm'] = module.string('required_field');
                    } else if (password && passwordConfirm !== password) {
                        errors['passwordConfirm'] = module.string('confirm_not_match');
                    }

                    module.$('.input-wrapper', $modal).removeClass('error');
                    module.$('.input-wrapper .errors', $modal).empty();

                    module.$.each(errors, function(key, val) {
                        var $el = $frames[key];
                        $el.addClass('error');
                        $el.find('.errors').html(val);
                        isValid = false;
                    });

                    return isValid;
                },
                handleRemoteError: function(errorCode) {
                    var $modal = module.$('#modal-signup-with-email'),
                        $emailFrame = module.$('.email-input-wrapper', $modal),
                        $nicknameFrame = module.$('.nickname-input-wrapper', $modal);

                    module.$('.input-wrapper', $modal).removeClass('error');
                    module.$('.input-wrapper .errors', $modal).empty();

                    switch(errorCode) {
                        case 1:
                            //email
                            $emailFrame.addClass('error');
                            $emailFrame.find('.errors')
                                .text(module.string("email_already_exists"));
                            return true;
                        case 2:
                            //nickname
                            $nicknameFrame.addClass('error');
                            $nicknameFrame.find('.errors')
                                .text(module.string('username_already_exists'));
                            return true;
                    }
                    return false;
                },
                trySubmit: function() {
                    var that = this,
                        $modal = module.$('#modal-signup-with-email'),
                        $form = module.$('#signup-form', $modal),
                        $submitBtn = module.$('.btn-signup', $modal),
                        formArray = $form.serializeArray(),
                        data = {};


                    if ($submitBtn.hasClass('progress')) {
                        return;
                    }

                    module.$.each(formArray, function(idx, s) {
                        data[s.name] = s.value;
                    });


                    if (!this.validate(data)) {
                        return;
                    }

                    $submitBtn.addClass('progress');
                    $modal.find('input').attr('readonly', 'readonly');


                    if (this.req) {
                        this.req.abort();
                    }

                    this.req = module.$.ajax({
                        url: module.api.signupWithEmailUrl,
                        data: JSON.stringify(data),
                        dataType:'json',
                        type: 'post',
                        contentType: 'application/json'
                    }).done(function(res) {
                        var errorCode, email, name, idx;

                        if (!res || !res.success) {
                            errorCode = parseInt(res.error);
                            if (!isNaN(errorCode)) {
                                that.handleRemoteError(errorCode);
                                return;
                            }
                            module.s.notify(module.string("failed_to_submit"), 'error');
                            return;
                        }

                        email = data.email;
                        idx = email.indexOf('@');
                        name = email.substring(0, idx);
                        name = name.replace(/[^A-Za-z0-9]/g, '').toLowerCase();

                        module.$.ajax({
                            url: module.api.changeResourceNameUrl,
                            data: JSON.stringify({
                                resource_name : name
                            }),
                            dataType:'json',
                            type: 'post',
                            contentType: 'application/json'
                        }).always(function() {
                            if (window.location.pathname === '/about') {
                                window.location = '/';
                            } else {
                                window.location.reload();
                            }
                        });
                    }).fail(function(jqXHR, textStatus, error) {
                        if (textStatus !== 'abort') {
                            module.s.notify(module.string("failed_to_submit"), 'error');
                        }
                    }).always(function() {
                        that.req = null;
                        $submitBtn.removeClass('progress');
                        $modal.find('input').removeAttr('readonly');
                    });
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new View({
                onAlways: function() {
                    callback && callback();
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.SigninWithEmailView = (function() {
        var View = DropbeatView.extend({
                tmpl: '#tmpl-modal-signin-with-email',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAlways = options.onAlways;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    module.view.placeholderCompat();

                    $modal.modal({ show: true, backdrop:'static'})
                        .on('submit', '#signin-form', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('click', '.btn-signin', function() {
                            that.trySubmit();
                            return false;
                        })
                        .on('hidden.bs.modal', function() {
                            that._onAlways && that._onAlways();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    $modal.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    $modal.modal('hide');
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                    if (this.req) {
                        this.req.abort();
                    }
                },
                validate: function(data) {
                    var email = data.email,
                        password = data.password,
                        errors = {},
                        isValid = true,
                        $modal = module.$('#modal-signin-with-email'),
                        $frames = {
                            email: module.$('.email-input-wrapper', $modal),
                            password : module.$('.password-input-wrapper', $modal)
                        };

                    if (!email) {
                        errors['email'] = module.string('required_field');
                    } else if (!validateEmail(email)) {
                        errors['email'] = module.string('invalid_email_format');
                    }

                    if (!password) {
                        errors['password'] = module.string('required_field');
                    }

                    module.$('.input-wrapper', $modal).removeClass('error');
                    module.$('.input-wrapper .errors', $modal).empty();

                    module.$.each(errors, function(key, val) {
                        var $el = $frames[key];
                        $el.addClass('error');
                        $el.find('.errors').html(val);
                        isValid = false;
                    });

                    return isValid;
                },
                handleRemoteError: function(errorCode) {
                    var $modal = module.$('#modal-signin-with-email'),
                        $emailFrame = module.$('.email-input-wrapper', $modal),
                        $passwordFrame = module.$('.password-input-wrapper', $modal);

                    module.$('.input-wrapper', $modal).removeClass('error');
                    module.$('.input-wrapper .errors', $modal).empty();

                    switch(errorCode) {
                        case 1:
                            //email
                            $emailFrame.addClass('error');
                            $emailFrame.find('.errors')
                                .text(module.string('email_not_found'));
                            return true;
                        case 2:
                            //password
                            $passwordFrame.addClass('error');
                            $passwordFrame.find('.errors')
                                .text(module.string('password_invalid'));
                            return true;
                    }
                    return false;
                },
                trySubmit: function() {
                    var that = this,
                        $modal = module.$('#modal-signin-with-email'),
                        $form = module.$('#signin-form', $modal),
                        $submitBtn = module.$('.btn-signin', $modal),
                        formArray = $form.serializeArray(),
                        data = {};


                    if ($submitBtn.hasClass('progress')) {
                        return;
                    }

                    module.$.each(formArray, function(idx, s) {
                        data[s.name] = s.value;
                    });

                    if (!this.validate(data)) {
                        return;
                    }

                    $submitBtn.addClass('progress');
                    $modal.find('input').attr('readonly', 'readonly');

                    if (this.req) {
                        this.req.abort();
                    }

                    this.req = module.$.ajax({
                        url: module.api.signinWithEmailUrl,
                        data: JSON.stringify(data),
                        dataType:'json',
                        type: 'post',
                        contentType: 'application/json'
                    }).done(function(res) {
                        var errorCode;
                        if (!res || !res.success) {
                            errorCode = parseInt(res.error);
                            if (!isNaN(errorCode)) {
                                that.handleRemoteError(errorCode);
                                return;
                            }
                            module.s.notify(module.string('failed_to_submit'), 'error');
                            return;
                        }
                        if (window.location.pathname === '/about') {
                            window.location = '/';
                        } else {
                            window.location.reload();
                        }
                    }).fail(function(jqXHR, textStatus, error) {
                        if (textStatus !== 'abort') {
                            module.s.notify(module.string('failed_to_submit'), 'error');
                        }
                    }).always(function() {
                        that.req = null;
                        $submitBtn.removeClass('progress');
                        $modal.find('input').removeAttr('readonly');
                    });
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options, callback) {
            if (view) {
                return;
            }
            view = new View({
                onAlways: function() {
                    callback && callback();
                    if (view) {
                        view.close ? view.close() : view.remove();
                        view = null;
                    }
                }
            });
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.AboutView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-about',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(callback) {
            if (view) {
                return;
            }
            view = new PageView({
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.LegalView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-legal',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(callback) {
            if (view) {
                return;
            }
            view = new PageView({
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.DropDropbeatView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-drop-dropbeat',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(callback) {
            if (view) {
                return;
            }
            view = new PageView({
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.NeedEmailView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-need-email',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAfterConfirm = options.onAfterConfirm;
                    module._.bindAll(this, 'show', 'hide', 'doSubmit');

                    module.$('#modal').on('click',
                        '#modal-need-email .btn-submit',
                        this.doSubmit);
                    module.$('#modal').on('submit',
                        '#modal-need-email .form-email', this.doSubmit);
                },
                doSubmit: function(e) {
                    var that = this,
                        $input = module.$('#modal-need-email .input-email'),
                        $btn = module.$('#modal-need-email .btn-submit'),
                        email = $input.val(),
                        errors = [];

                    if ($btn.attr('disabled')) {
                        return;
                    }

                    if (!email || email === '') {
                        errors.push({msg: module.string('required_field'), $target: $input});
                    } else if (!validateEmail(email)) {
                        errors.push({msg: module.string('invalid_email_format'), $target: $input});
                    } else if (email.indexOf(
                            module.constants.tempEmailPostfix) > -1) {
                        errors.push({msg: module.string('invalid_email'), $target: $input});
                    }

                    if (errors.length > 0) {
                        that.showError(errors);
                        return;
                    }

                    $input.attr('readonly', 'readonly');
                    $btn.attr('disabled', 'true').addClass('progress');
                    module.$('.text', $btn).text('Submitting..');

                    module.$.ajax({
                        url: module.api.emailChangeUrl,
                        data: JSON.stringify({
                            email: email
                        }),
                        dataType:'json',
                        type: 'post',
                        contentType: 'application/json'
                    }).done(function(res) {
                        if (!res.success) {
                            that.showError(res.error || 'Undefined error');
                            return;
                        }
                        module.$.get(module.api.userUrl).done(function(resp) {
                            that.hide();
                            module.s.notify(module.string('email_updated'), 'success');
                            window.location.reload();
                        });
                    }).fail(function(res, textStatus) {
                        if (res.responseJSON && res.responseJSON.error) {
                            that.showError(res.responseJSON.error);
                        } else {
                            that.showError('undefined error');
                        }
                    }).always(function(res) {
                        $btn.removeAttr('disabled');
                        $btn.removeClass('progress');
                        module.$('.text', $btn).text(module.string('submit'));

                        $input.removeAttr('readonly');
                    });
                    return false;
                },
                showError: function(errors) {
                    var errorArray,
                        $errorPrompt = module.$('.error-prompt', this.$modal);
                    if (!(errors instanceof Array)) {
                        errorArray = [{ msg: errors}];
                    } else {
                        errorArray = errors;
                    }
                    $errorPrompt.html('');
                    module.$.each(errorArray, function(idx, el) {
                        $errorPrompt.append(el.msg + '<br/>');
                        if (el.$target) {
                            el.$target.addClass('error');
                        }
                    });
                    $errorPrompt.slideDown();
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({
                            show: true,
                            backdrop:'static',
                            keyboard: false
                        }).on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$('#modal-need-email');
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$('#modal-need-email').remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(callback) {
            if (view) {
                return;
            }
            view = new PageView({
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.UpdateNewsView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-update-news',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {},
            newsCount = 2;

        subModule.show = module.$.proxy(function(callback) {
            if (view) {
                return;
            }
            if (module.$.cookie('read_update_news_' + newsCount)) {
                callback && callback();
                return;
            }
            view = new PageView({
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        module.$.cookie('read_update_news_' + newsCount,
                            true, {expires: 9999});
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.OpenInAppModalView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-open-in-app',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);

                    this._downloadUrl = options.downloadUrl;
                    this._inappUrl = options.inAppUrl;
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(module.$.extend({
                            appDownloadUrl:this._downloadUrl,
                            inAppUrl: this._inAppUrl
                        },this._context)));

                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(inAppUrl, downloadUrl, callback) {
            if (view) {
                return;
            }
            view = new PageView({
                    inAppUrl: inAppUrl,
                    downloadUrl: downloadUrl,
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        module.$.cookie('read_follow_dropbeat', true,
                            {expires: 9999});
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.FollowDropbeatView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-follow-dropbeat',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module._.bindAll(this, 'show', 'hide');
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    if (window['twttr'] && twttr.widgets) {
                        twttr.widgets.load( module.$('#modal-follow-dropbeat')[0] );
                    }
                    if (module.context.fbInitialized &&
                            window['FB'] && FB.XFBML && FB.XFBML.parse) {
                        FB.XFBML.parse(module.$('#modal-follow-dropbeat')[0]);
                    }
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(callback) {
            if (view) {
                return;
            }
            if (module.$.cookie('read_follow_dropbeat')) {
                callback && callback();
                return;
            }
            view = new PageView({
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        module.$.cookie('read_follow_dropbeat', true,
                            {expires: 9999});
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.ShareSingleTrackView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-share-track',
                urlInfoTmpl: '#tmpl-modal-share-track-url',
                embedTmpl: '#tmpl-modal-share-track-embed',
                initialize: function(options) {
                    var that = this,
                        $modal,
                        callback,
                        url;

                    module._.bindAll(this, 'hide');
                    DropbeatView.prototype.initialize.call(this, options);

                    this._track = options.track;
                    this._section = options.section;
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._urlTmpl = module._.template(module.$(this.urlInfoTmpl).html());
                    this._embedTmpl = module._.template(module.$(this.embedTmpl).html());
                    this.autoPlay = false;
                    this.url = null;

                    module.$.extend(this._context, {
                        playlist: options.playlist,
                        confirmText: module.string('close')
                    });

                    $modal = module.$(this._tmpl(module.$.extend({}, this._context,
                        {track: this._track})));

                    if (this.req) {
                        return;
                    }

                    module.$('#modal').append($modal);
                    this.$el = module.$('#modal-share-track');

                    this.$failure = module.$('.failure', $modal);
                    this.$spinner = module.$('.spinner', $modal);
                    this.$preview = module.$('.preview', $modal);
                    this.$tabs = module.$('.tabs', $modal);
                    this.$tabContentsWrapper = module.$('.tab-contents-wrapper', $modal);
                    this.$shareTabContents = module.$('.tab-contents-share', $modal);
                    this.$embedTabContents = module.$('.tab-contents-embed', $modal);

                    this.$el.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });

                    callback = function(url) {
                        var $preview,
                            playerUrl;

                        that.$tabContentsWrapper.show();
                        that.$shareTabContents.html(that._urlTmpl({
                                shareUrl: url,
                                trackName: that._track.title
                            }));

                        that.url = url;
                        that.displayEmbedUrl(url, that.autoPlay);

                        that.$artistSection = module.$('.artist-section', that.$el);
                        that.$mixSection = module.$('.mix-section', that.$el);

                        //$preview = module.$('<object type="application/x-shockwave-flash" ' +
                        //    'width="100%" height="140" ' +
                        //    'data="https://dropbeat.net/player.swf?url=' + url +
                        //    '"></object>');
                        //$preview.append('<param name="play" value="true" />');
                        //that.$preview.html($preview);

                        playerUrl = 'http://' + window.location.host + '/html5player?url=' + url;
                        $preview = module.$('<iframe width="100%" height="140" ' +
                            'src="' + playerUrl + '" frameborder="0" allowfullscreen></iframe>');
                        that.$preview.html($preview);

                        that.$shareTabContents.find('input').select();
                        setTimeout(function() {
                            that.$shareTabContents.find('input').select();
                        }, 500);
                    };

                    if (this._track.type === 'dropbeat') {
                        // we dont need to resolve share url
                        module.log('track-share', 'from-' + that._section,
                            this._track.unique_key);
                        url = 'http://' + window.location.host +
                            this._track.getResourcePath();
                        callback(url);
                        this.$spinner.hide();
                        return;
                    }

                    this.$spinner.show();
                    this.req = module.$.ajax({
                        url:module.api.trackShareUrl,
                        data: JSON.stringify({
                            track_name: this._track.title,
                            ref: this._track.id
                        }),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST'
                    }).done(function(res) {
                        var url, uid;

                        if (!res || !res.success || (!res.obj && !res.data)) {
                            that.$failure.show();
                            return;
                        }

                        if (res.obj) {
                            uid = res.obj.uid;
                        } else if(res.data) {
                            uid = res.data.uid;
                        }

                        if (!uid) {
                            that.$failure.show();
                            return;
                        }


                        module.log('track-share', 'from-' + that._section, uid);
                        url = 'http://' + window.location.host + '/?track=' + uid;
                        callback(url);
                    }).fail(function(jqXHR, errorText, err) {
                        if (errorText !== 'abort') {
                            that.$failure.show();
                        }
                    }).always(function() {
                        that.req = null;
                        that.$spinner.hide();
                    });
                },

                events: function() {
                    var events = DropbeatView.prototype.events;
                    if (module._.isFunction(events)) {
                        events = events.call(this);
                    }

                    return module.$.extend ({}, events, {
                        'click .btn-share' : 'onShareBtnClicked',
                        'click .tabs .tab' : 'onTabClicked',
                        'click .choice-autoplay': 'onAutoPlayClicked'
                    });
                },

                hide: function(callback) {
                    this.$el.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    this.$el.modal('hide');
                },


                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                    if (this.req) {
                        this.req.abort();
                        this.req = null;
                    }
                },

                displayEmbedUrl: function(url, autoPlay) {
                    var $object,
                        playerUrl;

                    //playerUrl = 'https://dropbeat.net/player.swf?url=' +
                    //        encodeURIComponent(url) + '&autoplay=' + (autoPlay ? 'true' : 'false');
                    //$object = module.$('<object type=\"application/x-shockwave-flash\" width=\"100%\" ' +
                    //    'height=\"140\" data=\"' + playerUrl +
                    //    '\" align=\"middle\" ></object>');

                    //$object.append('<param name=\"allowScriptAccess\" value=\"always\"/>');
                    //$object.append('<param name=\"quality\" value=\"high\"/>');
                    //$object.append('<param name=\"movie\" value=\"player.swf\" />');
                    //$object.append('<param name=\"bgcolor\" value=\"#f5f5f5\" />');
                    //$object.append('<param name=\"play\" value=\"true\" />');
                    //$object.append('<param name=\"wmode\" value=\"window\" />');
                    //$object.append('<param name=\"scale\" value=\"showall\" />');
                    //$object.append('<param name=\"devicefont\" value=\"false\" />');
                    //$object.append('<param name=\"salign\" value=\"\" />');
                    //$object.append('<param name=\"allowScriptAccess\" value=\"sameDomain\" />');
                    //$object.append('<a href=\"http://www.adobe.com/go/getflash\">' +
                    //    '<img src=\"http://www.adobe.com/images/' +
                    //        'shared/download_buttons/get_flash_player.gif\" alt=\"Get Adobe Flash player\" /></a>');

                    playerUrl = 'http://' + window.location.host +
                        '/html5player?url=' + url + '&autoplay=' + autoPlay;
                    $object = module.$('<iframe scrolling="no" width="100%" height="140" ' +
                        'src="' + playerUrl + '" frameborder="0" allowfullscreen></iframe>');

                    this.$embedTabContents.html(this._embedTmpl({
                            code:$object.prop('outerHTML').replace(/"/g, '&quot;'),
                            autoPlay: autoPlay
                        }));
                },

                onAutoPlayClicked: function(e) {
                    this.autoPlay = !this.autoPlay;
                    this.displayEmbedUrl(this.url, this.autoPlay);
                    this.$embedTabContents.find('input').select();
                },

                onTabClicked: function(e) {
                    var $btn = module.$(e.currentTarget),
                        target = $btn.data('target');
                    if ($btn.hasClass('selected')) {
                        return;
                    }

                    this.$tabs.find('.tab').removeClass('selected');
                    this.$tabs.find('.tab-' + target).addClass('selected');
                    this.$tabContentsWrapper.find('.tab-contents').removeClass('selected');
                    this.$tabContentsWrapper.find('.tab-contents-' + target).addClass('selected');
                    this.$tabContentsWrapper.find('.tab-contents-' + target).find('input').select();
                },

                onShareBtnClicked: function(e) {
                    var $btn = module.$(e.currentTarget),
                        url = $btn.data('popupHref'),
                        width, height;

                    if (!url) {
                        return;
                    }
                    width = $btn.data('width'),
                    height = $btn.data('height');

                    window.open(url, '_blank', 'location=0, height=' +
                        height + ', width=' + width);
                    return false;
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(track, section, callback) {
            if (view || !track) {
                return;
            }

            view = new PageView({
                    track: track,
                    section: section,
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.SharePlaylistView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-share-playlist',
                initialize: function(options) {
                    var that = this,
                        $modal;

                    module._.bindAll(this, 'hide');
                    DropbeatView.prototype.initialize.call(this, options);

                    this._onAfterConfirm = options.onAfterConfirm;
                    this._modal = this.tmpl.replace('tmpl-', '');

                    module.$.extend(this._context, {
                        playlist: options.playlist,
                        confirmText: module.string('close')
                    });


                    $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);

                    this.$el = module.$('#modal-share-playlist');
                    this.$el.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        })
                        .on('shown.bs.modal', function() {
                            module.$(this).find('input').select();
                        });
                },

                hide: function(callback) {
                    this.$el.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    this.$el.modal('hide');
                },

                events: function() {
                    var events = DropbeatView.prototype.events;
                    if (module._.isFunction(events)) {
                        events = events.call(this);
                    }

                    return module.$.extend ({}, events, {
                        'click .btn-share' : 'onShareBtnClicked'
                    });
                },

                onShareBtnClicked: function(e) {
                    var $btn = module.$(e.currentTarget),
                        url = $btn.data('popupHref'),
                        width, height;

                    if (!url) {
                        return;
                    }
                    width = $btn.data('width'),
                    height = $btn.data('height');

                    window.open(url, '_blank', 'location=0, height=' +
                        height + ', width=' + width);
                    return false;
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(playlist, url, callback) {
            var account, desc, thumbUrl, ytTracks;

            if (view) {
                return;
            }
            if (!playlist || !url) {
                return;
            }

            account = module.context.account;
            desc = 'created by ' + account.firstName + ' ' + account.lastName;
            ytTracks = module._.filter(playlist.playlist, function(track) {
                    return track.type==='youtube'
                });
            if (ytTracks.length > 0) {
                thumbUrl = 'http://img.youtube.com/vi/' + ytTracks[0].id +
                    '/sddefault.jpg';
            } else {
                thumbUrl = '/images/default_playlist_thumb.png';
            }

            view = new PageView({
                    playlist: {
                        name: playlist.name,
                        thumb: thumbUrl,
                        desc: desc,
                        url: url
                    },
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.SoundcloudPlaylistImportView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-soundcloud-playlist-import',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    this._onAfterConfirm = options.onAfterConfirm;
                    module._.bindAll(this, 'show', 'hide', 'onSubmit');
                    module.$('#modal').on('click',
                        '#modal-soundcloud-playlist-import .btn-submit',
                        this.onSubmit);
                    module.$('#modal').on('submit',
                        '#modal-soundcloud-playlist-import .soundcloud-playlist-form',
                        this.onSubmit);
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$('#modal-soundcloud-playlist-import');
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                onSubmit: function() {
                    var $modal = module.$('#modal-soundcloud-playlist-import'),
                        $form,
                        $submitBtn,
                        $urlInput,
                        inputUrl,
                        that = this;

                    $form = module.$('.soundcloud-playlist-form', $modal);
                    $urlInput = module.$('.import-url-input', $form);
                    $submitBtn = module.$('.btn-submit', $modal);

                    inputUrl = $urlInput.val().trim();
                    if (!inputUrl || inputUrl === '' ||
                            $submitBtn.hasClass('progress')) {
                        return false;
                    }
                    $urlInput.attr('readonly', 'readonly');
                    $submitBtn.attr('disabled', 'true')
                        .addClass('progress');
                    if (this._req) {
                        this._req.abort();
                    }
                    this._req = module.$.ajax({
                        url: module.api.importSoundcloud,
                        dataType: 'json',
                        data: {
                            u: inputUrl
                        }
                    }).done(function(res) {
                        if (!res.success) {
                            module.s.notify(
                                'Failed to import playlist from soundcloud:' +
                                res.error, 'error');
                            $urlInput.removeAttr('readonly');
                            $submitBtn.removeAttr('disabled')
                                .removeClass('progress');
                            return false;
                        }
                        if (!res.playlist || res.playlist.length === 0) {
                            module.s.notify(
                                'Nothing to import', 'error');
                            $urlInput.removeAttr('readonly');
                            $submitBtn.removeAttr('disabled')
                                .removeClass('progress');
                            return false;
                        }
                        that.generatePlaylist(res.playlist_name, res.playlist,
                            function(success) {
                                $urlInput.removeAttr('readonly');
                                $submitBtn.removeAttr('disabled')
                                    .removeClass('progress');
                                that.hide();
                                if (success) {
                                    module.s.notify('Import success', 'success');
                                } else {
                                    module.s.notify('Import failed', 'success');
                                }
                            });
                    }).fail(function(jqXHR, errorText, error) {
                        if (errorText !== 'abort') {
                            module.s.notify(
                                'Failed to import playlist from soundcloud:' +
                                res.responseText, 'error');
                        }
                        $urlInput.removeAttr('readonly');
                        $submitBtn.removeAttr('disabled')
                            .removeClass('progress');
                    }).always(function() {
                    });
                    return false;
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$('#modal-soundcloud-playlist-import').remove();
                    module.$('#modal').off('click',
                        '#modal-soundcloud-playlist-import .btn-submit',
                        this.onSubmit);
                    module.$('#modal').off('submit',
                        '#modal-soundcloud-playlist-import .soundcloud-playlist-form',
                        this.onSubmit);
                },
                generatePlaylist: function(title, tracks, cb) {
                    var uniqueTracks = [],
                        ids = [];
                    module.$.each(tracks, function(idx, track) {
                        if (!track.id || track.id === '' ||
                                ids.indexOf(track.id) > -1) {
                            return true;
                        }
                        ids.push(track.id);
                        uniqueTracks.push(track);
                    });
                    module.s.storage.createPlaylist(title, function(playlist) {
                        if (!playlist) {
                            cb && cb(false);
                            return;
                        }
                        module.s.storage.setPlaylist(uniqueTracks, playlist.seqId,
                                function() {
                            module.playlistManager.loadPlaylists(function() {
                                var selector;

                                module.s.playlistTabs.renderAll();
                                module.playlistManager.updateCurrentPlaylistView();
                                selector = '.my-playlist .playlists ';
                                selector += '.playlist[data-playlist-id=\'';
                                selector += playlist.seqId + '\']';
                                selector += ' .nonedit-mode-view .title';
                                module.$(selector).click();
                                cb && cb(true);
                            });
                        })
                    });
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(callback) {
            if (view) {
                return;
            }
            view = new PageView({
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.FbArtistFollowView = (function() {
        var PageView = DropbeatView.extend({

            tmpl: '#tmpl-modal-fb-artist-follow',

            initialize: function(options) {
                DropbeatView.prototype.initialize.call(this, options);

                this.followChanged = false;

                this.render();
                this.$failureDesc = module.$('.failure-desc', this.$el);
                this.$items = module.$('.items', this.$el);
                this.$dialog = module.$('.modal-dialog', this.$el);
                this.pages = {};

                this._onAfterConfirm = options.onAfterConfirm;
                this.itemTmpl = module._.template(module.$('#tmpl-fb-followable-item').html());

                this.show();
                this.autoFollow = options.autoFollow

                if (this.autoFollow) {
                    this.showDialogContents('starting');
                } else {
                    this.showDialogContents('loading');
                    this.initGraphApi();
                }
            },

            showDialogContents: function(type) {
                this.$dialog
                    .removeClass('loading')
                    .removeClass('failure')
                    .removeClass('starting')
                    .removeClass('choosing')
                    .removeClass('modal-sm')
                    .removeClass('modal-lg');

                if (type === 'loading') {
                    this.$dialog.addClass('modal-sm');
                    this.$dialog.addClass('loading');
                } else if (type === 'failure') {
                    this.$dialog.addClass('failure');
                } else if (type === 'starting') {
                    this.$dialog.addClass('starting');
                } else if (type === 'choosing') {
                    this.$dialog.addClass('choosing');
                    this.$dialog.addClass('modal-lg');
                }
            },

            events: function() {
                var events = DropbeatView.prototype.events;
                if (module._.isFunction(events)) {
                    events = events.call(this);
                }

                return module.$.extend ({}, events, {
                    'click .btn-done': 'onDoneClicked',
                    'click .btn-follow': 'onFollowBtnClicked',
                    'click .btn-start': 'onStartClicked',
                    'click .btn-retry' : 'onRetryClicked'
                });
            },

            render: function() {
                module.$('#modal').append(this._tmpl(this._context));
                this.$el = module.$('#modal-fb-artist-follow', module.$('#modal'));
            },

            remove: function() {
                DropbeatView.prototype.remove.apply(this, arguments);
                this.$el.remove();
                view = null;
                if (this.filterReq) {
                    this.filterReq.abort();
                    this.filterReq = null;
                }

                if (this.followReq) {
                    this.followReq.abort();
                    this.followReq = null;
                }

                if (this.followingReq) {
                    this.followingReq.abort();
                    this.followingReq = null;
                }
            },

            onDoneClicked: function() {
                module.$.cookie('done_artist_follow', true, {expires: 9999});
                this.hide();
            },

            onRetryClicked: function() {
                this.initGraphApi();
            },

            onFollowBtnClicked: function(e) {
                var $btn = module.$(e.currentTarget),
                    $btnText = module.$('.text', $btn),
                    $item = $btn.closest('.fb-item'),
                    that = this,
                    itemId = $item.data('id');

                if ($btn.hasClass('progress')) {
                    return;
                }
                this.followChanged = true;

                if (this.followReq) {
                    this.followReq.abort();
                    this.followReq = null;
                }

                $btn.addClass('progress');

                if ($btn.hasClass('following')) {
                    this.followReq = module.$.ajax({
                        url: module.api.unfollowArtistUrl,
                        data: JSON.stringify({
                            ids: [itemId]
                        }),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST'
                    }).done(function(req) {
                        if (!req || !req.success) {
                            module.s.notify('Failed to follow artist', 'error');
                            return;
                        }
                        $btn.removeClass('following');
                        $btnText.text($btnText.data('textFollow'));
                    }).fail(function(jqXHR, errorText, err) {
                        if (errorText !== 'abort') {
                            module.s.notify('Failed to follow artist', 'error');
                        }
                    });
                } else {
                    this.followReq = module.$.ajax({
                        url: module.api.followArtistUrl,
                        data: JSON.stringify({
                            ids: [itemId]
                        }),
                        contentType: 'application/json',
                        dataType: 'json',
                        type: 'POST'
                    }).done(function(req) {
                        if (!req || !req.success) {
                            module.s.notify('Failed to follow artist', 'error');
                            return;
                        }
                        $btn.addClass('following');
                        $btnText.text($btnText.data('textFollowing'));
                    }).fail(function(jqXHR, errorText, err) {
                        if (errorText !== 'abort') {
                            module.s.notify('Failed to follow artist', 'error');
                        }
                    });
                }
                this.followReq.always(function() {
                    $btn.removeClass('progress');
                    that.followReq = null;
                })
            },

            onStartClicked: function() {
                this.initGraphApi();
            },

            initGraphApi: function() {
                var that = this;
                this.showDialogContents('loading');
                this.fetchLike().done(function(likes) {
                    that.filterIdsThroughServer(likes);
                }).fail(function(type, m) {
                    if (type != null) {
                        that.showFailure(type, m);
                    }
                });
            },

            fetchLike: function(deferred, likes, skipTryLogin) {
                var that = this,
                    next, params;

                likes = likes || [];
                deferred = deferred || module.$.Deferred();

                if (!skipTryLogin) {
                    params = {
                        scope: 'email, user_likes',
                        return_scopes: true
                    };
                    if (that._rerequest) {
                        params.auth_type = 'rerequest';
                    }
                    FB.login(function(response) {
                        var scopes;
                        if (!response.authResponse) {
                            deferred.reject('failed_to_auth');
                            return;
                        }
                        if (response.authResponse.userID !==
                                module.context.account.fbId) {
                            deferred.reject('diff_user');
                            return;
                        }
                        scopes = response.authResponse.grantedScopes ?
                            response.authResponse.grantedScopes.split(',') : [];

                        if (scopes.indexOf('user_likes') === -1) {
                            that._rerequest = true;
                            deferred.reject('permission');
                            return;
                        }
                        that.fetchLike(deferred, likes, true);
                        return;
                    }, params);
                    return deferred;
                }

                FB.api('/me/likes', { limit: 1000 }, function(response) {
                    if (!response) {
                        deferred.reject('failed_to_get_response');
                        return;
                    }
                    if (response.error) {
                        if (response.error.type === 'OAuthException') {
                            deferred.reject('failed_to_auth');
                            return;
                        }
                        deferred.reject();
                        that.hide();
                        return;
                    }
                    if (response.data) {
                        module.$.each(response.data, function(idx, like) {
                            likes.push(like);
                        });
                    }
                    if (response.paging && response.paging.next) {
                        next = response.paging.next;
                        that.fetchLikeNext(deferred, next, likes);
                    } else {
                        deferred.resolve(likes);
                    }
                });

                return deferred;
            },

            fetchLikeNext: function(deferred, path, likes) {
                var that = this, next;

                if (this.fetchLikeReq) {
                    this.fetchLikeReq.abort();
                }
                this.fetchLikeReq = module.$.get(path).done(function(response) {
                    if (response.data) {
                        module.$.each(response.data, function(idx, like) {
                            likes.push(like);
                        });
                    }
                    if (response.paging && response.paging.next) {
                        next = response.paging.next;
                        that.fetchLikeNext(deferred, next, likes);
                    } else {
                        deferred.resolve(likes);
                    }
                }).fail(function(jqXHR, errorText, error) {
                    if (errorText !== 'abort') {
                        deferred.reject('abort');
                    }
                });
            },

            filterIdsThroughServer: function(pages) {
                var that = this, url,
                    pageMap = {},
                    ids = [],
                    items = [];

                module.$.each(pages, function(idx, page) {
                    var name = page.name.toUpperCase().latinize();
                    page.name = name;
                    pageMap[name] = page;
                    ids.push(name);
                });

                if (this.filterReq) {
                    this.filterReq.abort();
                    this.filterReq = null;
                }

                if (this.followingReq) {
                    this.followingReq.abort();
                    this.followingReq = null;
                }

                url = module.coreApi.artistFilterUrl;

                this.filterReq = module.$.ajax({
                    crossDomain: true,
                    url: url,
                    data: JSON.stringify({
                        q: ids
                    }),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST'
                });

                this.followingReq = module.$.ajax({
                    url: module.api.followingArtistUrl,
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'GET'
                });

                module.$.when(this.filterReq, this.followingReq)
                        .done(function(filterResult, followingResult) {
                    var filterResponse = filterResult[0],
                        followingResponse = followingResult[0],
                        followings;

                    if (!filterResponse || !filterResponse.success ||
                            !filterResponse.data) {
                        that.showFailure('server_error');
                        return;
                    }

                    followings = module.$.map(followingResponse.data || [], function(val, i) {
                        return val.name;
                    });

                    module.$.each(filterResponse.data, function(idx, p) {
                        var page, item, name = p.name.toUpperCase();

                        page = pageMap[name];
                        if (!page) {
                            return true;
                        }
                        item = {
                            id: p.id,
                            name: p.name,
                            fbId: page.id,
                            isFollowing: followings.indexOf(name) > -1
                        };
                        that.pages[p.id] = item;
                        items.push(item);
                    });

                    items.sort(function(l, r) {
                        if (l.name < r.name) {
                            return -1;
                        } else if (l.name > r.name) {
                            return 1;
                        } else {
                            return 0;
                        }
                    });

                    if (!that.autoFollow) {
                        that.renderItems(items);
                    } else {
                        that.followAll(items);
                    }

                }).fail(function(filterResult, followingResult) {
                    if (filterResult[0] !== 'abort' || followingResult !== 'abort') {
                        that.showFailure('internal');
                    }
                }).always(function() {
                    that.filterReq = null;
                    that.followingReq = null;
                });
            },

            followAll: function(items) {
                var ids = [], that = this;

                module.$.each(items, function(idx, item) {
                    ids.push(item.id);
                });

                if (ids.length === 0) {
                    module.$.cookie('done_artist_follow', true, {expires: 9999});
                    that.hide();
                    return;
                }

                this.followReq = module.$.ajax({
                    url: module.api.followArtistUrl,
                    data: JSON.stringify({
                        ids: ids
                    }),
                    contentType: 'application/json',
                    dataType: 'json',
                    type: 'POST'
                }).done(function(req) {
                    if (!req || !req.success) {
                        module.s.notify('Failed to follow artist', 'error');
                        return;
                    }
                    that.followChanged = true;
                    module.$.cookie('done_artist_follow', true, {expires: 9999});
                    that.hide();
                }).fail(function(jqXHR, errorText, err) {
                    if (errorText !== 'abort') {
                        module.s.notify('Failed to follow artist', 'error');
                    }
                });
            },

            renderItems: function(items) {
                var html = '',
                    that = this;
                if (!items || items.length === 0) {
                    html = '<div class=\'no-artist\'>You seem to have not liked any dj artists in facebook.</div>';
                } else {
                    module.$.each(items, function(idx, item) {
                        html += that.itemTmpl({item:item});
                    });
                }
                this.$items.html(html);
                this.showDialogContents('choosing');
            },

            showFailure: function(type, message) {
                var desc = '';

                if (type === 'failed_to_get_response') {
                    desc = 'Failed to get response from facebook.';
                } else if (type === 'failed_to_auth') {
                    desc = 'Failed to authenticate with facebook.';
                } else if (type === 'diff_user') {
                    desc = 'Your Facebook account is different than the account which is currently connected to Dropbeat.';
                } else if (type === 'permission') {
                    desc = 'You should grant access  to your Facebook \'Like\' pages.';
                } else if (type === 'empty') {
                    desc = 'You seem to have not liked any dj artists in facebook.';
                } else if (type === 'failed_with_undefined') {
                    desc = message;
                }
                this.$failureDesc.html(desc);
                this.showDialogContents('failure');
            },

            show: function() {
                var that = this;

                this.$el.modal({
                        show: true,
                        backdrop:'static',
                        keyboard: false
                    })
                    .on('hidden.bs.modal', function() {
                        that.remove();
                        that._onAfterConfirm &&
                            that._onAfterConfirm(that.followChanged);
                    });
            },

            hide: function(callback) {
                if (this.$el.is(':hidden')) {
                    callback && callback();
                    return;
                }
                this.$el.on('hidden.bs.modal', function() {
                    callback && callback();
                });
                this.$el.modal('hide');
            }
        }),
        view,
        subModule = {};

        subModule.show = module.$.proxy(function(callback, autoFollow) {
            if (view) {
                return;
            }

            view = new PageView({
                    autoFollow: autoFollow,
                    onAfterConfirm: function(isChanged) {
                        if (view) {
                            view.close ? view.close() : view.remove();
                            view = null;
                        }
                        if (isChanged) {
                            module.$(module).trigger('artistFollowChanged');
                        }
                        callback && callback();
                    }
                })
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);

        return subModule;
    } ());

    module.view.TracklistView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-tracklist',
                initialize: function(options) {
                    DropbeatView.prototype.initialize.call(this, options);
                    module._.bindAll(this, 'show', 'hide');
                    this._onAfterConfirm = options.onAfterConfirm;
                    this._modal = this.tmpl.replace('tmpl-', '');
                    module.$.extend(this._context, options.context);
                },
                show: function() {
                    var that = this,
                        $modal = module.$(this._tmpl(this._context));
                    module.$('#modal').append($modal);
                    $modal.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            that._onAfterConfirm && that._onAfterConfirm();
                        });
                },
                hide: function(callback) {
                    var $modal = module.$(this._modal);
                    if ($modal.size() > 0) {
                        $modal.on('hidden.bs.modal', function() {
                                callback && callback();
                            });
                        $modal.modal('hide');
                    } else {
                        callback && callback();
                    }
                },
                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(tracklist, section, callback) {
            if (view) {
                return;
            }
            view = new PageView({
                    context: {
                        parentTrack: tracklist.parentTrack,
                        tracks: tracklist.tracks,
                        section: section
                    },
                    onAfterConfirm: function() {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback();
                    }
                })
            view.show();
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    module.view.PopupMusicMenuView = (function() {
        var PageView = DropbeatView.extend({

                tmpl: '#tmpl-popup-music-menus',

                initialize: function(options) {
                    var that = this,
                        $popup,
                        d,
                        anchor,
                        carrot,
                        editable;

                    module._.bindAll(this, 'onWindowChanged', 'onWindowClicked');
                    DropbeatView.prototype.initialize.call(this, options);

                    editable = options.track.type === 'dropbeat' &&
                            module.context.account &&
                            module.context.account.resourceName === options.track.user.resource_name;

                    module.$.extend(this._context, {
                        track: options.track,
                        excludes: options.excludes,
                        editable: editable
                    });

                    $popup = module.$(this._tmpl(this._context));
                    module.$('#popup').append($popup);
                    module.$(window).on('resize', this.onWindowChanged);
                    module.$('.scroll, .scroll-y, .scroll-x').on('scroll', this.onWindowChanged);

                    this._onClose = options.onClose;
                    this.$el = module.$('#popup-music-menus');

                    anchor = options.anchor;
                    carrot  = this.$el.find('.carrot');

                    if (anchor.offset().top + this.$el.height() > module.$(window).height()) {
                        d = module.$(window).height() - anchor.offset().top;
                        this.$el.addClass('upside');
                        this.$el.css({
                            bottom:Math.round(d)
                        });
                    } else {
                        this.$el.css({
                            top: Math.round(anchor.offset().top + anchor.height()),
                        });
                    }

                    if (anchor.offset().left + this.$el.width() > module.$(window).width()) {
                        d = anchor.offset().left + anchor.width() / 2 - this.$el.width() + carrot.width() / 2;
                        this.$el.css({
                            left: Math.round(d)
                        });
                        carrot.addClass('right');
                    } else if (anchor.offset().left + anchor.width() / 2 - this.$el.width() / 2  < 0) {
                        d = anchor.offset().left + anchor.width() / 2 - carrot.width() / 2;
                        this.$el.css({
                            left: Math.round(d)
                        });
                        carrot.addClass('left');
                    } else {
                        this.$el.css({
                            left: Math.round(anchor.offset().left + anchor.width() / 2 - this.$el.width() / 2),
                        });
                        carrot.addClass('center');
                    }
                    this.$el.show();
                    this.$filter = module.$('.overlay-filter');
                    this.$filter.show().one('click', function() {
                        that._onClose && that._onClose();
                        that._onClose = null;
                        that.remove();
                    });
                    module.$(window).on('click', this.onWindowClicked);
                },

                events: function() {
                    var events = DropbeatView.prototype.events;
                    if (module._.isFunction(events)) {
                        events = events.call(this);
                    }

                    return module.$.extend ({}, events, {
                        'click a.menu' : 'onMenuClicked',
                    });
                },

                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(window).off('resize scroll', this.onWindowChanged);
                    module.$(window).off('click', this.onWindowClicked);
                    module.$('.scroll, .scroll-y, .scroll-x').off('scroll', this.onWindowChanged);
                },

                onWindowChanged: function(e) {
                    this.$filter.click();
                },

                onWindowClicked: function(e) {
                    this.$filter.click();
                    return false;
                },

                onMenuClicked: function(e) {
                    var $menu = module.$(e.currentTarget),
                        action = $menu.data('action');

                    this._onClose && this._onClose(action);
                    this._onClose = null;
                    subModule.hide();
                    this.$filter.hide();
                    return false;
                }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(options) {
            if (view) {
                return;
            }

            view = new PageView(module.$.extend({}, options, {
                    onClose: function(action) {
                        view.close ? view.close() : view.remove();
                        view = null;
                        options.onClose && options.onClose(action);
                    }
                }));
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.remove()
                view = null;
                callback && callback();
            }
        }, subModule);

        return subModule;
    } ());

    module.view.Failure404PageView = Failure404PageView;

    module.view.AboutPageView = AboutPageView;
    module.view.HomePageView = HomePageView;
    module.view.ExplorePageView = ExplorePageView;
    module.view.SharedPlaylistPageView = SharedPlaylistPageView;
    module.view.SharedTrackPageView = SharedTrackPageView;
    module.view.SearchResultPageView = SearchResultPageView;
    module.view.IframePageView = IframePageView;
    module.view.AppDownloadPageView = AppDownloadPageView;
    module.view.UploadPageView = UploadPageView;
    module.view.ProfilePageView = ProfilePageView;
    module.view.ResourcePageView = ResourcePageView;
    module.view.ResourceFollowingPageView = ResourceFollowingPageView;
    module.view.ResourceFollowersPageView = ResourceFollowersPageView;

    module.view.MobileSharedTrackPageView = MobileSharedTrackPageView;

    module.view.TrackPageView = TrackPageView;
    module.view.TrackEditPageView = TrackEditPageView;
    module.view.StatisticsPageView = StatisticsPageView;

    module.view.SoundcloudImportPageView = SoundcloudImportPageView;

    return module;
}($dbt));
