var $dbt = (function(module) {
    var view,
        Router;

    Backbone.Router.prototype.before = function () {};
    Backbone.Router.prototype.after = function () {};

// Backbone route override for before / after call on route
    Backbone.Router.prototype.route = function (route, name, callback) {
        if (!module._.isRegExp(route)) route = this._routeToRegExp(route);
            if (module._.isFunction(name)) {
                callback = name;
                name = '';
            }
            if (!callback) {
                callback = this[name];
            }

            var router = this;

            Backbone.history.route(route, function(fragment) {
                var args = router._extractParameters(route, fragment);

                router.before.call(router, name, args, function() {
                    callback && callback.apply(router, args);
                    router.after.apply(router, arguments);

                    router.trigger.apply(router, ['route:' + name].concat(args));
                    router.trigger('route', name, args);
                    Backbone.history.trigger('route', router, name, args);
                });
            });
        return this;
    };

    Router = Backbone.Router.extend({
        routes: {
            'ios(/)' : 'ios',
            'android(/)': 'android',
            'soundcloud-import(/)': 'soundcloudImport',
            'r/:name(/)': 'resource',
            'r/:ownerName/followers(/)': 'resourceFollowers',
            'r/:ownerName/following(/)': 'resourceFollowing',
            'r/:ownerName/:trackName/edit(/)': 'resourceTrackEdit',
            'r/:ownerName/:trackName(/)': 'resourceTrack',
            'statistics(/)': 'statistics',
            'upload(/)': 'upload',
            'me(/)': 'profile',
            'this-browser-is-not-supported(/)': 'notSupportedBrowser',
            'about(/)': 'about',
            'explore/:feedName(/)': 'explore',
            'home(/)': 'home',
            '(/)': 'root',
            '_=_(/)': 'root',
            '*path': 'failure404'
        },

        initialize:function() {
            this.beforeNavigates = {};
        },

        registerBeforeNavigate: function(key, callback) {
            this.beforeNavigates[key] = callback;
        },

        unregisterBeforeNavigate: function(key) {
            delete this.beforeNavigates[key];
        },

        navigate: function() {
            var canNavigate = true, msg;
            module.$.each(this.beforeNavigates, function(idx, callback) {
                if (callback && module._.isFunction(callback)) {
                    msg = callback();
                    if (!msg) {
                        return true;
                    }
                    canNavigate &= confirm(msg);
                }
                if (!canNavigate) {
                    return false;
                }
            });
            if (!canNavigate) {
                return;
            }
            return Backbone.Router.prototype.navigate.apply(this, arguments);
        },

        changeTitle: function(title) {
            document.title = title || 'Dropbeat';
        },

        before: function(route, args, callback) {
            var wrapped;
            if (route && ['ios', 'android', 'resourceTrack', 'resource']
                    .indexOf(route) > -1) {
                wrapped = callback;
            } else {
                wrapped = nonAndroid(this, nonIos(this, callback));
            }
            this.changeTitle();
            wrapped && wrapped();
        },

        after: function() {
        },

        failure404: function() {
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.Failure404PageView({
                path: window.location.pathname + window.location.search
            });
        },

        ios: function(path) {
            var context = {
                redirect : gup('redirect') ?
                    decodeURIComponent(gup('redirect')) : null,
                os:'ios'
            };

            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.AppDownloadPageView(context);
        },

        android: function(path) {
            var context = {
                redirect : gup('redirect') ?
                    decodeURIComponent(gup('redirect')) : null,
                os:'android'
            };

            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.AppDownloadPageView(context);
        },

        soundcloudImport: function() {
            loginRequired(this, function() {
                if (view && view.remove) {
                    view.remove();
                }
                view = new module.view.SoundcloudImportPageView({});
            })();
        },

        resource: function(resourceName) {
            var idx = resourceName.lastIndexOf('#');
            if (idx > -1) {
                resourceName = resourceName.substring(0, idx);
            }
            if (view && view.remove) {
                view.remove();
            }
            if ($dbt.context.account &&
                resourceName === $dbt.context.account.resourceName) {
                view = new module.view.ProfilePageView({});
                return;
            }

            view = new module.view.ResourcePageView({
                resourceName: resourceName
            });
        },

        resourceFollowers: function(userName) {
            if (view && view.remove) {
                view.remove();
            }
            if (userName.toLowerCase() === 'dropbeat') {
                view = new module.view.Failure404PageView({
                    path: window.location.pathname + window.location.search
                });
                return;
            }
            view = new module.view.ResourceFollowersPageView({
                resourceName: userName
            });
        },

        resourceFollowing: function(userName) {
            if (view && view.remove) {
                view.remove();
            }
            if (userName.toLowerCase() === 'dropbeat') {
                view = new module.view.Failure404PageView({
                    path: window.location.pathname + window.location.search
                });
                return;
            }
            view = new module.view.ResourceFollowingPageView({
                resourceName: userName
            });
        },

        resourceTrack: function(userName, trackName) {
            var idx = trackName.lastIndexOf('#');
            if (idx > -1) {
                trackName = trackName.substring(0, idx);
            }
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.TrackPageView({
                userName: userName,
                trackName: trackName
            });
        },

        resourceTrackEdit: function(userName, trackName) {
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.TrackEditPageView({
                userName: userName,
                trackName: trackName
            });
        },

        statistics: function() {
            loginRequired(this, function() {
                if (view && view.remove) {
                    view.remove();
                }
                view = new module.view.StatisticsPageView({});
            })();
            this.changeTitle('Statistics - Dropbeat');
        },

        upload: function() {
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.UploadPageView({});
            this.changeTitle('Upload - Dropbeat');
        },

        profile: function() {
            loginRequired(this, function() {
                module.router.navigate('/r/' + $dbt.context.account.resourceName,
                    {trigger:true, replace:true});
            })();
            this.changeTitle('Profile - Dropbeat');
        },

        notSupportedBrowser: function() {
            if (module.context.browserSupport.support) {
                window.location = '/';
                return;
            }
            module.view.ThisBrowserIsNotSupportedView.show();
        },

        copyright: function(path) {
            module.view.LegalView.show(function() {
                module.router.navigate('/', {trigger:true});
            });
            this.changeTitle('Copyright and legal - Dropbeat');
        },

        about: function(path) {
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.AboutPageView({});
            this.changeTitle('About - Dropbeat');
        },

        explore: function(feedName) {
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.ExplorePageView({
                name: feedName
            });
            this.changeTitle('Explore - Dropbeat');
        },

        home: function() {
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.HomePageView({});
            this.changeTitle('Home - Dropbeat');
        },

        root: function() {
            var q, sharedPlaylistUid, tab, sharedTrackUid;
            if (!module.context.browserSupport.support) {
                // Do nothing. We will do redirect
                return;
            }
            if (!module.s) {
                return;
            }
            if (view && view.remove) {
                view.remove();
            }

            if (sharedPlaylistUid = module.shareManager.onSharedList()) {
                view = new module.view.SharedPlaylistPageView({
                    playlistUid: sharedPlaylistUid
                });
                return false;
            }

            if (sharedTrackUid = gup('track')) {
                if (module.context.isMobile) {
                    view = new module.view.MobileSharedTrackPageView({
                        trackUid: sharedTrackUid
                    });
                } else {
                    view = new module.view.SharedTrackPageView({
                        trackUid: sharedTrackUid
                    });
                }
                return false;
            }

            q = gup('q');
            if (q) {
                q = decodeURIComponent(q);
                tab = gup('tab');
                view = new module.view.SearchResultPageView({
                    keyword: q === '_blank' ? null : q,
                    tab: tab
                });
                this.changeTitle(q + ' - Dropbeat search');
                return false;
            }

            if (!module.context.account) {
                $dbt.router.navigate('/about', { trigger: true, replace: true});
            } else {
                $dbt.router.navigate('/home', { trigger: true, replace: true});
            }
        }
    });

    function nonIos(context, callback) {
        var redirect = window.location.pathname + window.location.search;

        return function() {
            var playlistUid = gup('playlist'),
                trackUid = gup('track');
            if (module.context.isIOS && !playlistUid && !trackUid) {
                module.router.navigate('/ios?redirect=' + redirect,
                    {trigger:true, replace:true});
                return;
            }
            callback.apply(context, arguments);
        }
    }

    function nonAndroid(context, callback) {
        var redirect = window.location.pathname + window.location.search;

        return function() {
            var playlistUid = gup('playlist'),
                trackUid = gup('track');
            if (module.context.isAndroid && !playlistUid && !trackUid) {
                module.router.navigate('/android?redirect=' + redirect,
                    {trigger:true, replace:true});
                return;
            }
            callback.apply(context, arguments);
        }
    }

    function loginRequired(context, callback) {
        var redirect = window.location.pathname + window.location.search;
        return function() {
            if (!module.context.account) {
                module.view.NeedSigninView.show({
                    redirect:redirect
                });
                window.history.back();
                return;
            }
            callback.apply(context, arguments);
        }
    }


    function gup(name, url) {
        var regexS, regex, results;
        if (!url) {
            url = location.href
        }
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        regexS = "[\\?&]"+name+"=([^&#]*)";
        regex = new RegExp( regexS );
        results = regex.exec( url );
        return results == null ? null : results[1];
    }

    module = module || {};
    module.router = new Router();

    return module;
}($dbt));
