/*jslint browser: true*/
/*jslint nomen: true*/
/*global $, _*/
var $dbt = (function (module) {
    'use strict';

    module = module || {};
    module.feature = {};

    module.feature.urlAdder = {
        init: function () {
            var that = this,
                url,
                doSubmit = function () {
                    url = module.$(that.elems.urlAddInput).val();
                    if (!url ||
                            (url.indexOf('youtube.com') === -1
                            && url.indexOf('youtu.be') === -1
                            && url.indexOf('soundcloud.com') === -1)) {
                        module.s.notifyManager.invalidAdderUrl();
                    } else {
                        that.onSubmit(url);
                    }
                    return false;
                };

            module.$(this.elems.urlAddButton).click(doSubmit);
            module.$(this.elems.urlAddForm).on('submit', doSubmit);
        },

        elems: {
            urlAddForm: '.add-by-url-section .add-by-url-form',
            urlAddField: '.add-by-url-section .url-input-field-wrapper',
            urlAddInput: '.add-by-url-section #add-by-url-input',
            urlAddButton: '.add-by-url-section .add-button',
            loadingSpinner: '.add-by-url-section .loading-spinner'
        },

        adding: false,

        onSubmit: function (url) {
            var that = this;
            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }
            if (!this.adding) {
                this.hideAll();
                this.adding = true;

                module.coreLog('resolve', url);

                module.$.ajax({
                    crossDomain: true,
                    url: module.coreApi.resolveUrl,
                    data: {
                        'url': url,
                        'u': module.context.account.email
                    },
                    dataType: 'json',
                    success: function (data) {
                        that.urlAdderCallback(data.data);
                    }
                });
            }

        },

        urlAdderCallback: function (data) {
            var playlist =
                    module.playlistManager.getSelectedPlaylist(),
                success;

            if (data) {
                data.title = module.escapes.title(data.title);
                if (module.isKorean(data.title)) {
                    module.view.DropDropbeatView.show();
                    this.adding = false;
                    this.showAll();
                    this.clearInput();
                    return;
                }
                success = playlist.add(new module.s.Music(data), true);
                if (success) {
                    module.playlistManager.getSelectedPlaylist().sync();
                }
                module.log('playlist-add-from-url', 'add-' + data.type, data.title);
            } else {
                module.s.notifyManager.invalidAdderUrl();
            }
            this.adding = false;
            this.showAll();
            this.clearInput();
        },

        showAll: function () {
            module.$(this.elems.urlAddField).show();
            module.$(this.elems.loadingSpinner).hide();
        },

        hideAll: function () {
            module.$(this.elems.urlAddField).hide();
            module.$(this.elems.loadingSpinner).show();
        },

        clearInput: function () {
            module.$(this.elems.urlAddInput).val('');
        }
    };

    module.feature.search = {};

    module.feature.search.context = {
        searching: false,
        keyword: null
    };

    module.feature.search.box = {
        init: function () {
            var that = this;

            module.$('#dropbeat').on('keyup', '.search-input',
                module.$.proxy(this.onKeyUp, this));

            this._disableAutoComplete = false;
            this._autoCompleteReqs = {};
            this._submitReq = null;

            module.$('#dropbeat').on('click',
                    '.search-autocomplete .item', function(e) {
                var text = module.$(this).text(),
                    $searchMenu = module.$(e.currentTarget).closest('.search-menu'),
                    $input = module.$('.search-input', $searchMenu);

                $input.val(text);
                that.onSubmit(text);
            });

            module.$('.overlay-filter').on('click', function() {
                that.hideAutoComplete(false);
            });

            module.$('#dropbeat').on('submit', this.elems.searchForm, function(e) {
                var $searchMenu = module.$(e.currentTarget).closest('.search-menu'),
                    $input = module.$(that.elems.searchInput, $searchMenu);

                that.hideAutoComplete(true);
                that.onSubmit($input.val());
                return false;
            });
        },

        elems: {
            searchForm: '.search-menu .search-input-form',
            searchInput: '.search-input',
            searchResultSection: '.body-section .search-result-section',
            searchSpinner: '.body-section .search-spinner'
        },

        onKeyUp: function(e) {
            var that = this,
                $input = module.$(e.currentTarget),
                $searchMenu = $input.closest('.search-menu'),
                text = $input.val(),
                url = 'https://clients1.google.com/complete/search',
                data = {
                    'client': 'youtube',
                    'hl': 'en',
                    'gl': 'us',
                    'gs_rn' : '23',
                    'gs_ri': 'youtube',
                    'tok': 'I9KDmvOmJAg1Xq-coNjwGg',
                    'ds' : 'yt',
                    'cp' : '3',
                    'gs_gbg': 'K111AA607'
                },
                $selectedItem,
                size,
                req,
                ignoreKeyCodes = [
                    37, 39, 9, 16, 17, 18, 19, 20, 27, 33, 34, 35, 36, 45, 91,
                    92, 93, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121,
                    122, 123, 145
                ],
                $autocompleteTarget =
                    module.$('.search-autocomplete .items', $searchMenu);

// Case arrow down key pressed
            if (e.keyCode === 40) {
                size = module.$('.item', $autocompleteTarget).size();
                if (size === 0) {
                    return;
                }
                $selectedItem = module.$('.item:nth-child(' + (this._selectedIdx + 2) + ')',
                    $autocompleteTarget);
                if ($selectedItem.size() > 0) {
                    this._selectedIdx += 1;
                    module.$('.item.selected', $autocompleteTarget).removeClass('selected');
                    $selectedItem.addClass('selected');
                    $input.val($selectedItem.text());
                } else {
                    this._selectedIdx = -1;
                    module.$('.item.selected', $autocompleteTarget).removeClass('selected');
                    $input.val(this._q_text);
                }
                return;
// Case arrow up key pressed
            } else if (e.keyCode === 38) {
                size = module.$('.item', $autocompleteTarget).size();
                if (size === 0) {
                    return;
                }
                if (this._selectedIdx === 0) {
                    this._selectedIdx -= 1;
                    module.$('.item.selected', $autocompleteTarget).removeClass('selected');
                    $input.val(this._q_text);
                } else {
                    if (this._selectedIdx === -1) {
                        this._selectedIdx = size;
                    }
                    $selectedItem = module.$('.item:nth-child(' + this._selectedIdx + ')',
                        $autocompleteTarget);
                    if ($selectedItem.size() > 0) {
                        this._selectedIdx -= 1;
                        module.$('.item.selected', $autocompleteTarget).removeClass('selected');
                        $selectedItem.addClass('selected');
                        $input.val($selectedItem.text());
                    }
                }
                return;
            } else if (e.keyCode === 13) {
// Will handle in submit event
                module.log('search-section', 'search-with-keyword', text);
                return;
            } else if (ignoreKeyCodes.indexOf(e.keyCode) > -1) {
                return true;
            }

            if (this._disableAutoComplete ||
                    (this._lastText && this._lastText === text)) {
                return;
            }

            this._lastText = text;

            if (text === '' || e.keyCode === 27) {
                this.hideAutoComplete(true);
                return;
            }


            data['q'] = text;
            data['gs_id'] = this.makeRandId(2);

            req = module.$.ajax({
                url:url,
                data: data,
                dataType: 'jsonp'
            }).done(function(resp) {
                var q, words, appendix, respId, respHtml, excludeKoreanWords;
                delete that._autoCompleteReqs[data['gs_id']]

                if (that._disableAutoComplete) {
                    that.hideAutoComplete(true);
                    return;
                }

                q = resp[0];
                words = resp[1];
                appendix = resp[2];
                respId = appendix['j'];

                excludeKoreanWords = [];
                module.$.each(words, function(idx, item) {
                    if (!module.isKorean(item[0])) {
                        excludeKoreanWords.push(item[0]);
                    }
                });

                respHtml = '';
                module.$.each(excludeKoreanWords, function(idx, item) {
                    respHtml += '<li class=\'item\'>' + item + '</li>';
                });
                that._selectedIdx = -1;
                if (excludeKoreanWords && excludeKoreanWords.length > 0) {
                    that.showAutoComplete($autocompleteTarget, text, respHtml);
                } else {
                    that.hideAutoComplete(true);
                }
            }).fail(function(resp) {
                delete that._autoCompleteReqs[data['gs_id']]
            });
            this._autoCompleteReqs[data['gs_id']] = req;
        },

        makeRandId: function(length) {
            var text = '';
            var possible = 'abcdefghijklmnopqrstuvwxyz0123456789';

            for(var i=0; i < length; i++ ) {
                text += possible.charAt(
                    Math.floor(Math.random() * possible.length));
            }

            return text;
        },

        showAutoComplete: function($target, text, respHtml) {
            this._q_text = text
            $target.closest('.search-autocomplete').show();
            $target.html(respHtml);
            $target.show();
            module.$('.overlay-filter').show();
        },

        hideAutoComplete: function(hideWithFilter) {
            var $target;

            if (hideWithFilter) {
                module.$('.overlay-filter').click();
                return;
            }
            $target = module.$('.search-autocomplete .items')
            module.$.each(this._autoCompleteReqs, function(key, req) {
                req.abort();
            });
            this._q_text = null;
            $target.hide();
            $target.html('');
            $target.closest('.search-autocomplete').hide();
        },

        reset: function() {
            if (this._submitReq) {
                this._submitReq.abort();
                this._submitReq = null;
            }

            if (this._followingReq) {
                this._followingReq.abort();
                this._followingReq = null;
            }
            module.$(this.elems.searchResultSection).html('');

            this.hideAutoComplete(true);
            module.s.searchContext.keyword = null;
        },

        onSubmit: function (keyword) {
            var context = module.s.searchContext,
                path;

            module.$.each(this._autoCompleteReqs, function(idx, req) {
                req.abort();
            })
            if (this._submitReq) {
                this._submitReq.abort();
                this._submitReq = null;
            }
            this._autoCompleteReqs = {};
            this.hideAutoComplete(true);

            if (!keyword || keyword === '') {
                return;
            }

            if (context.searching || context.keyword === keyword) {
                return;
            }

            path = '/?q=' + encodeURIComponent(keyword);
            if (path === window.location.pathname) {
                return;
            }
            module.router.navigate(path, {trigger:true});
        },

        doSearch: function(keyword, callback) {
            var that = this,
                context = module.s.searchContext,
                data;

            if (module.isKorean(keyword)) {
                module.view.DropDropbeatView.show();
                return;
            }

            this._disableAutoComplete = true;

            context.searching = true;
            context.keyword = keyword;

// Set as trimed keyword
            keyword = keyword.trim();

            data = {'q': keyword};
            module.coreLog('search', keyword);

            this._submitReq = module.$.ajax({
                crossDomain: true,
                url: module.coreApi.searchUrl,
                data: data,
                dataType: 'json'
            }).done(function(data) {
                var excludeKoreans = {},
                    tracks = [];

                if (data && data.success && data.data) {
                    excludeKoreans.artists = data.data.artists;
                    module.$.each(data.data.tracks, function(idx, track) {
                        if (track.type === 'dropbeat') {
                            tracks.push(track);
                        } else if (track.title && !module.isKorean(track.title)) {
                            tracks.push(track);
                        }
                    });
                    excludeKoreans.tracks = tracks;

                    callback && callback(true, excludeKoreans);
                } else {
                    callback && callback(false, true);
                }
            }).fail(function(jqXHR, textStatus, error) {
                if (textStatus !== 'abort') {
                    callback && callback(false, true);
                } else {
                    callback && callback(false, false);
                }
                context.keyword = null;
            }).always(function(res, textStatus) {
                module.$(module.s.searchBox.elems.searchSpinner).hide();
                that._disableAutoComplete = false;
                context.searching = false;
                that._submitReq = null;
            });
            return this._submitReq;
        }
    };

    module.utils = module.utils || {};

    return module;
}($dbt));
