
var audioContext;


function decode(context, chunck, size) {

    audioContext.decodeAudioData(chunck, function(buffer) {
        var leftChannel = buff.getChannelData(0),
            len = leftChannel.length,
            barCount = 1000,
            i,
            barIdx,
            bars = [],
            maxValue;

        len = leftChannel.length;

        for (i = 0; i <  len; i++) {
            barIdx = Math.floor(i * (barCount - 1) / len);
            bars[barIdx] += Math.abs(leftChannel[i]);
        }

        // Normalize
        maxValue = Math.max.apply(null, bars);
        $.each(bars, function(idx, val) {
            bars[idx] = Math.round(val * 100 / maxValue);
        });

        postFinished(bars);
    }, function(e) {
        postFailure(e.err);
    });
}


self.onmessage = function(e) {
    var obj = e.data,
        chunck,
        size;

    if (obj.action === "init") {
        audioContext = obj.audioContext;
        chunck = obj.chunck;
        size = obj.size;
        decode(chunck, size);
    }
};

function _post(eventName, data) {
    self.postMessage({ eventName: eventName, data: data});
}

function postFailed(msg) {
    _post("onFailed", { msg: msg});
}

function postFinished(data) {
    _post("onFinished", {data:data});
}

function postProgress(percentage) {
    _post("onProgress", { percentage: percentage });
}

function postInitialized() {
    _post("onInitialized", {});
}
