var $dbt = (function(module) {
    var view,
        Router;

    Backbone.Router.prototype.before = function () {};
    Backbone.Router.prototype.after = function () {};

// Backbone route override for before / after call on route
    Backbone.Router.prototype.route = function (route, name, callback) {
        if (!module._.isRegExp(route)) route = this._routeToRegExp(route);
            if (module._.isFunction(name)) {
                callback = name;
                name = '';
            }
            if (!callback) {
                callback = this[name];
            }

            var router = this;

            Backbone.history.route(route, function(fragment) {
                var args = router._extractParameters(route, fragment);

                router.before.call(router, name, args, function() {
                    callback && callback.apply(router, args);
                    router.after.apply(router, arguments);

                    router.trigger.apply(router, ['route:' + name].concat(args));
                    router.trigger('route', name, args);
                    Backbone.history.trigger('route', router, name, args);
                });
            });
        return this;
    };

    Router = Backbone.Router.extend({
        routes: {
            'this-browser-is-not-supported(/)': 'notSupportedBrowser',
            'label_admin(/)': 'root',
            'label_admin/inbox(/)': 'inbox',
            'label_admin/settings(/)': 'settings',
            '*path': 'failure404'
        },

        initialize:function() {
            this.beforeNavigates = {};
        },

        registerBeforeNavigate: function(key, callback) {
            this.beforeNavigates[key] = callback;
        },

        unregisterBeforeNavigate: function(key) {
            delete this.beforeNavigates[key];
        },

        navigate: function() {
            var canNavigate = true, msg;
            module.$.each(this.beforeNavigates, function(idx, callback) {
                if (callback && module._.isFunction(callback)) {
                    msg = callback();
                    if (!msg) {
                        return true;
                    }
                    canNavigate &= confirm(msg);
                }
                if (!canNavigate) {
                    return false;
                }
            });
            if (!canNavigate) {
                return;
            }
            return Backbone.Router.prototype.navigate.apply(this, arguments);
        },

        changeTitle: function(title) {
            document.title = title || 'Dropbeat';
        },

        before: function(route, args, callback) {
            var wrapped;
            wrapped = callback;
            this.changeTitle();
            wrapped && wrapped();
        },

        after: function() {
        },

        failure404: function() {
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.Failure404PageView({
                path: window.location.pathname + window.location.search
            });
        },

        notSupportedBrowser: function() {
            if (module.context.browserSupport.support) {
                window.location = '/';
                return;
            }
            module.view.ThisBrowserIsNotSupportedView.show();
        },

        inbox: function() {
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.InboxPageView({});
            this.changeTitle('Inbox - Dropbeat Label Admin');
        },

        root: function() {
            module.router.navigate('/label_admin/inbox', 
                {trigger:true, replace:true});
        },

        settings: function() {
            if (view && view.remove) {
                view.remove();
            }
            view = new module.view.SettingsPageView({});
            this.changeTitle('Settings - Dropbeat Label Admin');
        }

    });

    function loginRequired(context, callback) {
        var redirect = window.location.pathname + window.location.search;
        return function() {
            if (!module.context.account) {
                module.view.NeedSigninView.show();
                window.history.back();
                return;
            }
            callback.apply(context, arguments);
        }
    }


    function gup(name, url) {
        var regexS, regex, results;
        if (!url) {
            url = location.href
        }
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        regexS = "[\\?&]"+name+"=([^&#]*)";
        regex = new RegExp( regexS );
        results = regex.exec( url );
        return results == null ? null : results[1];
    }

    module = module || {};
    module.router = new Router();

    return module;
}($dbt));
