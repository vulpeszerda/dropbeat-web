var $dbt = (function(module) {
    'use strict';

    var DropbeatView,
        DropbeatPageView,

        PlayableViewMixin,
        WaveformViewMixin,

        InboxPageView,
        SettingsPageView,
        ThisBrowserIsNotSupportedView,
        Failure404PageView,
        NewLabelModalView,
        TrackFilterModalView,

        ReceivedMusic;

    module = module || {};

////////////
// Mixins //
////////////

    WaveformViewMixin = {
        bind: function(context, $el) {
            module.$.extend(context, this);
            context.bindWaveformRenderer($el);
        },

        unbind: function(context) {
            if (context.unbindWaveformRenderer) {
                context.unbindWaveformRenderer();
            }
        },

        bindWaveformRenderer: function($el) {
            module._.bindAll(this, '_onWindowResize', '_onWindowScroll',
                '_onMusicProgressUpdate', '_onWaveformProgressDown',
                '_onWaveformProgressMove', '_onWaveformProgressUp',
                '_onWaveformLoadingMusic');

            this.waveformRawData = {};
            this.waveformData = {};
            this.waveformReq = {};
            this._musicContainer = '.playable-track';
            this._playSections = this._playSections || [];
            this._$playableFrame = $el;

            module.$(module).on('loadingMusic', this._onWaveformLoadingMusic);
            module.$(module).on('musicProgressUpdate', this._onMusicProgressUpdate);
            this._$playableFrame.on('mousedown', '.waveform-progress', this._onWaveformProgressDown);
            module.$('.contents .scroll-y').on('scroll', this._onWindowScroll);

            module.$(window).on('resize', this._onWindowResize);
            module.$(window).on('mousemove', this._onWaveformProgressMove);
            module.$(window).on('mouseup', this._onWaveformProgressUp);
        },

        unbindWaveformRenderer: function() {
            var that = this;

            module.$(module).off('loadingMusic', this._onWaveformLoadingMusic);
            module.$(module).off('musicProgressUpdate', this._onMusicProgressUpdate);
            this._$playableFrame.off('mousedown', '.waveform-progress', this._onWaveformProgressDown);
            module.$('.contents .scroll-y').off('scroll', this._onWindowScroll);

            module.$(window).off('resize', this._onWindowResize);
            module.$(window).off('mousemove', '.waveform-progress', this._onWaveformProgressMove);
            module.$(window).off('mouseup', '.waveform-progress', this._onWaveformProgressUp);

            module.$.each(this.waveformReq, function(key, req) {
                req.abort();
                delete that.waveformReq[key];
            });

            module.$.each(this.waveformData, function(key, val) {
                delete that.waveformData[key];
            });

            module.$.each(this.waveformRawData, function(key, val) {
                delete that.waveformRawData[key];
            });
        },

        _onWindowScroll: function(e) {
            var $scroll = module.$('.contents .scroll-y'),
                scrollTop = $scroll.scrollTop(),
                THRESHOLD = 100;

            if  (this._lastScroll && Math.abs(this.lastScroll - scrollTop) < THRESHOLD) {
                return;
            }
            this._lastScroll = scrollTop;
            this.updateWaveform();
        },

        _onWindowResize: function(e) {
            var $waveform = module.$('.waveform-progress', this._$playableFrame),
                $playingWaveform,
                $progress,
                percent,
                that = this;

// Only update waveform width changed
            if (this._waveformWidth && $waveform.width() === this._waveformWidth) {
                return;
            }

// Re calculate bin size
            module.$.each(this.waveformRawData, function(url, data) {
                var $el = module.$('.waveform-progress[data-url=\'' + url + '\']', that._$playableFrame);
                that.waveformData[url] = that._calculateBars($el, data);
            });
            $waveform.removeClass('loaded');
            this.updateWaveform(true);

// Update progressing waveform
            $progress = $waveform.find('.progress .waveform-canvas');
            if ($progress.size() > 0 && (percent = $progress.data('percent'))) {
                $playingWaveform = $progress.closest('.waveform-progress');
                this._updateWaveformProgress($playingWaveform, parseInt(percent));
            }
        },

        _calculateBars: function($el, data) {
            var barCount,
                barWidth = 2,
                marginWidth = 1,
                width = $el.width();

            barCount = Math.floor(width / (barWidth + marginWidth));
            if (barCount === 0) {
                return [];
            }

            return module.utils.downsizeArray(data, barCount);
        },

        updateWaveform: function() {
            var that = this,
                $waveform = module.$(that._musicContainer +
                    '.on .waveform-progress', this._$playableFrame),
                $scroll = module.$('.contents .scroll-y'),
                scrollTop = $scroll.scrollTop(),
                windowHeight = module.$(window).height();

            if ($waveform.size() === 0) {
                return;
            }

            this._waveformWidth = $waveform.width();

            $waveform.each(function(idx, el) {
                var $el = module.$(el),
                    $container = $el.closest(that._musicContainer),
                    url = $el.data('url'),
                    nonSecureUrl,
                    data,
                    offsetTop = $container.offset().top,
                    containerHeight = $container.height(),
                    musicData;

                musicData = extractMusicFromDOM(that, $container);

                if (that.waveformReq[url]) {
                    return;
                }

                if (!$waveform.closest('.track').hasClass('on') &&
                        (offsetTop + containerHeight < -100 ||
                            offsetTop > windowHeight + 100)) {
                    $el.hide();
                    return;
                } else {
                    $el.show();
                }
                if ($el.hasClass('loaded') || $el.hasClass('failed')) {
                    return;
                }

                data = that.waveformData[url];
                if (data) {
                    renderWaveform($el, data);
                    return;
                }

                if (musicData && musicData.waveform_data) {
                    that.waveformRawData[url] = musicData.waveform_data;
                    that.waveformData[url] = that._calculateBars($el, musicData.waveform_data);
                    renderWaveform($el, that.waveformData[url]);
                    return;
                }

// XXX:
// In case IE9, ajax request through http -> https will be blocked by browser
// So our waveform request which is to AWS will be also blocked cause its https
// But thanksfully AWS supports normal http request,
// we will replace request url to http
                if (url.startsWith('https://')) {
                    nonSecureUrl = url.replace('https://', 'http://');
                }
                that.waveformReq[url] = module.$.ajax({
                    url: nonSecureUrl || url,
                    dataType:'json',
                    crossDomain: true
                }).done(function(data) {
                    that.waveformRawData[url] = data;
                    that.waveformData[url] = that._calculateBars($el, data);
                    renderWaveform($el, that.waveformData[url]);
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus === 'abort') {
                        return;
                    }
                    that.waveformData[url] = [];
                    $el.addClass('failed');
                }).always(function() {
                    delete that.waveformReq[url];
                });
            });

            function renderWaveform($el, bars) {
                var $bgCanvas,
                    bgCanvas,
                    $drop = $el.find('.drop'),
                    $dropCanvas,
                    dropCanvas,
                    width = $el.width(),
                    height = $el.height(),
                    barWidth = 2,
                    marginWidth = 1,
                    colorNormal = '#aaaaaa',
                    //colorDrop = '#cd85ff',
                    colorDrop = '#f03eb1',
                    duration,
                    dropStart,
                    dropEnd,
                    fromIdx,
                    toIdx,
                    i,
                    fromX,
                    toX,
                    maxValue,
                    context;

                $bgCanvas = module.$('.bg .waveform-canvas', $el);
                if ($bgCanvas.size() === 0) {
                    $bgCanvas = module.$('<canvas class=\'waveform-canvas\'></canvas>');
                    $el.find('.bg').html($bgCanvas);
                }
                $bgCanvas.css({
                    width: width,
                    height: height
                });
                bgCanvas = $bgCanvas[0];

                $dropCanvas = module.$('.drop .waveform-canvas', $el);
                if ($dropCanvas.size() === 0) {
                    $dropCanvas = module.$('<canvas class=\'waveform-canvas\'></canvas>');
                    $el.find('.drop').html($dropCanvas);
                }
                $drop.html($dropCanvas);

                bgCanvas.width = width;
                bgCanvas.height = height;

                context = bgCanvas.getContext('2d');
                context.clearRect(0, 0, bgCanvas.width, bgCanvas.height);

                module.$.each(bars, function(idx, val) {

                    var x, barHeight, y;
                    x = idx * (barWidth + marginWidth);
                    barHeight = val * height * 0.009;
                    y = height - barHeight;
                    context.fillStyle = colorNormal;
                    context.fillRect(x, y, barWidth, barHeight);
                });
                context.save();

                if ($drop.size() > 0) {

                    duration = $el.data('duration');
                    dropStart = $el.find('.drop').data('dropStart');
                    dropEnd = $el.find('.drop').data('dropEnd');
                    fromX = Math.round((dropStart / duration) * width);
                    toX = Math.round((dropEnd / duration) * width);
                    fromIdx = Math.floor(fromX / (barWidth + marginWidth));
                    toIdx = Math.ceil(toX / (barWidth + marginWidth));

                    dropCanvas = $dropCanvas[0];
                    dropCanvas.width = width;
                    dropCanvas.height = height;

                    $dropCanvas.css({
                        width: width,
                        height: height
                    });

                    context = dropCanvas.getContext('2d');
                    context.globalAlpha=0.8;
                    context.clearRect(0, 0, dropCanvas.width, dropCanvas.height);

                    for(i = fromIdx; i < bars.length && i < toIdx; i++) {
                        var val, x, barHeight, y, delta, w;

                        val = bars[i];
                        x = i * (barWidth + marginWidth);
                        barHeight = val * height * 0.009;
                        y = height - barHeight;
                        context.fillStyle = colorDrop;
                        w = barWidth;
                        if (i === fromIdx) {
                            delta = x;
                            x = Math.max(x, fromX);
                            delta = x - delta;
                            w -= delta;
                        }
                        if (i === toIdx - 1) {
                            delta = Math.min(x + w, toX) - x;
                            w = delta;
                        }
                        context.fillRect(x, y, w, barHeight);
                    }
                }
                $el.addClass('loaded');
            }
        },

        _updateWaveformProgress: function($el, progress) {
            var $canvas = $el.find('.progress .waveform-canvas'),
                canvas,
                width = $el.width(),
                height = $el.height(),
                barWidth = 2,
                marginWidth = 1,
                colorNormal = '#606060',
                bars,
                i,
                toX,
                toIdx,
                maxValue,
                context,
                url = $el.data('url');

            if (!this.waveformData[url]) {
                return;
            }

            bars = this.waveformData[url];

            toX = Math.round((progress / 100) * width);
            toIdx = Math.ceil(toX / (barWidth + marginWidth));

            if ($canvas.size() === 0) {
                $el.find('.progress').html('<canvas class=\'waveform-canvas\'></canvas>');
                $canvas = $el.find('.progress .waveform-canvas');
            }
            $canvas.data('percent', progress);
            $canvas.css({
                width: width,
                height: height
            });
            canvas = $canvas[0];

            canvas.width = width;
            canvas.height = height;
            context = canvas.getContext('2d');
            context.clearRect(0, 0, canvas.width, canvas.height);

            for(i = 0; i < bars.length && i < toIdx; i++) {
                var val, x, barHeight, y, delta, w;

                val = bars[i];
                x = i * (barWidth + marginWidth);
                barHeight = val * height * 0.009;
                y = height - barHeight;
                context.fillStyle = colorNormal;
                w = barWidth;
                if (i === toIdx - 1) {
                    delta = Math.min(x + w, toX) - x;
                    w = delta;
                }
                context.fillRect(x, y, w, barHeight);
            }
        },

        _onWaveformProgressDown: function(e) {
            var playerManager = module.playerManager,
                currentMusic = playerManager.currentMusic,
                $el = module.$(e.currentTarget),
                $waveform, offsetLeft, x, left, width;

            if (!currentMusic ||
                    String($el.closest(this._musicContainer).data('musicId')) !==  currentMusic.id ||
                    !playerManager.playing ||
                    playerManager.currentPlayer.section === 'drop') {
                return;
            }
            this._waveformProgressTarget = $el;
            this._waveformProgressDown = true;
            if (this._seekTimer) {
                clearTimeout(this._seekTimer);
                this._seekTimer = null;
            }

            offsetLeft = $el.offset().left;
            width = $el.width();
            x = e.pageX - offsetLeft;
            left = x * 100 / width;

            if (left < 0) {
                left = 0;
            }
            if (left > $el.width()) {
                left = 100;
            }
            this._updateWaveformProgress($el, left);
        },

        _onWaveformProgressMove: function(e) {
            var $waveform, offsetLeft, x, left, width;

            if (this._waveformProgressDown && this._waveformProgressTarget) {
                offsetLeft = this._waveformProgressTarget.offset().left;
                width = this._waveformProgressTarget.width();
                x = e.pageX - offsetLeft;
                left = x * 100 / width;

                if (left < 0) {
                    left = 0;
                }
                if (left > this._waveformProgressTarget.width()) {
                    left = 100;
                }
                $waveform = this._waveformProgressTarget;
                this._updateWaveformProgress($waveform, left);
            }
        },

        _onWaveformProgressUp: function(e) {
            var playerManager = module.playerManager,
                currentMusic = playerManager.currentMusic,
                $musicContainer,
                seekTo, offsetLeft, x, left, duration, width, curr, $waveform;

            if (this._waveformProgressDown && this._waveformProgressTarget) {
                duration = parseInt(this._waveformProgressTarget.data('duration'));

                $musicContainer = this._waveformProgressTarget.closest(this._musicContainer);
                if (!currentMusic ||
                        String($musicContainer.data('musicId')) !==  currentMusic.id ||
                        !playerManager.playing ||
                        playerManager.currentPlayer.section === 'drop' ||
                        !duration) {

                    if (duration) {
                        curr = playerManager.getCurrentPlaybackTime();
                        left = curr * 100 / duration;
                    } else {
                        left = 0;
                    }
                    $waveform = this._waveformProgressTarget;
                    this._updateWaveformProgress($waveform, left);

                    this._waveformProgressTarget = null;
                    this._waveformProgressDown = null;
                    return;
                }
                offsetLeft = this._waveformProgressTarget.offset().left;
                width = this._waveformProgressTarget.width();

                x = e.pageX - offsetLeft;
                left = x * 100 / width;

                if (left < 0) {
                    left = 0;
                }
                if (left > this._waveformProgressTarget.width()) {
                    left = 100;
                }
                seekTo = duration * left / 100;

                playerManager.seekTo(seekTo);
                this._waveformProgressTarget = null;

                this._seekTimer = setTimeout(module.$.proxy(function() {
                    this._waveformProgressDown = false;
                }, this), 1000);
            }
        },

        _onWaveformLoadingMusic: function(e, m, s) {
            this._$playableFrame.find(this._musicContainer + ' .playback-time').text('00:00');
            this._$playableFrame.find(this._musicContainer + ' .duration-time').text('00:00');
            this._$playableFrame.find(this._musicContainer +
                ' .waveform-progress .progress canvas').remove();
        },

        _onMusicProgressUpdate: function(e, progText, music, section) {
            var $musicContainer,
                $targetMusic,
                $targetPlayback,
                $targetDuration,
                $targetProgress;

            if (!music) {
                return;
            }

            $targetMusic = this._$playableFrame.find(
                this._musicContainer + '[data-music-id=\'' + music.id + '\']');

            if ($targetMusic.size() === 0) {
                return;
            }

            $targetPlayback = $targetMusic.find('.playback-time');
            $targetPlayback.text(moment.duration({seconds:progText.playback}).format('mm:ss'));

            $targetDuration = $targetMusic.find('.duration-time');
            $targetDuration.text(moment.duration({seconds:progText.duration}).format('mm:ss'));

            if (this._waveformProgressDown) {
                return;
            }

            if (progText.duration > 0) {
                $targetProgress = $targetMusic.find('.waveform-progress');
                this._updateWaveformProgress($targetProgress, progText.playback * 100 / progText.duration);
            }
        }
    };

// Mixin class for playable view event listen
// call simply PlayableViewMixin.bind from view's initailize func
// and call PlayableViewMixin.unbind from view's remove func
    PlayableViewMixin = {
        bind: function(context, $el, sections) {
            module.$.extend(context, this);
            context.bindPlayableView($el, sections);
        },

        unbind: function(context) {
            var publicPlaylists, playlist, playlistId;

            if (context.unbindPlayableView) {
                context.unbindPlayableView();
            }
        },

        bindPlayableView: function($el, sections) {
            var that = this;

            module._.bindAll(this, '_onPlayMusicClicked',
                '_onPlayInfoUpdate', '_onTogglePlay',
                '_onDropBtnMouseEnter', '_onDropBtnClicked', '_onDropBtnMouseLeave',
                '_onPlayerStopped', '_onPlayPlaylistBtnClicked',
                '_onPlayCtrlClicked', '_onMusicFinished',
                '_onDownloadMusicClicked', '_onLoadingMusic', '_onPlayMusic');

            this._musicContainer = '.playable-track';
            this._$playableFrame = $el;
            this._playSections = sections || [];
            this._musicStorage = {};
            if (this.parentMusicStorage) {
                module.$.each(this.parentMusicStorage, function(key, val) {
                    that._musicStorage[key] = val;
                });
            }

            this._$playableFrame.on('click', '.play-music', this._onPlayMusicClicked);
            this._$playableFrame.on('click', '.btn-download', this._onDownloadMusicClicked);
            this._$playableFrame.on('click', '.listen-drop', this._onDropBtnClicked);
            this._$playableFrame.on('mouseenter', 'listen-drop', this._onDropBtnMouseEnter);
            this._$playableFrame.on('mouseleave', 'listen-drop', this._onDropBtnMouseLeave);
            this._$playableFrame.on('click', '.play-playlist-btn', this._onPlayPlaylistBtnClicked);
            this._$playableFrame.on('click', '.ctrl-play', this._onPlayCtrlClicked);
            module.$(module).on('playInfoUpdate loadingMusic', this._onPlayInfoUpdate);
            module.$(module).on('finishedMusic playerStopped', this._onMusicFinished);
            module.$(module).on('togglePlay', this._onTogglePlay);
            module.$(module).on('loadingMusic', this._onLoadingMusic);
            module.$(module).on('musicProgressUpdate', this._onPlayMusic);
            module.$(module).on('playerStopped', this._onPlayerStopped);
        },

        updatePlayState: function() {
            var playerManager = module.playerManager,
                currentTrack = playerManager.getCurrentMusic(),
                $onMusic;

            this._$playableFrame.find(this._musicContainer).removeClass('on');
            if (currentTrack) {
                $onMusic = this._$playableFrame.find(this._musicContainer +
                    '[data-music-id=\'' + currentTrack.id + '\']');
                $onMusic.addClass('on');

                if (playerManager.playing) {
                    $onMusic.find('.ctrl-play').addClass('pause');
                } else {
                    $onMusic.find('.ctrl-play').removeClass('pause');
                }
            }
        },

        _onTogglePlay: function(e, music, section, isPlaying) {
            var $musicContainer,
                $onMusic,
                $targetMusic;

            this.onTogglePlay && this.onTogglePlay.apply(this, arguments);

            $onMusic = this._$playableFrame.find(this._musicContainer + '.on');
            if (!music) {
                if (!isPlaying) {
                    $onMusic.find('.ctrl-play').removeClass('pause');
                }
            } else {
                $targetMusic = this._$playableFrame.find(
                    this._musicContainer + '[data-music-id=\'' + music.id + '\']');
                if ($targetMusic.size() > 0) {
                    if (isPlaying) {
                        $targetMusic.find('.ctrl-play').addClass('pause');
                    } else {
                        $targetMusic.find('.ctrl-play').removeClass('pause');
                    }
                }
            }
            if (section === 'drop') {
                module.$('.play-music', this._$playableFrame).removeClass('pause');
                module.$('.listen-drop', this._$playableFrame).removeClass('pause');

                if (module.playerManager.playing) {
                    $musicContainer = module.$(this._musicContainer + '.on', this._$playableFrame);
                    $musicContainer.find('.listen-drop').addClass('pause');
                }
            }
        },

        _onPlayerStopped: function() {
            var $onMusic = this._$playableFrame.find(this._musicContainer + '.on');
            $onMusic.removeClass('on');
        },

        _onPlayPlaylistBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $music = this._$playableFrame.find(this._musicContainer).first();

            if ($btn.hasClass('playing')) {
                return;
            }
            $music.find('.play-music').click();
        },

        _onDownloadMusicClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                resourcePath = $btn.data('target');

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }
            $dbt.view.FollowToDownloadView.show(resourcePath);
        },

        _onMusicFinished: function(e, music, section) {
            var $onMusic = this._$playableFrame.find(this._musicContainer + '.on');
            $onMusic.removeClass('on');
            $onMusic.find('.listen-drop').removeClass('loading');
        },

        _onPlayInfoUpdate: function(e, music, section) {
            var $onMusic = this._$playableFrame.find(this._musicContainer + '.on'),
                musicData,
                $newOnMusic;

            musicData = extractMusicFromDOM(this, $onMusic);

            if (section === 'drop') {
                if (!musicData || !musicData.drop || musicData.drop.dref !== music.id) {
                    $onMusic.removeClass('on');
                    $newOnMusic = this._$playableFrame
                        .find(this._musicContainer + '[data-drop-music-id=\'' + music.id + '\']');
                    $newOnMusic.addClass('on');
                }
                return;
            }
            $onMusic.removeClass('on');
            $newOnMusic = this._$playableFrame
                .find(this._musicContainer + '[data-music-id=\'' + music.id + '\']');
            $newOnMusic.addClass('on');
        },

        getPlaySection: function(e) {
            return this._playSections[0];
        },

        _onDropBtnClicked: function(e) {
            var $dropBtn = module.$(e.currentTarget),
                $musicContainer,
                musicData,
                currMusic,
                that = this,
                loading = false;

            if ($dropBtn.hasClass('loading')) {
                return;
            }
            this._$playableFrame.find('.listen-drop').removeClass('loading');

            $musicContainer = $dropBtn.closest(this._musicContainer);

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData || !musicData.drop) {
                return;
            }

            this._$playableFrame.find(this._musicContainer + '.on').removeClass('on');
            $musicContainer.addClass('on');

            currMusic = module.playerManager.getCurrentMusic();
            if (module.playerManager.currentPlayer &&
                    module.playerManager.currentPlayer.section === 'drop') {

// We should puase music
                if (!currMusic ||
                        currMusic.type !== musicData.drop.type ||
                        currMusic.id !== musicData.drop.dref ||
                        (!module.playerManager.playing && !$dropBtn.hasClass('loading'))) {
                    loading = true;
                    $dropBtn.addClass('loading');
                    module.$(module).one(
                        'playMusic',
                        function(e, music, s) {
                            bindDropTimer();
                            $dropBtn.removeClass('loading');
                        });
                }
                module.playerManager.onPlayMusic(
                    new module.music.DropMusic({
                        'id': musicData.drop.dref,
                        'title': musicData.title,
                        'type': musicData.drop.type,
                        'original': musicData
                    }), 'drop', musicData.drop.when);

                if (!loading) {
                    bindDropTimer();
                }
            } else {
                $dropBtn.addClass('loading');
                module.$(module).one(
                    'playMusic',
                    function(e, music, s) {
                        bindDropTimer();
                        $dropBtn.removeClass('loading');
                    });
                module.playerManager.onMusicClicked(
                    new module.music.DropMusic({
                        'id': musicData.drop.dref,
                        'title': musicData.title,
                        'type': musicData.drop.type,
                        'original': musicData
                    }), 'drop', { startPoint: musicData.drop.when});
            }

            function bindDropTimer() {
                if (that._dropPlayerTimer) {
                    clearTimeout(that._dropPlayerTimer);
                    that._dropPlayerTimer = null;
                }
                if (module.playerManager.playing) {
                    that._dropPlayerTimer = setTimeout(function() {
                        var currMusic;
                        that._dropPlayerTimer = null;
                        if (!module.playerManager.playing ||
                                !module.playerManager.currentPlayer ||
                                module.playerManager.currentPlayer.section !== 'drop') {
                            return;
                        }
                        currMusic = module.playerManager.getCurrentMusic();
                        if (currMusic.id === musicData.drop.dref &&
                                currMusic.type === musicData.drop.type) {
                            module.playerManager.stop('drop');
                        }
                    }, 20000);
                }
            }
        },

        _onDropBtnMouseEnter: function(e) {
            var $target = module.$(e.currentTarget),
                $wrapper = $target.closest('.title-wrapper');
            $wrapper.find('.play-music').addClass('hover');
        },

        _onDropBtnMouseLeave: function(e) {
            var $target = module.$(e.currentTarget),
                $wrapper = $target.closest('.title-wrapper');
            $wrapper.find('.play-music').removeClass('hover');
        },

        openTrackList: function($musicContainer, musicData) {
            var $wrapper,
                $tracks,
                $openedChildren,
                that = this;

            $wrapper = $musicContainer.closest('.a-addable-music-wrapper');
            if ($wrapper.size() === 0) {
                return;
            }

// Hide previous played music
            if (this._tracklistReq) {
                this._tracklistReq.abort();
                this._tracklistReq = null;
            }
            if (!$musicContainer.hasClass('opened')) {
                if (this._$currMusicContainer) {
                    hideSubTracks(this._$currMusicContainer,
                            $musicContainer,
                            function() {
                                $musicContainer.addClass('opened');
                                that._$currMusicContainer = $musicContainer;
                            });
                } else {
                    $musicContainer.addClass('opened');
                    this._$currMusicContainer = $musicContainer;
                }
            }
            if ($musicContainer.next().hasClass('tracks')) {
                $tracks = $musicContainer.next();

                $openedChildren =
                    module.$('.a-addable-music.opened', $tracks);
                $openedChildren.removeClass('opened');
                return;
            }
        },

        _onPlayCtrlClicked: function(e) {
            var $playCtrl = module.$(e.currentTarget),
                $musicContainer,
                $playlist,
                musicData,
                section = this.getPlaySection(e),
                playlistId,
                startPoint,
                cb;

            $musicContainer = module.$(e.currentTarget).closest(this._musicContainer);

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return;
            }

            $playlist = $musicContainer.closest('.a-playable-list');

            cb = module.$.proxy(function(musicData) {
                if ($playCtrl.hasClass('pause')) {
                    module.playerManager.onPlayMusic(musicData, section);
                    return;
                }

                module.s.progressController.init(section,
                    $musicContainer, musicData.id);

                this.openTrackList($musicContainer, musicData);

                if ($playlist.size() > 0) {
                    playlistId = $playlist.data('playlistId');
                }

                module.playerManager.onMusicClicked(
                    musicData,
                    section, {
                        playlistId: playlistId
                    });
            }, this);

            if (musicData.tag === 'undefined_track' &&
                    (!musicData.id || musicData.id === '')) {
                this._resolveUndefinedTrack($musicContainer, musicData, cb);
            } else {
                cb(musicData);
            }
        },

        _onPlayMusicClicked: function(e, startPoint) {
            var $musicContainer,
                $playlist,
                musicData,
                section = this.getPlaySection(e),
                cb, playlistId;

            $musicContainer = module.$(e.currentTarget).closest(this._musicContainer);
            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return
            }

            $playlist = $musicContainer.closest('.a-playable-list');

            cb = module.$.proxy(function(musicData) {
                module.s.progressController.init(section,
                    $musicContainer, musicData.id);

                this.openTrackList($musicContainer, musicData);

                if ($playlist.size() > 0) {
                    playlistId = $playlist.data('playlistId');
                }
                module.playerManager.onMusicClicked(
                    musicData,
                    section, {
                        playlistId: playlistId,
                        startPoint: startPoint
                    });
            }, this);

            if (musicData.tag === 'undefined_track' &&
                    (!musicData.id || musicData.id === '')) {
                this._resolveUndefinedTrack($musicContainer, musicData, cb);
            } else {
                cb(musicData);
            }
        },

        _resolveUndefinedTrack: function($musicContainer, music, callback) {
            var that = this;

            if (this._resolveReq) {
                this._resolveReq.abort();
            }

            this._resolveRes = module.$.ajax({
                url: module.coreApi.queryIndexUrl,
                data: JSON.stringify({
                    q: music.title
                }),
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                crossDomain: true
            }).done(function(res) {
                var id, type, $parentContainer, m, key;

                if (!res || !res.success || !res.data) {
                    module.s.notify(module.string('failed_to_fetch_music_info'), 'error');
                    return;
                }
                id = res.data;
                type = 'soundcloud';
                if (!/^\d+$/.test(id)) {
                    if (id.startsWith('http')) {
                        type = 'podcast';
                    } else {
                        type = 'youtube';
                    }
                }

                key = type + '_' + id;

                m = new module.music.Music({
                        id: id,
                        type: type,
                        title: music.title
                    });

                that.setMusicToStorage(m.getStorageKey(), m);
                $musicContainer.data('musicStorageKey', key);
                $musicContainer.attr('data-music-storage-key', key);
                $musicContainer.data('musicId', id);
                $musicContainer.attr('data-music-id', id);

                $parentContainer = $musicContainer.closest('.a-addable-music-wrapper');
                if ($parentContainer.size() > 0) {
                    $parentContainer.data('parentMusicId', id);
                    $parentContainer.attr('data-parent-music-id', id);
                }
                music.id = id;
                music.type = type;
                callback && callback(music);
            }).fail(function(jqXHR, errorText, err) {
                if (errorText !== 'abort') {
                    module.s.notify(module.string('failed_to_fetch_music_info'), 'error');
                }
            }).always(function() {
                that._resolveReq = null;
            });
        },

        _onLoadingMusic: function(e, m, s) {
            var $loadingMusic = this._$playableFrame.find(this._musicContainer + '.loading'),
                musicData,
                $targetMusic;

            if (!m) {
                $loadingMusic.removeClass('loading');
                return;
            }

            if ($loadingMusic.size() > 0) {
                musicData = extractMusicFromDOM(this, $loadingMusic);
                if (musicData.id === m.id && this._playSections.indexOf(s) > -1) {
                    return;
                }
                $loadingMusic.removeClass('loading');
            }

            $targetMusic = this._$playableFrame
                .find(this._musicContainer + '[data-music-id=\'' + m.id + '\']');
            $targetMusic.addClass('loading');

            this._playingMusic = null;
            this.onLoadingMusic && this.onLoadingMusic(e, m, s);
        },

        _onPlayMusic: function(e, progText, music, section) {
            var $loadingMusic = this._$playableFrame.find(this._musicContainer + '.loading');
            if (!music || progText.playback === 0 ||
                    (this._playingMusic &&
                    this._playingMusic.id === music.id)) {
                return;
            }
            this._playingMusic = music;
            $loadingMusic.removeClass('loading');
            this.onPlayMusic && this.onPlayMusic(e, progText, music, section);
        },

        unbindPlayableView: function() {
            if (this.tracklistReq) {
                this.tracklistReq.abort();
                this.tracklistReq = null;
            }

            if (this._resolveReq) {
                this._resolveReq.abort();
                this._resolveReq = null;
            }
            this._musicStorage = {};

            module.$(window).off('resize', this._onWindowResize);
            module.$(module).off('playInfoUpdate loadingMusic', this._onPlayInfoUpdate);
            module.$(module).off('finishedMusic playerStopped', this._onMusicFinished);
            module.$(module).off('togglePlay', this._onTogglePlay);
            module.$(module).off('loadingMusic', this._onLoadingMusic);
            module.$(module).off('musicProgressUpdate', this._onPlayMusic);
            module.$(module).off('playerStopped', this._onPlayerStopped);

            this._$playableFrame.off('click', '.play-music', this._onPlayMusicClicked);
            this._$playableFrame.off('click', '.btn-download', this._onDownloadMusicClicked);
            this._$playableFrame.off('click', '.listen-drop', this._onDropBtnClicked);
            this._$playableFrame.off('mouseenter', 'listen-drop', this._onDropBtnMouseEnter);
            this._$playableFrame.off('mouseleave', 'listen-drop', this._onDropBtnMouseLeave);
            this._$playableFrame.off('click', '.play-playlist', this._onPlayPlaylistBtnClicked);
            this._$playableFrame.off('click', '.ctrl-play', this._onPlayCtrlClicked);
        },

        setMusicToStorage: function(key, val) {
            this._musicStorage[key] = val;
        },

        updateMusicStorage: function(map) {
            module.$.extend(this._musicStorage, map);
        },

        getMusicFromStorage: function(key) {
            return this._musicStorage[key];
        },

        clearMusicStorage: function() {
            this._musicStorage = {};
        }
    };


///////////
// Model //
///////////

    ReceivedMusic = function (params) {
        var that = {},
            track,
            genres = module.context.genre['default'],
            model;

        that.labels = new Backbone.Collection(params.labels || []);
        that.didRead = Math.round(Math.random() * 2) > 1;
        that.isChecked = !!params.isChecked;
        that.registered_at = moment()
            .subtract(Math.floor(Math.random() * 100), 'hours').toDate();
        that.genre = genres[Math.floor(Math.random() * genres.length)].name.capitalize();

        if (!params.type || params.type === 'dropbeat') {
            params.unique_key = chance.string({length: 32});
            params.id = that.unique_key;
            track = new module.music.DropbeatMusic(params);
        } else {
            track = new module.music.Music(params);
        }
        module.$.extend(that, track);
        that.user.num_followers = Math.floor(Math.random() * 10000);
        that.user.nickname = chance.name().capitalize();
        that.user.total_playing = chance.natural({min: 1000, max: 20000});
        that.user.total_playing_month = chance.natural({min: 500, max: 3000});
        that.user.track_count = chance.natural({min:1, max: 40});
        that.storage_key = that.getStorageKey();
        that.description = chance.paragraph({sentance:2});

        that.labels.on('change add reset update remove', function() {
            model.trigger('change', model);
        });

        model = new Backbone.Model(that);
        return model;
    };

    module.music = module.music || {};
    module.music.ReceivedMusic = ReceivedMusic;


////////////////
// View Utils //
////////////////


    function initTooltipster($el, options) {
        options = options || {};
        options.theme = 'dropbeat-tooltip-theme';

        module.$('.tooltip', $el).tooltipster(options);
        module.$('.tooltip-right', $el).tooltipster(
            module.$.extend({}, options, {
                position:'right'
            }));
        module.$('.tooltip-always').tooltipster('show');
    }

    function extractMusicFromDOM(context, $el) {
        var musicStorageKey,
            musicData,
            drop;

        musicStorageKey = $el.data('musicStorageKey');
        if (musicStorageKey && context.getMusicFromStorage) {
            musicData = context.getMusicFromStorage(musicStorageKey);
            if (musicData) {
                return musicData;
            }
        }

        drop = {
            when: parseInt($el.data('dropSec')) || 0,
            type: $el.data('dropType'),
            dref: String($el.data('dropMusicId'))
        };
        if (!drop.type || !drop.dref) {
            drop = null;
        }
        musicData = {
            id: String($el.data('musicId')),
            title: unescape($el.data('musicTitle')),
            type: $el.data('musicType'),
            tag: $el.data('musicTag'),
            duration: $el.data('musicDuration'),
            drop: drop
        };
        musicData = new module.s.Music(musicData);
        if (!musicData.id || !musicData.type || !musicData.title) {
            return null;
        }

        return musicData;
    }

/////////////////////////
// Default view module //
/////////////////////////

    module.view = {
        init: function () {
            var that = this;

            this.placeholderCompat();

            module.$('body').on('click', '.overlay-filter', function() {
                module.$('.overlay-filter').hide();
            });

            module.$('body').on('click', 'a[data-href]', function(e) {
                var href = module.$(this).data('href'), path, a;
                a = document.createElement('a');
                a.href = href;
                path = a.pathname + a.search;
                if (!path.startsWith('/')) {
                    path = '/' + path;
                }
                module.$('.account-menus .menu-window').hide();
                module.$('.overlay-filter').hide();
                module.router.navigate(path, {trigger: true});
            });

            module.$('body').on('mouseenter', '.ellipsised-text', function(e) {
                var $el = module.$(this);
                if(this.offsetWidth < this.scrollWidth && !$el.attr('title')){
                    $el.attr('title', $el.text().trim());
                }
            });
        },

        initContentsBg: function() {
            var $imageFrame, size, selectedIdx, imgObj, $img, url;
            $imageFrame = module.$('.bg-image');
            size = module.constants.bgImages.length;
            selectedIdx = Math.floor(Math.random() * size);
            imgObj = module.constants.bgImages[selectedIdx];
            if (imgObj.name) {
                url = '/bg_images/' + imgObj.name;
            }
            if (imgObj.url) {
                url = imgObj.url;
            }

            $img = module.$('<img class=\'bg-image-inner\' src=\'' + url +
                    '\' data-width=\'' + imgObj.width +
                    '\' data-height=\'' + imgObj.height + '\'/>');
            $imageFrame.html($img);
            if (module.context.browserSupport.support) {
                $imageFrame.css(
                    'backgroundImage',
                    'url(\'' + url + '\')');
            }

            module.view.resizeContentsBg();
        },

        resizeContentsBg: function () {
            var $img = module.$('.bg-image img'),
                imgWidth = $img.data('width'),
                imgHeight = $img.data('height'),
                aspectRatio = imgWidth / imgHeight,
                $frame = module.$('.contents'),
                frameWidth = $frame.width(),
                frameHeight = $frame.height(),
                frameAspectRatio = frameWidth / frameHeight;
            if (frameAspectRatio > aspectRatio) {
// adjust image to window width
                $img.css({
                    width: frameWidth + 'px',
                    height: (frameWidth / aspectRatio) + 'px',
                    marginTop: -1 * (frameWidth / aspectRatio) / 2 + 'px',
                    marginLeft: -1 * frameWidth / 2
                });
            } else {
                $img.css({
                    width: frameHeight * aspectRatio + 'px',
                    height: frameHeight + 'px',
                    marginTop: -1 * frameHeight / 2,
                    marginLeft: -1 * (frameHeight * aspectRatio) / 2 + 'px'
                });
            }
        },

        placeholderCompat: function () {
// IE 7,8 does not support `placeholder`.
            module.$('input[placeholder]').placeholder();
        }
    };

///////////////////
// Default view  //
///////////////////

    DropbeatView = Backbone.View.extend({
        initialize: function(options) {
            var context,
                parentView;
            options = options || {};

            context = options.context || {};
            context = module.$.extend({}, module.context, context);
            this._context = { _context: context };

            parentView = options.parentView;
            if (parentView) {
                this.parentView = parentView;
            }

            if (this.tmpl) {
                this._tmpl = module._.template(module.$(this.tmpl).html());
            }
        },
        events: {
            'click .page-inner-link': 'pageInnerLink'
        },
        pageInnerLink: function(e) {
            var $el = module.$(e.currentTarget),
                targetId = $el.data('target'),
                $target = module.$('#' + targetId),
                scrollTop;
            if (!$target.size()) {
                return;
            }
            scrollTop = $target.offset().top;
            module.$(window).scrollTop(scrollTop);
            e.stopPropagation();
        }
    });

    DropbeatPageView = DropbeatView.extend({
        subViews: {},
        initialize: function(options) {
            var tmpl;

            module._.bindAll(this, 'remove');
            DropbeatView.prototype.initialize.call(this, options);

            tmpl = module._.template(module.$('#tmpl-default-page-frame').html());
            module.$('.contents-inner').html(tmpl({}));
            this._$pageTarget = module.$('.body-section-inner');
        },

        remove: function() {
            module.$.each(this.subViews, function(viewName, view) {
                if (view.remove) {
                    view.remove();
                }
            });

            DropbeatView.prototype.remove.apply(this, arguments);

// Stop all music when page removed
            if (module.context.isMobile) {
                module.playerManager.stop();
            }
        },

        unbind: function() {
            this.undelegateEvents();
            module.$.each(this.subViews, function(viewName, view) {
                if (view.remove) {
                    view.remove();
                }
            });
        }
    });

    ThisBrowserIsNotSupportedView = (function() {
        var PageView = DropbeatView.extend({
            tmpl: '#tmpl-this-browser-is-not-supported',
            initialize: function() {
                var tmpl = module._.template(module.$(this.tmpl).html());
                module.$('#dropbeat').html(tmpl());
                module.view.initContentsBg();
            }
        });
        return {
            show: function() {
                this.view = new PageView();
            }
        };
    }());

    SettingsPageView = DropbeatPageView.extend({
        subViews: (function() {
            return module.$.extend ({}, DropbeatPageView.prototype.subViews);
        }()),
        tmpl: '#tmpl-page-settings',
        initialize: function(options) {
            var that = this,
                tracks = [];

            module.$('.header-section .nav-menus .nav-menu').removeClass('selected');
            module.$('.header-section .nav-menus .nav-menu-settings').addClass('selected');

            DropbeatPageView.prototype.initialize.call(this, options);
            this.render();

            this.$optionChoiceText = module.$('.option-select .selected-option',
                this.$el).find('.text');
            this.$optionDropdown = module.$('.option-select .dropdown', this.$el);
            this.$filter = module.$('.overlay-filter');

            initTooltipster(module.$('.header-section'));
            initTooltipster(this.$el);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .option-select .option': 'onOptionClicked',
                'click .option-select .selected-option': 'onSelectOption',
                'click .checkbox': 'onToggleCheckbox',
            });
        },

        render:function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);
        },

        onSelectOption: function() {
            var that = this;
            if (this.$optionDropdown.is(':hidden')) {
                this.$filter.show().one('click', function() {
                    that.$optionDropdown.hide();
                });
                this.$optionDropdown.show();
            }
        },

        onOptionClicked: function(e) {
            var $target = module.$(e.currentTarget),
                today, url,
                mm, dd, timestamp;

            if ($target.hasClass('selected')) {
                return;
            }

            module.$('.option-select .option', this.$el).removeClass('selected');
            $target.addClass('selected');
            this.selectedOption = $target.data('key');

            this.$optionChoiceText.text($target.text());
            if (this.$filter.is(':visible')) {
                this.$filter.click();
            }
        },

        onToggleCheckbox: function(e) {
            var $checkbox = module.$(e.currentTarget),
                $wrapper = $checkbox.closest('.checkbox-wrapper');
            $wrapper.toggleClass('checked');
            $checkbox.toggleClass('checked');
        }

    });


    InboxPageView = DropbeatPageView.extend({
        subViews: (function() {
            return module.$.extend ({}, DropbeatPageView.prototype.subViews);
        }()),
        tmpl: '#tmpl-page-inbox',
        initialize: function(options) {
            var that = this,
                tracks = [];

            module._.bindAll(this,
                'removeTrackEl', 'renderTrack', 'renderTracks',
                'renderCustomLabels', 'onPlayInfoUpdate');

            module.$('.header-section .nav-menus .nav-menu').removeClass('selected');
            module.$('.header-section .nav-menus .nav-menu-inbox').addClass('selected');

            DropbeatPageView.prototype.initialize.call(this, options);
            this.setupTracks(tracks);
            this.setupTracks(tracks);
            this.setupTracks(tracks);
            this.setupTracks(tracks);

            this.allTracks = new Backbone.Collection(tracks);
            this.allTracks.comparator = function(track) {
                  return -track.get("registered_at");
            };
            this.allTracks.sort();

            this.pageTracks = this.allTracks.clone();
            this.currentTracks = this.allTracks.clone();
            this.currentTracks.on('remove', this.removeTrackEl);
            this.currentTracks.on('add', this.addTrackEl);
            this.currentTracks.on('change update', this.renderTrack);
            this.currentTracks.on('sort reset', this.renderTracks);
            this.customLabels = new Backbone.Collection([]);
            this.customLabels.on('change add', this.renderCustomLabels);
            this.render();
            initTooltipster(module.$('.header-section'));

            this.navMenu = 'inbox';

            this.$filter = module.$('.overlay-filter');
            this.$tracks = module.$('.tracks', this.$el);
            this.$searchInput = module.$('#search-keyword');
            this.labelMenusTmpl = module._.template(module.$('#tmpl-custom-label-menus').html());
            this.labelActionMenusTmpl = module._.template(module.$('#tmpl-custom-label-actions').html());
            this.trackTmpl = module._.template(module.$('#tmpl-received-track').html());
            this.$customLabelMenus = module.$('.custom-label-menus', this.$el);
            this.$customLabelActionMenus = module.$('.dropdown-action .custom-actions', this.$el);

            WaveformViewMixin.bind(this, this.$tracks);
            PlayableViewMixin.bind(this, this.$tracks, ['demo']);

            module.$(module).on('playInfoUpdate loadingMusic', this.onPlayInfoUpdate);

            module.$.each(tracks, function(idx, t) {
                var track, key;
                t = t.toJSON();
                key = module.music.Music.prototype.getStorageKey.call(t);
                that.setMusicToStorage(key, t);
            });

            this.customLabels.push({
                name:'custom label 1',
                textColor: '#ffffff',
                bgColor: '#f03eb1',
                id:module._.uniqueId('custom-label-')
            });
            this.customLabels.push({
                name:'custom label 2',
                textColor: '#ffffff',
                bgColor: '#88898c',
                id:module._.uniqueId('custom-label-')
            });

            module.$.each(this.allTracks.models, function(idx, model) {
                var typeIdx, label;

                if (Math.random() * 3 < 1) {
                    typeIdx = Math.floor(Math.random() * 3);
                    if (typeIdx === 0) {
                        label = that.customLabels.models[0]
                    } else if (typeIdx === 1) {
                        label = that.customLabels.models[1]
                    } else {
                        label = {
                            id:'important',
                            textColor:'#ffffff',
                            bgColor: '#ff0000',
                            name: 'Important'
                        }
                    }
                    model.get('labels').push(label);
                }
            });

            this.renderTracks();

        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .left-menus .menu': 'onLeftNavMenuClicked',
                'click .track-menus [data-action]': 'onTrackMenuClicked',
                'click .track-menu-dropdown-wrapper': 'onTrackDropdownMenuClicked',
                'click .btn-new-label': 'onNewLabelClicked',
                'click .track .checkbox': 'onToggleTrackCheckbox',
                'keydown #search-keyword' : 'onSearchKeywordType'
            });
        },

        render:function() {
            module.$('.contents-inner').html(this._tmpl(this._context));
            this.$el = module.$('#page-inbox');
        },

        onPlayInfoUpdate: function(e, music, section) {
            var tracks = this.allTracks.where({id: music.id}),
                $track;
            module.$.each(tracks, function(idx, track) {
                track.set('didRead', true);
            });

            this.$tracks.find('.track.opened').removeClass('opened');
            $track = this.$tracks.find('.track[data-music-id=\'' + music.id + '\']');
            if ($track.size() > 0 && section !== 'drop') {
                $track.addClass('opened');
            }

            this.updateWaveform();
        },

        removeTrackEl: function(track) {
            var $track = this.$tracks.find('.track[data-music-id=\'' + track.get('id') + '\']');
            if ($track.size() === 0) {
                return;
            }
            $track.remove();
        },

        addTrackEl: function(track) {
            var $track = this.$tracks.find('.track[data-music-id=\'' + track.get('id') + '\']');
            if ($track.size() > 0) {
                return;
            }
            $track = module.$(this.trackTmpl({
                showWaveform: !!track.get('waveform_url'),
                track: track.toJSON(), navMenu: this.navMenu}));
            this.$tracks.append($track);
            this.updateTrackPlayState(track.toJSON(), $track);
            initTooltipster($track);
        },

        renderTrack: function(track) {
            var $track = this.$tracks.find('.track[data-music-id=\'' + track.get('id') + '\']'),
                $newTrack;

            if ($track.size() === 0) {
                return;
            }
            $newTrack = module.$(this.trackTmpl({
                showWaveform: !!track.get('waveform_url'),
                track: track.toJSON(),
                navMenu: this.navMenu
            }));
            $track.replaceWith($newTrack);
            this.updateTrackPlayState(track.toJSON(), $newTrack);
            this.updateWaveform();
            initTooltipster($newTrack);
        },

        renderTracks: function() {
            var that = this;

            this.$tracks.empty();
            module.$.each(this.currentTracks.toJSON(), function(idx, track) {
                var $track = module.$(that.trackTmpl({
                        showWaveform: !!track.waveform_url,
                        track: track, navMenu: that.navMenu }));
                that.$tracks.append($track);
                that.updateTrackPlayState(track, $track);
            });
            this.updateWaveform();
            initTooltipster(this.$tracks);
        },

        updateTrackPlayState: function(track, $track) {
            var manager = module.playerManager,
                currMusic = manager.getCurrentMusic(),
                currSection = manager.getCurrentSection(),
                playback, duration;

            if (currMusic && track.id === currMusic.id && 'demo' === currSection) {
                $track.addClass('opened');
                $track.addClass('on');
                if (manager.playing) {
                    $track.find('.ctrl-play').addClass('pause');
                }
                duration = track.duration || manager.getTotalPlaybackTime();
                playback = manager.getCurrentPlaybackTime();
                module.$(module).trigger('musicProgressUpdate', [{
                        playback: playback,
                        duration: duration
                    }, currMusic, currSection]);
            }
        },

        renderCustomLabels: function() {
            this.$customLabelMenus.html(this.labelMenusTmpl({
                labels: this.customLabels.toJSON()
            }));
            this.$customLabelActionMenus.html(this.labelActionMenusTmpl({
                labels: this.customLabels.toJSON()
            }));
        },

        remove: function() {
            DropbeatPageView.prototype.remove.apply(this, arguments);

            WaveformViewMixin.unbind(this);
            PlayableViewMixin.unbind(this);
        },

        onLeftNavMenuClicked: function(e) {
            var $menu = module.$(e.currentTarget),
                menu = $menu.data('menu'),
                labelId,
                tracks = [];

            module.$('.left-menus .menu').removeClass('selected');
            $menu.addClass('selected');

            if (menu === 'inbox') {
                this.navMenu = 'inbox';
                tracks = this.allTracks.models;
                this.pageTracks.reset(tracks);
                this.currentTracks.reset(tracks);
                module.$.each(this.allTracks.models, function(idx, model) {
                    model.set('isChecked', false);
                });
                return;
            } else if (menu === 'custom-label') {
                labelId = $menu.data('labelId');
            } else if (menu === 'important') {
                labelId = 'important';
            } else {
                return;
            }
            this.navMenu = labelId;

            module.$.each(this.allTracks.models, function(idx, model) {
                var labels = model.get('labels');
                if (labels.where({id:labelId}).length > 0) {
                    tracks.push(model);
                }
            });
            this.pageTracks.reset(tracks);
            this.currentTracks.reset(tracks);

            module.$.each(this.allTracks.models, function(idx, model) {
                model.set('isChecked', false);
            });
        },

        setupTracks: function(collection) {
            collection.push(new module.music.ReceivedMusic({
                "created_at": "2015-10-18 11:12:02",
                "waveform_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/waveform/a4db7796b236a0adfe59df14c868b5cb/4b06d8b07a246e3d78a04df653cb0045.json",
                "name": "Lime Galaxy (Original Mix)",
                "unique_key": "596e6b19dad64ed3f0d614a84cdcdc8e",
                "track_type": "TRACK",
                "user_profile_image": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/profile/a4db7796b236a0adfe59df14c868b5cb/43de438ae89bca4dde6dc7b404ac534a.jpg",
                "drop_start": 91,
                "user_resource_name": "nektwork",
                "duration": 266,
                "resource_path": "lime-galaxy-original-mix",
                "downloadable": false,
                "drop_url": "https://dl.dropboxusercontent.com/s/xamqxvr33u4vrap/Lime%20Galaxy%20Original%20Mix_drop%20%282%29.mp3?dl=0",
                "stream_url": "https://dl.dropboxusercontent.com/s/92hgslkiw9pfyld/Lime%20Galaxy%20Original%20Mix%20%281%29.mp3?dl=0",
                "genre_id": 5,
                "user_name": "NEKTWORK",
                "coverart_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/coverart/a4db7796b236a0adfe59df14c868b5cb/52c41a4fcc4fbd73113be4dc636eb8f3.jpg",
                "description": "#151020 Lime Galaxy (Original Mix)\r\nhttps://www.youtube.com/watch?v=msjkQAbdYtQ&feature=youtu.be"
            }));

            collection.push(new module.music.ReceivedMusic({
                "created_at": "2015-10-18 11:12:02",
                "waveform_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/waveform/a4db7796b236a0adfe59df14c868b5cb/4b06d8b07a246e3d78a04df653cb0045.json",
                "name": "Lime Galaxy (Original Mix)",
                "unique_key": "596e6b19dad64ed3f0d614a84cdcdc8e",
                "track_type": "TRACK",
                "user_profile_image": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/profile/a4db7796b236a0adfe59df14c868b5cb/43de438ae89bca4dde6dc7b404ac534a.jpg",
                "drop_start": 91,
                "user_resource_name": "nektwork",
                "duration": 266,
                "resource_path": "lime-galaxy-original-mix",
                "downloadable": false,
                "drop_url": "https://dl.dropboxusercontent.com/s/xamqxvr33u4vrap/Lime%20Galaxy%20Original%20Mix_drop%20%282%29.mp3?dl=0",
                "stream_url": "https://dl.dropboxusercontent.com/s/92hgslkiw9pfyld/Lime%20Galaxy%20Original%20Mix%20%281%29.mp3?dl=0",
                "genre_id": 5,
                "user_name": "NEKTWORK",
                "coverart_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/coverart/a4db7796b236a0adfe59df14c868b5cb/52c41a4fcc4fbd73113be4dc636eb8f3.jpg",
                "description": "#151020 Lime Galaxy (Original Mix)\r\nhttps://www.youtube.com/watch?v=msjkQAbdYtQ&feature=youtu.be"
            }));

            collection.push(new module.music.ReceivedMusic({
                "created_at": "2015-10-18 09:26:54",
                "waveform_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/waveform/a4db7796b236a0adfe59df14c868b5cb/65d4f0eb2a5e1bea951cec6011e3dcf9.json",
                "name": "Emotion (Original mix)",
                "unique_key": "cb7a34b6f343c26f3cbefedcc0eee7e4",
                "track_type": "TRACK",
                "user_profile_image": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/profile/a4db7796b236a0adfe59df14c868b5cb/43de438ae89bca4dde6dc7b404ac534a.jpg",
                "drop_start": 70,
                "user_resource_name": "nektwork",
                "duration": 285,
                "resource_path": "emotion-original-mix",
                "downloadable": true,
                "drop_url": "https://dl.dropboxusercontent.com/s/kfpvswe9m1uixpu/emotion%20original%20mix_drop.mp3?dl=0",
                "stream_url": "https://dl.dropboxusercontent.com/s/0di1engnpf3bwib/emotion%20original%20mix.mp3?dl=0",
                "genre_id": 6,
                "user_name": "NEKTWORK",
                "coverart_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/coverart/a4db7796b236a0adfe59df14c868b5cb/2ac6caf20c3359cf5ad5a6184ae86b67.jpg",
                "description": "#20150906 Reupload\r\n\r\nYoutube\r\nhttps://www.youtube.com/c/zbkNEKTWORK\r\n\r\nFacebook\r\nhttps://www.facebook.com/gridrow"
            }));

            collection.push(new module.music.ReceivedMusic({
                "created_at": "2015-10-18 09:26:54",
                "waveform_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/waveform/a4db7796b236a0adfe59df14c868b5cb/1dcd93784b5ed5d64050e0e8d422d10c.json",
                "name": "Laugh Maker (Original Mix)",
                "unique_key": "4c62211d65a01cc0a7b810e2051ec726",
                "track_type": "TRACK",
                "user_profile_image": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/profile/a4db7796b236a0adfe59df14c868b5cb/43de438ae89bca4dde6dc7b404ac534a.jpg",
                "drop_start": 91,
                "user_resource_name": "nektwork",
                "duration": 281,
                "resource_path": "laugh-maker-original-mix",
                "downloadable": true,
                "drop_url": "https://dl.dropboxusercontent.com/s/g6fp57bx27g4f2a/laugh%20maker%20original%20mix_drop%20%281%29.mp3?dl=0",
                "stream_url": "https://dl.dropboxusercontent.com/s/alokyarrkbtb56p/laugh%20maker%20original%20mix.mp3?dl=0",
                "genre_id": 6,
                "user_name": "NEKTWORK",
                "coverart_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/coverart/a4db7796b236a0adfe59df14c868b5cb/d15dd62e308284d6ae5059023de0b352.jpg",
                "description": "#NEKTWORK 150920 Laugh Maker (Original Mix)\r\n\r\nYoutube Channel\r\nhttp://www.youtube.com/c/zbkNEKTWORK "
            }));

            collection.push(new module.music.ReceivedMusic({
                "created_at": "2015-10-18 09:26:08",
                "waveform_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/waveform/a4db7796b236a0adfe59df14c868b5cb/78b9280437aefc77b2d8db98e3ce23c8.json",
                "name": "Cloud-Paraside(Original Mix)",
                "unique_key": "74ffa792983b626f5f273cb96bb1db02",
                "track_type": "TRACK",
                "user_profile_image": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/profile/a4db7796b236a0adfe59df14c868b5cb/43de438ae89bca4dde6dc7b404ac534a.jpg",
                "drop_start": 90,
                "user_resource_name": "nektwork",
                "duration": 291,
                "resource_path": "cloudparasideoriginal-mix",
                "downloadable": true,
                "drop_url": "https://dl.dropboxusercontent.com/s/jvcnwa19cb91z3s/cloud-parasideoriginal%20mix_drop%20%281%29.mp3?dl=0",
                "stream_url": "https://dl.dropboxusercontent.com/s/ivxrnpjkri6vh1v/cloud-parasideoriginal%20mix.mp3?dl=0",
                "genre_id": 6,
                "user_name": "NEKTWORK",
                "coverart_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/coverart/a4db7796b236a0adfe59df14c868b5cb/9f6b5810769f461d30c1747118ec024f.jpg",
                "description": "#20150712 Cloude-Paradice(Original Mix)"
            }));

            collection.push(new ReceivedMusic({
                "created_at": "2015-10-18 09:26:07",
                "waveform_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/waveform/a4db7796b236a0adfe59df14c868b5cb/61cbe6b5938f8751397a025501fba447.json",
                "name": "Square Snow (Original Mix)",
                "unique_key": "6fdfbbf4216f2b702e8b69fb231c4646",
                "track_type": "TRACK",
                "user_profile_image": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/profile/a4db7796b236a0adfe59df14c868b5cb/43de438ae89bca4dde6dc7b404ac534a.jpg",
                "drop_start": 60,
                "user_resource_name": "nektwork",
                "duration": 259,
                "resource_path": "square-snow-original-mix",
                "downloadable": true,
                "drop_url": "https://dl.dropboxusercontent.com/s/n2t7r38p5xztcsh/square%20snow%20original%20mix_drop%20%281%29.mp3?dl=0",
                "stream_url": "https://dl.dropboxusercontent.com/s/iac7mx2ahmve3yz/square%20snow%20original%20mix.mp3?dl=0",
                "genre_id": 6,
                "user_name": "NEKTWORK",
                "coverart_url": "https://s3-ap-northeast-1.amazonaws.com/dropbeat/prod/coverart/a4db7796b236a0adfe59df14c868b5cb/4f63f0c8f52d751e0cd9b61f231f62b8.jpg",
                "description": "#20150411 Square Snow (Original Mix)"
            }));
        },

        onSearchKeywordType: function(e) {
            this._searchTimeout = setTimeout(module.$.proxy(function() {
                var keyword,
                    tracks = [],
                    _tracks;
                keyword = this.$searchInput.val().toLowerCase();
                _tracks = this.pageTracks.models;
                if (keyword.length > 0) {
                    module.$.each(_tracks, function(idx, track) {
                        var nickname = track.get('user').nickname.toLowerCase(),
                            trackname = track.get('track_name').toLowerCase();
                        if (nickname.indexOf(keyword) > -1 ||
                            trackname.indexOf(keyword) > -1) {
                            tracks.push(track);
                        }
                    });
                } else {
                    tracks = _tracks;
                }
                this.currentTracks.reset(tracks);
            }, this), 200);
        },

        onTrackDropdownMenuClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $dropdownWrapper = $btn.closest('.track-menu-dropdown-wrapper');
            $dropdownWrapper.addClass('opened');
            this.$filter.show().one('click', function() {
                $dropdownWrapper.removeClass('opened');
                return true;
            });
        },

        onTrackMenuClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                action = $btn.data('action'),
                handler;

            handler = this['onTrackAction' + action.dashToCamel(true)];
            if (handler) {
                handler.call(this, e);
            }
            this.$filter.click();
            return false;
        },

        onToggleTrackCheckbox: function(e) {
            var $checkbox = module.$(e.currentTarget),
                $track = $checkbox.closest('.track'),
                track;

            track = this.currentTracks.findWhere({id: String($track.data('musicId'))});
            if (!track) {
                return;
            }
            track.set('isChecked', !track.get('isChecked'));
        },

        onNewLabelClicked: function(e) {
            var that = this;
            NewLabelModalView.show(function(result) {
                var labelHtml;
                if (!result.name || result.name.length === 0) {
                    return;
                }
                result.id = module._.uniqueId('custom-label-');
                that.customLabels.add(result);
            });
        },

        onTrackActionToggleAll: function() {
            var setChecked = false,
                tracks = this.currentTracks;
            if (this.currentTracks.where({isChecked: true}).length === 0) {
                setChecked = true;
            }

            module.$.each(tracks.models, function(idx, model) {
                model.set('isChecked', setChecked);
            });
            if (setChecked) {
                module.$('.track-menu-select').addClass('checked');
            } else {
                module.$('.track-menu-select').removeClass('checked');
            }
        },

        onTrackActionSelectAll: function() {
            var setChecked = true,
                tracks = this.currentTracks;

            module.$.each(tracks.models, function(idx, model) {
                model.set('isChecked', setChecked);
            });
            module.$('.track-menu-select').addClass('checked');
        },

        onTrackActionSelectNone: function() {
            var setChecked = false,
                tracks = this.currentTracks;

            module.$.each(tracks.models, function(idx, model) {
                model.set('isChecked', setChecked);
            });
            module.$('.track-menu-select').removeClass('checked');
        },

        onTrackActionSelectRead: function() {
            module.$.each(this.currentTracks.models, function(idx, model) {
                model.set('isChecked', model.get('didRead'));
            });
        },

        onTrackActionSelectUnread: function() {
            module.$.each(this.currentTracks.models, function(idx, model) {
                model.set('isChecked', !model.get('didRead'));
            });
        },

        onTrackActionMarkAsRead: function() {
            var checked = this.currentTracks.where({isChecked: true});
            if (checked.length === 0) {
                return;
            }

            module.$.each(checked, function(idx, model) {
                model.set('didRead', true);
            });
        },

        onTrackActionMarkAsUnread: function() {
            var checked = this.currentTracks.where({isChecked: true});
            if (checked.length === 0) {
                return;
            }

            module.$.each(checked, function(idx, model) {
                model.set('didRead', false);
            });
        },

        onTrackActionMarkAsImportant: function() {
            var checked = this.currentTracks.where({isChecked: true});
            if (checked.length === 0) {
                return;
            }

            module.$.each(checked, function(idx, model) {
                var labels = model.get('labels');
                labels.push({
                    id:'important',
                    name: 'Important',
                    bgColor: '#ff0000',
                    textColor: '#ffffff'
                });
            });
        },

        onTrackActionMarkAsNotImportant: function() {
            var checked = this.currentTracks.where({isChecked: true});
            if (checked.length === 0) {
                return;
            }

            module.$.each(checked, function(idx, model) {
                var labels = model.get('labels'),
                    models = labels.where({id: 'important'});
                labels.remove(models);
            });
        },

        onTrackActionMarkAsCustomLabel: function(e) {
            var checked = this.currentTracks.where({isChecked: true}),
                $action = module.$(e.currentTarget),
                labelId = $action.data('labelId'),
                label;

            if (checked.length === 0 || !labelId) {
                return;
            }

            label = this.customLabels.findWhere({id:labelId});

            module.$.each(checked, function(idx, model) {
                var labels = model.get('labels');
                labels.push(label);
            });
        },

        onTrackActionDelete: function() {
            var checked = this.currentTracks.where({isChecked: true});
            if (checked.length === 0) {
                return;
            }

            this.currentTracks.remove(checked);
        },

        onTrackActionSelectFilter: function() {
            var that = this;

            TrackFilterModalView.show(function(result) {
                var _tracks = that.pageTracks.models,
                    tracks,
                    d;

                if (result.genre && result.genre !== '_') {
                    tracks = [];
                    module.$.each(_tracks, function(idx, track) {
                        if (track.get('genre').toLowerCase() === result.genre) {
                            tracks.push(track);
                        }
                    });
                    _tracks = tracks;
                }

                if (result.from) {
                    tracks = [];
                    d = moment(result.from).toDate();
                    module.$.each(_tracks, function(idx, track) {
                        if (track.get('registered_at') - d > 0) {
                            tracks.push(track);
                        }
                    });
                    _tracks = tracks;
                }

                if (result.to) {
                    tracks = [];
                    d = moment(result.to).add(1, 'days').toDate();
                    module.$.each(_tracks, function(idx, track) {
                        if (track.get('registered_at') - d < 0) {
                            tracks.push(track);
                        }
                    });
                    _tracks = tracks;
                }

                that.currentTracks.reset(_tracks);
                that._filterOption = result;
            }, this._filterOption);
        },

        onTrackActionSortByTime: function() {
            var tracks = this.currentTracks;
            tracks.comparator = function(track) {
                return -track.get("registered_at");
            };
            tracks.sort();
            module.$('.dropdown-sort .action', this.$el).removeClass('selected');
            module.$('.dropdown-sort .action-sort-by-time', this.$el).addClass('selected');
        },

        onTrackActionSortByFollowers: function() {
            var tracks = this.currentTracks;
            tracks.comparator = function(track) {
                return -track.get("user").num_followers;
            };
            tracks.sort();
            module.$('.dropdown-sort .action', this.$el).removeClass('selected');
            module.$('.dropdown-sort .action-sort-by-followers', this.$el).addClass('selected');
        },

        onTrackActionSortByTotalPlaying: function() {
            var tracks = this.currentTracks;
            tracks.comparator = function(track) {
                return -track.get("user").total_playing;
            };
            tracks.sort();
            module.$('.dropdown-sort .action', this.$el).removeClass('selected');
            module.$('.dropdown-sort .action-sort-by-total-playing', this.$el).addClass('selected');
        },

        onTrackActionSortByTotalPlayingMonth: function() {
            var tracks = this.currentTracks;
            tracks.comparator = function(track) {
                return -track.get("user").total_playing_month;
            };
            tracks.sort();
            module.$('.dropdown-sort .action', this.$el).removeClass('selected');
            module.$('.dropdown-sort .action-sort-by-total-playing-month', this.$el).addClass('selected');
        },

        onTrackActionSortByTrackCount: function() {
            var tracks = this.currentTracks;
            tracks.comparator = function(track) {
                return -track.get("user").track_count;
            };
            tracks.sort();
            module.$('.dropdown-sort .action', this.$el).removeClass('selected');
            module.$('.dropdown-sort .action-sort-by-total-playing-month', this.$el).addClass('selected');
        }
    });

    Failure404PageView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-404',
        initialize: function(options) {
            DropbeatPageView.prototype.initialize.call(this, options);

            module.$.extend(this._context, { path: options.path });
            this.render();
            module.$('header .header-menu .menu').removeClass('selected');
        },

        render:function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        }
    });

    TrackFilterModalView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-track-filter',
                initialize: function(options) {
                    var that = this,
                        $modal;

                    module._.bindAll(this, 'hide');
                    DropbeatView.prototype.initialize.call(this, options);

                    module.$.extend(this._context, {
                        confirmText: module.string('close')
                    });

                    $modal = module.$(this._tmpl(module.$.extend({}, this._context)));

                    module.$('#modal').append($modal);
                    this.$el = module.$('#modal-track-filter');
                    this.$submitBtn = module.$('.btn-save', this.$el);
                    this.$from = module.$('#filter-from');
                    this.$from.datepicker({
                        defaultDate: options.from ? moment(options.from).toDate() : '+1w',
                        changeMonth: true,
                        numberOfMonths: 1,
                        onClose: function(selectedDate) {
                            that.$to.datepicker('option', 'minDate', selectedDate);
                            that.updateSubmitBtn();
                        }
                    });
                    if (options.from) {
                        this.$from.val(options.from);
                    }

                    this.$to = module.$('#filter-to');
                    this.$to.datepicker({
                        defaultDate: options.to ? moment(options.to).toDate() : '+1w',
                        changeMonth: true,
                        numberOfMonths: 1,
                        onClose: function(selectedDate ) {
                            that.$from.datepicker('option', 'maxDate', selectedDate);
                            that.updateSubmitBtn();
                        }
                    });
                    if (options.to) {
                        this.$to.val(options.to);
                    }

                    this.$optionChoiceText = module.$('.option-select .selected-option',
                        this.$el).find('.text');
                    this.$optionDropdown = module.$('.option-select .dropdown', this.$el);
                    this.$filter = module.$('.overlay-filter');

                    this.result = {
                        from : options.from,
                        to : options.to,
                        genre : options.genre
                    };

                    setTimeout(function() {
                        if (options.genre) {
                            module.$('.option[data-key=\'' + options.genre + '\']',
                                this.$el).click();
                        } else {
                            module.$('.option[data-key=\'_\']', this.$el).click();
                        }
                    }, 0);

                    this.$el.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            options.onAfterConfirm && options.onAfterConfirm(that.result);
                        });
                },

                events: function() {
                    var events = DropbeatView.prototype.events;
                    if (module._.isFunction(events)) {
                        events = events.call(this);
                    }

                    return module.$.extend ({}, events, {
                        'submit #form-new-label': 'onSubmit',
                        'click .btn-save': 'onSubmit',
                        'click .option-select .option': 'onOptionClicked',
                        'click .option-select .selected-option': 'onSelectOption'
                    });
                },

                hide: function(callback) {
                    this.$el.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    this.$el.modal('hide');
                },


                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                    if (this.req) {
                        this.req.abort();
                        this.req = null;
                    }
                },

                onSubmit: function() {
                    if (this.$submitBtn.attr('disabled')) {
                        return;
                    }
                    this.result.from = this.$from.val().trim();
                    this.result.to = this.$to.val().trim();
                    this.result.genre = this.selectedOption;
                    this.hide();
                    return false;
                },

                updateSubmitBtn: function() {
                    var isValid = false;

                    if (this.$from.val() || this.$to.val()) {
                        isValid = true;
                    }
                    if (this.selectedOption) {
                        isValid = true;
                    }

                    if (isValid) {
                        this.$submitBtn.removeAttr('disabled');
                    } else {
                        this.$submitBtn.attr('disabled', true);
                    }
                },

                onSelectOption: function() {
                    var that = this;
                    if (this.$optionDropdown.is(':hidden')) {
                        this.$filter.show().one('click', function() {
                            that.$optionDropdown.hide();
                        });
                        this.$optionDropdown.show();
                    }
                },

                onOptionClicked: function(e) {
                    var $target = module.$(e.currentTarget),
                        today, url,
                        mm, dd, timestamp;

                    if ($target.hasClass('selected')) {
                        return;
                    }

                    module.$('.option-select .option', this.$el).removeClass('selected');
                    $target.addClass('selected');
                    this.selectedOption = $target.data('key');
                    this.updateSubmitBtn();

                    this.$optionChoiceText.text($target.text());
                    if (this.$filter.is(':visible')) {
                        this.$filter.click();
                    }
                }

            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(callback, options) {
            var thumbUrl;

            options = options || {};

            view = new PageView({
                    genre: options.genre,
                    from: options.from,
                    to: options.to,
                    onAfterConfirm: function(result) {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback(result);
                    }
                })
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());

    NewLabelModalView = (function() {
        var PageView = DropbeatView.extend({
                tmpl: '#tmpl-modal-new-label',
                initialize: function(options) {
                    var that = this,
                        $modal;

                    module._.bindAll(this, 'hide');
                    DropbeatView.prototype.initialize.call(this, options);

                    module.$.extend(this._context, {
                        confirmText: module.string('close')
                    });

                    $modal = module.$(this._tmpl(module.$.extend({}, this._context)));

                    module.$('#modal').append($modal);
                    this.$el = module.$('#modal-new-label');
                    this.$textColorInput = module.$('.input-text-color');
                    this.$bgColorInput = module.$('.input-bg-color');
                    this.$nameInput = module.$('.input-name', this.$el);
                    this.$submitBtn = module.$('.btn-save', this.$el);

                    this.result = {};
                    this.result.textColor = '#ffffff';
                    this.result.bgColor = '#ffff00';

                    this.$textColorInput.spectrum({
                        color:this.result.textColor,
                        change: function(color) {
                            that.result.textColor = color.toHexString();
                        }
                    });

                    this.$bgColorInput.spectrum({
                        color:this.result.bgColor,
                        change: function(color) {
                            that.result.bgColor = color.toHexString();
                        }
                    });

                    this.$el.modal({ show: true, backdrop:'static'})
                        .on('hidden.bs.modal', function() {
                            options.onAfterConfirm && options.onAfterConfirm(that.result);
                        });
                },

                events: function() {
                    var events = DropbeatView.prototype.events;
                    if (module._.isFunction(events)) {
                        events = events.call(this);
                    }

                    return module.$.extend ({}, events, {
                        'keydown .input-name': 'onKeydown',
                        'submit #form-new-label': 'onSubmit',
                        'click .btn-save': 'onSubmit'
                    });
                },

                hide: function(callback) {
                    this.$el.on('hidden.bs.modal', function() {
                            callback && callback();
                        });
                    this.$el.modal('hide');
                },


                remove: function() {
                    DropbeatView.prototype.remove.call(this);
                    module.$(this._modal).remove();
                    if (this.req) {
                        this.req.abort();
                        this.req = null;
                    }
                },

                onKeydown: function(e) {
                    var name = this.$nameInput.val().trim();
                    if (name.length === 0) {
                        this.$submitBtn.attr('disabled', 'disabled');
                    } else {
                        this.result.name = name;
                        this.$submitBtn.removeAttr('disabled');
                    }
                },

                onSubmit: function() {
                    if (this.$submitBtn.attr('disabled')) {
                        return;
                    }
                    this.result.name = this.$nameInput.val().trim();
                    if (this.result.name.length === 0) {
                        return;
                    }
                    this.hide();
                    return false; }
            }),
            view,
            subModule = {};

        subModule.show = module.$.proxy(function(callback) {
            var thumbUrl;

            view = new PageView({
                    onAfterConfirm: function(result) {
                        view.close ? view.close() : view.remove();
                        view = null;
                        callback && callback(result);
                    }
                })
        }, subModule);

        subModule.hide = module.$.proxy(function(callback) {
            if (view) {
                view.hide(function() {
                    view = null;
                    callback && callback();
                });
            }
        }, subModule);
        return subModule;
    } ());


    module.view.InboxPageView = InboxPageView;
    module.view.SettingsPageView = SettingsPageView;
    module.view.ThisBrowserIsNotSupportedView = ThisBrowserIsNotSupportedView;
    module.view.Failure404PageView = Failure404PageView;

    return module;
} ($dbt));
