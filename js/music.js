/*jslint browser: true*/
var $dbt = (function (module) {
    'use strict';

    module = module || {};
    module.music = module.music || {};

    var BaseMusicPrototype = (function(_prototype) {
        var prototype = module.$.extend({}, _prototype || {});
        prototype.getStorageKey = function() {
            if (this._storageKey) {
                return this._storageKey;
            }
            if (!this.id) {
                this._storageKey = module._.uniqueId('undefined_' + this.type);
                return this._storageKey;
            }
            return this.type + '_' + this.id;
        };

        prototype.getResourcePath = function() {
            return null;
        };
        return prototype;
    } (Backbone.Events));

    module.music.Music = function (params) {
        var that = this;

        that.id = String(params.id || '');
        that.title = params.title;
        that.type = params.type;
        that.artwork = params.artwork;
        that.duration = params.duration;
        that.drop = params.drop;
        that.tag = params.tag;
        that.created_at = params.created_at;
        that.desc = params.desc;
        that.track_name = params.track_name;
        that.genre = params.genre;
        that.waveform_url = params.waveform_url;
        that.mix_type = params.mix_type;
        
// This attribute only be used for caching youtube resolved url
        that.stream_url = null;

        if (params.user_name || params.nickname) {
            that.user = {
                nickname : params.user_name || params.nickname,
                resource_name: params.user_resource_name,
                profile_image: params.user_profile_image,
                id: params.user_id
            }
        }

        if (that.type === 'soundcloud') {
            if (that.artwork) {
                that.artwork = that.artwork.replace('-large', '-t500x500');
            } else {
                that.artwork = module.coreApi.imageUrl + '?uid=' + 
                    that.id + '&size=small';
            }
        }
    };

    module.music.DropbeatMusic = function (params) {
        var that = this;

        that.id = params.unique_key;
        that.uid = params.id;
        that.title = params.user_name + ' - ' + params.name;
        that.track_name = params.name;
        that.type = 'dropbeat';
        that.artwork = params.coverart_url;
        that.duration = params.duration;
        that.drop = {
            when: 0,
            start: params.drop_start,
            dref: params.drop_url,
            type: 'dropbeat'
        };

        that.stream_url = params.stream_url;
        that.waveform_url = params.waveform_url;
        that.unique_key = params.unique_key;
        that.track_type = params.track_type;
        that.user = {
            resource_name: params.user_resource_name,
            nickname: params.user_name,
            profile_image: params.user_profile_image,
            id: params.user_id
        };
        that.like_count = params.like_count;
        that.downloadable = params.downloadable;
        that.genre_id = params.genre_id;
        that.desc = params.description;
        that.resource_name = params.resource_path;
        that.created_at = params.created_at;

        that.getResourcePath = function() {
            return '/r/' + this.user.resource_name + '/' + this.resource_name + '/';
        };
        that.getStorageKey = function() {
            return this.type + '_' + this.unique_key;
        };
    };

    module.music.DropMusic = function (params) {
        var that = this;

        that.id = String(params.id);
        that.title = params.title;
        that.type = params.type;
        that.original = params.original;
    };

    module.music.PodcastMusic = function (params) {
        var that = this;

        that.id = params.stream_url;
        that.title = params.title;
        that.type = 'podcast';
    };

    module.music.Music.prototype = module.$.extend({}, BaseMusicPrototype);
    module.music.PodcastMusic.prototype = module.$.extend({}, BaseMusicPrototype);
    module.music.DropbeatMusic.prototype = module.$.extend({}, BaseMusicPrototype);
    module.music.DropMusic.prototype = module.$.extend({}, BaseMusicPrototype);

    function MusicQueue() {
        var that = this;

        that.q = [];
        that.playType = 'normal';
        that.idx = 0;

        that.init = function (listOfMusic, idx) {
            delete that.q;
            that.q = [];
            that.idx = idx || 0;
            if (listOfMusic) {
                that.q = that.q.concat(listOfMusic);
            }
        };

        that.push = function (music) {
            that.q.push(music);
        };

        that.pushEOL = function () {
// this method is needed for indicating stopping music iterating
// Music play stops when EOL popped from queue.
            that.q.push(module.constants.queueEOL);
        };

        that.walk = function (forward, commit) {
            var newIdx, repeatState, shuffleState;
            if (that.playType === 'normal') {
                if (forward) {
                    newIdx = that.idx + 1;
                } else {
                    newIdx = that.idx - 1;
                }
                if (that.q.length === 0) {
                    return;
                }
                if (that.q.length <= newIdx || newIdx < 0) {
                    repeatState = module.s.repeatControl.state;
                    shuffleState = module.s.shuffleControl.state;
// repeat one track case will be handled in player onFinish
// suffle never stop
                    if (repeatState === module.s.repeatState.noRepeat &&
                        shuffleState !== module.s.shuffleState.on) {
                        return;
                    } else {
                        if (forward) {
                            newIdx = 0;
                        } else {
                            newIdx = that.q.length - 1;
                        }
                    }
                }
                if (commit) {
                    that.idx = newIdx;
                }
                return that.q[newIdx];
            }
        };

        that.top = function () {
            if (that.q.length !== 0) {
                return that.q[0];
            }
        };

        that.removeWithId = function (musicId) {
            var i;

            for (i = 0; i < that.q.length; i += 1) {
                if (musicId === that.q[i].id) {
                    if (i <= that.idx) {
                        that.idx -= 1;
                    }
                    that.q.splice(i, 1);
                }
            }
        };
    }

    module.music.musicQueue = new MusicQueue();

    return module;
}($dbt));
