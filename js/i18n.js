var $dbt = (function(module) {
    // Library origin
    // https://github.com/gammasoft/browser-i18n
    var I18n = function(options){
        for (var prop in options) {
            this[prop] = options[prop];
        };
    };

    I18n.localeCache = {};

    I18n.prototype = {
        defaultLocale: "en",
        directory: "/locales",
        extension: ".json",

        getLocale: function(){
            return this.locale;
        },

        setLocale: function(locale) {
            var deferred = module.$.Deferred();
            if(!locale) {
                locale = module.$("html").attr("lang");
            }

            if(!locale) {
                locale = this.defaultLocale;
            }

            this.locale = locale;

            if(!(locale in I18n.localeCache)) {
                this.getLocaleFileFromServer(deferred);
            } else {
                deferred.resolve();
            }

            return deferred;
        },

        getLocaleFileFromServer: function(deferred){
            var localeFile, that = this;
            localeFile = null;

            module.$.ajax({
                url: this.directory + "/" + this.locale + this.extension,
                data: {
                    version:7
                },
                dataType: 'json',
                success: function(data){
                    localeFile = data;
                    I18n.localeCache[that.locale] = localeFile;
                    deferred.resolve();
                },
                error: function(res) {
                    //console.error(res.responseText);
                    deferred.reject();
                }
            });
        },

        __: function(){
            var msg;

            try {
                msg = I18n.localeCache[this.locale][arguments[0]];
                if (arguments.length > 1) {
                    msg = vsprintf(msg, Array.prototype.slice.call(arguments, 1));
                }
                if (!msg) {
                    //console.error(arguments[0] + " is undefined");
                    throw new Error('value not found');
                }
            } catch(e) {
                msg = ' ';
                if (window['Raygun'] && Raygun.send) {
                    Raygun.send(e, {
                        locale: this.locale,
                        args: Array.prototype.slice.call(arguments)
                    });
                }
            }
            if (msg.indexOf("\n") > -1) {
                msg = msg.replace(/\n/g, "<br/>");
            }
            return msg;
        },

        __n: function(singular, count){
            var msg = I18n.localeCache[this.locale][singular];

            count = parseInt(count, 10);
            if(count === 0)
                msg = msg.zero;
            else
                msg = count > 1 ? msg.other : msg.one;

            msg = vsprintf(msg, [count]);
            if (arguments.length > 2)
                msg = vsprintf(msg, Array.prototype.slice.call(arguments, 2));

            return msg;
        }
    };

    module = module || {};
    module.I18n = I18n;

    // Add shortcut

    module.string = function(args) {
        return module.i18n.__.apply($dbt.i18n, arguments);
    };

    module.string_n = function(args) {
        return module.i18n.__n.apply($dbt.i18n, arguments);
    };

    return module;
}($dbt));
