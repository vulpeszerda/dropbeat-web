/*jslint browser: true*/
var $dbt = (function (module) {
    'use strict';

    module = module || {};
    module.models = module.models || {};

    module.models.Account = function(params) {
        this._favoriteGenreIds = [];
        this._followings = {};
        this._userTrackLikes = {};
        this._externalTrackLikes = {};
        this._likeFetchReq = null;
        this._followFetchReq = null;
        this._likeReqs = {};
        this._followReqs = {};
        this._repostReqs = {};

        module.$.extend(this, params);

        this.repost = function(track, callback) {
            var that = this,
                data;

            module.view.ConfirmView.show({
                title: module.string('are_you_sure_repost_title'),
                text: module.string('are_you_sure_repost_desc'),
                confirmText: module.string('yes_repost'),
                closeText: module.string('cancel')
            }, function(confirmed) {
                if (!confirmed) {
                    return;
                }
                if (that._repostReqs[track.id]) {
                    return;
                }

                data = {
                    id: track.type === 'dropbeat' ? track.uid : track.id,
                    type: track.type,
                    title: track.title
                };

                if (track.type !== 'dropbeat' &&
                        track.user &&
                        track.user.resource_name &&
                        track.user.nickname) {

                    data.author_resource_name = track.user.resource_name;
                    data.author_nickname = track.user.nickname;

                    if (track.user.profile_image) {
                        data.author_profile_image = track.user.profile_image;
                    }
                }

                that._repostReqs[track.id] = module.$.ajax({
                    url:module.api.repostUrl,
                    dataType:'json',
                    cache:false,
                    data: JSON.stringify({
                        data:data
                    }),
                    contentType:'application/json',
                    type:'POST'
                }).done(function(res) {
                    if (!res || !res.success) {
                        callback && callback(false, res.error);
                        return;
                    }
                    callback && callback(true);
                }).fail(function(jqXHR, textStatus, error) {
                    callback && callback(false);
                }).always(function() {
                    delete that._repostReqs[track.id];
                });
            });

        };

        this.fetchGenres = function(callback) {
            var that = this;

            if (this._genreFetchReq) {
                this._genreFetchReq.abort();
            }

            this._genreFetchReq = module.$.ajax({
                url:module.api.genreFavoritesUrl,
                dataType:'json',
                cache:false
            }).done(function(res) {
                if (!res || !res.success || !res.data) {
                    callback && callback(false, res.error);
                    return;
                }
                that._favoriteGenreIds.length = 0;
                module.$.each(res.data, function(idx, id) {
                    that._favoriteGenreIds.push(id);
                });
                callback && callback(true, that._genreIds);
            }).fail(function(jqXHR, textStatus, error) {
                callback && callback(false, jqXHR, textStatus, error);

            }).always(function() {
                that._genreFetchReq = null;
            });
        };

        this.fetchLikes = function(callback) {
            var that = this;

            if (this._likeFetchReq) {
                this._likeFetchReq.abort();
            }

            this._likeFetchReq = module.$.ajax({
                url:$dbt.api.userLikeUrl,
                data: {
                    user_id:this.id
                },
                cache:false,
                dataType: 'json'
            }).done(function(res) {
                if (!res || !res.success || !res.data) {
                    callback && callback(false);
                    return;
                }
                that._userTrackLikes.length = 0;
                that._externalTrackLikes.length = 0;
                module.$.each(res.data, function(idx, like) {
                    var track = like.data,
                        type = like.type;

                    if (type === 'user_track') {
                        track = new module.music.DropbeatMusic(track);
                        that._userTrackLikes[track.id] = {
                            id : like.id,
                            track: track
                        };
                    } else {
                        track = new module.music.Music(track);
                        that._externalTrackLikes[track.id] = {
                            id : like.id,
                            track: track
                        };
                    }
                });
                callback && callback(
                    true, that._userTrackLikes, that._externalTrackLikes);

            }).fail(function(jqXHR, textStatus, error) {
                callback && callback(false, jqXHR, textStatus, error);

            }).always(function() {
                that._likeFetchReq = null;
            });
        };

        this.fetchFollowings = function(callback) {
            var that = this;

            if (this._followingFetchReq) {
                this._followingFetchReq.abort();
            }

            this._followingFetchReq = module.$.ajax({
                url: $dbt.api.followingUserUrl,
                data: {
                    user_id:this.id
                },
                cache:false,
                contentType: 'application/json',
                dataType: 'json'
            }).done(function(res) {
                if (!res || !res.success || !res.data) {
                    callback && callback(false);
                    return;
                }
                that._followings = {};
                module.$.each(res.data, function(idx, follow) {
                    var id = that._getUserLocalId(follow.user_type, follow.id),
                        type = follow.user_type,
                        user = {};

                    if (type === 'user') {
                        user = {
                            id: follow.id,
                            profile_image: follow.image,
                            nickname: follow.name,
                            resource_name: follow.resource_name,
                            type: 'user'
                        };
                    } else if (type === 'artist') {
                        user = {
                            id: follow.id,
                            profile_image: follow.image,
                            nickname: follow.name,
                            resource_name: follow.resource_name,
                            type: 'artist'
                        };
                    } else if (type === 'channel') {
                        user = {
                            id: follow.id,
                            profile_image: follow.image,
                            nickname: follow.name,
                            resource_name: follow.resource_name,
                            type: 'channel',
                            uid: follow.uid
                        };
                    }

                    that._followings[id] = follow;
                });
                callback && callback(true, that.getFollowings());

            }).fail(function (jqXHR, textStatus, error) {
                callback && callback(false, jqXHR, textStatus, error);

            }).always(function() {
                that._followingFetchReq = null;
            });
        };

        this.getFollowings = function() {
            var followings = [];
            module.$.each(this._followings, function(key, val) {
                followings.push(val);
            });
            return followings;
        };

        this._getUserLocalId = function(type, userId) {
            var prefix;

            if (type === 'artist') {
                prefix = 'a';
            } else if (type === 'user') {
                prefix = 'u';
            } else if (type === 'channel') {
                prefix = 'c';
            }
            if (!type || !userId) {
                return null;
            }

            return prefix + userId;
        };

        this.isFollowing = function(type, userId) {
            var id = this._getUserLocalId(type, userId);
            if (!id) {
                return false;
            }
            return !!this._followings[id];
        };

        this.isLiked = function(track) {
            if (!track.type || track.type === 'dropbeat') {
                return !!this._userTrackLikes[track.id];
            } else {
                return !!this._externalTrackLikes[track.id];
            }
        };

        this.like = function(track, callback) {
            var ajaxOption,
                like,
                that = this;

            if (track.type === 'dropbeat') {
                if (!track.uid) {
                    callback && callback(false);
                    return;
                }

                like = this._userTrackLikes[track.id];
                if (like) {
                    callback && callback(true);
                    return;
                }

                ajaxOption ={
                    url: module.api.userTrackLikeUrl,
                    data: JSON.stringify({
                        track_id: track.uid
                    }),
                    dataType:'json',
                    contentType: 'application/json',
                    type:'POST'
                };
            } else {
                like = this._externalTrackLikes[track.id];
                if (like) {
                    callback && callback(true);
                    return;
                }
                ajaxOption ={
                    url: module.api.externalTrackLikeUrl,
                    data: JSON.stringify({
                        data: {
                            id: track.id,
                            title: track.title,
                            type: track.type
                        }
                    }),
                    dataType:'json',
                    contentType: 'application/json',
                    type:'POST'
                };
            }

            if (this._likeReqs[track.id]) {
                return;
            }

            this._likeReqs[track.id] = module.$.ajax(ajaxOption).done(function(res) {
                var obj, likeId;

                if (!res || !res.success) {
                    callback && callback(false, res.error);
                    return;
                }
                obj = res.obj;
                likeId = obj.id;
                if (track.type === 'dropbeat') {
                    that._userTrackLikes[track.id] = {
                        id:likeId,
                        track:track
                    };
                } else {
                    that._externalTrackLikes[track.id] = {
                        id:likeId,
                        track:track
                    };
                }
                callback && callback(true);

                module.$(module).trigger('trackLiked', track);
            }).fail(function() {
                callback && callback(false);
            }).always(function() {
                delete that._likeReqs[track.id];
            });
        };

        this.dislike = function(track, callback) {
            var ajaxOption,
                like,
                that = this;

            if (track.type === 'dropbeat') {
                if (!track.id) {
                    callback && callback(false);
                    return;
                }

                like = this._userTrackLikes[track.id];
                if (!like) {
                    callback && callback(true);
                    return;
                }

                ajaxOption = {
                    url: module.api.userTrackLikeUrl,
                    data: JSON.stringify({
                        like_id: like.id
                    }),
                    dataType:'json',
                    contentType: 'application/json',
                    type:'DELETE'
                };
            } else {
                like = this._externalTrackLikes[track.id];
                if (!like) {
                    callback && callback(true);
                    return;
                }
                ajaxOption ={
                    url: module.api.externalTrackDisLikeUrl,
                    data: JSON.stringify({
                        id: like.id
                    }),
                    dataType:'json',
                    contentType: 'application/json',
                    type:'POST'
                };
            }

            if (this._likeReqs[track.id]) {
                return;
            }

            this._likeReqs[track.id] = module.$.ajax(ajaxOption).done(function(res) {
                if (!res || !res.success) {
                    callback && callback(false, res.error);
                    return;
                }
                if (track.type === 'dropbeat') {
                    delete that._userTrackLikes[track.id];

                } else {
                    delete that._externalTrackLikes[track.id];
                }
                callback && callback(true);

                module.$(module).trigger('trackDisliked', track);
            }).fail(function() {
                callback && callback(false);
            }).always(function() {
                delete that._likeReqs[track.id];
            });
        };

        this.follow = function(user, callback) {
            var data = {}, localId, that = this,
                userId = user.id,
                type = user.type || user.user_type;

            if (!userId) {
                callback && callback(false);
                return;
            }

            if (this.isFollowing(type, userId)) {
                callback && callback(true);
                return;
            }

            localId = this._getUserLocalId(type, userId);

            if (this._followReqs[localId]) {
                return;
            }

            if (type === 'artist') {
                data.artist_id = userId;
            } else if (type === 'user') {
                data.user_id = userId;
            } else if (type === 'channel') {
                data.channel_id = userId;
            }

            this._followReqs[localId] = module.$.ajax({
                url: module.api.followUserUrl,
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                type: 'POST'
            }).done(function(res) {
                if (!res || !res.success) {
                    callback && callback(false, res.error);
                    return;
                }
                that._followings[localId] = user;
                callback && callback(true);

                module.$(module).trigger('userFollow', user);
            }).fail(function(jqXHR, errorText, err) {
                callback && callback(false);
            }).always(function() {
                delete that._followReqs[localId];
            });
        };

        this.unfollow = function(user, callback) {
            var data = {}, localId, that = this,
                userId = user.id,
                type = user.type || user.user_type;

            if (!this.isFollowing(type, userId)) {
                callback && callback(true);
                return;
            }

            localId = this._getUserLocalId(type, userId);

            if (this._followReqs[localId]) {
                return;
            }

            if (type === 'artist') {
                data.artist_id = userId;
            } else if (type === 'user') {
                data.user_id = userId;
            } else if (type === 'channel') {
                data.channel_id = userId;
            }

            this._followReqs[localId] = module.$.ajax({
                url: module.api.unfollowUserUrl,
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                type: 'POST'
            }).done(function(res) {
                if (!res || !res.success) {
                    callback && callback(false, res.error);
                    return;
                }
                delete that._followings[localId];
                callback && callback(true);

                module.$(module).trigger('userUnfollow', user);
            }).fail(function(jqXHR, errorText, err) {
                callback && callback(false);
            }).always(function() {
                delete that._followReqs[localId];
            });
        };
    };

    return module;
}($dbt));
