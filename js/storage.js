/*jslint browser: true*/
var $dbt = (function (module) {
    'use strict';

    module = module || {};
    module.storage = {};

    module.storage.localStorage = {
        playlistKey: 'playlist',
        getPlaylist: function (idx) {
            var that = this,
                raw,
                playlist,
                tracks = [];

            idx = !idx ? 0 : idx;
            try {
                raw = localStorage.getItem(that.localPlaylistKey(idx));
            } catch (e) {
                // do nothing
            }
            if (raw) {
                raw = JSON.parse(raw);
                module.$.each(raw, function(idx, music) {
                    tracks.push(new module.s.Music(music));
                });

                playlist = new module.s.Playlist({
                    name: 'localPlaylist_' + idx,
                    tracks: tracks,
                    seqId: idx
                });
            } else {
                playlist = new module.s.Playlist({
                    name: 'localPlaylist_' + idx,
                    tracks: [],
                    seqId: idx
                });
            }
            return playlist;
        },

        setPlaylist: function (playlist, idx) {
            var that = this;

            localStorage.setItem(that.localPlaylistKey(idx), playlist);
        },

        localPlaylistKey: function (idx) {
            var that = this,
                key = that.playlistKey;

            if (idx) {
                key = key + idx;
            }
            return key;
        },

        flushPlaylist: function (idx) {
            var that = this;

            localStorage.setItem(that.localPlaylistKey(idx), '[]');
        }
    };

    module.storage.remoteStorage = {
        getPlaylistList: function(cb) {
            module.$.ajax({
                url: module.api.playlistListUrl,
                dataType: 'json',
                success: function (resp) {
                    var playlists = [];
                    if (!resp || !resp['data']) {
                        return;
                    }
                    module.$.each(resp['data'], function(idx, obj) {
                        var playlist;

                        playlist = new module.s.Playlist({
                            name: obj.name,
                            tracks: [],
                            seqId: obj.id
                        });
                        playlists.push(playlist);
                    });
                    playlists.reverse();
                    module.$.each(playlists, function(idx, playlist) {
                        playlist.idx = idx;
                    });
                    cb(playlists);
                },
                error: function(res) {
                    module.s.notify('Failed to get playlists', 'error');
                }
            });
        },

        getPlaylist: function (id, cb) {
            module.$.ajax({
                url: module.api.playlistUrl,
                data: {
                    id: id
                },
                dataType: 'json',
                success: function (resp) {
                    var playlist,
                        tracks = [];

                    if (!resp || !resp.success || !resp.playlist) {
                        cb && cb(false);
                        return;
                    }
                    module.$.each(resp.playlist.data, function(idx, t) {
                        var track
                        if (t.unique_key) {
                            track = new module.music.DropbeatMusic(t);
                        } else {
                            track = new module.music.Music(t);
                        }
                        tracks.push(track);
                    });

                    playlist = new module.s.Playlist({
                        name: resp.playlist.name,
                        tracks:tracks,
                        seqId:resp.playlist.id
                    });
                    cb && cb(playlist);
                },
                error: function(res) {
                    module.s.notify('Failed to get playlists', 'error');
                    cb && cb(false);
                }
            });
        },

        setPlaylist: function (playlist, id, cb) {
            module.$.ajax({
                method:'POST',
                url: module.api.playlistSetUrl,
                data: JSON.stringify({'data': playlist, 'playlist_id': id}),
                contentType: 'application/json',
                dataType: 'json',
                success: function (resp) {
                    cb && cb(resp.data);
                },
                error: function(res) {
                    module.s.notify('Failed to set playlists', 'error');
                }
            });
        },

        createPlaylist: function(name, cb) {
            module.$.ajax({
                method:'POST',
                url: module.api.playlistUrl,
                data: JSON.stringify({'name': name}),
                dataType: 'json',
                contentType: 'application/json',
                success: function(resp) {
                    var playlist, obj,
                        tracks = [];

                    obj = resp.obj;
                    if (obj.data) {
                        module.$.each(obj.data, function(idx, music) {
                            tracks.push(new module.s.Music(music));
                        });
                    }

                    playlist = new module.s.Playlist({
                        name: obj.name,
                        tracks:tracks,
                        seqId: obj.id
                    });
                    cb(playlist);
                },
                error: function(res) {
                    module.s.notify('Failed to create a playlist', 'error');
                }
            });
        },

        deletePlaylist: function(id, cb) {
            module.$.ajax({
                method:'DELETE',
                url: module.api.playlistUrl,
                data: JSON.stringify({'id': id}),
                contentType: 'application/json',
                dataType: 'json',
                success: function(resp) {
                    cb(resp.data);
                },
                error: function(res) {
                    module.s.notify('Failed to delete a playlist', 'error');
                }
            });
        },

        renamePlaylist: function(id, name, cb) {
            module.$.ajax({
                method:'PUT',
                url: module.api.playlistUrl,
                data: JSON.stringify({'id': id, 'name': name}),
                dataType: 'json',
                contentType: 'application/json',
                success: function(resp) {
                    cb(resp.data);
                },
                error: function(res) {
                    module.s.notify('failed to rename playlist', 'error');
                }
            });
        }
    };

    return module;
}($dbt));
