/*jslint browser: true*/
/*jslint nomen: true*/
/*global $, _*/
var $dbt = (function (module) {
    'use strict';

    module = module || {};
    module.playlist = module.playlist || {};

    module.playlist.Playlist = function (options) {
        var that = this,
            playlistManager = module.playlistManager;


        that.id = options.id;
        that.seqId = options.seqId;
        that.name = options.name || '';
        that.playlist = options.tracks || [];
        that.addable = true;
        that.sharedKey = null;
        that.loading = false;
        that.isInitialPlaylist = options.isInitialPlaylist;

        if (!module.context.account) {
            that.addable = false;
        }

        that.renderPlaylistRow = function (music, idx) {
            if (!music.id) {
                return '';
            }
            if (!that.rowTemplate) {
                that.rowTemplate =
                    module._.template(
                        module.$(playlistManager.elems.playlistRowTemplate).html()
                    );
            }

            return that.rowTemplate({music: music, idx: idx});
        };

        that.init = function (params) {
            throw 'Deprecated';
            if (params instanceof Array) {
                that.playlist = that.playlist.concat(params);
            }
        };

        that.getWithIdx = function (idx) {
            if (idx < that.playlist.length) {
                return that.playlist[idx];
            }
        };

        that.add = function (music, updateView) {
            var idx = that.findIdx(music.id),
                elems,
                playlistId;

            if (idx !== -1) {
                module.s.notify(module.string('already_in_playlist'), 'warn');
                return false;
            }
            if (!that.addable) {
                module.s.notify(module.string('playlist_not_editable'), 'error');
                return false;
            }

            if (that.loading) {
                module.s.notify(module.string('playlist_is_loading'), 'warn');
                return false;
            }

            that.playlist.push(music);
            if (updateView) {
                that.toTable(true);
                elems = module.playlistManager.elems;
                module.$(elems.playlist).stop();
                module.$(elems.playlist).animate({
                    scrollTop: 100 * that.length()
                }, '1000');
            }

            playlistId = module.s.playlistTabs.currentPlaylistId();

            if (module.playlistManager.playingPlaylistId
                    === playlistId) {
                module.s.musicQ.push(music);
                if (module.s.shuffleControl.isShuffle()) {
                    module.s.musicQ.init(
                        module.s.shuffleControl.shuffle(
                            module.s.musicQ.q.slice(0)));
                }
            }

            return true;
        };

        that.remove = function (music, updateView) {
            var idx = that.findIdx(music.id),
                playlistId;

            if (!that.addable) {
                module.s.notify(module.string('playlist_not_editable'), 'error');
                return false;
            }

            if (that.loading) {
                module.s.notify(module.string('playlist_is_loading'), 'warn');
                return false;
            }

            if (idx > -1) {
                that.playlist.splice(idx, 1);
            }

            if (updateView) {
                that.toTable(true);
            }

            playlistId = module.s.playlistTabs.currentPlaylistId();

            if (module.playlistManager.playingPlaylistId
                    === playlistId) {
                module.s.musicQ.removeWithId(music.id);
            }
            return true;
        };

        that.slicePlaylist = function (musicId) {
            var idx = that.findIdx(musicId);

            return that.playlist.slice(idx + 1);
        };

        that.findIdx = function (musicId) {
            var idx = -1,
                i;

            for (i = 0; i < that.playlist.length; i += 1) {
                if (that.playlist[i].id === musicId) {
                    idx = i;
                }
            }
            return idx;
        };

        that.loadFromRemote = function(callback) {
            var that = this;
            this.loading = true;

            module.s.storage.getPlaylist(that.seqId, function(playlist) {
                var playlistId, q, idx, i;

                that.loading = false;
                if (!playlist) {
                    callback(false);
                    return;
                }
                that.name = playlist.name;
                that.playlist.length = 0;
                module.$.each(playlist.playlist, function(idx, track) {
                    that.playlist.push(track);
                });

                playlistId = module.s.playlistTabs.currentPlaylistId();

                if (module.playlistManager.playingPlaylistId === playlistId) {
                    if (module.s.shuffleControl.isShuffle()) {
                        q = that.raw().slice(0);
                        module.s.shuffleControl.shuffle(q);
                        idx = -1;
                        if (module.playerManager.currentMusic) {
                            for (i = 0; i < q.length; i += 1) {
                                if (q[i].id ===
                                        module.playerManager.getCurrentMusic().id) {
                                    idx = i;
                                }
                            }
                        }

                        module.s.musicQ.init(q, idx);
                    } else {
                        if (module.playerManager.currentMusic) {
                            idx = that.findIdx(module.playerManager.currentMusic.id);
                        }
                        module.s.musicQ.init(that.playlist, idx);
                    }
                    module.s.playerBase.updateButton();
                }

                callback(true);
            });
        }

        that.sync = function () {
            if (!module.context.account) {
                return;
            }
            that.remoteSync();
        };

        that.remoteSync = function () {
// Sync all local playlists.
            module.s.storage.setPlaylist(
                that.toUploadablePlaylist(),
                that.seqId
            );
        };

        that.showLoading = function() {
            module.$('.playlist-section > .playlist .failure').hide();
            module.$('.playlist-section > .playlist .spinner').show();
            module.$('.playlist-section > .playlist .playlist-inner').hide();
        };

        that.hideLoading = function() {
            module.$('.playlist-section > .playlist .spinner').hide();
        };

        that.showError = function() {
            var playlistElems = playlistManager.elems;

            module.$(playlistElems.playlistInner).empty();
            module.$('.playlist-section > .playlist .failure').show();
            module.$('.playlist-section > .playlist .playlist-inner').hide();
        };

        that.hideError = function() {
            module.$('.playlist-section > .playlist .failure').hide();
        }

        that.toTable = function (clean, preventSortable) {
// Convertes `Playlist` to Table in HTML.

            var playlistManager = module.playlistManager,
                playerManager = module.playerManager,
                playlistElems = playlistManager.elems,
                playlistView = module.$(playlistElems.playlistInner),
                playlistRow =
                    module.$(playlistElems.playlistMusicContainer, playlistView),
                i,
                html,
                playlistId,
                track;

            that.hideError();
            that.hideLoading();
            playlistView.show();

            if (clean) {
                playlistRow.remove();
                playlistManager.clearMusicStorage();
            }

            for (i = 0; i < that.playlist.length; i += 1) {
                track = that.playlist[i];
                html = that.renderPlaylistRow(track, i + 1);
                playlistView.append(html);
                playlistManager.setMusicToStorage(track.getStorageKey(), track);
            }

            if (preventSortable) {
                playlistView.removeClass('sortable');
            } else {
                playlistView.addClass('sortable');
            }

            module.$(playlistElems.playlistMusicCount).text(that.playlist.length);

// Re-bold current music if playing.
            playlistId = module.s.playlistTabs.currentPlaylistId();

            if (playerManager.currentMusic
                    && playlistManager.playingPlaylistId === playlistId) {

                module.s.boldPlaylistTitle(playerManager.currentMusic.id);
            }
        };

        that.raw = function () {
            return that.playlist;
        };

        that.toUploadablePlaylist = function () {
            var i,
                m,
                pList = [],
                item;

            for (i = 0; i < that.playlist.length; i += 1) {
                m = that.playlist[i];
                if (m.type === 'dropbeat') {
                    if (!m.id) {
                        continue;
                    }
                    item = {
                        id: m.uid,
                        type: m.type
                    };
                } else {
                    item = {
                        title: m.title,
                        id: m.id,
                        type: m.type
                    };
                }
                pList.push(item)
            }
            return pList;
        };

        that.clear = function (updateView) {
            that.playlist = [];
            if (updateView) {
                that.toTable(true);
            }
        };

        that.length = function () {
            return that.playlist.length;
        };

        that.empty = function () {
            return that.length() === 0;
        };
    };

    module.playlist.boldTitle = function (id) {
        var selector,
            $newOnMusic;

        module.playlist.unboldTitle();

        selector = '.playlist-section .playlist .a-playlist-music' +
            '[data-music-id=\'' + id + '\']';
        $newOnMusic = module.$(selector);
        $newOnMusic.addClass('on');
    };

    module.playlist.unboldTitle = function () {
        var $onMusic;

        $onMusic = module.$('.playlist-section .playlist .a-playlist-music.on');
        $onMusic.removeClass('on');
    };

    return module;
}($dbt));
