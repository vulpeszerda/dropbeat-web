/*jslint browser: true*/
var $dbt = (function (module) {
    'use strict';

    var DropbeatView,
        DropbeatPageView,
        currPageView = null,
        WaveformViewMixin,
        PlayableViewMixin;

    module.view = module.view || {};

////////////////
// View Utils //
////////////////

    function extractMusicFromDOM(context, $el) {
        var musicStorageKey,
            musicData,
            drop;

        musicStorageKey = $el.data('musicStorageKey');
        if (musicStorageKey && context.getMusicFromStorage) {
            musicData = context.getMusicFromStorage(musicStorageKey);
            if (musicData) {
                return musicData;
            }
        }

        drop = {
            when: parseInt($el.data('dropSec')) || 0,
            type: $el.data('dropType'),
            dref: String($el.data('dropMusicId'))
        };
        if (!drop.type || !drop.dref) {
            drop = null;
        }
        musicData = {
            id: String($el.data('musicId')),
            title: unescape($el.data('musicTitle')),
            type: $el.data('musicType'),
            tag: $el.data('musicTag'),
            duration: $el.data('musicDuration'),
            drop: drop
        };
        musicData = new module.s.Music(musicData);
        if (!musicData.id || !musicData.type || !musicData.title) {
            return null;
        }

        return musicData;
    }

    function showDrop($el, music, drop) {
        var params = {},
            $drop = module.$('.drop', $el),
            $progress = module.$('.progress', $el),
            url = module.coreApi.dropUrl,
            totalTime,
            display,
            when;

// TODO : we need to handle this case later.
// we hide all youtube music's drop mark from progress
        if (music.type === 'youtube') {
            return;
        }

        if (music.duration) {
            if (music.type === 'dropbeat') {
                totalTime = parseInt(music.duration);
                if (drop) {
                    when = drop.start;
                }
            } else {
                totalTime = parseInt(music.duration) / 1000;
                if (drop) {
                    when = drop.when;
                }
            }
        } else {
            totalTime = module.playerManager.getTotalPlaybackTime();
            totalTime = totalTime === 0? 300 : totalTime;
            if (drop) {
                when = drop.when;
            }
        }
        if (totalTime > 30 * 60) {
            return;
        }

        display = function(drop) {
            var percent,
                dropDurationPercent,
                dropWidth,
                startPoint;
            percent = Math.min((100 * drop) / totalTime, 100);
            if (percent === 100) {
                return;
            }
            dropDurationPercent = Math.min((100 * 20) / totalTime, 100)

            dropWidth = $progress.width() * dropDurationPercent / 100;
            startPoint = $progress.width() * percent / 100;
            if (dropWidth + startPoint > $progress.width()) {
                dropWidth = $progress.width() - startPoint;
            }
            if (dropWidth < 100) {
                dropWidth = 100;
            }
            $drop.css({
                'width': dropWidth + 'px',
                'left': percent + '%'
            });
            $drop.show();
        };

        if (when) {
            if (when === -1) {
                return;
            }
            display(when);
            return;
        }

        if (music.type === 'youtube') {
            params['q'] = music.title;
        } else {
            params['q'] = music.id;
        }

        url += '?' + module.$.param(params);

        module.$.ajax({
            crossDomain: true,
            url: url,
            dataType: 'json'
        }).done(function(res) {
            if (!res.success || !res.drop || res.drop < 0) {
                return;
            }
            display(res.drop);
        });
    }

////////////
// Mixins //
////////////

    WaveformViewMixin = {
        bind: function(context, $el) {
            module.$.extend(context, this);
            context.bindWaveformRenderer($el);
        },

        unbind: function(context) {
            if (context.unbindWaveformRenderer) {
                context.unbindWaveformRenderer();
            }
        },

        bindWaveformRenderer: function($el) {
            module._.bindAll(this, '_onWindowResize', '_onWindowScroll',
                '_onMusicProgressUpdate', '_onWaveformProgressDown',
                '_onWaveformProgressMove', '_onWaveformProgressUp',
                '_onWaveformLoadingMusic');

            this.waveformRawData = {};
            this.waveformData = {};
            this.waveformReq = {};
            this._musicContainer = '.playable-track';
            this._playSections = this._playSections || [];
            this._$playableFrame = $el;

            module.$(module).on('loadingMusic', this._onWaveformLoadingMusic);
            module.$(module).on('musicProgressUpdate', this._onMusicProgressUpdate);
            this._$playableFrame.on('mousedown', '.waveform-progress', this._onWaveformProgressDown);
            module.$('.contents .scroll-y').on('scroll', this._onWindowScroll);

            module.$(window).on('resize', this._onWindowResize);
            module.$(window).on('mousemove', this._onWaveformProgressMove);
            module.$(window).on('mouseup', this._onWaveformProgressUp);
        },

        unbindWaveformRenderer: function() {
            var that = this;

            module.$(module).off('loadingMusic', this._onWaveformLoadingMusic);
            module.$(module).off('musicProgressUpdate', this._onMusicProgressUpdate);
            this._$playableFrame.off('mousedown', '.waveform-progress', this._onWaveformProgressDown);
            module.$('.contents .scroll-y').off('scroll', this._onWindowScroll);

            module.$(window).off('resize', this._onWindowResize);
            module.$(window).off('mousemove', '.waveform-progress', this._onWaveformProgressMove);
            module.$(window).off('mouseup', '.waveform-progress', this._onWaveformProgressUp);

            module.$.each(this.waveformReq, function(key, req) {
                req.abort();
                delete that.waveformReq[key];
            });

            module.$.each(this.waveformData, function(key, val) {
                delete that.waveformData[key];
            });

            module.$.each(this.waveformRawData, function(key, val) {
                delete that.waveformRawData[key];
            });
        },

        _onWindowScroll: function(e) {
            var $scroll = module.$('.contents .scroll-y'),
                scrollTop = $scroll.scrollTop(),
                THRESHOLD = 100;

            if  (this._lastScroll && Math.abs(this.lastScroll - scrollTop) < THRESHOLD) {
                return;
            }
            this._lastScroll = scrollTop;
            this.updateWaveform();
        },

        _onWindowResize: function(e) {
            var $waveform = module.$('.waveform-progress', this._$playableFrame),
                $playingWaveform,
                $progress,
                percent,
                that = this;

// Only update waveform width changed
            if (this._waveformWidth && $waveform.width() === this._waveformWidth) {
                return;
            }

// Re calculate bin size
            module.$.each(this.waveformRawData, function(url, data) {
                var $el = module.$('.waveform-progress[data-url=\'' + url + '\']', that._$playableFrame);
                that.waveformData[url] = that._calculateBars($el, data);
            });
            $waveform.removeClass('loaded');
            this.updateWaveform(true);

// Update progressing waveform
            $progress = $waveform.find('.progress .waveform-canvas');
            if ($progress.size() > 0 && (percent = $progress.data('percent'))) {
                $playingWaveform = $progress.closest('.waveform-progress');
                this._updateWaveformProgress($playingWaveform, parseInt(percent));
            }
        },

        _calculateBars: function($el, data) {
            var barCount,
                barWidth = 2,
                marginWidth = 1,
                width = $el.width();

            barCount = Math.floor(width / (barWidth + marginWidth));
            if (barCount === 0) {
                return [];
            }

            return module.utils.downsizeArray(data, barCount);
        },

        updateWaveform: function() {
            var that = this,
                $waveform = module.$('.waveform-progress', this._$playableFrame),
                $scroll = module.$('.contents .scroll-y'),
                scrollTop = $scroll.scrollTop(),
                windowHeight = module.$(window).height();

            if ($waveform.size() === 0) {
                return;
            }

            this._waveformWidth = $waveform.width();

            $waveform.each(function(idx, el) {
                var $el = module.$(el),
                    $container = $el.closest(that._musicContainer),
                    url = $el.data('url'),
                    nonSecureUrl,
                    data,
                    offsetTop = $container.offset().top,
                    containerHeight = $container.height(),
                    musicData;

                musicData = extractMusicFromDOM(that, $container);

                if (that.waveformReq[url]) {
                    return;
                }

                if (offsetTop + containerHeight < -100 || offsetTop > windowHeight + 100) {
                    $el.hide();
                    return;
                } else {
                    $el.show();
                }
                if ($el.hasClass('loaded') || $el.hasClass('failed')) {
                    return;
                }

                data = that.waveformData[url];
                if (data) {
                    renderWaveform($el, data);
                    return;
                }

                if (musicData && musicData.waveform_data) {
                    that.waveformRawData[url] = musicData.waveform_data;
                    that.waveformData[url] = that._calculateBars($el, musicData.waveform_data);
                    renderWaveform($el, that.waveformData[url]);
                    return;
                }

// XXX:
// In case IE9, ajax request through http -> https will be blocked by browser
// So our waveform request which is to AWS will be also blocked cause its https
// But thanksfully AWS supports normal http request,
// we will replace request url to http
                if (url.startsWith('https://')) {
                    nonSecureUrl = url.replace('https://', 'http://');
                }
                that.waveformReq[url] = module.$.ajax({
                    url: nonSecureUrl || url,
                    dataType:'json',
                    crossDomain: true
                }).done(function(data) {
                    that.waveformRawData[url] = data;
                    that.waveformData[url] = that._calculateBars($el, data);
                    renderWaveform($el, that.waveformData[url]);
                }).fail(function(jqXHR, textStatus, error) {
                    if (textStatus === 'abort') {
                        return;
                    }
                    that.waveformData[url] = [];
                    $el.addClass('failed');
                }).always(function() {
                    delete that.waveformReq[url];
                });
            });

            function renderWaveform($el, bars) {
                var $bgCanvas,
                    bgCanvas,
                    $drop = $el.find('.drop'),
                    $dropCanvas,
                    dropCanvas,
                    width = $el.width(),
                    height = $el.height(),
                    barWidth = 2,
                    marginWidth = 1,
                    colorNormal = '#636365',
                    //colorDrop = '#cd85ff',
                    colorDrop = '#f03eb1',
                    duration,
                    dropStart,
                    dropEnd,
                    fromIdx,
                    toIdx,
                    i,
                    fromX,
                    toX,
                    maxValue,
                    context;

                $bgCanvas = module.$('.bg .waveform-canvas', $el);
                if ($bgCanvas.size() === 0) {
                    $bgCanvas = module.$('<canvas class=\'waveform-canvas\'></canvas>');
                    $el.find('.bg').html($bgCanvas);
                }
                $bgCanvas.css({
                    width: width,
                    height: height
                });
                bgCanvas = $bgCanvas[0];

                $dropCanvas = module.$('.drop .waveform-canvas', $el);
                if ($dropCanvas.size() === 0) {
                    $dropCanvas = module.$('<canvas class=\'waveform-canvas\'></canvas>');
                    $el.find('.drop').html($dropCanvas);
                }
                $drop.html($dropCanvas);

                bgCanvas.width = width;
                bgCanvas.height = height;

                context = bgCanvas.getContext('2d');
                context.clearRect(0, 0, bgCanvas.width, bgCanvas.height);

                module.$.each(bars, function(idx, val) {

                    var x, barHeight, y;
                    x = idx * (barWidth + marginWidth);
                    barHeight = val * height * 0.009;
                    y = height - barHeight;
                    context.fillStyle = colorNormal;
                    context.fillRect(x, y, barWidth, barHeight);
                });
                context.save();

                if ($drop.size() > 0) {

                    duration = $el.data('duration');
                    dropStart = $el.find('.drop').data('dropStart');
                    dropEnd = $el.find('.drop').data('dropEnd');
                    fromX = Math.round((dropStart / duration) * width);
                    toX = Math.round((dropEnd / duration) * width);
                    fromIdx = Math.floor(fromX / (barWidth + marginWidth));
                    toIdx = Math.ceil(toX / (barWidth + marginWidth));

                    dropCanvas = $dropCanvas[0];
                    dropCanvas.width = width;
                    dropCanvas.height = height;

                    $dropCanvas.css({
                        width: width,
                        height: height
                    });

                    context = dropCanvas.getContext('2d');
                    context.globalAlpha=0.8;
                    context.clearRect(0, 0, dropCanvas.width, dropCanvas.height);

                    for(i = fromIdx; i < bars.length && i < toIdx; i++) {
                        var val, x, barHeight, y, delta, w;

                        val = bars[i];
                        x = i * (barWidth + marginWidth);
                        barHeight = val * height * 0.009;
                        y = height - barHeight;
                        context.fillStyle = colorDrop;
                        w = barWidth;
                        if (i === fromIdx) {
                            delta = x;
                            x = Math.max(x, fromX);
                            delta = x - delta;
                            w -= delta;
                        }
                        if (i === toIdx - 1) {
                            delta = Math.min(x + w, toX) - x;
                            w = delta;
                        }
                        context.fillRect(x, y, w, barHeight);
                    }
                }
                $el.addClass('loaded');
            }
        },

        _updateWaveformProgress: function($el, progress) {
            var $canvas = $el.find('.progress .waveform-canvas'),
                canvas,
                width = $el.width(),
                height = $el.height(),
                barWidth = 2,
                marginWidth = 1,
                colorNormal = '#ffffff',
                bars,
                i,
                toX,
                toIdx,
                maxValue,
                context,
                url = $el.data('url');

            if (!this.waveformData[url]) {
                return;
            }

            bars = this.waveformData[url];

            toX = Math.round((progress / 100) * width);
            toIdx = Math.ceil(toX / (barWidth + marginWidth));

            if ($canvas.size() === 0) {
                $el.find('.progress').html('<canvas class=\'waveform-canvas\'></canvas>');
                $canvas = $el.find('.progress .waveform-canvas');
            }
            $canvas.data('percent', progress);
            $canvas.css({
                width: width,
                height: height
            });
            canvas = $canvas[0];

            canvas.width = width;
            canvas.height = height;
            context = canvas.getContext('2d');
            context.clearRect(0, 0, canvas.width, canvas.height);

            for(i = 0; i < bars.length && i < toIdx; i++) {
                var val, x, barHeight, y, delta, w;

                val = bars[i];
                x = i * (barWidth + marginWidth);
                barHeight = val * height * 0.009;
                y = height - barHeight;
                context.fillStyle = colorNormal;
                w = barWidth;
                if (i === toIdx - 1) {
                    delta = Math.min(x + w, toX) - x;
                    w = delta;
                }
                context.fillRect(x, y, w, barHeight);
            }
        },

        _onWaveformProgressDown: function(e) {
            var playerManager = module.playerManager,
                currentMusic = playerManager.currentMusic,
                $el = module.$(e.currentTarget),
                $waveform, offsetLeft, x, left, width;

            if (!currentMusic ||
                    String($el.closest(this._musicContainer).data('musicId')) !==  currentMusic.id ||
                    !playerManager.playing ||
                    playerManager.currentPlayer.section === 'drop') {
                return;
            }
            this._waveformProgressTarget = $el;
            this._waveformProgressDown = true;
            if (this._seekTimer) {
                clearTimeout(this._seekTimer);
                this._seekTimer = null;
            }

            offsetLeft = $el.offset().left;
            width = $el.width();
            x = e.pageX - offsetLeft;
            left = x * 100 / width;

            if (left < 0) {
                left = 0;
            }
            if (left > $el.width()) {
                left = 100;
            }
            this._updateWaveformProgress($el, left);
        },

        _onWaveformProgressMove: function(e) {
            var $waveform, offsetLeft, x, left, width;

            if (this._waveformProgressDown && this._waveformProgressTarget) {
                offsetLeft = this._waveformProgressTarget.offset().left;
                width = this._waveformProgressTarget.width();
                x = e.pageX - offsetLeft;
                left = x * 100 / width;

                if (left < 0) {
                    left = 0;
                }
                if (left > this._waveformProgressTarget.width()) {
                    left = 100;
                }
                $waveform = this._waveformProgressTarget;
                this._updateWaveformProgress($waveform, left);
            }
        },

        _onWaveformProgressUp: function(e) {
            var playerManager = module.playerManager,
                currentMusic = playerManager.currentMusic,
                $musicContainer,
                seekTo, offsetLeft, x, left, duration, width, curr, $waveform;

            if (this._waveformProgressDown && this._waveformProgressTarget) {
                duration = parseInt(this._waveformProgressTarget.data('duration'));

                $musicContainer = this._waveformProgressTarget.closest(this._musicContainer);
                if (!currentMusic ||
                        String($musicContainer.data('musicId')) !==  currentMusic.id ||
                        !playerManager.playing ||
                        playerManager.currentPlayer.section === 'drop' ||
                        !duration) {

                    if (duration) {
                        curr = playerManager.getCurrentPlaybackTime();
                        left = curr * 100 / duration;
                    } else {
                        left = 0;
                    }
                    $waveform = this._waveformProgressTarget;
                    this._updateWaveformProgress($waveform, left);

                    this._waveformProgressTarget = null;
                    this._waveformProgressDown = null;
                    return;
                }
                offsetLeft = this._waveformProgressTarget.offset().left;
                width = this._waveformProgressTarget.width();

                x = e.pageX - offsetLeft;
                left = x * 100 / width;

                if (left < 0) {
                    left = 0;
                }
                if (left > this._waveformProgressTarget.width()) {
                    left = 100;
                }
                seekTo = duration * left / 100;

                playerManager.seekTo(seekTo);
                this._waveformProgressTarget = null;

                this._seekTimer = setTimeout(module.$.proxy(function() {
                    this._waveformProgressDown = false;
                }, this), 1000);
            }
        },

        _onWaveformLoadingMusic: function(e, m, s) {
            this._$playableFrame.find(this._musicContainer + ' .playback-time').text('00:00');
            this._$playableFrame.find(this._musicContainer + ' .duration-time').text('00:00');
            this._$playableFrame.find(this._musicContainer +
                ' .waveform-progress .progress canvas').remove();
        },

        _onMusicProgressUpdate: function(e, progText, music, section) {
            var $musicContainer,
                $targetMusic,
                $targetPlayback,
                $targetDuration,
                $targetProgress;

            if (!music) {
                return;
            }

            $targetMusic = this._$playableFrame.find(
                this._musicContainer + '[data-music-id=\'' + music.id + '\']');

            if ($targetMusic.size() === 0) {
                return;
            }

            $targetPlayback = $targetMusic.find('.playback-time');
            $targetPlayback.text(moment.duration({seconds:progText.playback}).format('mm:ss'));

            $targetDuration = $targetMusic.find('.duration-time');
            $targetDuration.text(moment.duration({seconds:progText.duration}).format('mm:ss'));

            if (this._waveformProgressDown) {
                return;
            }

            if (progText.duration > 0) {
                $targetProgress = $targetMusic.find('.waveform-progress');
                this._updateWaveformProgress($targetProgress, progText.playback * 100 / progText.duration);
            }
        }
    };

// Mixin class for playable view event listen
// call simply PlayableViewMixin.bind from view's initailize func
// and call PlayableViewMixin.unbind from view's remove func
    PlayableViewMixin = {
        bind: function(context, $el, sections) {
            module.$.extend(context, this);
            context.bindPlayableView($el, sections);
        },

        unbind: function(context) {
            var publicPlaylists, playlist, playlistId;

            if (context.unbindPlayableView) {
                context.unbindPlayableView();
            }
        },

        bindPlayableView: function($el, sections) {
            var that = this;

            module._.bindAll(this, '_onPlayMusicClicked', 
                '_onPlayInfoUpdate', '_onTogglePlay',
                '_onDropBtnMouseEnter', '_onDropBtnClicked', '_onDropBtnMouseLeave',
                '_onPlayerStopped', '_onPlayPlaylistBtnClicked',
                '_onPlayCtrlClicked', '_onMusicFinished',
                '_onDownloadMusicClicked', '_onLoadingMusic', '_onPlayMusic');

            this._musicContainer = '.playable-track';
            this._$playableFrame = $el;
            this._playSections = sections || [];
            this._musicStorage = {};
            if (this.parentMusicStorage) {
                module.$.each(this.parentMusicStorage, function(key, val) {
                    that._musicStorage[key] = val;
                });
            }

            this._$playableFrame.on('click', '.play-music', this._onPlayMusicClicked);
            this._$playableFrame.on('click', '.btn-download', this._onDownloadMusicClicked);
            this._$playableFrame.on('click', '.listen-drop', this._onDropBtnClicked);
            this._$playableFrame.on('mouseenter', 'listen-drop', this._onDropBtnMouseEnter);
            this._$playableFrame.on('mouseleave', 'listen-drop', this._onDropBtnMouseLeave);
            this._$playableFrame.on('click', '.play-playlist-btn', this._onPlayPlaylistBtnClicked);
            this._$playableFrame.on('click', '.ctrl-play', this._onPlayCtrlClicked);
            module.$(module).on('playInfoUpdate loadingMusic', this._onPlayInfoUpdate);
            module.$(module).on('finishedMusic playerStopped', this._onMusicFinished);
            module.$(module).on('togglePlay', this._onTogglePlay);
            module.$(module).on('loadingMusic', this._onLoadingMusic);
            module.$(module).on('musicProgressUpdate', this._onPlayMusic);
            module.$(module).on('playerStopped', this._onPlayerStopped);
        },

        updatePlayState: function() {
            var playerManager = module.playerManager,
                currentTrack = playerManager.getCurrentMusic(),
                $onMusic;

            this._$playableFrame.find(this._musicContainer).removeClass('on');
            if (currentTrack) {
                $onMusic = this._$playableFrame.find(this._musicContainer +
                    '[data-music-id=\'' + currentTrack.id + '\']');
                $onMusic.addClass('on');

                if (playerManager.playing) {
                    $onMusic.find('.ctrl-play').addClass('pause');
                } else {
                    $onMusic.find('.ctrl-play').removeClass('pause');
                }
            }
        },

        _onTogglePlay: function(e, music, section, isPlaying) {
            var $musicContainer,
                $onMusic,
                $targetMusic;

            this.onTogglePlay && this.onTogglePlay.apply(this, arguments);

            $onMusic = this._$playableFrame.find(this._musicContainer + '.on');
            if (!music) {
                if (!isPlaying) {
                    $onMusic.find('.ctrl-play').removeClass('pause');
                }
            } else {
                $targetMusic = this._$playableFrame.find(
                    this._musicContainer + '[data-music-id=\'' + music.id + '\']');
                if ($targetMusic.size() > 0) {
                    if (isPlaying) {
                        $targetMusic.find('.ctrl-play').addClass('pause');
                    } else {
                        $targetMusic.find('.ctrl-play').removeClass('pause');
                    }
                }
            }
            if (section === 'drop') {
                module.$('.play-music', this._$playableFrame).removeClass('pause');
                module.$('.listen-drop', this._$playableFrame).removeClass('pause');

                if (module.playerManager.playing) {
                    $musicContainer = module.$(this._musicContainer + '.on', this._$playableFrame);
                    $musicContainer.find('.listen-drop').addClass('pause');
                }
            }
        },

        _onPlayerStopped: function() {
            var $onMusic = this._$playableFrame.find(this._musicContainer + '.on');
            $onMusic.removeClass('on');
        },

        _onPlayPlaylistBtnClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                $music = this._$playableFrame.find(this._musicContainer).first();

            if ($btn.hasClass('playing')) {
                return;
            }
            $music.find('.play-music').click();
        },

        _onDownloadMusicClicked: function(e) {
            var $btn = module.$(e.currentTarget),
                resourcePath = $btn.data('target');

            if (!module.context.account) {
                module.view.NeedSigninView.show();
                return;
            }
            $dbt.view.FollowToDownloadView.show(resourcePath);
        },

        _onMusicFinished: function(e, music, section) {
            var $onMusic = this._$playableFrame.find(this._musicContainer + '.on');
            $onMusic.removeClass('on');
            $onMusic.find('.listen-drop').removeClass('loading');
        },

        _onPlayInfoUpdate: function(e, music, section) {
            var $onMusic = this._$playableFrame.find(this._musicContainer + '.on'),
                musicData,
                $newOnMusic;

            musicData = extractMusicFromDOM(this, $onMusic);

            if (section === 'drop') {
                if (!musicData || !musicData.drop || musicData.drop.dref !== music.id) {
                    $onMusic.removeClass('on');
                    $newOnMusic = this._$playableFrame
                        .find(this._musicContainer + '[data-drop-music-id=\'' + music.id + '\']');
                    $newOnMusic.addClass('on');
                }
                return;
            }
            $onMusic.removeClass('on');
            $newOnMusic = this._$playableFrame
                .find(this._musicContainer + '[data-music-id=\'' + music.id + '\']');
            $newOnMusic.addClass('on');
        },

        getPlaySection: function(e) {
            return this._playSections[0];
        },

        _onDropBtnClicked: function(e) {
            var $dropBtn = module.$(e.currentTarget),
                $musicContainer,
                musicData,
                currMusic,
                that = this,
                loading = false;

            if ($dropBtn.hasClass('loading')) {
                return;
            }

            $musicContainer = $dropBtn.closest(this._musicContainer);

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData || !musicData.drop) {
                return;
            }

            this._$playableFrame.find(this._musicContainer + '.on').removeClass('on');
            $musicContainer.addClass('on');

            currMusic = module.playerManager.getCurrentMusic();
            if (module.playerManager.currentPlayer &&
                    module.playerManager.currentPlayer.section === 'drop') {

// We should puase music
                if (!currMusic ||
                        currMusic.type !== musicData.drop.type ||
                        currMusic.id !== musicData.drop.dref ||
                        (!module.playerManager.playing && !$dropBtn.hasClass('loading'))) {
                    loading = true;
                    $dropBtn.addClass('loading');
                    module.$(module).one(
                        'playMusic',
                        function(e, music, s) {
                            bindDropTimer();
                            $dropBtn.removeClass('loading');
                        });
                }
                module.playerManager.onPlayMusic(
                    new module.music.DropMusic({
                        'id': musicData.drop.dref,
                        'title': musicData.title,
                        'type': musicData.drop.type,
                        'original': musicData
                    }), 'drop', musicData.drop.when);

                if (!loading) {
                    bindDropTimer();
                }
            } else {
                $dropBtn.addClass('loading');
                module.$(module).one(
                    'playMusic',
                    function(e, music, s) {
                        bindDropTimer();
                        $dropBtn.removeClass('loading');
                    });
                module.playerManager.onMusicClicked(
                    new module.music.DropMusic({
                        'id': musicData.drop.dref,
                        'title': musicData.title,
                        'type': musicData.drop.type,
                        'original': musicData
                    }), 'drop', { startPoint: musicData.drop.when});
            }

            function bindDropTimer() {
                if (that._dropPlayerTimer) {
                    clearTimeout(that._dropPlayerTimer);
                    that._dropPlayerTimer = null;
                }
                if (module.playerManager.playing) {
                    that._dropPlayerTimer = setTimeout(function() {
                        var currMusic;
                        that._dropPlayerTimer = null;
                        if (!module.playerManager.playing ||
                                !module.playerManager.currentPlayer ||
                                module.playerManager.currentPlayer.section !== 'drop') {
                            return;
                        }
                        currMusic = module.playerManager.getCurrentMusic();
                        if (currMusic.id === musicData.drop.dref &&
                                currMusic.type === musicData.drop.type) {
                            module.playerManager.stop('drop');
                        }
                    }, 20000);
                }
            }
        },

        _onDropBtnMouseEnter: function(e) {
            var $target = module.$(e.currentTarget),
                $wrapper = $target.closest('.title-wrapper');
            $wrapper.find('.play-music').addClass('hover');
        },

        _onDropBtnMouseLeave: function(e) {
            var $target = module.$(e.currentTarget),
                $wrapper = $target.closest('.title-wrapper');
            $wrapper.find('.play-music').removeClass('hover');
        },

        openTrackList: function($musicContainer, musicData) {
            var $wrapper,
                $tracks,
                $openedChildren,
                that = this;

            $wrapper = $musicContainer.closest('.a-addable-music-wrapper');

// Hide previous played music
            if (this._tracklistReq) {
                this._tracklistReq.abort();
                this._tracklistReq = null;
            }
            if (!$musicContainer.hasClass('opened')) {
                if (this._$currMusicContainer) {
                    hideSubTracks(this._$currMusicContainer,
                            $musicContainer,
                            function() {
                                $musicContainer.addClass('opened');
                                that._$currMusicContainer = $musicContainer;
                            });
                } else {
                    $musicContainer.addClass('opened');
                    this._$currMusicContainer = $musicContainer;
                }
            }
            if ($musicContainer.next().hasClass('tracks')) {
                $tracks = $musicContainer.next();

                $openedChildren =
                    module.$('.a-addable-music.opened', $tracks);
                $openedChildren.removeClass('opened');
                return;
            }
        },

        _onPlayCtrlClicked: function(e) {
            var $playCtrl = module.$(e.currentTarget),
                $musicContainer,
                $playlist,
                musicData,
                section = this.getPlaySection(e),
                playlistId,
                startPoint,
                cb;

            $musicContainer = module.$(e.currentTarget).closest(this._musicContainer);

            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return;
            }

            $playlist = $musicContainer.closest('.a-playable-list');

            cb = module.$.proxy(function(musicData) {
                if ($playCtrl.hasClass('pause')) {
                    module.playerManager.onPlayMusic(musicData, section);
                    return;
                }

                module.s.progressController.init(section,
                    $musicContainer, musicData.id);

                this.openTrackList($musicContainer, musicData);

                if ($playlist.size() > 0) {
                    playlistId = $playlist.data('playlistId');
                }

                module.playerManager.onMusicClicked(
                    musicData,
                    section, {
                        playlistId: playlistId
                    });
            }, this);

            if (musicData.tag === 'undefined_track' &&
                    (!musicData.id || musicData.id === '')) {
                this._resolveUndefinedTrack($musicContainer, musicData, cb);
            } else {
                cb(musicData);
            }
        },

        _onPlayMusicClicked: function(e, startPoint) {
            var $musicContainer,
                $playlist,
                musicData,
                section = this.getPlaySection(e),
                cb, playlistId;

            $musicContainer = module.$(e.currentTarget).closest(this._musicContainer);
            musicData = extractMusicFromDOM(this, $musicContainer);
            if (!musicData) {
                return
            }

            $playlist = $musicContainer.closest('.a-playable-list');

            cb = module.$.proxy(function(musicData) {
                module.s.progressController.init(section,
                    $musicContainer, musicData.id);

                this.openTrackList($musicContainer, musicData);

                if ($playlist.size() > 0) {
                    playlistId = $playlist.data('playlistId');
                }
                module.playerManager.onMusicClicked(
                    musicData,
                    section, {
                        playlistId: playlistId,
                        startPoint: startPoint
                    });
            }, this);

            if (musicData.tag === 'undefined_track' &&
                    (!musicData.id || musicData.id === '')) {
                this._resolveUndefinedTrack($musicContainer, musicData, cb);
            } else {
                cb(musicData);
            }
        },

        _resolveUndefinedTrack: function($musicContainer, music, callback) {
            var that = this;

            if (this._resolveReq) {
                this._resolveReq.abort();
            }

            this._resolveRes = module.$.ajax({
                url: module.coreApi.queryIndexUrl,
                data: JSON.stringify({
                    q: music.title
                }),
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json',
                crossDomain: true
            }).done(function(res) {
                var id, type, $parentContainer, m, key;

                if (!res || !res.success || !res.data) {
                    module.s.notify(module.string('failed_to_fetch_music_info'), 'error');
                    return;
                }
                id = res.data;
                type = 'soundcloud';
                if (!/^\d+$/.test(id)) {
                    if (id.startsWith('http')) {
                        type = 'podcast';
                    } else {
                        type = 'youtube';
                    }
                }

                key = type + '_' + id;

                m = new module.music.Music({
                        id: id,
                        type: type,
                        title: music.title
                    });

                that.setMusicToStorage(m.getStorageKey(), m);
                $musicContainer.data('musicStorageKey', key);
                $musicContainer.attr('data-music-storage-key', key);
                $musicContainer.data('musicId', id);
                $musicContainer.attr('data-music-id', id);

                $parentContainer = $musicContainer.closest('.a-addable-music-wrapper');
                if ($parentContainer.size() > 0) {
                    $parentContainer.data('parentMusicId', id);
                    $parentContainer.attr('data-parent-music-id', id);
                }
                music.id = id;
                music.type = type;
                callback && callback(music);
            }).fail(function(jqXHR, errorText, err) {
                if (errorText !== 'abort') {
                    module.s.notify(module.string('failed_to_fetch_music_info'), 'error');
                }
            }).always(function() {
                that._resolveReq = null;
            });
        },

        _onLoadingMusic: function(e, m, s) {
            var $loadingMusic = this._$playableFrame.find(this._musicContainer + '.loading'),
                musicData,
                $targetMusic;

            if (!m) {
                $loadingMusic.removeClass('loading');
                return;
            }

            if ($loadingMusic.size() > 0) {
                musicData = extractMusicFromDOM(this, $loadingMusic);
                if (musicData.id === m.id && this._playSections.indexOf(s) > -1) {
                    return;
                }
                $loadingMusic.removeClass('loading');
            }

            $targetMusic = this._$playableFrame
                .find(this._musicContainer + '[data-music-id=\'' + m.id + '\']');
            $targetMusic.addClass('loading');

            this._playingMusic = null;
            this.onLoadingMusic && this.onLoadingMusic(e, m, s);
        },

        _onPlayMusic: function(e, progText, music, section) {
            var $loadingMusic = this._$playableFrame.find(this._musicContainer + '.loading');
            if (!music || progText.playback === 0 ||
                    (this._playingMusic &&
                    this._playingMusic.id === music.id)) {
                return;
            }
            this._playingMusic = music;
            $loadingMusic.removeClass('loading');
            this.onPlayMusic && this.onPlayMusic(e, progText, music, section);
        },

        unbindPlayableView: function() {
            if (this.tracklistReq) {
                this.tracklistReq.abort();
                this.tracklistReq = null;
            }

            if (this._resolveReq) {
                this._resolveReq.abort();
                this._resolveReq = null;
            }
            this._musicStorage = {};

            module.$(window).off('resize', this._onWindowResize);
            module.$(module).off('playInfoUpdate loadingMusic', this._onPlayInfoUpdate);
            module.$(module).off('finishedMusic playerStopped', this._onMusicFinished);
            module.$(module).off('togglePlay', this._onTogglePlay);
            module.$(module).off('loadingMusic', this._onLoadingMusic);
            module.$(module).off('musicProgressUpdate', this._onPlayMusic);
            module.$(module).off('playerStopped', this._onPlayerStopped);

            this._$playableFrame.off('click', '.play-music', this._onPlayMusicClicked);
            this._$playableFrame.off('click', '.btn-download', this._onDownloadMusicClicked);
            this._$playableFrame.off('click', '.listen-drop', this._onDropBtnClicked);
            this._$playableFrame.off('mouseenter', 'listen-drop', this._onDropBtnMouseEnter);
            this._$playableFrame.off('mouseleave', 'listen-drop', this._onDropBtnMouseLeave);
            this._$playableFrame.off('click', '.play-playlist', this._onPlayPlaylistBtnClicked);
            this._$playableFrame.off('click', '.ctrl-play', this._onPlayCtrlClicked);
        },

        setMusicToStorage: function(key, val) {
            this._musicStorage[key] = val;
        },

        updateMusicStorage: function(map) {
            module.$.extend(this._musicStorage, map);
        },

        getMusicFromStorage: function(key) {
            return this._musicStorage[key];
        },

        clearMusicStorage: function() {
            this._musicStorage = {};
        }
    };

///////////////
// Base view //
///////////////

    DropbeatView = Backbone.View.extend({
        initialize: function(options) {
            var context,
                parentView;
            options = options || {};

            context = options.context || {};
            context = module.$.extend({}, module.context, context);
            this._context = { _context: context };

            parentView = options.parentView;
            if (parentView) {
                this.parentView = parentView;
            }

            if (this.tmpl) {
                this._tmpl = module._.template(module.$(this.tmpl).html());
            }
        }
    });

    DropbeatPageView = DropbeatView.extend({
        initialize: function(options) {
            DropbeatView.prototype.initialize.apply(this, arguments);
            this._$pageTarget = module.$('#dropbeat .view-holder');
        },

        render: function() {
            this._$pageTarget.html(this._tmpl(this._context));
            this.$el = module.$('.page', this._$pageTarget);
        }
    });

////////////////
// Page views //
////////////////


    module.view.setPage = function(view) {
        currPageView && currPageView.remove();
        currPageView = view;
    };

    module.view.FailureView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-failure',
        initialize: function(options) {
            DropbeatPageView.prototype.initialize.apply(this, arguments);
            this._context = module.$.extend({ message: options.message });
            this.render();
        }
    });

    module.view.LoadingView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-loading',
        initialize: function(options) {
            var progress;

            DropbeatPageView.prototype.initialize.apply(this, arguments);
            this.render();

            progress = new Mprogress({
                template: 3,
                parent: '#loading-progress'
            });
            progress.start();
        }
    });

    module.view.PlayerView = DropbeatPageView.extend({
        tmpl: '#tmpl-page-player',
        initialize: function(options) {
            var track = options.track;

            module._.bindAll(this, 'onPlayMusic');

            DropbeatPageView.prototype.initialize.apply(this, arguments);
            module.$.extend(this._context, {
                track:options.track,
                trackLink: options.trackLink,
                showWaveform: !!track.waveform_url
            });
            this.render();

            WaveformViewMixin.bind(this, this.$el);
            PlayableViewMixin.bind(this, this.$el, ['player']);

            this.setMusicToStorage(track.getStorageKey(), track);
            this.updateWaveform();

            this.$player = module.$('#external-player');
            this.$contents = module.$('.play-contents', this.$el);
            this.$overlayFilter = module.$('.overlay-filter', this.$el);
            this.$bubble = module.$('.share-bubble', this.$el);
            
            if (options.autoPlay) {
                module.$('.ctrl-play', this.$el).click();
            }
            module.$(module).on('playMusicFirst', this.onPlayMusic);
        },

        events: function() {
            var events = DropbeatPageView.prototype.events;
            if (module._.isFunction(events)) {
                events = events.call(this);
            }

            return module.$.extend ({}, events, {
                'click .btn-share': 'onShareBtnClicked',
                'click .overlay-filter': 'onFilterClicked',
            });
        },

        remove: function() {
            PlayableViewMixin.unbind(this);
            WaveformViewMixin.unbind(this);
            module.$(module).off('playMusicFirst', this.onPlayMusic);
        },

        onFilterClicked: function(e) {
            this.$overlayFilter.hide();
            this.$bubble.hide();
        },

        onResize: function(e) {
            var $iframe = this.$player.find('iframe'),
                height, width, aspectRatio, videoAspectRatio  = 16 / 9,
                vHeight, vWidth, marginH, marginV, styles;

            width = this.$contents.width();
            height = 140;

            aspectRatio = width / height;

            if (videoAspectRatio > aspectRatio) {
                vHeight = height; 
                vWidth = videoAspectRatio * vHeight;
            } else {
                vWidth = width; 
                vHeight = vWidth / videoAspectRatio; 
            }

            marginV = height - vHeight;
            marginH = width - vWidth;
        
            styles = {
                left: 140 + (marginH / 2) + 'px',
                right: (marginH / 2) + 'px',
                top: (marginV / 2) + 'px',
                bottom: 'auto',
                height: vHeight,
                width: vWidth
            };

            this.$player.css(styles);
            $iframe.css({
                width: vWidth,
                height: vHeight
            });
        },

        onPlayMusic: function(e, music, section) {
            //showDrop(module.$('#player'), music, music.drop);
        },

        onShareBtnClicked: function(e) {
            if (this.$overlayFilter.is(':visible')) {
                this.$overlayFilter.click();
                return;
            }

            this.$overlayFilter.show();
            this.$bubble.show();
            this.$bubble.find('input').select();
        }
    });

    return module;
}($dbt));
