/*jslint browser: true*/
/*global $*/
var $dbt = (function (module) {
    'use strict';

    var control;

    module = module || {};
    module.player = module.player || {};
    module.player.control = module.player.control || {};
    control = module.player.control;

    control.init = function () {
        var that = this;

        module.$(that.base.elems.titleLabelScroller).on(
            'mouseenter',
            function () {
                var $marquee = module.$(this),
                    $title = module.$(this).find(that.base.elems.titleLabel),
                    speed,
                    animPeriod;

                if ($title.width() <= $marquee.width()) {
                    $marquee.removeClass('marquee');
                    return;
                }

                $marquee.addClass('marquee');

                speed = 20;
                animPeriod = speed * $title.width();
                $title.css({left: 0});
                $title.animate(
                    {left: -$title.width()},
                    animPeriod,
                    'linear',
                    function () {
                        $marquee.trigger('marquee');
                    }
                );
                $marquee.bind(
                    'marquee',
                    function () {
                        var contentsWidth = $title.width(),
                            frameWidth = $marquee.width(),
                            animPeriod = speed * (contentsWidth + frameWidth);
                        $title.css({left: frameWidth});
                        $title.animate(
                            {left: -contentsWidth},
                            animPeriod,
                            'linear',
                            function () {
                                $marquee.trigger('marquee');
                            }
                        );
                    }
                );
            }
        ).on(
            'mouseleave',
            function () {
                var $title = module.$(this).find(that.base.elems.titleLabel);
                module.$(this).removeClass('marquee');
                $title.unbind('marquee');
                $title.clearQueue();
                $title.stop();
                $title.css({left: 0});
            }
        );

        that.repeat.control.init();
        that.shuffle.control.init();
        that.video.control.init();
        that.volume.control.init();
        that.base.updateButton();
        that.base.updatePlayButton(false);
    };

    control.base = {
        elems: {
            ctrlPrev: '#player .controls .ctrl-prev',
            ctrlPlay: '#player .controls .ctrl-play',
            ctrlNext: '#player .controls .ctrl-next',
            titleLabel: '.title',
            titleLabelScroller: '#player .title-scroll-wrapper',
            playerFilter: '.player-section .player-filter'
        },

        updateButton: function () {
            if (module.s.musicQ.walk(false)) {
                module.$(this.elems.ctrlPrev).removeClass('disabled');
            } else {
                module.$(this.elems.ctrlPrev).addClass('disabled');
            }

            if (module.s.musicQ.walk(true)) {
                module.$(this.elems.ctrlNext).removeClass('disabled');
            } else {
                module.$(this.elems.ctrlNext).addClass('disabled');
            }
        },

        updatePlayButton: function(enable) {
            if (enable) {
                module.$(this.elems.ctrlPlay).removeClass('disabled');
            } else  {
                module.$(this.elems.ctrlPlay).addClass('disabled');
            }
        },

        hideYTVideo: function() {
            module.$(this.elems.playerFilter).removeClass('playing');
        },

        showYTVideo: function() {
            module.$(this.elems.playerFilter).addClass('playing');
        }
    };

    control.toggle = function (music, section, isPlaying) {
        module.$(module).trigger('togglePlay', [music, section, isPlaying]);
    };

    control.repeat = {};

    control.repeat.state = {
        noRepeat: {klass: ''},
        repeatOne: {klass: 'repeat-one'},
        repeatPlaylist: {klass: 'repeat'}
    };

    control.repeat.sequence = [
// We do not used `shortcuts` to init this here. (it's array!)
        control.repeat.state.noRepeat,
        control.repeat.state.repeatPlaylist,
        control.repeat.state.repeatOne
    ];

    control.repeat.control = {
        init: function () {
            var that = this,
                toggle = module.$(that.elems.ctrlRepeat),
                repeatState = module.s.repeatState,
                repeatSequence = module.s.repeatSequence,
                playerControl = module.s.playerBase;

            that.state = repeatState.noRepeat;
            that._throttledOnClick = module._.throttle(function() {
                module.log('player', 'repeat-ctrl',
                    that.state.klass || 'no-repeat');
            }, 3000, {leading: false});

            toggle.click(function () {
                var repeatElem = that.elems.ctrlRepeat,
                    seqIdx = module.$.inArray(that.state, repeatSequence);

                that.state =
                    repeatSequence[(seqIdx + 1) % repeatSequence.length];

                module.$.each(repeatState, function (state, stateObj) {
                    module.$(repeatElem).removeClass(stateObj.klass);
                });

                that._throttledOnClick();

                module.$(repeatElem).addClass(that.state.klass);
                playerControl.updateButton();
            });
        },

        elems: {
            ctrlRepeat: '#player .controls .ctrl-repeat'
        },

        state: null
    };

    control.shuffle = {};

    control.shuffle.state = {
        on: 'shuffle',
        off: ''
    };

    control.shuffle.control = {
        init: function () {
            var that = this,
                elem = module.$(that.elems.shuffleToggle),
                shuffleState = control.shuffle.state;

            that.state = shuffleState.off;

            elem.click(function () {
                var playlist,
                    currMusic,
                    currMusicIdx,
                    q, idx, i,
                    playlistId;

                currMusic = module.playerManager.currentMusic;
                playlistId = module.playlistManager.playingPlaylistId;

                if (!that.isShuffle()) {
                    that.state = shuffleState.on;
                    elem.addClass(shuffleState.on);

                    module.log('player', 'shuffle', 'on');
                } else {
                    that.state = shuffleState.off;
                    elem.removeClass(shuffleState.on);

                    module.log('player', 'shuffle', 'off');
                }

                if (playlistId && currMusic &&
                        (playlist =
                            module.playlistManager.getPlaylist(playlistId))) {

                    currMusicIdx = playlist.findIdx(currMusic.id);
                    if (currMusicIdx !== -1 && that.isShuffle()) {
                        q = module.s.musicQ.q.slice(0);
                        that.shuffle(q);

                        idx = -1;

                        for (i = 0; i < q.length; i += 1) {
                            if (q[i].id === currMusic.id) {
                                idx = i;
                                break;
                            }
                        }
                        module.s.musicQ.init(q, idx);
                        return;
                    } else if (currMusicIdx !== -1) {
                        module.s.musicQ.init(
                            playlist.playlist, currMusicIdx);
                        return;
                    }
                }
                module.s.musicQ.init();
            });
        },

        elems: {
            shuffleToggle: '#player .controls .ctrl-shuffle'
        },

        state: null,

        shuffle: function (array) {
            var i,
                j,
                temp;

            for (i = array.length - 1; i > 0; i -= 1) {
                j = Math.floor(Math.random() * (i + 1));
                temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
            return array;
        },

        isShuffle: function () {
            var shuffleState = module.s.shuffleState;
            return this.state === shuffleState.on;
        }
    };

    control.video = {
        state: {
            hidden: 'hideen',
            exposed: 'exposed'
        },

        control: {
            state: null,
            elems: {
                videoFrame: '#external-player',
                videoContent: '#external-player iframe',
                videoToggle: '#player .controls .ctrl-video',
                videoToggleMenu: '#player .mini-extra-controls .video-menu',
                videoCloseBtn: '#external-player .close-button'
            },
            init: function() {
                var that = this;

                module._.bindAll(this, 'onWindowResize', 'hideVideo', 'showVideo',
                    'onVideoMenuClicked');
                this.state = control.video.state.hidden;
                this.$wrapper = module.$(this.elems.videoFrame);
                this.$el = module.$(this.elems.videoToggle);
                this.$menuEl = module.$(this.elems.videoToggleMenu);

                module.$(module).on('loadingMusic playMusic', function(e, music, section) {
                    if (music.type === 'youtube') {
                        that.$el.removeClass('disabled');
                        that.$menuEl.removeClass('disabled');
                        module.$('.player-filter').addClass('clickable');
                    } else {
                        that.$el.addClass('disabled');
                        that.$menuEl.addClass('disabled');
                        module.$('.player-filter').removeClass('clickable');
                        module.s.playerBase.hideYTVideo();
                        that.hideVideo();
                    }
                });

                module.$(module).on('playerStopped', function(e, section) {
                    that.$el.addClass('disabled');
                    that.$menuEl.addClass('disabled');
                    module.$('.player-filter').removeClass('clickable');
                    module.s.playerBase.hideYTVideo();
                    that.hideVideo();
                });

                this.$el.on('click', this.onVideoMenuClicked);
                this.$menuEl.on('click', this.onVideoMenuClicked);

                module.$(this.elems.videoCloseBtn).on('click', that.hideVideo);
            },
            onVideoMenuClicked: function(e) {
                if (this.$el.hasClass('disabled')) {
                    return;
                }
                if (this.$el.hasClass('hide-video')) {
                    this.hideVideo();
                    module.log('player', 'hide-video');
                } else {
                    this.showVideo();
                    module.log('player', 'show-video');
                }
            },
            hideVideo : function() {
                this.state = control.video.state.hidden;
                this.$wrapper.removeClass('exposed');
                this.$el.removeClass('hide-video');
                this.$el.text('show video');
                this.$menuEl.find('.text').text('Show video');
                module.$(window).off('resize', this.onWindowResize);
                if (!this.$wrapper.hasClass('promotion-liveset')) {
                    this.$wrapper.addClass('minimized');
                }
                module.$(module).trigger('hideVideo');
                // hack
                module.$('.z-index-hack-fixed').removeClass('hacked');
            },
            showVideo: function() {
                this.state = control.video.state.exposed;
                this.$wrapper.removeClass('minimized');
                this.$wrapper.addClass('exposed');
                this.$el.addClass('hide-video');
                this.$el.text('hide video');
                this.$menuEl.find('.text').text('Hide video');
                module.$(window).on('resize', this.onWindowResize);
                this.onWindowResize();
                module.$(module).trigger('showVideo');
                // hack
                module.$('.z-index-hack-fixed').addClass('hacked');
            },
            isVideoExposed: function() {
                return this.state === control.video.exposed;
            },
            onWindowResize: function() {
                var contentAspectRatio = 374 / 211,
                    winAspectRatio,
                    winWidth,
                    winHeight,
                    contentWidth,
                    contentHeight;

                winWidth = this.$wrapper.width();
                winHeight = this.$wrapper.height();
                winAspectRatio = winWidth / winHeight;
                if (winAspectRatio > contentAspectRatio) {
                    contentHeight = winHeight;
                    contentWidth = contentHeight * contentAspectRatio;
                } else {
                    contentWidth = winWidth;
                    contentHeight = contentWidth / contentAspectRatio;
                }
                module.$(this.elems.videoContent).css({
                    width: contentWidth,
                    height: contentHeight,
                    marginTop: (-1 * (contentHeight / 2)) + 'px',
                    marginLeft: (-1 * (contentWidth / 2)) + 'px',
                    top: (winHeight / 2) + 'px'
                });
            }
        }
    };

    control.volume = {
        control: {
            elems: {
                volumeCtrl: '#player .extra-controls .ctrl-volume',
                volumeBar: '#player .extra-controls .volume-bar',
                volumeBarInner: '#player .extra-controls .volume-bar-inner',
                volumeBarHandle: '#player .extra-controls .volume-bar-value',
                menuVolumeBar: '#player .mini-extra-controls .volume-bar',
                menuVolumeBarInner: '#player .mini-extra-controls .volume-bar-inner',
                menuVolumeBarHandle: '#player .mini-extra-controls .volume-bar-value'
            },
            volume: 100,
            isMuted: false,
            init: function() {
                var that = this;

                module._.bindAll(this, 'updateUi',
                    'onShowVolumeCtrl', 'onHideVolumeCtrl',
                    'onMouseDown', 'onMouseMove');

                this.$el = module.$(this.elems.volumeCtrl),
                this.$volumeBar = module.$(this.elems.volumeBar),
                this.$volumeBarHandle = module.$(this.elems.volumeBarHandle);
                this.$menuVolumeBar = module.$(this.elems.menuVolumeBar),
                this.$menuVolumeBarHandle = module.$(this.elems.menuVolumeBarHandle);

                this._startDrag = false;
                this._mouseenterTimer = null;

                this.$el.on('mouseenter', this.onShowVolumeCtrl);
                this.$volumeBar.on('mouseleave', this.onHideVolumeCtrl);
                this.$volumeBar.on('mousedown', this.onMouseDown);
                this.$volumeBar.on('mousemove', this.onMouseMove);
                this.$menuVolumeBar.on('mousedown', this.onMouseDown);
                this.$menuVolumeBar.on('mousemove', this.onMouseMove);


                this.$el.on('click', function() {
                    if (that.isMuted) {
                        module.log('player', 'mute', 'off');
                        module.$(this).removeClass('muted');
                        that.isMuted = false;
                        that.updateUi(that.volume);
                    } else {
                        module.log('player', 'mute', 'on');
                        module.$(this).addClass('muted');
                        that.isMuted = true;
                        that.updateUi(0);
                    }
                    module.$.each(module.playerManager.players.objs,
                            function(key, player) {
                        if (that.isMuted) {
                            player.mute();
                        } else {
                            player.unmute();
                        }
                    });
                });

                module.$(window).on('mouseup', function (event) {
                    if (that._startDrag &&
                            !that.$el.is(':hover') &&
                            !that.$volumeBar.is(':hover')) {
                        that._startDrag = false;
                        that.$volumeBar.hide();
                        if (that._mouseenterTimer) {
                            clearInterval(that._mouseenterTimer);
                        }
                        if (that.volume !== that._prevVolume) {
                            module.log('player', 'volume-changed');
                        }
                    } else {
                        that._startDrag = false;
                    }
                });
            },
            updateUi: function(value) {
                this.$volumeBarHandle.css('height', value + '%');
                this.$menuVolumeBarHandle.css('width', value + '%');
            },
            onMouseDown: function(e) {
                this._startDrag = true;
                this.updateByMouse(e);
            },
            onMouseMove: function(e) {
                if (this._startDrag) {
                    this.updateByMouse(e);
                }
            },
            onShowVolumeCtrl: function() {
                var that = this;
                if (this.isMuted) {
                    this.updateUi(0);
                } else {
                    this.updateUi(this.volume);
                }
                this.$volumeBar.show();
                this._prevVolume = this.volume;
                this._mouseenterTimer = setInterval(function() {
                    if (!that.$el.is(':hover') && !that.$volumeBar.is(':hover') &&
                         !that._startDrag) {
                        that.$volumeBar.hide();
                        clearInterval(that._mouseenterTimer);
                        if (that.volume !== that._prevVolume) {
                            module.log('player', 'volume-changed');
                        }
                    }
                }, 3000);
            },
            onHideVolumeCtrl: function() {
                var that = this;
                if (that._startDrag) {
                    return;
                }
                that.$volumeBar.hide();
                if (that._mouseenterTimer) {
                    clearInterval(that._mouseenterTimer);
                }
                if (that.volume !== that._prevVolume) {
                    module.log('player', 'volume-changed');
                }
            },
            updateVolume: function(pos) {
                this.updateUi(pos);
                this.volume = pos;
                if (this.volume < 20) {
                    this.$el.addClass('under20');
                    this.$el.removeClass('under60');
                } else if(this.volume < 60) {
                    this.$el.removeClass('under20');
                    this.$el.addClass('under60');
                } else {
                    this.$el.removeClass('under20');
                    this.$el.removeClass('under60');
                }
                if (this.volume === 0 && !this.isMuted) {
                    this.isMuted = true;
                    this.$el.addClass('muted');
                    module.$.each(module.playerManager.players.objs,
                            function(key, player) {
                        player.mute();
                    });
                } else if(this.volume > 0 && this.isMuted) {
                    this.isMuted = false;
                    this.$el.removeClass('muted');
                    module.$.each(module.playerManager.players.objs,
                            function(key, player) {
                        player.unmute();
                    });
                }
                module.$.each(module.playerManager.players.objs,
                        function(key, player) {
                    player.setVolume(pos);
                });
            },
            updateByMouse: function(e) {
                var $frame = module.$(e.currentTarget).closest('.volume-bar'),
                    $volumeBarInner = module.$('.volume-bar-inner', $frame),
                    horizontal,
                    barMax,
                    cursor,
                    pos;

                horizontal = $frame.hasClass('horizontal');

                if (horizontal) {
                    barMax = $volumeBarInner.width();
                    cursor = e.pageX - $volumeBarInner.offset().left;
                } else {
                    barMax = $volumeBarInner.height();
                    cursor = barMax - (e.pageY - $volumeBarInner.offset().top);
                }

                if (cursor < 0) {
                    cursor = 0;
                }
                if (cursor > barMax) {
                    cursor = barMax;
                }

                pos = (cursor / barMax) * 100;
                if (this._startDrag) {
                    this.updateVolume(pos);
                }
            }
        }
    }

    return module;
}($dbt));
