#!/bin/bash

BASEDIR=$(dirname $0)
JSDIR=$BASEDIR/js
LIBDIR=$BASEDIR/lib
DESTPATH=$BASEDIR/js/dropbeat.record_demo.min*.js
TEMPPATH=$BASEDIR/js/dropbeat.record_demo.combinded.js
DATETIME=`date +%Y%m%d%H%M%S`

rm $DESTPATH
DESTPATH=$BASEDIR/js/dropbeat.record_demo.min.$DATETIME.js

rm $TEMPPATH

touch $TEMPPATH
cat $LIBDIR/jquery-1.11.0.js >> $TEMPPATH
cat $LIBDIR/jquery_ui/jquery-ui.min.js >> $TEMPPATH
cat $LIBDIR/jquery.extension.js >> $TEMPPATH
cat $LIBDIR/jquery.compat.js >> $TEMPPATH
cat $LIBDIR/jquery.cookie.js >> $TEMPPATH
cat $LIBDIR/jquery.iecors.js >> $TEMPPATH
cat $LIBDIR/sprintf.min.js >> $TEMPPATH
cat $LIBDIR/bootstrap/bootstrap.carousel.js >> $TEMPPATH
cat $LIBDIR/bootstrap/bootstrap.modal.js >> $TEMPPATH
cat $LIBDIR/soundmanager2-nodebug-jsmin.js >> $TEMPPATH
cat $LIBDIR/jquery_tooltipster/jquery.tooltipster.js >> $TEMPPATH
cat $LIBDIR/notify-custom.js >> $TEMPPATH
cat $LIBDIR/underscore.min.js >> $TEMPPATH
cat $LIBDIR/backbone.min.js >> $TEMPPATH 
cat $LIBDIR/jquery.placeholder.js >> $TEMPPATH
cat $LIBDIR/moment.js >> $TEMPPATH
cat $LIBDIR/chance.min.js >> $TEMPPATH
cat $LIBDIR/spectrum/spectrum.js >> $TEMPPATH

cat $JSDIR/dropbeat.record_demo.js >> $TEMPPATH
cat $JSDIR/music.js >> $TEMPPATH
cat $JSDIR/notification.js >> $TEMPPATH
cat $JSDIR/player.js >> $TEMPPATH
cat $JSDIR/playercontrol.js >> $TEMPPATH
cat $JSDIR/playermanager.js >> $TEMPPATH
cat $JSDIR/playlist.js >> $TEMPPATH
cat $JSDIR/view.record_demo.js >> $TEMPPATH
cat $JSDIR/progress.js >> $TEMPPATH
cat $JSDIR/router.record_demo.js >> $TEMPPATH
cat $JSDIR/utils.js >> $TEMPPATH
cat $JSDIR/i18n.js >> $TEMPPATH

yui-compressor $TEMPPATH > $DESTPATH -v

export datekey=$DATETIME
sed -i -- "s/dropbeat\.record_demo\.min\.[^\.]*\.js/dropbeat\.record_demo\.min\.${datekey}\.js/g" ./record_demo.html
rm ./record_demo.html--
