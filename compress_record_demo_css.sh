#!/bin/bash

BASEDIR=$(dirname $0)
CSSDIR=$BASEDIR/css
LESSDIR=$BASEDIR/less
LIBDIR=$BASEDIR/lib
DEST_GTE_IE9_PATH=$BASEDIR/css/dropbeat.record_demo.min*
TEMP_LESSPATH=$BASEDIR/combinded.record_demo.less
TEMP_SHARED_CSSPATH=$BASEDIR/shared.record_demo.css
TEMP_CSSPATH=$BASEDIR/combinded.record_demo.css
DATETIME=`date +%Y%m%d%H%M%S`

rm $TEMP_SHARED_CSSPATH
touch $TEMP_SHARED_CSSPATH

cat $LIBDIR/jquery_ui/jquery-ui.min.css >> $TEMP_SHARED_CSSPATH
cat $LIBDIR/jquery_ui/jquery-ui.structure.min.css >> $TEMP_SHARED_CSSPATH
cat $LIBDIR/jquery_ui/jquery-ui.theme.min.css >> $TEMP_SHARED_CSSPATH
cat $LIBDIR/bootstrap/glyphicons.halflings.css >> $TEMP_SHARED_CSSPATH
cat $LIBDIR/jquery_tooltipster/tooltipster.css >> $TEMP_SHARED_CSSPATH
cat $LIBDIR/bootstrap/bootstrap.modal.css >> $TEMP_SHARED_CSSPATH

cat $CSSDIR/glyphicons.regular.css >> $TEMP_SHARED_CSSPATH
cat $CSSDIR/glyphicons.social.css >> $TEMP_SHARED_CSSPATH
cat $CSSDIR/base.css >> $TEMP_SHARED_CSSPATH

rm $TEMP_CSSPATH
touch $TEMP_CSSPATH

cat $TEMP_SHARED_CSSPATH >> $TEMP_CSSPATH

rm $TEMP_LESSPATH
touch $TEMP_LESSPATH
cat $LESSDIR/dropbeat.record_demo.less >> $TEMP_LESSPATH
lessc -x $TEMP_LESSPATH >> $TEMP_CSSPATH 

rm $DEST_GTE_IE9_PATH
DEST_GTE_IE9_PATH=$BASEDIR/css/dropbeat.record_demo.min.$DATETIME.css

touch $DEST_GTE_IE9_PATH
yui-compressor $TEMP_CSSPATH >> $DEST_GTE_IE9_PATH

rm $TEMP_LESSPATH
rm $TEMP_CSSPATH
rm $TEMP_SHARED_CSSPATH

export datekey=$DATETIME
sed -i -- "s/dropbeat\.record_demo\.min\.[^\.]*\.css/dropbeat\.record_demo\.min\.${datekey}\.css/g" ./record_demo.html

rm ./record_demo.html--
